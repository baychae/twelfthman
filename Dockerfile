# cd into twelfthman home
# build container - docker build --rm -t local/c7-systemd-httpd .
# run container -  docker run -tid --privileged=true --name=phalcon -p 8080:80 -v /Users/mrsinclair/Sites/twelfthman:/var/www/html local/c7-phalcon
# docker exec -ti phalcon bash
# map to different ports
FROM centos:7
ENV container docker
RUN (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == \
systemd-tmpfiles-setup.service ] || rm -f $i; done); \
rm -f /lib/systemd/system/multi-user.target.wants/*;\
rm -f /etc/systemd/system/*.wants/*;\
rm -f /lib/systemd/system/local-fs.target.wants/*; \
rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
rm -f /lib/systemd/system/basic.target.wants/*;\
rm -f /lib/systemd/system/anaconda.target.wants/*;
VOLUME [ "/var/www/html" ]
RUN rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
RUN rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
RUN yum update -y
RUN yum install wget -y; yum install nano -y
RUN yum -y install epel-release
#RUN wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
#RUN wget https://centos7.iuscommunity.org/ius-release.rpm
#RUN rpm -Uvh ius-release*.rpm
#RUN yum -y update
#RUN yum -y install openssl-devel php56u-fpm php56u-common php56u-opcache php56u-xml php56u-mcrypt php56u-gd php56u-devel php56u-mysql php56u-intl php56u-mbstring php56u-bcmath php56u-pear mod_php56u
RUN yum -y install httpd; sed -i 's#AllowOverride None#AllowOverride All#gI' /etc/httpd/conf/httpd.conf; yum clean all; systemctl enable httpd.service
RUN yum -y install libcgroup git pcre-devel gcc make re2c
RUN yum -y install php71w-fpm php71w-intl php71w-opcache php71w-devel mod_php71w php71w-cli php71w-common php71w-gd php71w-mbstring php71w-mcrypt php71w-mysqlnd php71w-xml php71w-pear openssl-devel
RUN pecl config-set php_ini /etc/php.ini; pear config-set php_ini /etc/php.ini
RUN git clone --depth=1 "git://github.com/phalcon/cphalcon.git" /usr/local/src/cphalcon
RUN cd /usr/local/src/cphalcon/build && ./install
RUN echo "extension=phalcon.so" >> /etc/php.d/phalcon.ini
RUN sed -i 's#display_errors = Off#display_errors = On#gI' /etc/php.ini; \
sed -i 's#error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT#error_reporting = E_ALL#gI' /etc/php.ini; \
sed -i 's#;error_log = php_errors.log#error_log = /var/log/php-fpm/php_errors.log#gI' /etc/php.ini
RUN sed -i 's@<LocationMatch "^/+$">@#<LocationMatch "^/+$">@gI' /etc/httpd/conf.d/welcome.conf; \
sed -i 's@Options -Indexes@#Options -Indexes@gI' /etc/httpd/conf.d/welcome.conf; \
sed -i 's@ErrorDocument 403 /.noindex.html@#ErrorDocument 403 /.noindex.html@gI' /etc/httpd/conf.d/welcome.conf; \
sed -i 's@</LocationMatch>@#</LocationMatch>@gI' /etc/httpd/conf.d/welcome.conf;
RUN pecl install mongodb
RUN pecl install xdebug
RUN pecl install redis
RUN cd /var/www/html; git clone https://github.com/jokkedk/webgrind.git;
RUN echo 'xdebug.remote_enable=true' >> /etc/php.ini
RUN echo 'xdebug.remote_port="9000"' >> /etc/php.ini
RUN echo 'xdebug.profiler_enable=1' >> /etc/php.ini
RUN echo 'xdebug.profiler_output_dir="/tmp/"' >> /etc/php.ini
RUN echo 'xdebug.profiler_enable_trigger=1' >> /etc/php.ini
RUN echo 'xdebug.trace_enable_trigger=1' >> /etc/php.ini
RUN echo ';xdebug.remote_mode="jit"' >> /etc/php.ini
RUN echo 'xdebug.remote_host="192.168.1.3"' >> /etc/php.ini
RUN echo 'xdebug.idekey="PHPSTORM"' >> /etc/php.ini
RUN echo 'xdebug.remote_log="/var/log/xdebug/xlog"' >> /etc/php.ini
RUN yum install graphviz -y
RUN httpd -k restart
CMD ["/usr/sbin/init"]