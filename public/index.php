<?php

ini_set('display_errors', 1);
ini_set('date.timezone', 'Europe/London');
ini_set('xdebug.idekey', 'PHPSTORM');
ini_set('memory_limit', '2048M');

define("SITE_ROOT", realpath('..') . '/');

try {
    header('Content-type: text/html; charset=utf-8;');

    /**
     * Include services
     */
    require_once SITE_ROOT . 'apps/config/services.php';

    /**
     * Create the application handler
     */
    $application = new \Phalcon\Mvc\Application();

    /**
     * Assign the DI provided by services.php
     */
    $application->setDI($di);

    /**
     * Include modules (their services loaded first)
     */
    require __DIR__ . '/../apps/config/modules.php';

    $debug = new \Phalcon\Debug();
    $debug->listen();

    echo $application->handle()->getContent();
} catch (Phalcon\Exception $e) {
    echo $e->getMessage();
} catch (PDOException $e) {
    echo $e->getMessage();
}