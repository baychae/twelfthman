/**
 * Created by mrsinclair on 20/06/2017.
 */
window.onload = function () {

    if (document.getElementById("upload-new-image-file")) {
        document.getElementById("upload-new-image-file").addEventListener('change', wysibbImageHelper.previewUploadImage);
    }

};