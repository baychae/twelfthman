/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var scroller = {

    moveScroll: function() {
        setTimeout(function() {
            var position = $.cookie("scrollPosition"); 
            $(document).scrollTop(position);
        }, 100);
    },
    saveScrollPos: function() {
        $.cookie("scrollPosition", $(document).scrollTop(), { expires : 1 }); 
    },
    observeEditorModeSw: function() {
        var obbcodeModeSwitch = document.querySelector('#bbcode-mswitch');
        // create an observer instance
        var oObserverCallback = new MutationObserver(function(mutations) {
            var sCurrentClass = mutations[0].target.className;
            bbMode = sCurrentClass === 'wysibb-toolbar-btn mswitch on' ? scroller.saveScrollPos() : scroller.moveScroll();
        });
        // configuration of the observer:
        var oObserverConfig = { attributes: true, childList: true, characterData: true }
        // pass in the target node, as well as the observer options
        oObserverCallback.observe(obbcodeModeSwitch, oObserverConfig);
    }
};

$(document).ready(function() {
       
    //Add listeners - Save/move scroll position on events
    scroller.observeEditorModeSw();
    $(window).on("beforeunload",function() { scroller.saveScrollPos(); });
    $(window).load(function() { scroller.moveScroll() ; });
});

