wysibbImageHelper = function () {

    function deleteImage() {

        result = confirm("Are you sure you want to delete and publish?");

        if (result) {
            return true;
        } else {
            return false;
        }
    }

    function isPublished() {
        var published = document.getElementById('publish').checked;

        if (published) {
            return true;
        } else {
            return false;
        }
    }

    function checkPublishNewsAndConfirm() {

        if (isPublished()) {
            result = confirm("Are you sure you want to delete and publish?");
            if (result) {
                removeArticleFeatureArt();
                this.type = "";
            }
        } else {
            removeArticleFeatureArt();
            this.type = "";
        }
    }

    function unPublishAlert() {
        var checked = document.getElementById('publish').checked;
        if (!checked) {
            alert("This will unpublish this article if updated!");
        }
    }


    function showDialog() {
        // if (document.getElementById('img-admin-newsarticle')) {
        //     getSetCurrentEmbedCode();
        // }

        document.getElementById("embed-image-dialog").style.display = "block";
    }

    function hideDialog() {
        document.getElementById('embed-image-dialog').style.display = "none";
    }

    function selectAllText() {
        this.focus();
        this.select();
    }

    function previewEmbedImage(event) {
        var embedDiv = document.getElementById("image-preview");
        var embedCodeTextArea = document.getElementById("embed-image-text").value;
        // var embedCodeDiv = document.getElementById('img-admin-newsarticle');

        if (clearPreviewPlaceholder()) {
            embedDiv.innerHTML = embedCodeTextArea;
            hideDialog();
        } else if (embedCodeTextArea === embedCodeDiv.innerHTML) {
            hideDialog();
        } else {
            // document.getElementById('img-admin-newsarticle').innerHTML = embedCodeTextArea;
            hideDialog();
        }
    }

    function insertArticleFeatureArt() {
        window.setTimeout(function () {

            if (document.getElementById("image-preview")) {

                imagePreview = document.getElementById("image-preview");
                var imageType = imagePreview.tagName;

                if (document.getElementById("article-feature-image")) {
                    articleFeatureImageToDelete = document.getElementById("article-feature-image");
                    articleFeatureImageToDelete.parentNode.removeChild(articleFeatureImageToDelete);
                }

                //Create the image placeholder used by both upload and embed images.
                var oArticleFeatureArtDiv = document.createElement("div");
                oArticleFeatureArtDiv.id = "article-feature-image";
                oArticleFeatureArtDiv.className += "wysibb-feature-art wysibb-embed-image";//wysibb-feature-art gets stripped in toBB jquery.wysibb.js
                oArticleFeatureArtDiv.contentEditable = false;

                if (imageType === "IMG") {
                    //Create the image
                    var oArticleFeatureImg = document.createElement("img");

                    oArticleFeatureImg.src = document.getElementById("image-preview").src;
                    oArticleFeatureArtDiv.insertBefore(oArticleFeatureImg, oArticleFeatureArtDiv.childNodes[0]);
                } else if (imageType === "DIV") {
                    oArticleFeatureArtDiv.innerHTML = imagePreview.innerHTML;
                    var embedCode = imagePreview.firstElementChild;
                }

                //Find the editor and insert the container with image
                if (document.getElementById("editor-html")) {
                    var preview_editor_element = document.getElementById("editor-html");
                    preview_editor_element.insertBefore(oArticleFeatureArtDiv, preview_editor_element.firstChild);
                }
            }


        }, 100);
    }

    function previewUploadImage() {
        clearPreviewPlaceholder();

        var file = this.files[0];
        var reader = new FileReader();
        var image_form_preview = document.createElement("img");
        var preview_form_element = document.getElementById("image-preview");

        if (preview_form_element.tagName == "DIV") {
            var preview_warning = document.createElement("div");
            preview_warning.className = "note";
            preview_warning.textContent = "* Preview. Save/update to keep changes.";

            var line_break = document.createElement("br");

            image_form_preview.id = "image-form-preview";

            reader.onloadend = function () {
                image_form_preview.src = reader.result;
                preview_form_element.appendChild(image_form_preview);
                preview_form_element.appendChild(line_break);
                preview_form_element.appendChild(preview_warning);
            }

            if (file) {
                reader.readAsDataURL(file);
            } else {
                image_form_preview.src = "";
            }
        } else if (preview_form_element.tagName == "IMG") {
            reader.onloadend = function () {
                document.getElementById("image-preview").src = reader.result;
            }
            
        }


    }

    function clearPreviewPlaceholder() {
        var previewPlaceHolder = document.getElementById("image-preview");

        if (previewPlaceHolder) {
            while (previewPlaceHolder.firstChild) {
                previewPlaceHolder.removeChild(previewPlaceHolder.firstChild);
            }
            return true;
        }

        return false;
    }

    function getSetCurrentEmbedCode() {
        // var embedCode = document.getElementById('img-admin-newsarticle').innerHTML;
        var embedTextArea = document.getElementById('embed-image-text');
        embedTextArea.value = embedCode;
    }

    function removeArticleFeatureArt() {
        if (document.getElementById("article-feature-image")) {
            var article_feature_image = document.getElementById("article-feature-image");
            article_feature_image.parentNode.removeChild(article_feature_image);
        }
    }


    function updateNewsArticle(event) {
        var isPublish = document.getElementById('publish').checked;

        if (isPublish) {
            var result = confirm("Click ok to publish this article.");

            if (result) {
                removeArticleFeatureArt();
                var html = $("#editor").htmlcode();
                $("#content_html").val(html);
            } else {
                insertArticleFeatureArt()
                event.preventDefault();
            }
        }
    }

    function updateMyAccount() {

        var updating = document.getElementById('update_my_account');

        if (updating) {
            removeArticleFeatureArt();
            var html = $("#editor").htmlcode();
            $("#content_html").val(html);
        }


    }

    return {
        hideDialog: hideDialog,
        selectAllText: selectAllText,
        previewEmbedImage: previewEmbedImage,
        showDialog: showDialog,
        previewUploadImage: previewUploadImage,
        insertArticleFeatureArt: insertArticleFeatureArt,
        deleteImage: deleteImage,
        checkPublishNewsAndConfirm: checkPublishNewsAndConfirm,
        updateNewsArticle: updateNewsArticle,
        updateMyAccount: updateMyAccount
    }
}();


