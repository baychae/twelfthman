var wbbOpt = {
    themeName: "twelfthman",
    resize_maxheight: 400,
    buttons: "quote,|,bold,strike,italic,underline,",
    resize_maxheight: 4600,
    allButtons: {
        quote: {
            title: CURLANG.quote,
            buttonHTML: '<span id="icon-quotes" class="btn-inner fonticon icon-quotes "></span>',
            transform: {
                '<blockquote contenteditable="false">{SELTEXT}</blockquote>': "[quote]{SELTEXT}[/quote]"
            }
        },
        bold: {
            title: CURLANG.bold,
            buttonHTML: '<span id="icon-embolden" class="icon-embolden fonticon btn-inner"></span>',
            excmd: 'bold',
            hotkey: 'ctrl+b',
            transform: {
                '<b>{SELTEXT}</b>': "[b]{SELTEXT}[/b]",
                '<strong>{SELTEXT}</strong>': "[b]{SELTEXT}[/b]"
            }
        },
        strike: {
            title: CURLANG.strike,
            buttonHTML: '<span id="icon-strike" class="icon-strike fonticon btn-inner"></span>',
            excmd: 'strikeThrough',
            transform: {
                '<strike>{SELTEXT}</strike>': "[strike]{SELTEXT}[/strike]"
            }
        },
        italic: {
            title: CURLANG.italic,
            buttonHTML: '<span id="icon-italicise" class="icon-italicise fonticon btn-inner"></span>',
            excmd: 'italic',
            hotkey: 'ctrl+i',
            transform: {
                '<i>{SELTEXT}</i>': "[i]{SELTEXT}[/i]"
            }
        },
        underline: {
            title: CURLANG.underline,
            buttonHTML: '<span id="icon-underline" class="icon-underline fonticon btn-inner"></span>',
            excmd: 'underline',
            hotkey: 'ctrl+u',
            transform: {
                '<u>{SELTEXT}</u>': "[u]{SELTEXT}[/u]"
            }
        }

    }
}

$(document).ready(function () {
    $("#editor").wysibb(wbbOpt);
});
     
        
 
