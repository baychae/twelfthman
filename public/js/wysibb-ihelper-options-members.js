/**
 * Created by mrsinclair on 16/05/2016.
 */

window.onload = function () {

    if (document.getElementById("upload-new-image-file")) {
        document.getElementById("upload-new-image-file").addEventListener('change', wysibbImageHelper.previewUploadImage);
    }

    if (document.getElementById("delete-image")) {
        document.getElementById("delete-image").addEventListener('click', function (event) {
            var result = confirm("You are about to delete your avatar pic. Click Ok to continue deletion.")

            if (result) {
                return true;
            } else {
                event.preventDefault();
            }
        });
    }

    if (document.getElementById("editor-html") && document.getElementById("content_html")) {
        document.getElementById("update_my_account").addEventListener('click', function () {
            var html = document.getElementById("editor-html").innerHTML;
            document.getElementById("content_html").setAttribute('value', html);
        });
    }

    if (document.getElementById("image-preview").tagName == "IMG") {

        document.getElementsByName("delete_image")[0].addEventListener('click', function (event) {

            var result = confirm("Are you sure you want to delete the image. It will be lost forever!");

            if (result) {
            } else {
                event.preventDefault();
            }

        });

        document.getElementById("upload-new-image-file").addEventListener('click', function () {

            document.getElementById("image-preview").addEventListener('DOMNodeInserted', function () {

                document.getElementById("image-preview").parentNode.removeChild(document.getElementById("image-preview"));

                var div = document.createElement("div");
                div.setAttribute("id", "image-preview");
                div.setAttribute("class", "no-select");

                document.getElementById("upload-new-image-file").insertBefore(div, document.getElementById("upload-new-image-file").childNodes[0]);

            });


        });

    }

    if (document.getElementsByName("delete_group")[0]) {

        document.getElementsByName("delete_group")[0].addEventListener('click', function () {
            var result = confirm("Are you sure you want to delete the Group! It will be permanently deleted by clicking OK!");

            if (result) {
            } else {
                event.preventDefault();
            }
        });

    }

};