/**
 * Created by mrsinclair on 16/05/2016.
 */

window.onload = function () {

    if (document.getElementById("upload-new-image-file")) {
        document.getElementById("upload-new-image-file").addEventListener('change', wysibbImageHelper.previewUploadImage);
    }

    if (document.getElementById("delete-image")) {
        document.getElementById("delete-image").addEventListener('click', function (event) {
            var result = confirm("You are about to delete your avatar pic. Click Ok to continue deletion.")

            if (result) {
                return true;
            } else {
                event.preventDefault();
            }
        });
    }

    if (document.getElementById("editor-html") && document.getElementById("content_html")) {
        document.getElementById("update_my_account").addEventListener('click', function () {
            var html = document.getElementById("editor-html").innerHTML;
            document.getElementById("content_html").setAttribute('value', html);
        });
    }

};