/**
 * Created by Richard Sinclair on 29/09/2015.
 */

localnav = function() {
    function goBack (){
        window.history.back();
    }

    return{
        goBack: goBack
    }
}();

window.onload = function() {
    if (document.getElementById("go-back")) {
        document.getElementById("go-back").addEventListener('click', localnav.goBack);
    }
};