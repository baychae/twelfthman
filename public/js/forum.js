/**
 * Created by mrsinclair on 07/12/2016.
 */

$(document).ready(function () {

    $(document).on("click", "#forum-search-toggle-title-results", function () { //Set page size cookie
        $("#forum-search-toggle-title-results").toggleClass("forum-search-results-max forum-search-results-min")
        $("#forum-topic-title-results").toggle();
    });

    $(document).on("click", "#forum-search-toggle-content-results", function () { //Set page size cookie
        $("#forum-search-toggle-content-results").toggleClass("forum-search-results-max forum-search-results-min")
        $("#forum-topic-content-results").toggle();
    });

    $('#forum a[href*="session"]').on('click', function () { //Set page size cookie
        $.cookie("reason", "member-profile", {expire: 1, path: '/'})
        $.cookie("profile-id", this.text, {expire: 1, path: '/'})
        console.log($(this).val());
    });

    $('#forum-topics a[href*="session"]').on('click', function () { //Set page size cookie
        $.cookie("reason", "member-profile", {expire: 1, path: '/'})
        $.cookie("profile-id", this.text, {expire: 1, path: '/'})
    });

    $('.user-details a[href*="session"]').on('click', function () { //Set page size cookie
        $.cookie("reason", "member-profile", {expire: 1, path: '/'})
        $.cookie("profile-id", this.text, {expire: 1, path: '/'})
    });

    $("#ui-id-1").on('click', function () {
        $("#ui-id-1").autocomplete("close");
    })

    $("#forum-topic-search").autocomplete({
        position: {my: "right-5 top+10", at: "right bottom"},
        // create: function () {
        //
        //     $(this).data("ui-autocomplete")._isDivider = function( item ) {
        //         return false;
        //     }
        //
        //     $(this).data("ui-autocomplete")._renderItem = function (ul, item) {
        //
        //         ul.append("<div id='forum-topic-search-results'></div>");
        //         ul = $(ul).find("#forum-topic-search-results");
        //
        //         return $("<li>")
        //             .attr("data-value", item.label)
        //             .append("<a href='forum/" + item.forum_id + "/" + item.topic_id + "/1'>" + item.label + "</a>")
        //             .appendTo(ul);
        //     }
        // },
        classes: {
            "ui-autocomplete": "highlight"
        },
        source: function (request, response) {// username and password supplied by inline javascript
            $.ajax({
                url: "https://" + Solr.username + ":" + Solr.password + "@" + Solr.host + ":" + Solr.port + "/solr/" + Solr.collection + "/search",
                data: {
                    ident: "on",
                    wt: "json",
                    sort: "id desc",
                    q: 'title_suggest_ngram:' + request.term
                },
                dataType: "jsonp",
                jsonp: 'json.wrf',
                success: function (data) {
                    response($.map(data.response.docs, function (item) {
                        return {
                            label: item.title,
                            topic_id: item.id,
                            forum_id: item.forum_id
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            log("Selected: " + ui.item.value + " aka " + ui.item.id);
        },
        open: function (event, ui) {
            ui.click(function () {
                $(event.target).autocomplete("close");
            });
        }
    }).data("ui-autocomplete")._renderItem = function (ul, item) {

        if ($("#forum-topic-title-results", ul).length === 0) {
            ul.append("<div id='forum-topic-title-results'><h3>Search Results: </h3></div>");
        }

        ul = $(ul).find("#forum-topic-title-results");

        return $("<li>")
            .attr("data-value", item.label)
            .append("<a href='forum/" + item.forum_id + "/" + item.topic_id + "/1'>" + item.label + "</a>")
            .appendTo(ul);
    };

});



