/**
 * Created by Richard Sinclair on 07/07/2016.
 */



window.onload = function () {
    if (document.getElementById(Options.logout.elementId)) {

        document.getElementById(Options.logout.elementId).addEventListener('click', function (event) {

            result = confirm("Are you sure you want to logout?");

            if (result) {
                window.location = Options.baseUri + Options.logout.endSessionUri;
                event.preventDefault();
            } else {
                event.preventDefault();
            }
        });
    }

};