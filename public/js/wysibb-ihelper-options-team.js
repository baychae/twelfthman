/**
 * Created by Richard Sinclair on 25/10/2016.
 */

window.onload = function () {

    if (document.getElementById("upload-new-image-file")) {
        document.getElementById("upload-new-image-file").addEventListener('change', wysibbImageHelper.previewUploadImage);
    }

    if (document.getElementById("delete-image")) {
        document.getElementById("delete-image").addEventListener('click', function (event) {
            var result = confirm("You are about to delete the team image. Click Ok to continue deletion.")

            if (result) {
                return true;
            } else {
                event.preventDefault();
            }
        });
    }

};