/**
 * Created by mrsinclair on 16/05/2016.
 */

window.onload = function () {

    wysibbImageHelper.insertArticleFeatureArt();

    if (document.getElementById("embed-image-cancel")) {
        document.getElementById("embed-image-cancel").addEventListener('click', wysibbImageHelper.hideDialog);
    }
    if (document.getElementById("embed-image-text")) {
        document.getElementById("embed-image-text").addEventListener('click', wysibbImageHelper.selectAllText);
    }

    if (document.getElementById("embed-image-ok")) {
        document.getElementById("embed-image-ok").addEventListener('click', wysibbImageHelper.previewEmbedImage);
    }

    if (document.getElementById("embed-new-image")) {
        document.getElementById("embed-new-image").addEventListener('click', wysibbImageHelper.showDialog);
    }

    if (document.getElementById("upload-new-image-file")) {
        document.getElementById("upload-new-image-file").addEventListener('change', wysibbImageHelper.previewUploadImage);
    }

    if (document.getElementById("bbcode-mswitch")) {
        document.getElementById("bbcode-mswitch").addEventListener('click', wysibbImageHelper.insertArticleFeatureArt);
    }

    if (document.getElementById("image-preview")) {
        document.getElementById("image-preview").addEventListener('DOMNodeInserted', wysibbImageHelper.insertArticleFeatureArt);
    }

    if (document.getElementById("embed-image-delete")) {
        document.getElementById("embed-image-delete").addEventListener('click', wysibbImageHelper.checkPublishNewsAndConfirm);
    }

    if (document.getElementById("delete-news-image")) {
        document.getElementById("delete-news-image").addEventListener('click', wysibbImageHelper.checkPublishNewsAndConfirm);
    }

    if (document.getElementById("delete-image")) {
        document.getElementById("delete-image").addEventListener('click', wysibbImageHelper.deleteImage);
    }

    if (document.getElementById("update-news-article")) {
        document.getElementById("update-news-article").addEventListener('click', wysibbImageHelper.updateNewsArticle);
    }

    if (document.getElementById("update_my_account")) {
        document.getElementById("update_my_account").addEventListener('click', wysibbImageHelper.updateMyAccount);
    }

};