var wbbOpt = {
  themeName: "twelfthman",
    buttons: "quote,|,bold,strike,italic,underline,|,fontcolor,fontsize,|,justifyleft,justifycenter,justifyright,|,link,video,img,embed,|,smilebox",
    resize_maxheight: 4600,
  allButtons: {
      smilebox: {
          type: 'smilebox',
          title: 'Emoticon',
          buttonHTML: '<span class="fonticon ve-tlb-smilebox1">\u263A</span>'
      },
      quote: {
          title: CURLANG.quote,
          buttonHTML: '<span id="icon-quotes" class="btn-inner fonticon icon-quotes "></span>',
          transform : { 
                  '<blockquote contenteditable="false">{SELTEXT}</blockquote>':"[quote]{SELTEXT}[/quote]"
          }
      },
      bold: {
          title: CURLANG.bold,
          buttonHTML: '<span id="icon-embolden" class="icon-embolden fonticon btn-inner"></span>',
          excmd: 'bold',
          hotkey: 'ctrl+b',
          transform : {
              '<b>{SELTEXT}</b>':"[b]{SELTEXT}[/b]",
	      '<strong>{SELTEXT}</strong>':"[b]{SELTEXT}[/b]"
          }
      },
      strike: {
          title: CURLANG.strike,
          buttonHTML: '<span id="icon-strike" class="icon-strike fonticon btn-inner"></span>',
          excmd: 'strikeThrough',
          transform : {
              '<strike>{SELTEXT}</strike>':"[strike]{SELTEXT}[/strike]"
          }
      },
      italic: {
          title: CURLANG.italic,
          buttonHTML: '<span id="icon-italicise" class="icon-italicise fonticon btn-inner"></span>',
          excmd: 'italic',
	  hotkey: 'ctrl+i',
          transform : {
              '<i>{SELTEXT}</i>':"[i]{SELTEXT}[/i]"
          }
      },
      underline: {
          title: CURLANG.underline,
          buttonHTML: '<span id="icon-underline" class="icon-underline fonticon btn-inner"></span>',
          excmd: 'underline',
	  hotkey: 'ctrl+u',
          transform : {
              '<u>{SELTEXT}</u>':"[u]{SELTEXT}[/u]"
          }
      },
      fontcolor: {
              type: "colorpicker",
              title: CURLANG.fontcolor,
              excmd: "foreColor",
              valueBBname: "color",
              subInsert: true,
              colors: "#000000,#444444,#666666,#999999,#b6b6b6,#cccccc,#d8d8d8,#efefef,#f4f4f4,#ffffff,-, \
                               #ff0000,#980000,#ff7700,#ffff00,#00ff00,#00ffff,#1e84cc,#0000ff,#9900ff,#ff00ff,-, \
                               #f4cccc,#dbb0a7,#fce5cd,#fff2cc,#d9ead3,#d0e0e3,#c9daf8,#cfe2f3,#d9d2e9,#ead1dc, \
                               #ea9999,#dd7e6b,#f9cb9c,#ffe599,#b6d7a8,#a2c4c9,#a4c2f4,#9fc5e8,#b4a7d6,#d5a6bd, \
                               #e06666,#cc4125,#f6b26b,#ffd966,#93c47d,#76a5af,#6d9eeb,#6fa8dc,#8e7cc3,#c27ba0, \
                               #cc0000,#a61c00,#e69138,#f1c232,#6aa84f,#45818e,#3c78d8,#3d85c6,#674ea7,#a64d79, \
                               #900000,#85200C,#B45F06,#BF9000,#38761D,#134F5C,#1155Cc,#0B5394,#351C75,#741B47, \
                               #660000,#5B0F00,#783F04,#7F6000,#274E13,#0C343D,#1C4587,#073763,#20124D,#4C1130",
              transform: {
                      '<font color="{COLOR}">{SELTEXT}</font>':'[color={COLOR}]{SELTEXT}[/color]'
              }
      },
      fontsize: {
                                      type: 'select',
                                      title: CURLANG.fontsize,
                                      options: "fs_verysmall,fs_small,fs_normal,fs_big,fs_verybig"
      },
      justifyleft: {
              title: CURLANG.justifyleft,
              buttonHTML: '<span class="icon-justify_left fonticon"></span>',
              groupkey: 'align',
              transform: {
                      '<p style="text-align:left">{SELTEXT}</p>': '[left]{SELTEXT}[/left]'
              }
      },
      justifyright: {
              title: CURLANG.justifyright,
              buttonHTML: '<span class="icon-justify_right fonticon"></span>',
              groupkey: 'align',
              transform: {
                      '<p style="text-align:right">{SELTEXT}</p>': '[right]{SELTEXT}[/right]'
              }
      },
      justifycenter: {
              title: CURLANG.justifycenter,
              buttonHTML: '<span class="icon-justify_centre fonticon"></span>',
              groupkey: 'align',
              transform: {
                  '<p style="text-align:center">{SELTEXT}</p>': '[center]{SELTEXT}[/center]',
                  '<p style="text-align:center">{SELTEXT}</p>': '[centre]{SELTEXT}[/centre]'
              }
      },
      link : {
              title: CURLANG.link,
              buttonHTML: '<span class="icon-link fonticon"></span>',
              modal: {
                      title: CURLANG.modal_link_title,
                      width: "500px",
                      tabs: [
                              {
                                      input: [
                                              {param: "SELTEXT",title:CURLANG.modal_link_text, type: "div"},
                                              {param: "URL",title:CURLANG.modal_link_url,validation: '^http(s)?://'}
                                      ]
                              }
                      ]
              },
              transform : {
                      '<a href="{URL}">{SELTEXT}</a>':"[url={URL}]{SELTEXT}[/url]",
                      '<a href="{URL}">{URL}</a>':"[url]{URL}[/url]"
              }
      },
      video: {
              title: CURLANG.video,
              buttonHTML: '<span class="icon-video fonticon"></span>',
              modal: {
                      title: CURLANG.video,
                      width: "600px",
                      tabs: [
                              {
                                      title: CURLANG.video,
                                      input: [
                                              {param: "SRC",title:CURLANG.modal_video_text}
                                      ]
                              }
                      ],
                      onSubmit: function(cmd,opt,queryState) {
                              var url = this.$modal.find('input[name="SRC"]').val();
                              if (url) {
                                      url = url.replace(/^\s+/,"").replace(/\s+$/,"");
                              }
                              var a;
                              if (url.indexOf("youtu.be")!=-1) {
                                      a = url.match(/^http[s]*:\/\/youtu\.be\/([a-z0-9_-]+)/i);
                              }else{
                                      a = url.match(/^http[s]*:\/\/www\.youtube\.com\/watch\?.*?v=([a-z0-9_-]+)/i);
                              }
                              if (a && a.length==2) {
                                      var code = a[1];
                                      this.insertAtCursor(this.getCodeByCommand(cmd,{src:code}));
                              }
                              this.closeModal();
                              this.updateUI();
                              return false;
                      }
              },
              transform: {
                  '<iframe src="//www.youtube.com/embed/{SRC}" width="480" height="390" frameborder="0"></iframe>': '[youtube]{SRC}[/youtube]'
              }
      },
      img : {
              title: CURLANG.img,
              buttonHTML: '<span class="icon-image fonticon"></span>',
              addWrap: true,
              modal: {
                      title: CURLANG.modal_img_title,
                      width: "600px",
                      tabs: [
                              {
                                      title: CURLANG.modal_img_tab1,
                                      input: [
                                              {param: "SRC",title:CURLANG.modal_imgsrc_text,validation: '^http(s)?://.*?\.(jpg|png|gif|jpeg)$'}
                                      ]
                              }
                      ],
                      onLoad: this.imgLoadModal
              },
              transform : {
                      '<img src="{SRC}" />':"[img]{SRC}[/img]",
                      '<img src="{SRC}" width="{WIDTH}" height="{HEIGHT}"/>':"[img width={WIDTH},height={HEIGHT}]{SRC}[/img]"
              }
      },
      embed : {
              title: "Getty Embed",
              buttonHTML: '<span class="icon-getty-embed fonticon"></span>',
              modal: {
                title: "Insert embed code",
                width: "600px",
                tabs: [
                  { //First tab
                    input: [ //List of form fields
                      {param: "SELTEXT",title:"Enter embed code",validation: '<div class="getty embed image"'}
                    ]
                  }
                ]
              },
              transform : {
                  '<div class="wysibb-embed-image" contenteditable="false" >{SELTEXT}</div>': "[embed]{SELTEXT}[/embed]",
                  '<div class="wysibb-embed-image" contenteditable="false" >{SELTEXT}</div>': "[embed]{SELTEXT}[/embed]"
              }
      }

  },
    smileList: [
        {
            title: CURLANG.badtongue,
            img: '<img title="badtongue" alt="badtongue" src="img/emoticons/Smilies/badtongue.png" class="sm">',
            bbcode: "[emoticon option='png']badtongue[/emoticon]"
        },
        {
            title: CURLANG.bigsmile,
            img: '<img title="bigsmile" alt="bigsmile" src="img/emoticons/Smilies/bigsmile.png" class="sm">',
            bbcode: "[emoticon option='png']bigsmile[/emoticon]"
        },
        {
            title: CURLANG.blink,
            img: '<img title="blink" alt="blink" src="img/emoticons/Smilies/blink.png" class="sm">',
            bbcode: "[emoticon option='png']blink[/emoticon]"
        },
        {
            title: CURLANG.blush,
            img: '<img title="blush" alt="blush" src="img/emoticons/Smilies/blush.png" class="sm">',
            bbcode: "[emoticon option='png']blush[/emoticon]"
        },
        {
            title: CURLANG.bounce,
            img: '<img title="bounce" alt="bounce" src="img/emoticons/Smilies/bounce.gif" class="sm">',
            bbcode: "[emoticon option='gif']bounce[/emoticon]"
        },
        {
            title: CURLANG.content,
            img: '<img title="content" alt="content" src="img/emoticons/Smilies/content.png" class="sm">',
            bbcode: "[emoticon option='png']content[/emoticon]"
        },
        {
            title: CURLANG.cool,
            img: '<img title="cool" alt="cool" src="img/emoticons/Smilies/cool.png" class="sm">',
            bbcode: "[emoticon option='png']cool[/emoticon]"
        },
        {
            title: CURLANG.cry,
            img: '<img title="cry" alt="cry" src="img/emoticons/Smilies/cry.png" class="sm">',
            bbcode: "[emoticon option='png']cry[/emoticon]"
        },
        {
            title: CURLANG.dead,
            img: '<img title="dead" alt="dead" src="img/emoticons/Smilies/dead.png" class="sm">',
            bbcode: "[emoticon option='png']dead[/emoticon]"
        },
        {
            title: CURLANG.elephant,
            img: '<img title="elephant" alt="elephant" src="img/emoticons/Smilies/elephant.png" class="sm">',
            bbcode: "[emoticon option='png']elephant[/emoticon]"
        },
        {
            title: CURLANG.erm,
            img: '<img title="erm" alt="erm" src="img/emoticons/Smilies/erm.png" class="sm">',
            bbcode: "[emoticon option='png']erm[/emoticon]"
        },
        {
            title: CURLANG.evil,
            img: '<img title="evil" alt="evil" src="img/emoticons/Smilies/evil.png" class="sm">',
            bbcode: "[emoticon option='png']evil[/emoticon]"
        },
        {
            title: CURLANG.finger,
            img: '<img title="finger" alt="finger" src="img/emoticons/Smilies/finger.png" class="sm">',
            bbcode: "[emoticon option='png']finger[/emoticon]"
        },
        {
            title: CURLANG.flag,
            img: '<img title="flag" alt="flag" src="img/emoticons/Smilies/flag.gif" class="sm">',
            bbcode: "[emoticon option='gif']flag[/emoticon]"
        },
        {
            title: CURLANG.glare,
            img: '<img title="glare" alt="glare" src="img/emoticons/Smilies/glare.png" class="sm">',
            bbcode: "[emoticon option='png']glare[/emoticon]"
        },
        {
            title: CURLANG.grr,
            img: '<img title="grr" alt="grr" src="img/emoticons/Smilies/grr.png" class="sm">',
            bbcode: "[emoticon option='png']grr[/emoticon]"
        },
        {
            title: CURLANG.happy,
            img: '<img title="happy" alt="happy" src="img/emoticons/Smilies/happy.png" class="sm">',
            bbcode: "[emoticon option='png']happy[/emoticon]"
        },
        {
            title: CURLANG.heh,
            img: '<img title="heh" alt="heh" src="img/emoticons/Smilies/heh.png" class="sm">',
            bbcode: "[emoticon option='png']heh[/emoticon]"
        },
        {
            title: CURLANG.hm,
            img: '<img title="hm" alt="hm" src="img/emoticons/Smilies/hm.png" class="sm">',
            bbcode: "[emoticon option='png']hm[/emoticon]"
        },
        {
            title: CURLANG.huh,
            img: '<img title="huh" alt="huh" src="img/emoticons/Smilies/huh.png" class="sm">',
            bbcode: "[emoticon option='png']huh[/emoticon]"
        },
        {
            title: CURLANG.laugh,
            img: '<img title="laugh" alt="laugh" src="img/emoticons/Smilies/laugh.png" class="sm">',
            bbcode: "[emoticon option='png']laugh[/emoticon]"
        },
        {
            title: CURLANG.mal,
            img: '<img title="mal" alt="mal" src="img/emoticons/Smilies/mal.png" class="sm">',
            bbcode: "[emoticon option='png']mal[/emoticon]"
        },
        {
            title: CURLANG.megson,
            img: '<img title="megson" alt="megson" src="img/emoticons/Smilies/megson.png" class="sm">',
            bbcode: "[emoticon option='png']megson[/emoticon]"
        },
        {
            title: CURLANG.ninja,
            img: '<img title="ninja" alt="ninja" src="img/emoticons/Smilies/ninja.png" class="sm">',
            bbcode: "[emoticon option='png']ninja[/emoticon]"
        },
        {
            title: CURLANG.owl,
            img: '<img title="owl" alt="owl" src="img/emoticons/Smilies/owl.png" class="sm">',
            bbcode: "[emoticon option='png']owl[/emoticon]"
        },
        {
            title: CURLANG.pig,
            img: '<img title="pig" alt="pig" src="img/emoticons/Smilies/pig.png" class="sm">',
            bbcode: "[emoticon option='png']pig[/emoticon]"
        },
        {
            title: CURLANG.pinch,
            img: '<img title="pinch" alt="pinch" src="img/emoticons/Smilies/pinch.png" class="sm">',
            bbcode: "[emoticon option='png']pinch[/emoticon]"
        },
        {
            title: CURLANG.plain,
            img: '<img title="plain" alt="plain" src="img/emoticons/Smilies/plain.png" class="sm">',
            bbcode: "[emoticon option='png']plain[/emoticon]"
        },
        {
            title: CURLANG.rolleyes,
            img: '<img title="rolleyes" alt="rolleyes" src="img/emoticons/Smilies/rolleyes.png" class="sm">',
            bbcode: "[emoticon option='png']rolleyes[/emoticon]"
        },
        {
            title: CURLANG.sad,
            img: '<img title="sad" alt="sad" src="img/emoticons/Smilies/sad.png" class="sm">',
            bbcode: "[emoticon option='png']sad[/emoticon]"
        },
        {
            title: CURLANG.salute,
            img: '<img title="salute" alt="salute" src="img/emoticons/Smilies/salute.png" class="sm">',
            bbcode: "[emoticon option='png']salute[/emoticon]"
        },
        {
            title: CURLANG.shock,
            img: '<img title="shock" alt="shock" src="img/emoticons/Smilies/shock.png" class="sm">',
            bbcode: "[emoticon option='png']shock[/emoticon]"
        },
        {
            title: CURLANG.sick,
            img: '<img title="sick" alt="sick" src="img/emoticons/Smilies/sick.png" class="sm">',
            bbcode: "[emoticon option='png']sick[/emoticon]"
        },
        {
            title: CURLANG.sigh,
            img: '<img title="sigh" alt="sigh" src="img/emoticons/Smilies/sigh.png" class="sm">',
            bbcode: "[emoticon option='png']sigh[/emoticon]"
        },
        {
            title: CURLANG.sleep,
            img: '<img title="sleep" alt="sleep" src="img/emoticons/Smilies/sleep.png" class="sm">',
            bbcode: "[emoticon option='png']sleep[/emoticon]"
        },
        {
            title: CURLANG.smile,
            img: '<img title="smile" alt="smile" src="img/emoticons/Smilies/smile.png" class="sm">',
            bbcode: "[emoticon option='png']smile[/emoticon]"
        },
        {
            title: CURLANG.smug,
            img: '<img title="smug" alt="smug" src="img/emoticons/Smilies/smug.png" class="sm">',
            bbcode: "[emoticon option='png']smug[/emoticon]"
        },
        {
            title: CURLANG.thumbsup,
            img: '<img title="thumbsup" alt="thumbsup" src="img/emoticons/Smilies/thumbsup.png" class="sm">',
            bbcode: "[emoticon option='png']thumbsup[/emoticon]"
        },
        {
            title: CURLANG.tongue,
            img: '<img title="tongue" alt="tongue" src="img/emoticons/Smilies/tongue.png" class="sm">',
            bbcode: "[emoticon option='png']tongue[/emoticon]"
        },
        {
            title: CURLANG.um,
            img: '<img title="um" alt="um" src="img/emoticons/Smilies/um.png" class="sm">',
            bbcode: "[emoticon option='png']um[/emoticon]"
        },
        {
            title: CURLANG.unhappy,
            img: '<img title="unhappy" alt="unhappy" src="img/emoticons/Smilies/unhappy.png" class="sm">',
            bbcode: "[emoticon option='png']unhappy[/emoticon]"
        },
        {
            title: CURLANG.wanker,
            img: '<img title="wanker" alt="wanker" src="img/emoticons/Smilies/wanker.gif" class="sm">',
            bbcode: "[emoticon option='gif']wanker[/emoticon]"
        },
        {
            title: CURLANG.wink,
            img: '<img title="wink" alt="wink" src="img/emoticons/Smilies/wink.png" class="sm">',
            bbcode: "[emoticon option='png']wink[/emoticon]"
        },
        {
            title: CURLANG.wow,
            img: '<img title="wow" alt="wow" src="img/emoticons/Smilies/wow.png" class="sm">',
            bbcode: "[emoticon option='png']wow[/emoticon]"
        },
        {
            title: CURLANG.badtongue,
            img: '<img title="badtongue" alt="badtongue" src="img/emoticons/Smilies/badtongue.png" class="sm">',
            bbcode: "[emoticon option='png']badtongue[/emoticon]"
        },
        {
            title: CURLANG.bigsmile,
            img: '<img title="bigsmile" alt="bigsmile" src="img/emoticons/Smilies/bigsmile.png" class="sm">',
            bbcode: "[emoticon option='png']bigsmile[/emoticon]"
        },
        {
            title: CURLANG.blink,
            img: '<img title="blink" alt="blink" src="img/emoticons/Smilies/blink.png" class="sm">',
            bbcode: "[emoticon option='png']blink[/emoticon]"
        },
        {
            title: CURLANG.blush,
            img: '<img title="blush" alt="blush" src="img/emoticons/Smilies/blush.png" class="sm">',
            bbcode: "[emoticon option='png']blush[/emoticon]"
        },
        {
            title: CURLANG.bounce,
            img: '<img title="bounce" alt="bounce" src="img/emoticons/Smilies/bounce.gif" class="sm">',
            bbcode: "[emoticon option='gif']bounce[/emoticon]"
        },
        {
            title: CURLANG.content,
            img: '<img title="content" alt="content" src="img/emoticons/Smilies/content.png" class="sm">',
            bbcode: "[emoticon option='png']content[/emoticon]"
        },
        {
            title: CURLANG.cool,
            img: '<img title="cool" alt="cool" src="img/emoticons/Smilies/cool.png" class="sm">',
            bbcode: "[emoticon option='png']cool[/emoticon]"
        },
        {
            title: CURLANG.cry,
            img: '<img title="cry" alt="cry" src="img/emoticons/Smilies/cry.png" class="sm">',
            bbcode: "[emoticon option='png']cry[/emoticon]"
        },
        {
            title: CURLANG.dead,
            img: '<img title="dead" alt="dead" src="img/emoticons/Smilies/dead.png" class="sm">',
            bbcode: "[emoticon option='png']dead[/emoticon]"
        },
        {
            title: CURLANG.elephant,
            img: '<img title="elephant" alt="elephant" src="img/emoticons/Smilies/elephant.png" class="sm">',
            bbcode: "[emoticon option='png']elephant[/emoticon]"
        },
        {
            title: CURLANG.erm,
            img: '<img title="erm" alt="erm" src="img/emoticons/Smilies/erm.png" class="sm">',
            bbcode: "[emoticon option='png']erm[/emoticon]"
        },
        {
            title: CURLANG.evil,
            img: '<img title="evil" alt="evil" src="img/emoticons/Smilies/evil.png" class="sm">',
            bbcode: "[emoticon option='png']evil[/emoticon]"
        },
        {
            title: CURLANG.finger,
            img: '<img title="finger" alt="finger" src="img/emoticons/Smilies/finger.png" class="sm">',
            bbcode: "[emoticon option='png']finger[/emoticon]"
        },
        {
            title: CURLANG.flag,
            img: '<img title="flag" alt="flag" src="img/emoticons/Smilies/flag.gif" class="sm">',
            bbcode: "[emoticon option='gif']flag[/emoticon]"
        },
        {
            title: CURLANG.glare,
            img: '<img title="glare" alt="glare" src="img/emoticons/Smilies/glare.png" class="sm">',
            bbcode: "[emoticon option='png']glare[/emoticon]"
        },
        {
            title: CURLANG.grr,
            img: '<img title="grr" alt="grr" src="img/emoticons/Smilies/grr.png" class="sm">',
            bbcode: "[emoticon option='png']grr[/emoticon]"
        },
        {
            title: CURLANG.happy,
            img: '<img title="happy" alt="happy" src="img/emoticons/Smilies/happy.png" class="sm">',
            bbcode: "[emoticon option='png']happy[/emoticon]"
        },
        {
            title: CURLANG.heh,
            img: '<img title="heh" alt="heh" src="img/emoticons/Smilies/heh.png" class="sm">',
            bbcode: "[emoticon option='png']heh[/emoticon]"
        },
        {
            title: CURLANG.hm,
            img: '<img title="hm" alt="hm" src="img/emoticons/Smilies/hm.png" class="sm">',
            bbcode: "[emoticon option='png']hm[/emoticon]"
        },
        {
            title: CURLANG.huh,
            img: '<img title="huh" alt="huh" src="img/emoticons/Smilies/huh.png" class="sm">',
            bbcode: "[emoticon option='png']huh[/emoticon]"
        },
        {
            title: CURLANG.laugh,
            img: '<img title="laugh" alt="laugh" src="img/emoticons/Smilies/laugh.png" class="sm">',
            bbcode: "[emoticon option='png']laugh[/emoticon]"
        },
        {
            title: CURLANG.mal,
            img: '<img title="mal" alt="mal" src="img/emoticons/Smilies/mal.png" class="sm">',
            bbcode: "[emoticon option='png']mal[/emoticon]"
        },
        {
            title: CURLANG.megson,
            img: '<img title="megson" alt="megson" src="img/emoticons/Smilies/megson.png" class="sm">',
            bbcode: "[emoticon option='png']megson[/emoticon]"
        },
        {
            title: CURLANG.ninja,
            img: '<img title="ninja" alt="ninja" src="img/emoticons/Smilies/ninja.png" class="sm">',
            bbcode: "[emoticon option='png']ninja[/emoticon]"
        },
        {
            title: CURLANG.owl,
            img: '<img title="owl" alt="owl" src="img/emoticons/Smilies/owl.png" class="sm">',
            bbcode: "[emoticon option='png']owl[/emoticon]"
        },
        {
            title: CURLANG.pig,
            img: '<img title="pig" alt="pig" src="img/emoticons/Smilies/pig.png" class="sm">',
            bbcode: "[emoticon option='png']pig[/emoticon]"
        },
        {
            title: CURLANG.pinch,
            img: '<img title="pinch" alt="pinch" src="img/emoticons/Smilies/pinch.png" class="sm">',
            bbcode: "[emoticon option='png']pinch[/emoticon]"
        },
        {
            title: CURLANG.plain,
            img: '<img title="plain" alt="plain" src="img/emoticons/Smilies/plain.png" class="sm">',
            bbcode: "[emoticon option='png']plain[/emoticon]"
        },
        {
            title: CURLANG.rolleyes,
            img: '<img title="rolleyes" alt="rolleyes" src="img/emoticons/Smilies/rolleyes.png" class="sm">',
            bbcode: "[emoticon option='png']rolleyes[/emoticon]"
        },
        {
            title: CURLANG.sad,
            img: '<img title="sad" alt="sad" src="img/emoticons/Smilies/sad.png" class="sm">',
            bbcode: "[emoticon option='png']sad[/emoticon]"
        },
        {
            title: CURLANG.salute,
            img: '<img title="salute" alt="salute" src="img/emoticons/Smilies/salute.png" class="sm">',
            bbcode: "[emoticon option='png']salute[/emoticon]"
        },
        {
            title: CURLANG.shock,
            img: '<img title="shock" alt="shock" src="img/emoticons/Smilies/shock.png" class="sm">',
            bbcode: "[emoticon option='png']shock[/emoticon]"
        },
        {
            title: CURLANG.sick,
            img: '<img title="sick" alt="sick" src="img/emoticons/Smilies/sick.png" class="sm">',
            bbcode: "[emoticon option='png']sick[/emoticon]"
        },
        {
            title: CURLANG.sigh,
            img: '<img title="sigh" alt="sigh" src="img/emoticons/Smilies/sigh.png" class="sm">',
            bbcode: "[emoticon option='png']sigh[/emoticon]"
        },
        {
            title: CURLANG.sleep,
            img: '<img title="sleep" alt="sleep" src="img/emoticons/Smilies/sleep.png" class="sm">',
            bbcode: "[emoticon option='png']sleep[/emoticon]"
        },
        {
            title: CURLANG.smile,
            img: '<img title="smile" alt="smile" src="img/emoticons/Smilies/smile.png" class="sm">',
            bbcode: "[emoticon option='png']smile[/emoticon]"
        },
        {
            title: CURLANG.smug,
            img: '<img title="smug" alt="smug" src="img/emoticons/Smilies/smug.png" class="sm">',
            bbcode: "[emoticon option='png']smug[/emoticon]"
        },
        {
            title: CURLANG.thumbsup,
            img: '<img title="thumbsup" alt="thumbsup" src="img/emoticons/Smilies/thumbsup.png" class="sm">',
            bbcode: "[emoticon option='png']thumbsup[/emoticon]"
        },
        {
            title: CURLANG.tongue,
            img: '<img title="tongue" alt="tongue" src="img/emoticons/Smilies/tongue.png" class="sm">',
            bbcode: "[emoticon option='png']tongue[/emoticon]"
        },
        {
            title: CURLANG.um,
            img: '<img title="um" alt="um" src="img/emoticons/Smilies/um.png" class="sm">',
            bbcode: "[emoticon option='png']um[/emoticon]"
        },
        {
            title: CURLANG.unhappy,
            img: '<img title="unhappy" alt="unhappy" src="img/emoticons/Smilies/unhappy.png" class="sm">',
            bbcode: "[emoticon option='png']unhappy[/emoticon]"
        },
        {
            title: CURLANG.wanker,
            img: '<img title="wanker" alt="wanker" src="img/emoticons/Smilies/wanker.gif" class="sm">',
            bbcode: "[emoticon option='gif']wanker[/emoticon]"
        },
        {
            title: CURLANG.wink,
            img: '<img title="wink" alt="wink" src="img/emoticons/Smilies/wink.png" class="sm">',
            bbcode: "[emoticon option='png']wink[/emoticon]"
        },
        {
            title: CURLANG.wow,
            img: '<img title="wow" alt="wow" src="img/emoticons/Smilies/wow.png" class="sm">',
            bbcode: "[emoticon option='png']wow[/emoticon]"
        },
        {
            title: CURLANG.red_card,
            img: '<img title="red_card" alt="red_card" src="img/emoticons/Cards/red_card.png" class="sm">',
            bbcode: "[emoticon option='png']red_card[/emoticon]"
        },
        {
            title: CURLANG.yellow_card,
            img: '<img title="yellow_card" alt="yellow_card" src="img/emoticons/Cards/yellow_card.png" class="sm">',
            bbcode: "[emoticon option='png']yellow_card[/emoticon]"
        },
        {
            title: CURLANG.algeria,
            img: '<img title="algeria" alt="algeria" src="img/emoticons/Flags/algeria.png" class="sm">',
            bbcode: "[emoticon option='png']algeria[/emoticon]"
        },
        {
            title: CURLANG.argantina,
            img: '<img title="argantina" alt="argantina" src="img/emoticons/Flags/argantina.png" class="sm">',
            bbcode: "[emoticon option='png']argantina[/emoticon]"
        },
        {
            title: CURLANG.australia,
            img: '<img title="australia" alt="australia" src="img/emoticons/Flags/australia.png" class="sm">',
            bbcode: "[emoticon option='png']australia[/emoticon]"
        },
        {
            title: CURLANG.brazil,
            img: '<img title="brazil" alt="brazil" src="img/emoticons/Flags/brazil.png" class="sm">',
            bbcode: "[emoticon option='png']brazil[/emoticon]"
        },
        {
            title: CURLANG.cameroon,
            img: '<img title="cameroon" alt="cameroon" src="img/emoticons/Flags/cameroon.png" class="sm">',
            bbcode: "[emoticon option='png']cameroon[/emoticon]"
        },
        {
            title: CURLANG.canada,
            img: '<img title="canada" alt="canada" src="img/emoticons/Flags/canada.png" class="sm">',
            bbcode: "[emoticon option='png']canada[/emoticon]"
        },
        {
            title: CURLANG.chile,
            img: '<img title="chile" alt="chile" src="img/emoticons/Flags/chile.png" class="sm">',
            bbcode: "[emoticon option='png']chile[/emoticon]"
        },
        {
            title: CURLANG.croatia,
            img: '<img title="croatia" alt="croatia" src="img/emoticons/Flags/croatia.png" class="sm">',
            bbcode: "[emoticon option='png']croatia[/emoticon]"
        },
        {
            title: CURLANG.czech_republic,
            img: '<img title="czech_republic" alt="czech_republic" src="img/emoticons/Flags/czech_republic.png" class="sm">',
            bbcode: "[emoticon option='png']czech_republic[/emoticon]"
        },
        {
            title: CURLANG.denmark,
            img: '<img title="denmark" alt="denmark" src="img/emoticons/Flags/denmark.png" class="sm">',
            bbcode: "[emoticon option='png']denmark[/emoticon]"
        },
        {
            title: CURLANG.england,
            img: '<img title="england" alt="england" src="img/emoticons/Flags/england.png" class="sm">',
            bbcode: "[emoticon option='png']england[/emoticon]"
        },
        {
            title: CURLANG.france,
            img: '<img title="france" alt="france" src="img/emoticons/Flags/france.png" class="sm">',
            bbcode: "[emoticon option='png']france[/emoticon]"
        },
        {
            title: CURLANG.germany,
            img: '<img title="germany" alt="germany" src="img/emoticons/Flags/germany.png" class="sm">',
            bbcode: "[emoticon option='png']germany[/emoticon]"
        },
        {
            title: CURLANG.ghana,
            img: '<img title="ghana" alt="ghana" src="img/emoticons/Flags/ghana.png" class="sm">',
            bbcode: "[emoticon option='png']ghana[/emoticon]"
        },
        {
            title: CURLANG.greece,
            img: '<img title="greece" alt="greece" src="img/emoticons/Flags/greece.png" class="sm">',
            bbcode: "[emoticon option='png']greece[/emoticon]"
        },
        {
            title: CURLANG.honduras,
            img: '<img title="honduras" alt="honduras" src="img/emoticons/Flags/honduras.png" class="sm">',
            bbcode: "[emoticon option='png']honduras[/emoticon]"
        },
        {
            title: CURLANG.ireland,
            img: '<img title="ireland" alt="ireland" src="img/emoticons/Flags/ireland.png" class="sm">',
            bbcode: "[emoticon option='png']ireland[/emoticon]"
        },
        {
            title: CURLANG.italy,
            img: '<img title="italy" alt="italy" src="img/emoticons/Flags/italy.png" class="sm">',
            bbcode: "[emoticon option='png']italy[/emoticon]"
        },
        {
            title: CURLANG.ivory_coast,
            img: '<img title="ivory_coast" alt="ivory_coast" src="img/emoticons/Flags/ivory_coast.png" class="sm">',
            bbcode: "[emoticon option='png']ivory_coast[/emoticon]"
        },
        {
            title: CURLANG.japan,
            img: '<img title="japan" alt="japan" src="img/emoticons/Flags/japan.png" class="sm">',
            bbcode: "[emoticon option='png']japan[/emoticon]"
        },
        {
            title: CURLANG.mexico,
            img: '<img title="mexico" alt="mexico" src="img/emoticons/Flags/mexico.png" class="sm">',
            bbcode: "[emoticon option='png']mexico[/emoticon]"
        },
        {
            title: CURLANG.netherlands,
            img: '<img title="netherlands" alt="netherlands" src="img/emoticons/Flags/netherlands.png" class="sm">',
            bbcode: "[emoticon option='png']netherlands[/emoticon]"
        },
        {
            title: CURLANG.new_zealand,
            img: '<img title="new_zealand" alt="new_zealand" src="img/emoticons/Flags/new_zealand.png" class="sm">',
            bbcode: "[emoticon option='png']new_zealand[/emoticon]"
        },
        {
            title: CURLANG.nigeria,
            img: '<img title="nigeria" alt="nigeria" src="img/emoticons/Flags/nigeria.png" class="sm">',
            bbcode: "[emoticon option='png']nigeria[/emoticon]"
        },
        {
            title: CURLANG.north_korea,
            img: '<img title="north_korea" alt="north_korea" src="img/emoticons/Flags/north_korea.png" class="sm">',
            bbcode: "[emoticon option='png']north_korea[/emoticon]"
        },
        {
            title: CURLANG.northern_ireland,
            img: '<img title="northern_ireland" alt="northern_ireland" src="img/emoticons/Flags/northern_ireland.png" class="sm">',
            bbcode: "[emoticon option='png']northern_ireland[/emoticon]"
        },
        {
            title: CURLANG.paraguay,
            img: '<img title="paraguay" alt="paraguay" src="img/emoticons/Flags/paraguay.png" class="sm">',
            bbcode: "[emoticon option='png']paraguay[/emoticon]"
        },
        {
            title: CURLANG.poland,
            img: '<img title="poland" alt="poland" src="img/emoticons/Flags/poland.png" class="sm">',
            bbcode: "[emoticon option='png']poland[/emoticon]"
        },
        {
            title: CURLANG.portugal,
            img: '<img title="portugal" alt="portugal" src="img/emoticons/Flags/portugal.png" class="sm">',
            bbcode: "[emoticon option='png']portugal[/emoticon]"
        },
        {
            title: CURLANG.russia,
            img: '<img title="russia" alt="russia" src="img/emoticons/Flags/russia.png" class="sm">',
            bbcode: "[emoticon option='png']russia[/emoticon]"
        },
        {
            title: CURLANG.scotland,
            img: '<img title="scotland" alt="scotland" src="img/emoticons/Flags/scotland.png" class="sm">',
            bbcode: "[emoticon option='png']scotland[/emoticon]"
        },
        {
            title: CURLANG.serbia,
            img: '<img title="serbia" alt="serbia" src="img/emoticons/Flags/serbia.png" class="sm">',
            bbcode: "[emoticon option='png']serbia[/emoticon]"
        },
        {
            title: CURLANG.slovakia,
            img: '<img title="slovakia" alt="slovakia" src="img/emoticons/Flags/slovakia.png" class="sm">',
            bbcode: "[emoticon option='png']slovakia[/emoticon]"
        },
        {
            title: CURLANG.slovenia,
            img: '<img title="slovenia" alt="slovenia" src="img/emoticons/Flags/slovenia.png" class="sm">',
            bbcode: "[emoticon option='png']slovenia[/emoticon]"
        },
        {
            title: CURLANG.south_africa,
            img: '<img title="south_africa" alt="south_africa" src="img/emoticons/Flags/south_africa.png" class="sm">',
            bbcode: "[emoticon option='png']south_africa[/emoticon]"
        },
        {
            title: CURLANG.south_korea,
            img: '<img title="south_korea" alt="south_korea" src="img/emoticons/Flags/south_korea.png" class="sm">',
            bbcode: "[emoticon option='png']south_korea[/emoticon]"
        },
        {
            title: CURLANG.spain,
            img: '<img title="spain" alt="spain" src="img/emoticons/Flags/spain.png" class="sm">',
            bbcode: "[emoticon option='png']spain[/emoticon]"
        },
        {
            title: CURLANG.sweden,
            img: '<img title="sweden" alt="sweden" src="img/emoticons/Flags/sweden.png" class="sm">',
            bbcode: "[emoticon option='png']sweden[/emoticon]"
        },
        {
            title: CURLANG.switzerland,
            img: '<img title="switzerland" alt="switzerland" src="img/emoticons/Flags/switzerland.png" class="sm">',
            bbcode: "[emoticon option='png']switzerland[/emoticon]"
        },
        {
            title: CURLANG.uk,
            img: '<img title="uk" alt="uk" src="img/emoticons/Flags/uk.png" class="sm">',
            bbcode: "[emoticon option='png']uk[/emoticon]"
        },
        {
            title: CURLANG.ukraine,
            img: '<img title="ukraine" alt="ukraine" src="img/emoticons/Flags/ukraine.png" class="sm">',
            bbcode: "[emoticon option='png']ukraine[/emoticon]"
        },
        {
            title: CURLANG.united_states_of_america,
            img: '<img title="united_states_of_america" alt="united_states_of_america" src="img/emoticons/Flags/united_states_of_america.png" class="sm">',
            bbcode: "[emoticon option='png']united_states_of_america[/emoticon]"
        },
        {
            title: CURLANG.uruguay,
            img: '<img title="uruguay" alt="uruguay" src="img/emoticons/Flags/uruguay.png" class="sm">',
            bbcode: "[emoticon option='png']uruguay[/emoticon]"
        },
        {
            title: CURLANG.wales,
            img: '<img title="wales" alt="wales" src="img/emoticons/Flags/wales.png" class="sm">',
            bbcode: "[emoticon option='png']wales[/emoticon]"
        },
        {
            title: CURLANG.balls,
            img: '<img title="balls" alt="balls" src="img/emoticons/Items/balls.png" class="sm">',
            bbcode: "[emoticon option='png']balls[/emoticon]"
        },
        {
            title: CURLANG.bin,
            img: '<img title="bin" alt="bin" src="img/emoticons/Items/bin.png" class="sm">',
            bbcode: "[emoticon option='png']bin[/emoticon]"
        },
        {
            title: CURLANG.black_boots,
            img: '<img title="black_boots" alt="black_boots" src="img/emoticons/Items/black_boots.png" class="sm">',
            bbcode: "[emoticon option='png']black_boots[/emoticon]"
        },
        {
            title: CURLANG.cocktail,
            img: '<img title="cocktail" alt="cocktail" src="img/emoticons/Items/cocktail.png" class="sm">',
            bbcode: "[emoticon option='png']cocktail[/emoticon]"
        },
        {
            title: CURLANG.coffee,
            img: '<img title="coffee" alt="coffee" src="img/emoticons/Items/coffee.png" class="sm">',
            bbcode: "[emoticon option='png']coffee[/emoticon]"
        },
        {
            title: CURLANG.crap,
            img: '<img title="crap" alt="crap" src="img/emoticons/Items/crap.png" class="sm">',
            bbcode: "[emoticon option='png']crap[/emoticon]"
        },
        {
            title: CURLANG.factory,
            img: '<img title="factory" alt="factory" src="img/emoticons/Items/factory.png" class="sm">',
            bbcode: "[emoticon option='png']factory[/emoticon]"
        },
        {
            title: CURLANG.guinness,
            img: '<img title="guinness" alt="guinness" src="img/emoticons/Items/guinness.png" class="sm">',
            bbcode: "[emoticon option='png']guinness[/emoticon]"
        },
        {
            title: CURLANG.handbag,
            img: '<img title="handbag" alt="handbag" src="img/emoticons/Items/handbag.png" class="sm">',
            bbcode: "[emoticon option='png']handbag[/emoticon]"
        },
        {
            title: CURLANG.hendos,
            img: '<img title="hendos" alt="hendos" src="img/emoticons/Items/hendos.png" class="sm">',
            bbcode: "[emoticon option='png']hendos[/emoticon]"
        },
        {
            title: CURLANG.hobnobs,
            img: '<img title="hobnobs" alt="hobnobs" src="img/emoticons/Items/hobnobs.png" class="sm">',
            bbcode: "[emoticon option='png']hobnobs[/emoticon]"
        },
        {
            title: CURLANG.house,
            img: '<img title="house" alt="house" src="img/emoticons/Items/house.png" class="sm">',
            bbcode: "[emoticon option='png']house[/emoticon]"
        },
        {
            title: CURLANG.meter,
            img: '<img title="meter" alt="meter" src="img/emoticons/Items/meter.png" class="sm">',
            bbcode: "[emoticon option='png']meter[/emoticon]"
        },
        {
            title: CURLANG.office,
            img: '<img title="office" alt="office" src="img/emoticons/Items/office.png" class="sm">',
            bbcode: "[emoticon option='png']office[/emoticon]"
        },
        {
            title: CURLANG.old_spice,
            img: '<img title="old_spice" alt="old_spice" src="img/emoticons/Items/old_spice.png" class="sm">',
            bbcode: "[emoticon option='png']old_spice[/emoticon]"
        },
        {
            title: CURLANG.pineapple,
            img: '<img title="pineapple" alt="pineapple" src="img/emoticons/Items/pineapple.png" class="sm">',
            bbcode: "[emoticon option='png']pineapple[/emoticon]"
        },
        {
            title: CURLANG.pink_boot,
            img: '<img title="pink_boot" alt="pink_boot" src="img/emoticons/Items/pink_boot.png" class="sm">',
            bbcode: "[emoticon option='png']pink_boot[/emoticon]"
        },
        {
            title: CURLANG.pint,
            img: '<img title="pint" alt="pint" src="img/emoticons/Items/pint.png" class="sm">',
            bbcode: "[emoticon option='png']pint[/emoticon]"
        },
        {
            title: CURLANG.poppy,
            img: '<img title="poppy" alt="poppy" src="img/emoticons/Items/poppy.png" class="sm">',
            bbcode: "[emoticon option='png']poppy[/emoticon]"
        },
        {
            title: CURLANG.razor,
            img: '<img title="razor" alt="razor" src="img/emoticons/Items/razor.png" class="sm">',
            bbcode: "[emoticon option='png']razor[/emoticon]"
        },
        {
            title: CURLANG.talc,
            img: '<img title="talc" alt="talc" src="img/emoticons/Items/talc.png" class="sm">',
            bbcode: "[emoticon option='png']talc[/emoticon]"
        },
        {
            title: CURLANG.white_boots,
            img: '<img title="white_boots" alt="white_boots" src="img/emoticons/Items/white_boots.png" class="sm">',
            bbcode: "[emoticon option='png']white_boots[/emoticon]"
        },
        {
            title: CURLANG.wine,
            img: '<img title="wine" alt="wine" src="img/emoticons/Items/wine.png" class="sm">',
            bbcode: "[emoticon option='png']wine[/emoticon]"
        },
        {
            title: CURLANG.cft,
            img: '<img title="cft" alt="cft" src="img/emoticons/Other/cft.png" class="sm">',
            bbcode: "[emoticon option='png']cft[/emoticon]"
        },
        {
            title: CURLANG.duh,
            img: '<img title="duh" alt="duh" src="img/emoticons/Other/duh.png" class="sm">',
            bbcode: "[emoticon option='png']duh[/emoticon]"
        },
        {
            title: CURLANG.warning,
            img: '<img title="warning" alt="warning" src="img/emoticons/Other/warning.png" class="sm">',
            bbcode: "[emoticon option='png']warning[/emoticon]"
        },
        {
            title: CURLANG.red_card,
            img: '<img title="red_card" alt="red_card" src="img/emoticons/Cards/red_card.png" class="sm">',
            bbcode: "[emoticon option='png']red_card[/emoticon]"
        },
        {
            title: CURLANG.yellow_card,
            img: '<img title="yellow_card" alt="yellow_card" src="img/emoticons/Cards/yellow_card.png" class="sm">',
            bbcode: "[emoticon option='png']yellow_card[/emoticon]"
        },
        {
            title: CURLANG.accrington_stanley,
            img: '<img title="accrington_stanley" alt="accrington_stanley" src="img/emoticons/Shirts/accrington_stanley.png" class="sm">',
            bbcode: "[emoticon option='png']accrington_stanley[/emoticon]"
        },
        {
            title: CURLANG.aldershot_town,
            img: '<img title="aldershot_town" alt="aldershot_town" src="img/emoticons/Shirts/aldershot_town.png" class="sm">',
            bbcode: "[emoticon option='png']aldershot_town[/emoticon]"
        },
        {
            title: CURLANG.arsenal,
            img: '<img title="arsenal" alt="arsenal" src="img/emoticons/Shirts/arsenal.png" class="sm">',
            bbcode: "[emoticon option='png']arsenal[/emoticon]"
        },
        {
            title: CURLANG.aston_villa,
            img: '<img title="aston_villa" alt="aston_villa" src="img/emoticons/Shirts/aston_villa.png" class="sm">',
            bbcode: "[emoticon option='png']aston_villa[/emoticon]"
        },
        {
            title: CURLANG.barnet,
            img: '<img title="barnet" alt="barnet" src="img/emoticons/Shirts/barnet.png" class="sm">',
            bbcode: "[emoticon option='png']barnet[/emoticon]"
        },
        {
            title: CURLANG.barnsley,
            img: '<img title="barnsley" alt="barnsley" src="img/emoticons/Shirts/barnsley.png" class="sm">',
            bbcode: "[emoticon option='png']barnsley[/emoticon]"
        },
        {
            title: CURLANG.birmingham_city,
            img: '<img title="birmingham_city" alt="birmingham_city" src="img/emoticons/Shirts/birmingham_city.png" class="sm">',
            bbcode: "[emoticon option='png']birmingham_city[/emoticon]"
        },
        {
            title: CURLANG.blackburn_rovers,
            img: '<img title="blackburn_rovers" alt="blackburn_rovers" src="img/emoticons/Shirts/blackburn_rovers.png" class="sm">',
            bbcode: "[emoticon option='png']blackburn_rovers[/emoticon]"
        },
        {
            title: CURLANG.blackpool,
            img: '<img title="blackpool" alt="blackpool" src="img/emoticons/Shirts/blackpool.png" class="sm">',
            bbcode: "[emoticon option='png']blackpool[/emoticon]"
        },
        {
            title: CURLANG.bolton_wanderers,
            img: '<img title="bolton_wanderers" alt="bolton_wanderers" src="img/emoticons/Shirts/bolton_wanderers.png" class="sm">',
            bbcode: "[emoticon option='png']bolton_wanderers[/emoticon]"
        },
        {
            title: CURLANG.bournemouth,
            img: '<img title="bournemouth" alt="bournemouth" src="img/emoticons/Shirts/bournemouth.png" class="sm">',
            bbcode: "[emoticon option='png']bournemouth[/emoticon]"
        },
        {
            title: CURLANG.bradford_city,
            img: '<img title="bradford_city" alt="bradford_city" src="img/emoticons/Shirts/bradford_city.png" class="sm">',
            bbcode: "[emoticon option='png']bradford_city[/emoticon]"
        },
        {
            title: CURLANG.brentford,
            img: '<img title="brentford" alt="brentford" src="img/emoticons/Shirts/brentford.png" class="sm">',
            bbcode: "[emoticon option='png']brentford[/emoticon]"
        },
        {
            title: CURLANG.brighton_and_hove_albion,
            img: '<img title="brighton_and_hove_albion" alt="brighton_and_hove_albion" src="img/emoticons/Shirts/brighton_and_hove_albion.png" class="sm">',
            bbcode: "[emoticon option='png']brighton_and_hove_albion[/emoticon]"
        },
        {
            title: CURLANG.bristol_city,
            img: '<img title="bristol_city" alt="bristol_city" src="img/emoticons/Shirts/bristol_city.png" class="sm">',
            bbcode: "[emoticon option='png']bristol_city[/emoticon]"
        },
        {
            title: CURLANG.bristol_rovers,
            img: '<img title="bristol_rovers" alt="bristol_rovers" src="img/emoticons/Shirts/bristol_rovers.png" class="sm">',
            bbcode: "[emoticon option='png']bristol_rovers[/emoticon]"
        },
        {
            title: CURLANG.burnley,
            img: '<img title="burnley" alt="burnley" src="img/emoticons/Shirts/burnley.png" class="sm">',
            bbcode: "[emoticon option='png']burnley[/emoticon]"
        },
        {
            title: CURLANG.burton_albion,
            img: '<img title="burton_albion" alt="burton_albion" src="img/emoticons/Shirts/burton_albion.png" class="sm">',
            bbcode: "[emoticon option='png']burton_albion[/emoticon]"
        },
        {
            title: CURLANG.bury,
            img: '<img title="bury" alt="bury" src="img/emoticons/Shirts/bury.png" class="sm">',
            bbcode: "[emoticon option='png']bury[/emoticon]"
        },
        {
            title: CURLANG.cardiff_city,
            img: '<img title="cardiff_city" alt="cardiff_city" src="img/emoticons/Shirts/cardiff_city.png" class="sm">',
            bbcode: "[emoticon option='png']cardiff_city[/emoticon]"
        },
        {
            title: CURLANG.carlisle_united,
            img: '<img title="carlisle_united" alt="carlisle_united" src="img/emoticons/Shirts/carlisle_united.png" class="sm">',
            bbcode: "[emoticon option='png']carlisle_united[/emoticon]"
        },
        {
            title: CURLANG.charlton_athletic,
            img: '<img title="charlton_athletic" alt="charlton_athletic" src="img/emoticons/Shirts/charlton_athletic.png" class="sm">',
            bbcode: "[emoticon option='png']charlton_athletic[/emoticon]"
        },
        {
            title: CURLANG.chelsea,
            img: '<img title="chelsea" alt="chelsea" src="img/emoticons/Shirts/chelsea.png" class="sm">',
            bbcode: "[emoticon option='png']chelsea[/emoticon]"
        },
        {
            title: CURLANG.cheltenham_town,
            img: '<img title="cheltenham_town" alt="cheltenham_town" src="img/emoticons/Shirts/cheltenham_town.png" class="sm">',
            bbcode: "[emoticon option='png']cheltenham_town[/emoticon]"
        },
        {
            title: CURLANG.chesterfield,
            img: '<img title="chesterfield" alt="chesterfield" src="img/emoticons/Shirts/chesterfield.png" class="sm">',
            bbcode: "[emoticon option='png']chesterfield[/emoticon]"
        },
        {
            title: CURLANG.colchester_united,
            img: '<img title="colchester_united" alt="colchester_united" src="img/emoticons/Shirts/colchester_united.png" class="sm">',
            bbcode: "[emoticon option='png']colchester_united[/emoticon]"
        },
        {
            title: CURLANG.coventry_city,
            img: '<img title="coventry_city" alt="coventry_city" src="img/emoticons/Shirts/coventry_city.png" class="sm">',
            bbcode: "[emoticon option='png']coventry_city[/emoticon]"
        },
        {
            title: CURLANG.crawley_town,
            img: '<img title="crawley_town" alt="crawley_town" src="img/emoticons/Shirts/crawley_town.png" class="sm">',
            bbcode: "[emoticon option='png']crawley_town[/emoticon]"
        },
        {
            title: CURLANG.crewe_alexandra,
            img: '<img title="crewe_alexandra" alt="crewe_alexandra" src="img/emoticons/Shirts/crewe_alexandra.png" class="sm">',
            bbcode: "[emoticon option='png']crewe_alexandra[/emoticon]"
        },
        {
            title: CURLANG.crystal_palace,
            img: '<img title="crystal_palace" alt="crystal_palace" src="img/emoticons/Shirts/crystal_palace.png" class="sm">',
            bbcode: "[emoticon option='png']crystal_palace[/emoticon]"
        },
        {
            title: CURLANG.dagenham_and_redbridge,
            img: '<img title="dagenham_and_redbridge" alt="dagenham_and_redbridge" src="img/emoticons/Shirts/dagenham_and_redbridge.png" class="sm">',
            bbcode: "[emoticon option='png']dagenham_and_redbridge[/emoticon]"
        },
        {
            title: CURLANG.derby_county,
            img: '<img title="derby_county" alt="derby_county" src="img/emoticons/Shirts/derby_county.png" class="sm">',
            bbcode: "[emoticon option='png']derby_county[/emoticon]"
        },
        {
            title: CURLANG.doncaster_rovers,
            img: '<img title="doncaster_rovers" alt="doncaster_rovers" src="img/emoticons/Shirts/doncaster_rovers.png" class="sm">',
            bbcode: "[emoticon option='png']doncaster_rovers[/emoticon]"
        },
        {
            title: CURLANG.everton,
            img: '<img title="everton" alt="everton" src="img/emoticons/Shirts/everton.png" class="sm">',
            bbcode: "[emoticon option='png']everton[/emoticon]"
        },
        {
            title: CURLANG.exeter_city,
            img: '<img title="exeter_city" alt="exeter_city" src="img/emoticons/Shirts/exeter_city.png" class="sm">',
            bbcode: "[emoticon option='png']exeter_city[/emoticon]"
        },
        {
            title: CURLANG.fleetwood_town,
            img: '<img title="fleetwood_town" alt="fleetwood_town" src="img/emoticons/Shirts/fleetwood_town.png" class="sm">',
            bbcode: "[emoticon option='png']fleetwood_town[/emoticon]"
        },
        {
            title: CURLANG.fulham,
            img: '<img title="fulham" alt="fulham" src="img/emoticons/Shirts/fulham.png" class="sm">',
            bbcode: "[emoticon option='png']fulham[/emoticon]"
        },
        {
            title: CURLANG.gillingham,
            img: '<img title="gillingham" alt="gillingham" src="img/emoticons/Shirts/gillingham.png" class="sm">',
            bbcode: "[emoticon option='png']gillingham[/emoticon]"
        },
        {
            title: CURLANG.hartlepool_united,
            img: '<img title="hartlepool_united" alt="hartlepool_united" src="img/emoticons/Shirts/hartlepool_united.png" class="sm">',
            bbcode: "[emoticon option='png']hartlepool_united[/emoticon]"
        },
        {
            title: CURLANG.hereford_united,
            img: '<img title="hereford_united" alt="hereford_united" src="img/emoticons/Shirts/hereford_united.png" class="sm">',
            bbcode: "[emoticon option='png']hereford_united[/emoticon]"
        },
        {
            title: CURLANG.huddersfield_town,
            img: '<img title="huddersfield_town" alt="huddersfield_town" src="img/emoticons/Shirts/huddersfield_town.png" class="sm">',
            bbcode: "[emoticon option='png']huddersfield_town[/emoticon]"
        },
        {
            title: CURLANG.hull_city,
            img: '<img title="hull_city" alt="hull_city" src="img/emoticons/Shirts/hull_city.png" class="sm">',
            bbcode: "[emoticon option='png']hull_city[/emoticon]"
        },
        {
            title: CURLANG.ipswich_town,
            img: '<img title="ipswich_town" alt="ipswich_town" src="img/emoticons/Shirts/ipswich_town.png" class="sm">',
            bbcode: "[emoticon option='png']ipswich_town[/emoticon]"
        },
        {
            title: CURLANG.leeds_united,
            img: '<img title="leeds_united" alt="leeds_united" src="img/emoticons/Shirts/leeds_united.png" class="sm">',
            bbcode: "[emoticon option='png']leeds_united[/emoticon]"
        },
        {
            title: CURLANG.leicester_city,
            img: '<img title="leicester_city" alt="leicester_city" src="img/emoticons/Shirts/leicester_city.png" class="sm">',
            bbcode: "[emoticon option='png']leicester_city[/emoticon]"
        },
        {
            title: CURLANG.leyton_orient,
            img: '<img title="leyton_orient" alt="leyton_orient" src="img/emoticons/Shirts/leyton_orient.png" class="sm">',
            bbcode: "[emoticon option='png']leyton_orient[/emoticon]"
        },
        {
            title: CURLANG.liverpool,
            img: '<img title="liverpool" alt="liverpool" src="img/emoticons/Shirts/liverpool.png" class="sm">',
            bbcode: "[emoticon option='png']liverpool[/emoticon]"
        },
        {
            title: CURLANG.macclesfield_town,
            img: '<img title="macclesfield_town" alt="macclesfield_town" src="img/emoticons/Shirts/macclesfield_town.png" class="sm">',
            bbcode: "[emoticon option='png']macclesfield_town[/emoticon]"
        },
        {
            title: CURLANG.manchester_city,
            img: '<img title="manchester_city" alt="manchester_city" src="img/emoticons/Shirts/manchester_city.png" class="sm">',
            bbcode: "[emoticon option='png']manchester_city[/emoticon]"
        },
        {
            title: CURLANG.manchester_united,
            img: '<img title="manchester_united" alt="manchester_united" src="img/emoticons/Shirts/manchester_united.png" class="sm">',
            bbcode: "[emoticon option='png']manchester_united[/emoticon]"
        },
        {
            title: CURLANG.middlesbrough,
            img: '<img title="middlesbrough" alt="middlesbrough" src="img/emoticons/Shirts/middlesbrough.png" class="sm">',
            bbcode: "[emoticon option='png']middlesbrough[/emoticon]"
        },
        {
            title: CURLANG.millwall,
            img: '<img title="millwall" alt="millwall" src="img/emoticons/Shirts/millwall.png" class="sm">',
            bbcode: "[emoticon option='png']millwall[/emoticon]"
        },
        {
            title: CURLANG.milton_keynes_dons,
            img: '<img title="milton_keynes_dons" alt="milton_keynes_dons" src="img/emoticons/Shirts/milton_keynes_dons.png" class="sm">',
            bbcode: "[emoticon option='png']milton_keynes_dons[/emoticon]"
        },
        {
            title: CURLANG.morecambe,
            img: '<img title="morecambe" alt="morecambe" src="img/emoticons/Shirts/morecambe.png" class="sm">',
            bbcode: "[emoticon option='png']morecambe[/emoticon]"
        },
        {
            title: CURLANG.newcastle_united,
            img: '<img title="newcastle_united" alt="newcastle_united" src="img/emoticons/Shirts/newcastle_united.png" class="sm">',
            bbcode: "[emoticon option='png']newcastle_united[/emoticon]"
        },
        {
            title: CURLANG.northampton_town,
            img: '<img title="northampton_town" alt="northampton_town" src="img/emoticons/Shirts/northampton_town.png" class="sm">',
            bbcode: "[emoticon option='png']northampton_town[/emoticon]"
        },
        {
            title: CURLANG.norwich_city,
            img: '<img title="norwich_city" alt="norwich_city" src="img/emoticons/Shirts/norwich_city.png" class="sm">',
            bbcode: "[emoticon option='png']norwich_city[/emoticon]"
        },
        {
            title: CURLANG.nottingham_forest,
            img: '<img title="nottingham_forest" alt="nottingham_forest" src="img/emoticons/Shirts/nottingham_forest.png" class="sm">',
            bbcode: "[emoticon option='png']nottingham_forest[/emoticon]"
        },
        {
            title: CURLANG.notts_county,
            img: '<img title="notts_county" alt="notts_county" src="img/emoticons/Shirts/notts_county.png" class="sm">',
            bbcode: "[emoticon option='png']notts_county[/emoticon]"
        },
        {
            title: CURLANG.oldham_athletic,
            img: '<img title="oldham_athletic" alt="oldham_athletic" src="img/emoticons/Shirts/oldham_athletic.png" class="sm">',
            bbcode: "[emoticon option='png']oldham_athletic[/emoticon]"
        },
        {
            title: CURLANG.oxford_united,
            img: '<img title="oxford_united" alt="oxford_united" src="img/emoticons/Shirts/oxford_united.png" class="sm">',
            bbcode: "[emoticon option='png']oxford_united[/emoticon]"
        },
        {
            title: CURLANG.peterborough_united,
            img: '<img title="peterborough_united" alt="peterborough_united" src="img/emoticons/Shirts/peterborough_united.png" class="sm">',
            bbcode: "[emoticon option='png']peterborough_united[/emoticon]"
        },
        {
            title: CURLANG.plymouth_argyle,
            img: '<img title="plymouth_argyle" alt="plymouth_argyle" src="img/emoticons/Shirts/plymouth_argyle.png" class="sm">',
            bbcode: "[emoticon option='png']plymouth_argyle[/emoticon]"
        },
        {
            title: CURLANG.port_vale,
            img: '<img title="port_vale" alt="port_vale" src="img/emoticons/Shirts/port_vale.png" class="sm">',
            bbcode: "[emoticon option='png']port_vale[/emoticon]"
        },
        {
            title: CURLANG.portsmouth,
            img: '<img title="portsmouth" alt="portsmouth" src="img/emoticons/Shirts/portsmouth.png" class="sm">',
            bbcode: "[emoticon option='png']portsmouth[/emoticon]"
        },
        {
            title: CURLANG.preston_north_end,
            img: '<img title="preston_north_end" alt="preston_north_end" src="img/emoticons/Shirts/preston_north_end.png" class="sm">',
            bbcode: "[emoticon option='png']preston_north_end[/emoticon]"
        },
        {
            title: CURLANG.queens_park_rangers,
            img: '<img title="queens_park_rangers" alt="queens_park_rangers" src="img/emoticons/Shirts/queens_park_rangers.png" class="sm">',
            bbcode: "[emoticon option='png']queens_park_rangers[/emoticon]"
        },
        {
            title: CURLANG.reading,
            img: '<img title="reading" alt="reading" src="img/emoticons/Shirts/reading.png" class="sm">',
            bbcode: "[emoticon option='png']reading[/emoticon]"
        },
        {
            title: CURLANG.rochdale,
            img: '<img title="rochdale" alt="rochdale" src="img/emoticons/Shirts/rochdale.png" class="sm">',
            bbcode: "[emoticon option='png']rochdale[/emoticon]"
        },
        {
            title: CURLANG.rotherham_united,
            img: '<img title="rotherham_united" alt="rotherham_united" src="img/emoticons/Shirts/rotherham_united.png" class="sm">',
            bbcode: "[emoticon option='png']rotherham_united[/emoticon]"
        },
        {
            title: CURLANG.scunthorpe_united,
            img: '<img title="scunthorpe_united" alt="scunthorpe_united" src="img/emoticons/Shirts/scunthorpe_united.png" class="sm">',
            bbcode: "[emoticon option='png']scunthorpe_united[/emoticon]"
        },
        {
            title: CURLANG.sheffield_united,
            img: '<img title="sheffield_united" alt="sheffield_united" src="img/emoticons/Shirts/sheffield_united.png" class="sm">',
            bbcode: "[emoticon option='png']sheffield_united[/emoticon]"
        },
        {
            title: CURLANG.sheffield_wednesday,
            img: '<img title="sheffield_wednesday" alt="sheffield_wednesday" src="img/emoticons/Shirts/sheffield_wednesday.png" class="sm">',
            bbcode: "[emoticon option='png']sheffield_wednesday[/emoticon]"
        },
        {
            title: CURLANG.shrewsbury_town,
            img: '<img title="shrewsbury_town" alt="shrewsbury_town" src="img/emoticons/Shirts/shrewsbury_town.png" class="sm">',
            bbcode: "[emoticon option='png']shrewsbury_town[/emoticon]"
        },
        {
            title: CURLANG.southampton,
            img: '<img title="southampton" alt="southampton" src="img/emoticons/Shirts/southampton.png" class="sm">',
            bbcode: "[emoticon option='png']southampton[/emoticon]"
        },
        {
            title: CURLANG.southend_united,
            img: '<img title="southend_united" alt="southend_united" src="img/emoticons/Shirts/southend_united.png" class="sm">',
            bbcode: "[emoticon option='png']southend_united[/emoticon]"
        },
        {
            title: CURLANG.stevenage,
            img: '<img title="stevenage" alt="stevenage" src="img/emoticons/Shirts/stevenage.png" class="sm">',
            bbcode: "[emoticon option='png']stevenage[/emoticon]"
        },
        {
            title: CURLANG.stoke_city,
            img: '<img title="stoke_city" alt="stoke_city" src="img/emoticons/Shirts/stoke_city.png" class="sm">',
            bbcode: "[emoticon option='png']stoke_city[/emoticon]"
        },
        {
            title: CURLANG.sunderland,
            img: '<img title="sunderland" alt="sunderland" src="img/emoticons/Shirts/sunderland.png" class="sm">',
            bbcode: "[emoticon option='png']sunderland[/emoticon]"
        },
        {
            title: CURLANG.swansea_city,
            img: '<img title="swansea_city" alt="swansea_city" src="img/emoticons/Shirts/swansea_city.png" class="sm">',
            bbcode: "[emoticon option='png']swansea_city[/emoticon]"
        },
        {
            title: CURLANG.swindon_town,
            img: '<img title="swindon_town" alt="swindon_town" src="img/emoticons/Shirts/swindon_town.png" class="sm">',
            bbcode: "[emoticon option='png']swindon_town[/emoticon]"
        },
        {
            title: CURLANG.torquay_united,
            img: '<img title="torquay_united" alt="torquay_united" src="img/emoticons/Shirts/torquay_united.png" class="sm">',
            bbcode: "[emoticon option='png']torquay_united[/emoticon]"
        },
        {
            title: CURLANG.tottenham_hotspur,
            img: '<img title="tottenham_hotspur" alt="tottenham_hotspur" src="img/emoticons/Shirts/tottenham_hotspur.png" class="sm">',
            bbcode: "[emoticon option='png']tottenham_hotspur[/emoticon]"
        },
        {
            title: CURLANG.tranmere_rovers,
            img: '<img title="tranmere_rovers" alt="tranmere_rovers" src="img/emoticons/Shirts/tranmere_rovers.png" class="sm">',
            bbcode: "[emoticon option='png']tranmere_rovers[/emoticon]"
        },
        {
            title: CURLANG.walsall,
            img: '<img title="walsall" alt="walsall" src="img/emoticons/Shirts/walsall.png" class="sm">',
            bbcode: "[emoticon option='png']walsall[/emoticon]"
        },
        {
            title: CURLANG.watford,
            img: '<img title="watford" alt="watford" src="img/emoticons/Shirts/watford.png" class="sm">',
            bbcode: "[emoticon option='png']watford[/emoticon]"
        },
        {
            title: CURLANG.west_bromwich_albion,
            img: '<img title="west_bromwich_albion" alt="west_bromwich_albion" src="img/emoticons/Shirts/west_bromwich_albion.png" class="sm">',
            bbcode: "[emoticon option='png']west_bromwich_albion[/emoticon]"
        },
        {
            title: CURLANG.west_ham_united,
            img: '<img title="west_ham_united" alt="west_ham_united" src="img/emoticons/Shirts/west_ham_united.png" class="sm">',
            bbcode: "[emoticon option='png']west_ham_united[/emoticon]"
        },
        {
            title: CURLANG.wigan_athletic,
            img: '<img title="wigan_athletic" alt="wigan_athletic" src="img/emoticons/Shirts/wigan_athletic.png" class="sm">',
            bbcode: "[emoticon option='png']wigan_athletic[/emoticon]"
        },
        {
            title: CURLANG.wimbledon,
            img: '<img title="wimbledon" alt="wimbledon" src="img/emoticons/Shirts/wimbledon.png" class="sm">',
            bbcode: "[emoticon option='png']wimbledon[/emoticon]"
        },
        {
            title: CURLANG.wolverhampton_wanderers,
            img: '<img title="wolverhampton_wanderers" alt="wolverhampton_wanderers" src="img/emoticons/Shirts/wolverhampton_wanderers.png" class="sm">',
            bbcode: "[emoticon option='png']wolverhampton_wanderers[/emoticon]"
        },
        {
            title: CURLANG.wycombe_wanderers,
            img: '<img title="wycombe_wanderers" alt="wycombe_wanderers" src="img/emoticons/Shirts/wycombe_wanderers.png" class="sm">',
            bbcode: "[emoticon option='png']wycombe_wanderers[/emoticon]"
        },
        {
            title: CURLANG.yeovil_town,
            img: '<img title="yeovil_town" alt="yeovil_town" src="img/emoticons/Shirts/yeovil_town.png" class="sm">',
            bbcode: "[emoticon option='png']yeovil_town[/emoticon]"
        },
        {
            title: CURLANG.york_city,
            img: '<img title="york_city" alt="york_city" src="img/emoticons/Shirts/york_city.png" class="sm">',
            bbcode: "[emoticon option='png']york_city[/emoticon]"
        },
        {
            title: CURLANG.algeria,
            img: '<img title="algeria" alt="algeria" src="img/emoticons/Flags/algeria.png" class="sm">',
            bbcode: "[emoticon option='png']algeria[/emoticon]"
        },
        {
            title: CURLANG.argantina,
            img: '<img title="argantina" alt="argantina" src="img/emoticons/Flags/argantina.png" class="sm">',
            bbcode: "[emoticon option='png']argantina[/emoticon]"
        },
        {
            title: CURLANG.australia,
            img: '<img title="australia" alt="australia" src="img/emoticons/Flags/australia.png" class="sm">',
            bbcode: "[emoticon option='png']australia[/emoticon]"
        },
        {
            title: CURLANG.brazil,
            img: '<img title="brazil" alt="brazil" src="img/emoticons/Flags/brazil.png" class="sm">',
            bbcode: "[emoticon option='png']brazil[/emoticon]"
        },
        {
            title: CURLANG.cameroon,
            img: '<img title="cameroon" alt="cameroon" src="img/emoticons/Flags/cameroon.png" class="sm">',
            bbcode: "[emoticon option='png']cameroon[/emoticon]"
        },
        {
            title: CURLANG.canada,
            img: '<img title="canada" alt="canada" src="img/emoticons/Flags/canada.png" class="sm">',
            bbcode: "[emoticon option='png']canada[/emoticon]"
        },
        {
            title: CURLANG.chile,
            img: '<img title="chile" alt="chile" src="img/emoticons/Flags/chile.png" class="sm">',
            bbcode: "[emoticon option='png']chile[/emoticon]"
        },
        {
            title: CURLANG.croatia,
            img: '<img title="croatia" alt="croatia" src="img/emoticons/Flags/croatia.png" class="sm">',
            bbcode: "[emoticon option='png']croatia[/emoticon]"
        },
        {
            title: CURLANG.czech_republic,
            img: '<img title="czech_republic" alt="czech_republic" src="img/emoticons/Flags/czech_republic.png" class="sm">',
            bbcode: "[emoticon option='png']czech_republic[/emoticon]"
        },
        {
            title: CURLANG.denmark,
            img: '<img title="denmark" alt="denmark" src="img/emoticons/Flags/denmark.png" class="sm">',
            bbcode: "[emoticon option='png']denmark[/emoticon]"
        },
        {
            title: CURLANG.england,
            img: '<img title="england" alt="england" src="img/emoticons/Flags/england.png" class="sm">',
            bbcode: "[emoticon option='png']england[/emoticon]"
        },
        {
            title: CURLANG.france,
            img: '<img title="france" alt="france" src="img/emoticons/Flags/france.png" class="sm">',
            bbcode: "[emoticon option='png']france[/emoticon]"
        },
        {
            title: CURLANG.germany,
            img: '<img title="germany" alt="germany" src="img/emoticons/Flags/germany.png" class="sm">',
            bbcode: "[emoticon option='png']germany[/emoticon]"
        },
        {
            title: CURLANG.ghana,
            img: '<img title="ghana" alt="ghana" src="img/emoticons/Flags/ghana.png" class="sm">',
            bbcode: "[emoticon option='png']ghana[/emoticon]"
        },
        {
            title: CURLANG.greece,
            img: '<img title="greece" alt="greece" src="img/emoticons/Flags/greece.png" class="sm">',
            bbcode: "[emoticon option='png']greece[/emoticon]"
        },
        {
            title: CURLANG.honduras,
            img: '<img title="honduras" alt="honduras" src="img/emoticons/Flags/honduras.png" class="sm">',
            bbcode: "[emoticon option='png']honduras[/emoticon]"
        },
        {
            title: CURLANG.ireland,
            img: '<img title="ireland" alt="ireland" src="img/emoticons/Flags/ireland.png" class="sm">',
            bbcode: "[emoticon option='png']ireland[/emoticon]"
        },
        {
            title: CURLANG.italy,
            img: '<img title="italy" alt="italy" src="img/emoticons/Flags/italy.png" class="sm">',
            bbcode: "[emoticon option='png']italy[/emoticon]"
        },
        {
            title: CURLANG.ivory_coast,
            img: '<img title="ivory_coast" alt="ivory_coast" src="img/emoticons/Flags/ivory_coast.png" class="sm">',
            bbcode: "[emoticon option='png']ivory_coast[/emoticon]"
        },
        {
            title: CURLANG.japan,
            img: '<img title="japan" alt="japan" src="img/emoticons/Flags/japan.png" class="sm">',
            bbcode: "[emoticon option='png']japan[/emoticon]"
        },
        {
            title: CURLANG.mexico,
            img: '<img title="mexico" alt="mexico" src="img/emoticons/Flags/mexico.png" class="sm">',
            bbcode: "[emoticon option='png']mexico[/emoticon]"
        },
        {
            title: CURLANG.netherlands,
            img: '<img title="netherlands" alt="netherlands" src="img/emoticons/Flags/netherlands.png" class="sm">',
            bbcode: "[emoticon option='png']netherlands[/emoticon]"
        },
        {
            title: CURLANG.new_zealand,
            img: '<img title="new_zealand" alt="new_zealand" src="img/emoticons/Flags/new_zealand.png" class="sm">',
            bbcode: "[emoticon option='png']new_zealand[/emoticon]"
        },
        {
            title: CURLANG.nigeria,
            img: '<img title="nigeria" alt="nigeria" src="img/emoticons/Flags/nigeria.png" class="sm">',
            bbcode: "[emoticon option='png']nigeria[/emoticon]"
        },
        {
            title: CURLANG.north_korea,
            img: '<img title="north_korea" alt="north_korea" src="img/emoticons/Flags/north_korea.png" class="sm">',
            bbcode: "[emoticon option='png']north_korea[/emoticon]"
        },
        {
            title: CURLANG.northern_ireland,
            img: '<img title="northern_ireland" alt="northern_ireland" src="img/emoticons/Flags/northern_ireland.png" class="sm">',
            bbcode: "[emoticon option='png']northern_ireland[/emoticon]"
        },
        {
            title: CURLANG.paraguay,
            img: '<img title="paraguay" alt="paraguay" src="img/emoticons/Flags/paraguay.png" class="sm">',
            bbcode: "[emoticon option='png']paraguay[/emoticon]"
        },
        {
            title: CURLANG.poland,
            img: '<img title="poland" alt="poland" src="img/emoticons/Flags/poland.png" class="sm">',
            bbcode: "[emoticon option='png']poland[/emoticon]"
        },
        {
            title: CURLANG.portugal,
            img: '<img title="portugal" alt="portugal" src="img/emoticons/Flags/portugal.png" class="sm">',
            bbcode: "[emoticon option='png']portugal[/emoticon]"
        },
        {
            title: CURLANG.russia,
            img: '<img title="russia" alt="russia" src="img/emoticons/Flags/russia.png" class="sm">',
            bbcode: "[emoticon option='png']russia[/emoticon]"
        },
        {
            title: CURLANG.scotland,
            img: '<img title="scotland" alt="scotland" src="img/emoticons/Flags/scotland.png" class="sm">',
            bbcode: "[emoticon option='png']scotland[/emoticon]"
        },
        {
            title: CURLANG.serbia,
            img: '<img title="serbia" alt="serbia" src="img/emoticons/Flags/serbia.png" class="sm">',
            bbcode: "[emoticon option='png']serbia[/emoticon]"
        },
        {
            title: CURLANG.slovakia,
            img: '<img title="slovakia" alt="slovakia" src="img/emoticons/Flags/slovakia.png" class="sm">',
            bbcode: "[emoticon option='png']slovakia[/emoticon]"
        },
        {
            title: CURLANG.slovenia,
            img: '<img title="slovenia" alt="slovenia" src="img/emoticons/Flags/slovenia.png" class="sm">',
            bbcode: "[emoticon option='png']slovenia[/emoticon]"
        },
        {
            title: CURLANG.south_africa,
            img: '<img title="south_africa" alt="south_africa" src="img/emoticons/Flags/south_africa.png" class="sm">',
            bbcode: "[emoticon option='png']south_africa[/emoticon]"
        },
        {
            title: CURLANG.south_korea,
            img: '<img title="south_korea" alt="south_korea" src="img/emoticons/Flags/south_korea.png" class="sm">',
            bbcode: "[emoticon option='png']south_korea[/emoticon]"
        },
        {
            title: CURLANG.spain,
            img: '<img title="spain" alt="spain" src="img/emoticons/Flags/spain.png" class="sm">',
            bbcode: "[emoticon option='png']spain[/emoticon]"
        },
        {
            title: CURLANG.sweden,
            img: '<img title="sweden" alt="sweden" src="img/emoticons/Flags/sweden.png" class="sm">',
            bbcode: "[emoticon option='png']sweden[/emoticon]"
        },
        {
            title: CURLANG.switzerland,
            img: '<img title="switzerland" alt="switzerland" src="img/emoticons/Flags/switzerland.png" class="sm">',
            bbcode: "[emoticon option='png']switzerland[/emoticon]"
        },
        {
            title: CURLANG.uk,
            img: '<img title="uk" alt="uk" src="img/emoticons/Flags/uk.png" class="sm">',
            bbcode: "[emoticon option='png']uk[/emoticon]"
        },
        {
            title: CURLANG.ukraine,
            img: '<img title="ukraine" alt="ukraine" src="img/emoticons/Flags/ukraine.png" class="sm">',
            bbcode: "[emoticon option='png']ukraine[/emoticon]"
        },
        {
            title: CURLANG.united_states_of_america,
            img: '<img title="united_states_of_america" alt="united_states_of_america" src="img/emoticons/Flags/united_states_of_america.png" class="sm">',
            bbcode: "[emoticon option='png']united_states_of_america[/emoticon]"
        },
        {
            title: CURLANG.uruguay,
            img: '<img title="uruguay" alt="uruguay" src="img/emoticons/Flags/uruguay.png" class="sm">',
            bbcode: "[emoticon option='png']uruguay[/emoticon]"
        },
        {
            title: CURLANG.wales,
            img: '<img title="wales" alt="wales" src="img/emoticons/Flags/wales.png" class="sm">',
            bbcode: "[emoticon option='png']wales[/emoticon]"
        },
        {
            title: CURLANG.balls,
            img: '<img title="balls" alt="balls" src="img/emoticons/Items/balls.png" class="sm">',
            bbcode: "[emoticon option='png']balls[/emoticon]"
        },
        {
            title: CURLANG.bin,
            img: '<img title="bin" alt="bin" src="img/emoticons/Items/bin.png" class="sm">',
            bbcode: "[emoticon option='png']bin[/emoticon]"
        },
        {
            title: CURLANG.black_boots,
            img: '<img title="black_boots" alt="black_boots" src="img/emoticons/Items/black_boots.png" class="sm">',
            bbcode: "[emoticon option='png']black_boots[/emoticon]"
        },
        {
            title: CURLANG.cocktail,
            img: '<img title="cocktail" alt="cocktail" src="img/emoticons/Items/cocktail.png" class="sm">',
            bbcode: "[emoticon option='png']cocktail[/emoticon]"
        },
        {
            title: CURLANG.coffee,
            img: '<img title="coffee" alt="coffee" src="img/emoticons/Items/coffee.png" class="sm">',
            bbcode: "[emoticon option='png']coffee[/emoticon]"
        },
        {
            title: CURLANG.crap,
            img: '<img title="crap" alt="crap" src="img/emoticons/Items/crap.png" class="sm">',
            bbcode: "[emoticon option='png']crap[/emoticon]"
        },
        {
            title: CURLANG.factory,
            img: '<img title="factory" alt="factory" src="img/emoticons/Items/factory.png" class="sm">',
            bbcode: "[emoticon option='png']factory[/emoticon]"
        },
        {
            title: CURLANG.guinness,
            img: '<img title="guinness" alt="guinness" src="img/emoticons/Items/guinness.png" class="sm">',
            bbcode: "[emoticon option='png']guinness[/emoticon]"
        },
        {
            title: CURLANG.handbag,
            img: '<img title="handbag" alt="handbag" src="img/emoticons/Items/handbag.png" class="sm">',
            bbcode: "[emoticon option='png']handbag[/emoticon]"
        },
        {
            title: CURLANG.hendos,
            img: '<img title="hendos" alt="hendos" src="img/emoticons/Items/hendos.png" class="sm">',
            bbcode: "[emoticon option='png']hendos[/emoticon]"
        },
        {
            title: CURLANG.hobnobs,
            img: '<img title="hobnobs" alt="hobnobs" src="img/emoticons/Items/hobnobs.png" class="sm">',
            bbcode: "[emoticon option='png']hobnobs[/emoticon]"
        },
        {
            title: CURLANG.house,
            img: '<img title="house" alt="house" src="img/emoticons/Items/house.png" class="sm">',
            bbcode: "[emoticon option='png']house[/emoticon]"
        },
        {
            title: CURLANG.meter,
            img: '<img title="meter" alt="meter" src="img/emoticons/Items/meter.png" class="sm">',
            bbcode: "[emoticon option='png']meter[/emoticon]"
        },
        {
            title: CURLANG.office,
            img: '<img title="office" alt="office" src="img/emoticons/Items/office.png" class="sm">',
            bbcode: "[emoticon option='png']office[/emoticon]"
        },
        {
            title: CURLANG.old_spice,
            img: '<img title="old_spice" alt="old_spice" src="img/emoticons/Items/old_spice.png" class="sm">',
            bbcode: "[emoticon option='png']old_spice[/emoticon]"
        },
        {
            title: CURLANG.pineapple,
            img: '<img title="pineapple" alt="pineapple" src="img/emoticons/Items/pineapple.png" class="sm">',
            bbcode: "[emoticon option='png']pineapple[/emoticon]"
        },
        {
            title: CURLANG.pink_boot,
            img: '<img title="pink_boot" alt="pink_boot" src="img/emoticons/Items/pink_boot.png" class="sm">',
            bbcode: "[emoticon option='png']pink_boot[/emoticon]"
        },
        {
            title: CURLANG.pint,
            img: '<img title="pint" alt="pint" src="img/emoticons/Items/pint.png" class="sm">',
            bbcode: "[emoticon option='png']pint[/emoticon]"
        },
        {
            title: CURLANG.poppy,
            img: '<img title="poppy" alt="poppy" src="img/emoticons/Items/poppy.png" class="sm">',
            bbcode: "[emoticon option='png']poppy[/emoticon]"
        },
        {
            title: CURLANG.razor,
            img: '<img title="razor" alt="razor" src="img/emoticons/Items/razor.png" class="sm">',
            bbcode: "[emoticon option='png']razor[/emoticon]"
        },
        {
            title: CURLANG.talc,
            img: '<img title="talc" alt="talc" src="img/emoticons/Items/talc.png" class="sm">',
            bbcode: "[emoticon option='png']talc[/emoticon]"
        },
        {
            title: CURLANG.white_boots,
            img: '<img title="white_boots" alt="white_boots" src="img/emoticons/Items/white_boots.png" class="sm">',
            bbcode: "[emoticon option='png']white_boots[/emoticon]"
        },
        {
            title: CURLANG.wine,
            img: '<img title="wine" alt="wine" src="img/emoticons/Items/wine.png" class="sm">',
            bbcode: "[emoticon option='png']wine[/emoticon]"
        },
        {
            title: CURLANG.cft,
            img: '<img title="cft" alt="cft" src="img/emoticons/Other/cft.png" class="sm">',
            bbcode: "[emoticon option='png']cft[/emoticon]"
        },
        {
            title: CURLANG.duh,
            img: '<img title="duh" alt="duh" src="img/emoticons/Other/duh.png" class="sm">',
            bbcode: "[emoticon option='png']duh[/emoticon]"
        },
        {
            title: CURLANG.warning,
            img: '<img title="warning" alt="warning" src="img/emoticons/Other/warning.png" class="sm">',
            bbcode: "[emoticon option='png']warning[/emoticon]"
        },
        {
            title: CURLANG.accrington_stanley,
            img: '<img title="accrington_stanley" alt="accrington_stanley" src="img/emoticons/Shirts/accrington_stanley.png" class="sm">',
            bbcode: "[emoticon option='png']accrington_stanley[/emoticon]"
        },
        {
            title: CURLANG.aldershot_town,
            img: '<img title="aldershot_town" alt="aldershot_town" src="img/emoticons/Shirts/aldershot_town.png" class="sm">',
            bbcode: "[emoticon option='png']aldershot_town[/emoticon]"
        },
        {
            title: CURLANG.arsenal,
            img: '<img title="arsenal" alt="arsenal" src="img/emoticons/Shirts/arsenal.png" class="sm">',
            bbcode: "[emoticon option='png']arsenal[/emoticon]"
        },
        {
            title: CURLANG.aston_villa,
            img: '<img title="aston_villa" alt="aston_villa" src="img/emoticons/Shirts/aston_villa.png" class="sm">',
            bbcode: "[emoticon option='png']aston_villa[/emoticon]"
        },
        {
            title: CURLANG.barnet,
            img: '<img title="barnet" alt="barnet" src="img/emoticons/Shirts/barnet.png" class="sm">',
            bbcode: "[emoticon option='png']barnet[/emoticon]"
        },
        {
            title: CURLANG.barnsley,
            img: '<img title="barnsley" alt="barnsley" src="img/emoticons/Shirts/barnsley.png" class="sm">',
            bbcode: "[emoticon option='png']barnsley[/emoticon]"
        },
        {
            title: CURLANG.birmingham_city,
            img: '<img title="birmingham_city" alt="birmingham_city" src="img/emoticons/Shirts/birmingham_city.png" class="sm">',
            bbcode: "[emoticon option='png']birmingham_city[/emoticon]"
        },
        {
            title: CURLANG.blackburn_rovers,
            img: '<img title="blackburn_rovers" alt="blackburn_rovers" src="img/emoticons/Shirts/blackburn_rovers.png" class="sm">',
            bbcode: "[emoticon option='png']blackburn_rovers[/emoticon]"
        },
        {
            title: CURLANG.blackpool,
            img: '<img title="blackpool" alt="blackpool" src="img/emoticons/Shirts/blackpool.png" class="sm">',
            bbcode: "[emoticon option='png']blackpool[/emoticon]"
        },
        {
            title: CURLANG.bolton_wanderers,
            img: '<img title="bolton_wanderers" alt="bolton_wanderers" src="img/emoticons/Shirts/bolton_wanderers.png" class="sm">',
            bbcode: "[emoticon option='png']bolton_wanderers[/emoticon]"
        },
        {
            title: CURLANG.bournemouth,
            img: '<img title="bournemouth" alt="bournemouth" src="img/emoticons/Shirts/bournemouth.png" class="sm">',
            bbcode: "[emoticon option='png']bournemouth[/emoticon]"
        },
        {
            title: CURLANG.bradford_city,
            img: '<img title="bradford_city" alt="bradford_city" src="img/emoticons/Shirts/bradford_city.png" class="sm">',
            bbcode: "[emoticon option='png']bradford_city[/emoticon]"
        },
        {
            title: CURLANG.brentford,
            img: '<img title="brentford" alt="brentford" src="img/emoticons/Shirts/brentford.png" class="sm">',
            bbcode: "[emoticon option='png']brentford[/emoticon]"
        },
        {
            title: CURLANG.brighton_and_hove_albion,
            img: '<img title="brighton_and_hove_albion" alt="brighton_and_hove_albion" src="img/emoticons/Shirts/brighton_and_hove_albion.png" class="sm">',
            bbcode: "[emoticon option='png']brighton_and_hove_albion[/emoticon]"
        },
        {
            title: CURLANG.bristol_city,
            img: '<img title="bristol_city" alt="bristol_city" src="img/emoticons/Shirts/bristol_city.png" class="sm">',
            bbcode: "[emoticon option='png']bristol_city[/emoticon]"
        },
        {
            title: CURLANG.bristol_rovers,
            img: '<img title="bristol_rovers" alt="bristol_rovers" src="img/emoticons/Shirts/bristol_rovers.png" class="sm">',
            bbcode: "[emoticon option='png']bristol_rovers[/emoticon]"
        },
        {
            title: CURLANG.burnley,
            img: '<img title="burnley" alt="burnley" src="img/emoticons/Shirts/burnley.png" class="sm">',
            bbcode: "[emoticon option='png']burnley[/emoticon]"
        },
        {
            title: CURLANG.burton_albion,
            img: '<img title="burton_albion" alt="burton_albion" src="img/emoticons/Shirts/burton_albion.png" class="sm">',
            bbcode: "[emoticon option='png']burton_albion[/emoticon]"
        },
        {
            title: CURLANG.bury,
            img: '<img title="bury" alt="bury" src="img/emoticons/Shirts/bury.png" class="sm">',
            bbcode: "[emoticon option='png']bury[/emoticon]"
        },
        {
            title: CURLANG.cardiff_city,
            img: '<img title="cardiff_city" alt="cardiff_city" src="img/emoticons/Shirts/cardiff_city.png" class="sm">',
            bbcode: "[emoticon option='png']cardiff_city[/emoticon]"
        },
        {
            title: CURLANG.carlisle_united,
            img: '<img title="carlisle_united" alt="carlisle_united" src="img/emoticons/Shirts/carlisle_united.png" class="sm">',
            bbcode: "[emoticon option='png']carlisle_united[/emoticon]"
        },
        {
            title: CURLANG.charlton_athletic,
            img: '<img title="charlton_athletic" alt="charlton_athletic" src="img/emoticons/Shirts/charlton_athletic.png" class="sm">',
            bbcode: "[emoticon option='png']charlton_athletic[/emoticon]"
        },
        {
            title: CURLANG.chelsea,
            img: '<img title="chelsea" alt="chelsea" src="img/emoticons/Shirts/chelsea.png" class="sm">',
            bbcode: "[emoticon option='png']chelsea[/emoticon]"
        },
        {
            title: CURLANG.cheltenham_town,
            img: '<img title="cheltenham_town" alt="cheltenham_town" src="img/emoticons/Shirts/cheltenham_town.png" class="sm">',
            bbcode: "[emoticon option='png']cheltenham_town[/emoticon]"
        },
        {
            title: CURLANG.chesterfield,
            img: '<img title="chesterfield" alt="chesterfield" src="img/emoticons/Shirts/chesterfield.png" class="sm">',
            bbcode: "[emoticon option='png']chesterfield[/emoticon]"
        },
        {
            title: CURLANG.colchester_united,
            img: '<img title="colchester_united" alt="colchester_united" src="img/emoticons/Shirts/colchester_united.png" class="sm">',
            bbcode: "[emoticon option='png']colchester_united[/emoticon]"
        },
        {
            title: CURLANG.coventry_city,
            img: '<img title="coventry_city" alt="coventry_city" src="img/emoticons/Shirts/coventry_city.png" class="sm">',
            bbcode: "[emoticon option='png']coventry_city[/emoticon]"
        },
        {
            title: CURLANG.crawley_town,
            img: '<img title="crawley_town" alt="crawley_town" src="img/emoticons/Shirts/crawley_town.png" class="sm">',
            bbcode: "[emoticon option='png']crawley_town[/emoticon]"
        },
        {
            title: CURLANG.crewe_alexandra,
            img: '<img title="crewe_alexandra" alt="crewe_alexandra" src="img/emoticons/Shirts/crewe_alexandra.png" class="sm">',
            bbcode: "[emoticon option='png']crewe_alexandra[/emoticon]"
        },
        {
            title: CURLANG.crystal_palace,
            img: '<img title="crystal_palace" alt="crystal_palace" src="img/emoticons/Shirts/crystal_palace.png" class="sm">',
            bbcode: "[emoticon option='png']crystal_palace[/emoticon]"
        },
        {
            title: CURLANG.dagenham_and_redbridge,
            img: '<img title="dagenham_and_redbridge" alt="dagenham_and_redbridge" src="img/emoticons/Shirts/dagenham_and_redbridge.png" class="sm">',
            bbcode: "[emoticon option='png']dagenham_and_redbridge[/emoticon]"
        },
        {
            title: CURLANG.derby_county,
            img: '<img title="derby_county" alt="derby_county" src="img/emoticons/Shirts/derby_county.png" class="sm">',
            bbcode: "[emoticon option='png']derby_county[/emoticon]"
        },
        {
            title: CURLANG.doncaster_rovers,
            img: '<img title="doncaster_rovers" alt="doncaster_rovers" src="img/emoticons/Shirts/doncaster_rovers.png" class="sm">',
            bbcode: "[emoticon option='png']doncaster_rovers[/emoticon]"
        },
        {
            title: CURLANG.everton,
            img: '<img title="everton" alt="everton" src="img/emoticons/Shirts/everton.png" class="sm">',
            bbcode: "[emoticon option='png']everton[/emoticon]"
        },
        {
            title: CURLANG.exeter_city,
            img: '<img title="exeter_city" alt="exeter_city" src="img/emoticons/Shirts/exeter_city.png" class="sm">',
            bbcode: "[emoticon option='png']exeter_city[/emoticon]"
        },
        {
            title: CURLANG.fleetwood_town,
            img: '<img title="fleetwood_town" alt="fleetwood_town" src="img/emoticons/Shirts/fleetwood_town.png" class="sm">',
            bbcode: "[emoticon option='png']fleetwood_town[/emoticon]"
        },
        {
            title: CURLANG.fulham,
            img: '<img title="fulham" alt="fulham" src="img/emoticons/Shirts/fulham.png" class="sm">',
            bbcode: "[emoticon option='png']fulham[/emoticon]"
        },
        {
            title: CURLANG.gillingham,
            img: '<img title="gillingham" alt="gillingham" src="img/emoticons/Shirts/gillingham.png" class="sm">',
            bbcode: "[emoticon option='png']gillingham[/emoticon]"
        },
        {
            title: CURLANG.hartlepool_united,
            img: '<img title="hartlepool_united" alt="hartlepool_united" src="img/emoticons/Shirts/hartlepool_united.png" class="sm">',
            bbcode: "[emoticon option='png']hartlepool_united[/emoticon]"
        },
        {
            title: CURLANG.hereford_united,
            img: '<img title="hereford_united" alt="hereford_united" src="img/emoticons/Shirts/hereford_united.png" class="sm">',
            bbcode: "[emoticon option='png']hereford_united[/emoticon]"
        },
        {
            title: CURLANG.huddersfield_town,
            img: '<img title="huddersfield_town" alt="huddersfield_town" src="img/emoticons/Shirts/huddersfield_town.png" class="sm">',
            bbcode: "[emoticon option='png']huddersfield_town[/emoticon]"
        },
        {
            title: CURLANG.hull_city,
            img: '<img title="hull_city" alt="hull_city" src="img/emoticons/Shirts/hull_city.png" class="sm">',
            bbcode: "[emoticon option='png']hull_city[/emoticon]"
        },
        {
            title: CURLANG.ipswich_town,
            img: '<img title="ipswich_town" alt="ipswich_town" src="img/emoticons/Shirts/ipswich_town.png" class="sm">',
            bbcode: "[emoticon option='png']ipswich_town[/emoticon]"
        },
        {
            title: CURLANG.leeds_united,
            img: '<img title="leeds_united" alt="leeds_united" src="img/emoticons/Shirts/leeds_united.png" class="sm">',
            bbcode: "[emoticon option='png']leeds_united[/emoticon]"
        },
        {
            title: CURLANG.leicester_city,
            img: '<img title="leicester_city" alt="leicester_city" src="img/emoticons/Shirts/leicester_city.png" class="sm">',
            bbcode: "[emoticon option='png']leicester_city[/emoticon]"
        },
        {
            title: CURLANG.leyton_orient,
            img: '<img title="leyton_orient" alt="leyton_orient" src="img/emoticons/Shirts/leyton_orient.png" class="sm">',
            bbcode: "[emoticon option='png']leyton_orient[/emoticon]"
        },
        {
            title: CURLANG.liverpool,
            img: '<img title="liverpool" alt="liverpool" src="img/emoticons/Shirts/liverpool.png" class="sm">',
            bbcode: "[emoticon option='png']liverpool[/emoticon]"
        },
        {
            title: CURLANG.macclesfield_town,
            img: '<img title="macclesfield_town" alt="macclesfield_town" src="img/emoticons/Shirts/macclesfield_town.png" class="sm">',
            bbcode: "[emoticon option='png']macclesfield_town[/emoticon]"
        },
        {
            title: CURLANG.manchester_city,
            img: '<img title="manchester_city" alt="manchester_city" src="img/emoticons/Shirts/manchester_city.png" class="sm">',
            bbcode: "[emoticon option='png']manchester_city[/emoticon]"
        },
        {
            title: CURLANG.manchester_united,
            img: '<img title="manchester_united" alt="manchester_united" src="img/emoticons/Shirts/manchester_united.png" class="sm">',
            bbcode: "[emoticon option='png']manchester_united[/emoticon]"
        },
        {
            title: CURLANG.middlesbrough,
            img: '<img title="middlesbrough" alt="middlesbrough" src="img/emoticons/Shirts/middlesbrough.png" class="sm">',
            bbcode: "[emoticon option='png']middlesbrough[/emoticon]"
        },
        {
            title: CURLANG.millwall,
            img: '<img title="millwall" alt="millwall" src="img/emoticons/Shirts/millwall.png" class="sm">',
            bbcode: "[emoticon option='png']millwall[/emoticon]"
        },
        {
            title: CURLANG.milton_keynes_dons,
            img: '<img title="milton_keynes_dons" alt="milton_keynes_dons" src="img/emoticons/Shirts/milton_keynes_dons.png" class="sm">',
            bbcode: "[emoticon option='png']milton_keynes_dons[/emoticon]"
        },
        {
            title: CURLANG.morecambe,
            img: '<img title="morecambe" alt="morecambe" src="img/emoticons/Shirts/morecambe.png" class="sm">',
            bbcode: "[emoticon option='png']morecambe[/emoticon]"
        },
        {
            title: CURLANG.newcastle_united,
            img: '<img title="newcastle_united" alt="newcastle_united" src="img/emoticons/Shirts/newcastle_united.png" class="sm">',
            bbcode: "[emoticon option='png']newcastle_united[/emoticon]"
        },
        {
            title: CURLANG.northampton_town,
            img: '<img title="northampton_town" alt="northampton_town" src="img/emoticons/Shirts/northampton_town.png" class="sm">',
            bbcode: "[emoticon option='png']northampton_town[/emoticon]"
        },
        {
            title: CURLANG.norwich_city,
            img: '<img title="norwich_city" alt="norwich_city" src="img/emoticons/Shirts/norwich_city.png" class="sm">',
            bbcode: "[emoticon option='png']norwich_city[/emoticon]"
        },
        {
            title: CURLANG.nottingham_forest,
            img: '<img title="nottingham_forest" alt="nottingham_forest" src="img/emoticons/Shirts/nottingham_forest.png" class="sm">',
            bbcode: "[emoticon option='png']nottingham_forest[/emoticon]"
        },
        {
            title: CURLANG.notts_county,
            img: '<img title="notts_county" alt="notts_county" src="img/emoticons/Shirts/notts_county.png" class="sm">',
            bbcode: "[emoticon option='png']notts_county[/emoticon]"
        },
        {
            title: CURLANG.oldham_athletic,
            img: '<img title="oldham_athletic" alt="oldham_athletic" src="img/emoticons/Shirts/oldham_athletic.png" class="sm">',
            bbcode: "[emoticon option='png']oldham_athletic[/emoticon]"
        },
        {
            title: CURLANG.oxford_united,
            img: '<img title="oxford_united" alt="oxford_united" src="img/emoticons/Shirts/oxford_united.png" class="sm">',
            bbcode: "[emoticon option='png']oxford_united[/emoticon]"
        },
        {
            title: CURLANG.peterborough_united,
            img: '<img title="peterborough_united" alt="peterborough_united" src="img/emoticons/Shirts/peterborough_united.png" class="sm">',
            bbcode: "[emoticon option='png']peterborough_united[/emoticon]"
        },
        {
            title: CURLANG.plymouth_argyle,
            img: '<img title="plymouth_argyle" alt="plymouth_argyle" src="img/emoticons/Shirts/plymouth_argyle.png" class="sm">',
            bbcode: "[emoticon option='png']plymouth_argyle[/emoticon]"
        },
        {
            title: CURLANG.port_vale,
            img: '<img title="port_vale" alt="port_vale" src="img/emoticons/Shirts/port_vale.png" class="sm">',
            bbcode: "[emoticon option='png']port_vale[/emoticon]"
        },
        {
            title: CURLANG.portsmouth,
            img: '<img title="portsmouth" alt="portsmouth" src="img/emoticons/Shirts/portsmouth.png" class="sm">',
            bbcode: "[emoticon option='png']portsmouth[/emoticon]"
        },
        {
            title: CURLANG.preston_north_end,
            img: '<img title="preston_north_end" alt="preston_north_end" src="img/emoticons/Shirts/preston_north_end.png" class="sm">',
            bbcode: "[emoticon option='png']preston_north_end[/emoticon]"
        },
        {
            title: CURLANG.queens_park_rangers,
            img: '<img title="queens_park_rangers" alt="queens_park_rangers" src="img/emoticons/Shirts/queens_park_rangers.png" class="sm">',
            bbcode: "[emoticon option='png']queens_park_rangers[/emoticon]"
        },
        {
            title: CURLANG.reading,
            img: '<img title="reading" alt="reading" src="img/emoticons/Shirts/reading.png" class="sm">',
            bbcode: "[emoticon option='png']reading[/emoticon]"
        },
        {
            title: CURLANG.rochdale,
            img: '<img title="rochdale" alt="rochdale" src="img/emoticons/Shirts/rochdale.png" class="sm">',
            bbcode: "[emoticon option='png']rochdale[/emoticon]"
        },
        {
            title: CURLANG.rotherham_united,
            img: '<img title="rotherham_united" alt="rotherham_united" src="img/emoticons/Shirts/rotherham_united.png" class="sm">',
            bbcode: "[emoticon option='png']rotherham_united[/emoticon]"
        },
        {
            title: CURLANG.scunthorpe_united,
            img: '<img title="scunthorpe_united" alt="scunthorpe_united" src="img/emoticons/Shirts/scunthorpe_united.png" class="sm">',
            bbcode: "[emoticon option='png']scunthorpe_united[/emoticon]"
        },
        {
            title: CURLANG.sheffield_united,
            img: '<img title="sheffield_united" alt="sheffield_united" src="img/emoticons/Shirts/sheffield_united.png" class="sm">',
            bbcode: "[emoticon option='png']sheffield_united[/emoticon]"
        },
        {
            title: CURLANG.sheffield_wednesday,
            img: '<img title="sheffield_wednesday" alt="sheffield_wednesday" src="img/emoticons/Shirts/sheffield_wednesday.png" class="sm">',
            bbcode: "[emoticon option='png']sheffield_wednesday[/emoticon]"
        },
        {
            title: CURLANG.shrewsbury_town,
            img: '<img title="shrewsbury_town" alt="shrewsbury_town" src="img/emoticons/Shirts/shrewsbury_town.png" class="sm">',
            bbcode: "[emoticon option='png']shrewsbury_town[/emoticon]"
        },
        {
            title: CURLANG.southampton,
            img: '<img title="southampton" alt="southampton" src="img/emoticons/Shirts/southampton.png" class="sm">',
            bbcode: "[emoticon option='png']southampton[/emoticon]"
        },
        {
            title: CURLANG.southend_united,
            img: '<img title="southend_united" alt="southend_united" src="img/emoticons/Shirts/southend_united.png" class="sm">',
            bbcode: "[emoticon option='png']southend_united[/emoticon]"
        },
        {
            title: CURLANG.stevenage,
            img: '<img title="stevenage" alt="stevenage" src="img/emoticons/Shirts/stevenage.png" class="sm">',
            bbcode: "[emoticon option='png']stevenage[/emoticon]"
        },
        {
            title: CURLANG.stoke_city,
            img: '<img title="stoke_city" alt="stoke_city" src="img/emoticons/Shirts/stoke_city.png" class="sm">',
            bbcode: "[emoticon option='png']stoke_city[/emoticon]"
        },
        {
            title: CURLANG.sunderland,
            img: '<img title="sunderland" alt="sunderland" src="img/emoticons/Shirts/sunderland.png" class="sm">',
            bbcode: "[emoticon option='png']sunderland[/emoticon]"
        },
        {
            title: CURLANG.swansea_city,
            img: '<img title="swansea_city" alt="swansea_city" src="img/emoticons/Shirts/swansea_city.png" class="sm">',
            bbcode: "[emoticon option='png']swansea_city[/emoticon]"
        },
        {
            title: CURLANG.swindon_town,
            img: '<img title="swindon_town" alt="swindon_town" src="img/emoticons/Shirts/swindon_town.png" class="sm">',
            bbcode: "[emoticon option='png']swindon_town[/emoticon]"
        },
        {
            title: CURLANG.torquay_united,
            img: '<img title="torquay_united" alt="torquay_united" src="img/emoticons/Shirts/torquay_united.png" class="sm">',
            bbcode: "[emoticon option='png']torquay_united[/emoticon]"
        },
        {
            title: CURLANG.tottenham_hotspur,
            img: '<img title="tottenham_hotspur" alt="tottenham_hotspur" src="img/emoticons/Shirts/tottenham_hotspur.png" class="sm">',
            bbcode: "[emoticon option='png']tottenham_hotspur[/emoticon]"
        },
        {
            title: CURLANG.tranmere_rovers,
            img: '<img title="tranmere_rovers" alt="tranmere_rovers" src="img/emoticons/Shirts/tranmere_rovers.png" class="sm">',
            bbcode: "[emoticon option='png']tranmere_rovers[/emoticon]"
        },
        {
            title: CURLANG.walsall,
            img: '<img title="walsall" alt="walsall" src="img/emoticons/Shirts/walsall.png" class="sm">',
            bbcode: "[emoticon option='png']walsall[/emoticon]"
        },
        {
            title: CURLANG.watford,
            img: '<img title="watford" alt="watford" src="img/emoticons/Shirts/watford.png" class="sm">',
            bbcode: "[emoticon option='png']watford[/emoticon]"
        },
        {
            title: CURLANG.west_bromwich_albion,
            img: '<img title="west_bromwich_albion" alt="west_bromwich_albion" src="img/emoticons/Shirts/west_bromwich_albion.png" class="sm">',
            bbcode: "[emoticon option='png']west_bromwich_albion[/emoticon]"
        },
        {
            title: CURLANG.west_ham_united,
            img: '<img title="west_ham_united" alt="west_ham_united" src="img/emoticons/Shirts/west_ham_united.png" class="sm">',
            bbcode: "[emoticon option='png']west_ham_united[/emoticon]"
        },
        {
            title: CURLANG.wigan_athletic,
            img: '<img title="wigan_athletic" alt="wigan_athletic" src="img/emoticons/Shirts/wigan_athletic.png" class="sm">',
            bbcode: "[emoticon option='png']wigan_athletic[/emoticon]"
        },
        {
            title: CURLANG.wimbledon,
            img: '<img title="wimbledon" alt="wimbledon" src="img/emoticons/Shirts/wimbledon.png" class="sm">',
            bbcode: "[emoticon option='png']wimbledon[/emoticon]"
        },
        {
            title: CURLANG.wolverhampton_wanderers,
            img: '<img title="wolverhampton_wanderers" alt="wolverhampton_wanderers" src="img/emoticons/Shirts/wolverhampton_wanderers.png" class="sm">',
            bbcode: "[emoticon option='png']wolverhampton_wanderers[/emoticon]"
        },
        {
            title: CURLANG.wycombe_wanderers,
            img: '<img title="wycombe_wanderers" alt="wycombe_wanderers" src="img/emoticons/Shirts/wycombe_wanderers.png" class="sm">',
            bbcode: "[emoticon option='png']wycombe_wanderers[/emoticon]"
        },
        {
            title: CURLANG.yeovil_town,
            img: '<img title="yeovil_town" alt="yeovil_town" src="img/emoticons/Shirts/yeovil_town.png" class="sm">',
            bbcode: "[emoticon option='png']yeovil_town[/emoticon]"
        },
        {
            title: CURLANG.york_city,
            img: '<img title="york_city" alt="york_city" src="img/emoticons/Shirts/york_city.png" class="sm">',
            bbcode: "[emoticon option='png']york_city[/emoticon]"
        }
    ]
}

$(document).ready(function(){
    $("#editor").wysibb(wbbOpt);
});
     
        
 
