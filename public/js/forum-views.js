/**
 * Created by Richard Sinclair on 07/11/2016.
 */

$(document).ready(function () {

    $("#view-forums").on('click', function () { //Set page size cookie
        $.cookie("view", "forums", {expire: 1, path: '/'})
    });

    $("#view-topics").on('click', function () { //Set page size cookie
        $.cookie("view", "topics", {expire: 1, path: '/'})
    });

});