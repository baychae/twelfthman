<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

ini_set('memory_limit', '2048M');

define("SCRIPT_DIR", realpath('scripts') . '/');

include_once SCRIPT_DIR . "../../vendor/autoload.php";

include "./db_config.php";

////////////////////
/* CLEAR DOWN MONGO
 *
 */
echo "DELETING mongodb->topic_rating_stats ... ";
$msg = $mongodb->topic_rating_stats->drop();

if ($msg->ok >= 0) {
    echo "Ok" . PHP_EOL;
} else {
    echo "Failed";
    exit(1);
}


////////////////////
/* CLEAR DOWN MONGO
 *
 */
echo "DELETING mongodb->topic_view_counter_stats ... ";
$msg = $mongodb->topic_view_counter_stats->drop();

if ($msg->ok >= 0) {
    echo "Ok" . PHP_EOL;
} else {
    echo "Failed";
    exit(1);
}

////////////////////
/* CLEAR DOWN MONGO
 *
 */
echo "DELETING mongodb->forum_topic_post_stats ... ";
$msg = $mongodb->forum_topic_post_stats->drop();

if ($msg->ok >= 0) {
    echo "Ok" . PHP_EOL;
} else {
    echo "Failed";
    exit(1);
}

////////////////////
/* CLEAR DOWN SITE USER MESSAGES
 *
 */
echo "DELETING mongodb->site_user_message ... ";
$msg = $mongodb->site_user_message->drop();

if ($msg->ok >= 0) {
    echo "Ok" . PHP_EOL;
} else {
    echo "Failed";
    exit(1);
}

////////////////////
/* INSTALL STORED PROCEDURES in SOURCE DATABASE
 *
 */
echo "INSTALL STORED PROCEDURES in SOURCE DATABASE...\n";

include SCRIPT_DIR . "CREATE_DUMP_USER_IMAGES.php";

include SCRIPT_DIR . "CREATE_DUMP_USER_TEAM_IMAGES.php";

include SCRIPT_DIR . "CREATE_DUMP_NEWS_IMAGES.php";

include SCRIPT_DIR . "CREATE_DUMP_USER_TEAM_IMAGES.php";

include SCRIPT_DIR . "CREATE_DUMP_ACHIEVEMENT_TYPES.php";

////////////////////
/* PREPARE TARGET DATABASE
 * 
 */
echo "CLEARDOWN ALL SQL DATA\n";

include SCRIPT_DIR . "cleardowndb.php";


////////////////////
/* RESET DATABASE AUTOINCREMENT
 *
 */

$reset_auto_increment = "ALTER TABLE forum AUTO_INCREMENT = 1; ";
$reset_auto_increment .= "ALTER TABLE forum_topic AUTO_INCREMENT = 1; ";
$reset_auto_increment .= "ALTER TABLE forum_topic_post AUTO_INCREMENT = 1; ";
$reset_auto_increment .= "ALTER TABLE forum_topic_posts_flagged AUTO_INCREMENT = 1; ";
$reset_auto_increment .= "ALTER TABLE forum_type AUTO_INCREMENT = 1; ";
$reset_auto_increment .= "ALTER TABLE news_group AUTO_INCREMENT = 1; ";
$reset_auto_increment .= "ALTER TABLE news_article AUTO_INCREMENT = 1; ";
$reset_auto_increment .= "ALTER TABLE site_user AUTO_INCREMENT = 1; ";
$reset_auto_increment .= "ALTER TABLE `match` AUTO_INCREMENT = 1; ";
$reset_auto_increment .= "ALTER TABLE team AUTO_INCREMENT = 1; ";
$reset_auto_increment .= "ALTER TABLE league AUTO_INCREMENT = 1; ";
$reset_auto_increment .= "ALTER TABLE site_user_achievement_type AUTO_INCREMENT = 1; ";

echo "\nRESETTING AUTO INCREMENT... \n";
//$result = $conn_tawny->multi_query($reset_auto_increment);

/* execute multi query */
if ($conn_twelfthman->multi_query($reset_auto_increment)) {
    do {
        /* store first result set */
        if ($result = $conn_twelfthman->store_result()) {
            while ($row = $result->fetch_row()) {
                echo $row[0];
            }
            $result->free();
        }
        /* print divider */
        if ($conn_twelfthman->more_results()) {
            echo "-----------------\n";
        }
    } while (mysqli_more_results($conn_twelfthman) && mysqli_next_result($conn_twelfthman));
}

echo "Switch of strict" . PHP_EOL;

$non_strict_date_modes = "ONLY_FULL_GROUP_BY,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION";
$back_to_strict_modes = "ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION";

$set_global_dtrict_off = "SET GLOBAL sql_mode = '" . $non_strict_date_modes . "'";
$set_session_strict_off = "SET SESSION sql_mode = '" . $non_strict_date_modes . "'";

echo "switch strict dates off global" . PHP_EOL;
$result = $conn_tawny->real_query($set_global_dtrict_off);
if ($result) {
    echo "OK\n";
} else {
    echo "FAILED ... Migration halted at inserting matches..." . $conn_tawny->error . PHP_EOL;
    exit();
}

echo "switch strict dates off session" . PHP_EOL;
$result = $conn_tawny->real_query($set_session_strict_off);
if ($result) {
    echo "OK\n";
} else {
    echo "FAILED ... Migration halted at inserting matches..." . $conn_tawny->error . PHP_EOL;
    exit();
}

////////////////////
/* MIGRATE SITE USER ACHIEVEMENT TYPES
 *
 */

echo "\nMigrate Site User Achievement Types.. to ...." . TARGET_DB . "\n";

$result = $conn_tawny->real_query("call dump_user_achievement_images();");

if ($result) {
    echo "OK\n";
} else {
    echo "FAILED ... dump_user_achievement_images..." . $conn_tawny->error . PHP_EOL;
    exit();
}

////////////////////
/* REMOVE DUPLICATES USERS
 * 
 */

include SCRIPT_DIR . "remove_duplicate_users.php";

////////////////////
/* Migrate site users
 * 
 */

echo "\nMigrate Site Users.. to ...." . TARGET_DB . PHP_EOL;

$users_migrate_sql = include SCRIPT_DIR . "INSERT_SITE_USERS.php";
$conn_tawny->real_query($users_migrate_sql);
if (!$conn_tawny->error) {
    echo "OK\n";
} else {
    echo "FAILED ... Migration halted at site users..." . $conn_tawny->error . "\n";
    exit();
}

////////////////////
/* Migrate site user friends
 *
 */

echo "\nMigrate Site User friends.. to ...." . TARGET_DB . PHP_EOL;

$users_friends_migrate_sql = include SCRIPT_DIR . "MIGRATE_SITE_USER_FRIENDS.php";
$conn_tawny->real_query($users_friends_migrate_sql);
if (!$conn_tawny->error) {
    echo "OK\n";
} else {
    echo "FAILED ... Migration halted at site users..." . $conn_tawny->error . "\n";
    exit();
}



////////////////////
/* Migrate site users achievements
 *
 */

echo "Migrate Site Users Achievements .. to ...." . TARGET_DB . PHP_EOL;

$user_achievements_migrate_sql = include SCRIPT_DIR . "INSERT_SITE_USER_ACHIEVEMENTS.php";
$conn_tawny->real_query($user_achievements_migrate_sql);

if (!$conn_tawny->error) {
    echo "OK\n";
} else {
    echo PHP_EOL . "FAILED to Migrate Forums... Migration halted..." . $conn_tawny->error . PHP_EOL;
    exit();
}



////////////////////
/* Migrate site users avatars
 *
 */

echo "Migrate Site User Avatars.. ";

$result = $conn_tawny->real_query("call dump_user_images();");

if (!$conn_tawny->error) {
    echo "OK\n";
} else {
    echo "FAILED ... Migration halted at moving avatars..." . $conn_tawny->error . "\n";
    exit();
}

////////////////////
/* Migrate site signatures
 *
 */

echo "Migrate Site User Signatures.. " . PHP_EOL;

$site_user_signiture = "node " . SCRIPT_DIR . "site_user_signature_bb_to_html.js";
echo shell_exec($site_user_signiture);

////////////////////
/* Migrate Leagues
*
*/

echo "Migrating Leagues... ";

$migrate_leagues = include SCRIPT_DIR . "INSERT_LEAGUES.php";
$result = $conn_tawny->real_query($migrate_leagues);

if (!$conn_tawny->error) {
    echo "OK\n";
} else {
    echo "FAILED ... Migration halted..." . $conn_tawny->error . PHP_EOL;
    exit();
}

//Post insert processing of leagues -- Convert link to pretty url

echo "Updating Leagues with link... " . PHP_EOL;

$get_leagues = "SELECT * FROM league WHERE id > 0";
$result = $conn_twelfthman->query($get_leagues, MYSQLI_STORE_RESULT);

if (!$conn_tawny->error) {

    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {

        $link = preg_replace('/[\s-]+/', '-', strtolower($row["link"]));

        $update_link_sql = "UPDATE " . TARGET_DB . ".league set link = '" . $link . "' WHERE id = " . $row["id"];

        if ($conn_twelfthman->query($update_link_sql) === TRUE) {
            echo "Successfully created link for " . $row["name"] . ": " . $link . PHP_EOL;
        } else {
            echo "Error updating record: " . $conn_twelfthman->error;
        }

    }


} else {
    echo "FAILED: " . $conn_twelfthman->error . "\n";
    exit();
}

////////////////////
/* Migrate site user team groups and images
 *
 */

echo "Migrate Site User Team Groups and Images..DUMP IMAGES ";


$result = $conn_tawny->real_query("call dump_user_team_images();");

if (!$conn_tawny->error) {
    echo "OK\n";
} else {
    echo "FAILED ... Site User Team Groups and Images..." . $conn_tawny->error . PHP_EOL;
    exit();
}

////////////////////
/* Migrate site user team groups and images
 *
 */

echo "UPDATE Team Groups.. ";

$users_team_update = include SCRIPT_DIR . "UPDATE_CORE_USER_TEAMS.php";
$result = $conn_tawny->real_query($users_team_update);

if (!$conn_tawny->error) {
    echo "OK\n";
} else {
    echo "FAILED ... Migration halted at moving Team Flags..." . $conn_tawny->error . PHP_EOL;
    exit();
}

////////////////////
/* Migrate Site User Messages
*
*/

include SCRIPT_DIR . "migrate_site_user_messages.php";

///////////////////
/* Migrate Forum Types
 *
 */

echo "Migrate Forum Types";

$migrate_forum_types_sql = include SCRIPT_DIR . "FORUM_TYPES.php";

$conn_root->real_query($migrate_forum_types_sql);

if (!$conn_root->error) {
    echo "OK\n";
} else {
    echo "FAILED to Migrate Forum Types... Migration halted..." . $conn_root->error . PHP_EOL;
    echo "\n" . $migrate_forum_types_sql . "\n";
    exit();
}

////////////////////
/* Migrate Forums
 * 
 */

echo "Migrate Forums.. ";

$migrate_forums_sql = include SCRIPT_DIR . "FORUM.php";

$conn_root->real_query($migrate_forums_sql);

if (!$conn_root->error) {
    echo "OK\n";
} else {
    echo "FAILED to Migrate Forums... Migration halted..." . $conn_root->error . PHP_EOL;
    echo "\n" . $migrate_forums_sql . "\n";
    exit();
}

////////////////////
/* Migrate Forum Threads
 * 
 */
echo "Migrate Forum Thread... ";

$migrate_forums_sql = include SCRIPT_DIR . "FORUM_TOPICS.php";
$result = $conn_tawny->real_query($migrate_forums_sql);

if($result) {
    echo "OK\n";
} else {
    echo "FAILED ... Migration halted..." . $conn_tawny->error . "\n";
    exit();
}

////////////////////
/* Migrate Forum Thread Posts
 * 
 */
echo "Migrate Forum Thread Posts... ";

$migrate_forums_sql = include SCRIPT_DIR . "FORUM_TOPIC_POSTS.php";
$result = $conn_tawny->real_query($migrate_forums_sql);

if (!$conn_tawny->error) {
    echo "OK" . PHP_EOL;
} else {
    echo "FAILED ... Migration halted..." . $conn_tawny->error . PHP_EOL;
    exit();
}

////////////////////
/* Update Forum Thread Posts with new topic_ordinal
 *
 */
$get_forum_topic_post_sql = "SELECT * FROM forum_topic_post ORDER BY topic_id ASC, id ASC";

$result = $conn_twelfthman->query($get_forum_topic_post_sql, MYSQLI_STORE_RESULT);

if (!$conn_tawny->error) {

    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {                     //NEED 2G memory_limit php
        $rows[] = $row;
    }

    $ordinal = 1;

    echo "Updating topic ordinal." . PHP_EOL;

    $count = 0;

    foreach ($rows as $index => $row) {
        $update_link_sql = "UPDATE " . TARGET_DB . ".forum_topic_post " .
            "set topic_ordinal = '" . $ordinal . "' " .
            "WHERE id = " . $row["id"];

        if ($conn_twelfthman->query($update_link_sql) === true) {
            $count++;
        } else {
            echo "Error updating record: " . $conn_twelfthman->error;
            exit(1);
        }

        if ($rows[$index]["topic_id"] == $rows[$index + 1]["topic_id"]) {
            $ordinal++;
        } else {
            $ordinal = 1;
        }
    }

    echo "Updated " . $count . "topic replies with topic_ordinal" . PHP_EOL;

} else {
    echo "FAILED: " . $conn_twelfthman->error . "\n";
    exit();
}

////////////////////
/* Migrate Forum Thread Posts Stats
 *
 */
echo "Migrate Forum Thread Posts Stats... ";
$forum_topic_post_count_sql = include SCRIPT_DIR . "FETCH_FORUM_TOPIC_POST_COUNT.php";

$result = $conn_twelfthman->query($forum_topic_post_count_sql, MYSQLI_STORE_RESULT);

if ($result) {
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $mongodb->forum_topic_post_stats->insertOne([
            'forum_id' => intval($row['forum_id']),
            'topic_id' => intval($row['topic_id']),
            'post_id' => intval($row['post_id']),
            'post_count' => intval($row['post_count']),
            'created' => $row['created'],
            'modified' => $row['modified']
        ]);
    }
} else {
    echo "FAILED: " . $conn_twelfthman->error . "\n";
    exit();
}

////////////////////
/* Migrate News Images before articles
 *
 */
//

echo "Migrate news images before articles... ";
$result = $conn_tawny->real_query("CALL dump_news_images();");

if (!$conn_tawny->error) {
    echo "OK\n";
} else {
    echo "FAILED ... Migration halted...: " . $result->error . "\n";
    exit();
}


////////////////////
/* Migrate News Groups
 *
 */
echo "Migrate news groups... ";
$migrate_news_groups_sql = include(SCRIPT_DIR . "MIGRATE_NEWS_GROUPS.php");
$result = $conn_tawny->real_query($migrate_news_groups_sql);

if (!$conn_tawny->error) {
    echo "OK\n";
} else {
    echo "FAILED ... Migration halted... " . $conn_tawny->error . PHP_EOL;
    exit();
}

////////////////////
/* Migrate News  articles
 *
 */
echo "Migrate news articles... ";
$migrate_news_articles_sql = include(SCRIPT_DIR . "INSERT_NEWS_ARTICLES.php");
$result = $conn_tawny->real_query($migrate_news_articles_sql);

if (!$conn_tawny->error) {
    echo "OK\n";
} else {
    echo "FAILED ... Migration halted... " . $conn_tawny->error . PHP_EOL;
    exit();
}

echo "Convert News Titles htmlchars" . PHP_EOL;

$result = $conn_twelfthman->query("Select * from " . TARGET_DB . ".news_article ");

while ($row = $result->fetch_assoc()) {
    $new_title = htmlspecialchars_decode($row["title"], ENT_QUOTES);
    $conn_twelfthman->query("UPDATE news_article SET title = \"" . $new_title . "\" where id = " . intval($row["id"]));
}


echo "Convert mew article BB to html using twelfthman standards" . PHP_EOL;

////////////////////
/* Convert news article BB to html using twelfthmans method
 *
 */
$node_script_convert_news_article_bb = "node " . SCRIPT_DIR . "/news_article_bb_to_html.js";
echo shell_exec($node_script_convert_news_article_bb);

////////////////////
/* Convert embedded getty images into twelfthman acceptable image
 *
 */

include SCRIPT_DIR . "UPDATE_NEWS_ARTICLE_GETTY.php";


////////////////////
/* Migrate matches
 *
 */


echo ";Migrate Matches.. ";

$insert_matches = include SCRIPT_DIR . "INSERT_MATCHES.php";

$result = $conn_tawny->real_query($insert_matches);

if ($result) {
    echo "OK\n";
} else {
    echo "FAILED ... Migration halted at inserting matches..." . $conn_tawny->error . PHP_EOL;
    exit();
}

//Update matches to include upshot
echo "Post insert update upshot" . PHP_EOL;
$result = $conn_twelfthman->query("SELECT * FROM " . TARGET_DB . ".team WHERE name = 'Sheffield Wednesday'");

if ($result) {
    echo "OK\n";
} else {
    echo "FAILED ... Migration halted at inserting upshot..." . $conn_twelfthman->error . PHP_EOL;
    exit();
}

$row = $result->fetch_assoc();
$our_team_id = $row["id"];

$result = $conn_twelfthman->query("SELECT * FROM " . TARGET_DB . ".match WHERE id > 0 ");

while ($row = $result->fetch_assoc()) {
    $upshot = null;

    if ($row["home_team_id"] == $our_team_id) {// Playing at home
        $row["home_score"] > $row["away_score"] ? $upshot = 1 : $upshot = 0;
    } else {                                 // Playing away
        $row["away_score"] > $row["home_score"] ? $upshot = 1 : $upshot = 0;
    }

    $conn_twelfthman->query("UPDATE " . TARGET_DB . ".MATCH SET " . TARGET_DB . ".MATCH.upshot = " . $upshot . " WHERE " . TARGET_DB . ".MATCH.ID = " . $row["id"] . " ");
}

////////////////////
/* Migrate Forum Topics Ratings
*
*/
echo "Migrate forum topic ratings... ";
$migrate_topic_ratings_sql = include SCRIPT_DIR . "GET_thread_ratings.php";
$result = $conn_tawny->query($migrate_topic_ratings_sql);

while($row = $result->fetch_assoc()) {
    $myArray[] = $row;
}

if($result) {
    echo "OK\n";
} else {
    echo "FAILED ... Migration halted...\n";
    exit();
}

$my_json = json_encode($myArray);
$decoded_json = json_decode($my_json);

foreach($decoded_json as $row) {
    $row->forum_id = intval($row->forum_id);
    $row->topic_id = intval($row->topic_id);
    $row->users = explode(",", $row->users);
}

file_put_contents('./dump_mongo/topic_ratings_export.json', json_encode($decoded_json));
$import_topic_ratings_json_to_mongo = "mongoimport --db twelfthman --upsert --jsonArray --collection topic_rating_stats < ./dump_mongo/topic_ratings_export.json";
echo `$import_topic_ratings_json_to_mongo`;


////////////////////
/* Migrate Forum Topic View Counters
*
*/

echo "Migrate topic view counters... ";
$migrate_thread_views_sql = include SCRIPT_DIR . "thread_views.php";
$result = $conn_tawny->query($migrate_thread_views_sql);

while($row = $result->fetch_assoc()) {
    $thread_views_Array[] = $row;
}

$thread_views_encoded_json = json_encode($thread_views_Array);
$thread_views_decoded_json = json_decode($thread_views_encoded_json);

foreach($thread_views_decoded_json as $row) {
    $row->forum_id = intval($row->forum_id);
    $row->topic_id = intval($row->topic_id);
    $row->users = array_map('intval', explode(",", $row->users));
}

$thread_views_reencoded_json = json_encode($thread_views_decoded_json, JSON_PRETTY_PRINT);

file_put_contents('./dump_mongo/topic_views_export.json', $thread_views_reencoded_json);

$import_topic_ratings_json_to_mongo = "mongoimport --db twelfthman --upsert --jsonArray --collection topic_view_counter_stats < ./dump_mongo/topic_views_export.json";
echo `$import_topic_ratings_json_to_mongo`;

//SEARCH FOR AND REPLACE ILLEGAL DATES


echo "SWITCHING DATBASE BACK TO STRICT MODE";
$set_global_strict = "SET GLOBAL sql_mode = '" . $back_to_strict_modes . "'";
$set_session_strict = "SET SESSION sql_mode = '" . $back_to_strict_modes . "'";

echo "switch strict dates on global" . PHP_EOL;
$result = $conn_tawny->real_query($set_global_strict);
if ($result) {
    echo "OK\n";
} else {
    echo "FAILED ... Migration halted at inserting matches..." . $conn_tawny->error . PHP_EOL;
    exit();
}

echo "switch strict dates on session" . PHP_EOL;
$result = $conn_tawny->real_query($set_session_strict);
if ($result) {
    echo "OK\n";
} else {
    echo "FAILED ... Migration halted at inserting matches..." . $conn_tawny->error . PHP_EOL;
    exit();
}

$conn_twelfthman->close();
$conn_tawny->close();


if($conn_tawny->connect_error) {
    die("Connection failed: " . $conn_tawny->connect_error);
}