<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 27/06/2017
 * Time: 09:38
 */

$emoticons = "";

$path = "../../public/img/emoticons/"; //path to group icons
$emoticon_dir_list[0] = $path;
$dirnum = 1;

foreach (glob($path . "*", GLOB_ONLYDIR) as $dir) {
    $emoticon_dir_list[$dirnum] = $dir;
    $dirnum++;
}

foreach ($emoticon_dir_list as $dir_path) {
    $dir_handle = @opendir($dir_path) or $reserror = ('Unable to open ' . $dir_path);

    if (isset($reserror) == FALSE) { //if unable to access direcoty, give error
        $array = array();
        while (($file = readdir($dir_handle)) !== false) {
            if ($file != '.' && $file != '..') {
                $pathinfo = pathinfo($file);

                if (isset($pathinfo['extension'])) {
                    $ext = strtolower($pathinfo['extension']);
                } else {
                    $ext = '';
                }

                if ($ext == 'png' || $ext == 'gif') { //allow png or gif images
                    $array[] = array("name" => str_replace("." . $ext, "", $file), "ext" => $ext); //add file to array
                }
            }
        }

        $image_dir = str_replace("../../public/", "", $dir_path); // strip this out ../../public/

        if ($array) {
            sort($array);

            foreach ($array as $value) {

                $emoticons .= "{" . PHP_EOL
                    . "\t" . "title:CURLANG."
                    . $value["name"]
                    . ", " . PHP_EOL
                    . "\timg: '<img title=\"" . $value["name"] . "\" alt=\"" . $value["name"] . "\" src=\"" . $image_dir . "/" . $value["name"] . "." . $value["ext"] . "\" class=\"sm\">', " . PHP_EOL
                    . "\tbbcode:\"[emoticon option='" . $value["ext"] . "']" . $value["name"] . "[/emoticon]\"" . PHP_EOL
                    . "}," . PHP_EOL;
            }
        }

    }
}

$emoticons .= rtrim($emoticons, "," . PHP_EOL);


$myfile = fopen("output.txt", "w") or die("Unable to open file!");
fwrite($myfile, "[" . PHP_EOL . $emoticons . PHP_EOL . "]" . PHP_EOL);
fclose($myfile);

