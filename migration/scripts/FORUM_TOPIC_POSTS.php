<?php 
return
    "INSERT INTO " . TARGET_DB . ".forum_topic_post(topic_id, user_id, content_bb, created,  modified, status, view_type)
select 
" . TARGET_DB . ".forum_topic.id as topic_id,
" . TARGET_DB . ".site_user.id as user_id , 
" . SOURCE_DB . ".forum_post.post_text as content_bb,
date_format(" . SOURCE_DB . ".forum_post.post_date , '%Y-%m-%d %T.000') as created,
IF(" . SOURCE_DB . ".forum_post.post_edited IS NULL, date_format(" . SOURCE_DB . ".forum_post.post_date , '%Y-%m-%d %T.000'), date_format(" . SOURCE_DB . ".forum_post.post_edited , '%Y-%m-%d %T.000')) as modified,
IF(" . SOURCE_DB . ".forum_post.post_deleted = 1 && " . SOURCE_DB . ".forum_post.post_hidden = 1, 'D', 
	IF( " . SOURCE_DB . ".forum_post.post_deleted = 1 && " . SOURCE_DB . ".forum_post.post_hidden = 0, 'D',
		IF( " . SOURCE_DB . ".forum_post.post_deleted = 0 && " . SOURCE_DB . ".forum_post.post_hidden = 1, 'H', 'A'
		)
	) 
) as status,
" . TARGET_DB . ".forum_topic.view_type as view_type

from " . TARGET_DB . ".forum_topic
join " . SOURCE_DB . ".forum_thread on " . SOURCE_DB . ".forum_thread.thread_name = " . TARGET_DB . ".forum_topic.title
join " . SOURCE_DB . ".forum_post on " . SOURCE_DB . ".forum_post.thread_id = " . SOURCE_DB . ".forum_thread.thread_id
join " . TARGET_DB . ".site_user on " . SOURCE_DB . ".forum_post.user_id = " . TARGET_DB . ".site_user.login_id
order by " . SOURCE_DB . ".forum_post.post_id asc";