<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 05/07/2016
 * Time: 13:19
 */

//define("HOME_DIR", realpath('.') . '/');
//include HOME_DIR . "db_config.php";


echo "\nDELETING twelfthman.FORUM TOPIC POSTS FLAGGED... ";
$delete_forum_topic_posts_flagged = "DELETE FROM forum_topic_posts_flagged where forum_topic_posts_flagged.id > 0";
$result = $conn_twelfthman->query($delete_forum_topic_posts_flagged);

if (!$conn_twelfthman->error) {
    echo "OK\n";
} else {
    echo "FAILED: " . $conn_twelfthman->error . "\n";
    exit();
}

echo "\nDELETING twelfthman.FORUM TOPIC POSTS... ";
$delete_forum_topic_posts = "DELETE FROM forum_topic_post where forum_topic_post.id > 0";
$result = $conn_twelfthman->query($delete_forum_topic_posts);

if (!$conn_twelfthman->error) {
    echo "OK\n";
} else {
    echo "FAILED\n";
    exit();
}

echo "\nDELETING FORUM TOPICS... ";
$delete_forum_topic = "DELETE FROM forum_topic where forum_topic.id > 0";
$result = $conn_twelfthman->real_query($delete_forum_topic);

if (!$conn_twelfthman->error) {
    echo "OK\n";
} else {
    echo "FAILED: " . $conn_twelfthman->error . PHP_EOL;
    exit();
}

echo "\nDELETING FORUM... ";
$delete_forum = "DELETE FROM forum where forum.id > 0";
$result = $conn_twelfthman->query($delete_forum);

if (!$conn_twelfthman->error) {
    echo "OK\n";
} else {
    echo "FAILED\n";
    exit();
}

echo "\nDELETING FORUM TYPES... ";
$delete_forum_type = "DELETE FROM forum_type where forum_type.id > 0";
$result = $conn_twelfthman->query($delete_forum_type);

if (!$conn_twelfthman->error) {
    echo "OK\n";
} else {
    echo "FAILED\n" . $conn_twelfthman->error . PHP_EOL;;
    exit();
}

echo "\nDELETING SITE USERS ACHIEVEMENTS... ";
$delete_site_users_achievements = "DELETE FROM site_user_achievement where site_user_achievement.id > 0";
$result = $conn_twelfthman->query($delete_site_users_achievements);

if (!$conn_twelfthman->error) {
    echo "OK\n";
} else {
    echo "FAILED\n";
    exit();
}

echo "\nDELETING SITE USERS ACHIEVEMENT TYPES... ";
$delete_site_users_achievement_type = "DELETE FROM site_user_achievement_type where site_user_achievement_type.id > 0";
$result = $conn_twelfthman->query($delete_site_users_achievement_type);

if (!$conn_twelfthman->error) {
    echo "OK\n";
} else {
    echo "FAILED\n";
    exit();
}

echo "\nDELETING SITE USERS FRIENDS... ";
$delete_site_user_friends = "DELETE FROM site_user_friend WHERE site_user_friend.id > 0";
$conn_twelfthman->query($delete_site_user_friends);

if (!$conn_twelfthman->error) {
    echo "OK\n";
} else {
    echo "FAILED" . $conn_twelfthman->error . "\n";
    exit();
}



echo "\nDELETING SITE USERS... ";
$delete_site_users = "DELETE FROM site_user where site_user.id > 0";
$result = $conn_twelfthman->query($delete_site_users);

if (!$conn_twelfthman->error) {
    echo "OK\n";
} else {
    echo "FAILED" . $conn_twelfthman->error . "\n";
    exit();
}

echo "\nDELETING MATCHES... ";
$delete_matches = "DELETE FROM `match` where `match`.id > 0";
$result = $conn_twelfthman->query($delete_matches);

if (!$conn_twelfthman->error) {
    echo "OK\n";
} else {
    echo "FAILED" . $conn_twelfthman->error . "\n";
    exit();
}

echo "\nDELETING SITE USERS TEAM GROUPS... ";
$delete_site_users_team = "DELETE FROM team";
$result = $conn_twelfthman->query($delete_site_users_team);

if (!$conn_twelfthman->error) {
    echo "OK\n";
} else {
    echo "FAILED" . $conn_twelfthman->error . "\n";
    exit();
}

echo "\nRemoving Users Teams from" . realpath('../public/img/user/teams/') . "\n";
$remove_team_pics = "find " . realpath('../public/img/teams') . " -type f -delete";
echo exec($remove_team_pics);

echo "\nRemoving Achievement Type images...\n";
$delete_achievement_types = 'find ' . realpath('../public/img/user-achievements/') . ' -type f -delete';
echo exec($delete_achievement_types);

echo "\nDELETING NEWS ARTICLES... ";
$delete_news_articles = "DELETE FROM news_article where news_article.id > 0";
$result = $conn_twelfthman->query($delete_news_articles);

if (!$conn_twelfthman->error) {
    echo "OK\n";
} else {
    echo "FAILED\n";
    exit();
}

echo "\nDELETING NEWS GROUPS... ";
$delete_news_groups = "DELETE FROM news_group where news_group.id > 0";
$result = $conn_twelfthman->query($delete_news_groups);

if (!$conn_twelfthman->error) {
    echo "OK\n";
} else {
    echo "FAILED: " . $conn_twelfthman->error . PHP_EOL;
    exit();
}

//Delete all teams
echo "\nDELETING LEAGUES... ";
$delete_teams = "DELETE FROM team where team.id > 0";
$result = $conn_twelfthman->query($delete_teams);

if (!$conn_twelfthman->error) {
    echo "OK\n";
} else {
    echo "FAILED\n";
    exit();
}

//This comes last as matches then teams will need deleting
echo "\nDELETING LEAGUES... ";
$delete_leagues = "DELETE FROM league where league.id > 0";
$result = $conn_twelfthman->query($delete_leagues);

if (!$conn_twelfthman->error) {
    echo "OK\n";
} else {
    echo "FAILED\n";
    exit();
}

//$user_uid = posix_getpwuid(posix_getuid());
//$user = $user_uid['name'];

echo "\nRemoving News Article Images from" . realpath('../public/img/news/articles/') . "\n";
$remove_article_images = "find " . realpath('../public/img/news/articles') . " -type f -delete";
echo exec($remove_article_images);

echo "\nRemoving User Avatar Images Images\n";
$delete_avatars = 'find ' . realpath('../public/img/user-avatars/') . ' -type f -delete';
echo exec($delete_avatars);

$copy_default = 'cp ' . realpath('../public/img/default-avatar.png') . ' ' . realpath('../public/img/user-avatars/');
echo exec($copy_default);

//Remove any Achievement Type images
echo "\nRemoving Achievement Type images...\n";
$delete_achievement_types = 'find ' . realpath('../public/img/user-achievements/') . ' -type f -delete';
echo exec($delete_achievement_types);

//$mongo->close();
//$conn_tawny->close();