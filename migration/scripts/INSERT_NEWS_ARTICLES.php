<?php

return "INSERT INTO " . TARGET_DB . ".news_article(title,content_bb,created,published_first, publish, image_upload, image_embed, news_group_id, user_id)
SELECT 
IF(" . SOURCE_DB . ".news_data.news_title IS NULL OR " . SOURCE_DB . ".news_data.news_title = \"\", \"NO TITLE\", convert(cast(convert(" . SOURCE_DB . ".news_data.news_title using latin1) as binary) using utf8)) AS title,
convert(cast(convert(" . SOURCE_DB . ".news_data.news_contents using latin1) as binary) using utf8) as content_bb,
" . SOURCE_DB . ".news_data.news_date,
" . SOURCE_DB . ".news_data.news_date,
IF(" . SOURCE_DB . ".news_data.news_hide = 0, 1, 0) AS publish,  
IF(" . SOURCE_DB . ".news_data.news_image_name IS NOT NULL, " . SOURCE_DB . ".news_data.news_image_name, NULL)  AS image_upload,
IF(" . SOURCE_DB . ".news_data.news_image_type = 'text/html', " . SOURCE_DB . ".news_data.news_image, NULL)  AS image_embed,
" . TARGET_DB . ".news_group.id AS news_group_id,
(SELECT id FROM " . TARGET_DB . ".site_user WHERE " . TARGET_DB . ".site_user.login_id = " . SOURCE_DB . ".news_data.user_id) as user_id 
FROM " . SOURCE_DB . ".news_data
JOIN " . SOURCE_DB . ".news_group ON " . SOURCE_DB . ".news_group.news_group_id = " . SOURCE_DB . ".news_data.news_group_id
JOIN " . TARGET_DB . ".news_group ON " . TARGET_DB . ".news_group.label = " . SOURCE_DB . ".news_group.news_group_name
ORDER BY " . SOURCE_DB . ".news_data.news_id ASC";