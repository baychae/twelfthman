<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 05/07/2016
 * Time: 14:47
 */

echo "Amending email addresses of duplicate accounts... user can log in with account last used to post... \n";

$sql = "select distinct cu1.* 
from core_user cu1
join core_user cu2
	on cu1.user_email like cu2.user_email
where cu1.user_id != cu2.user_id
and cu1.user_email != \"\"
order by cu1.user_email asc";

$result = $conn_tawny->query($sql);

if ($result) {
    echo "OK\n";
} else {
    echo "FAILED\n" . $conn_tawny->error;
    exit();
}

$set = $result->fetch_all(MYSQLI_ASSOC);

$second = $set;

$dupes = [];

foreach ($set as $index => $row1) {//Get list of dupe accounts

    foreach ($second as $index2 => $set2) {

        if (strcasecmp($row1["user_email"], $set2["user_email"]) == 0 && $row1["user_id"] != $set2["user_id"] && $row1["user_email"] != "") {

            $sql_get_last_post = "select core_user.* from core_user
                                 join forum_post on forum_post.user_id = core_user.user_id
                                 where core_user.user_email = '" . $set2["user_email"] . "'
                                 order by forum_post.post_id asc
                                 limit 0,1 ";

            $result = $conn_tawny->query($sql_get_last_post);
            $res = $result->fetch_assoc();

            $keep_user = $res["user_id"];

            echo var_dump($keep_user);

            if (strcasecmp($set2["user_id"], $keep_user) != 0) {

                $sql_mark_dupe = 'UPDATE core_user '
                    . 'SET core_user.user_email = CONCAT( REPLACE(core_user.user_email,"@","AT"), "_", SUBSTRING(MD5(RAND()) FROM 1 FOR 6), "_", "dupe@owlsonline.com")  '
                    . 'WHERE core_user.user_id = "' . $set2["user_id"] . '"';
                $err = $conn_tawny->query($sql_mark_dupe);

                echo "\n Found duplicate email for user " . $set2["user_id"] . " now modified with _dupe@owlsonline.com ";
            }

        }

    }


}
