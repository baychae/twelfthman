<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 22/06/2017
 * Time: 14:35
 */

//dump images to DUMP_ACHIEVEMENT_TYPES

$create_dump_user_achievement_images_sql = 'CREATE DEFINER=`root`@`localhost` PROCEDURE `dump_user_achievement_images`()
BEGIN

    DECLARE achievement_id INT ;
    DECLARE achievement_name VARCHAR(255);
    DECLARE done BOOLEAN;
    DECLARE achievement_image_name VARCHAR(255);
    DECLARE time_stamp DATETIME(4);
    DECLARE duplicate_key INT DEFAULT 0;
    
	
    DECLARE cur1 CURSOR FOR 
    select
    ' . SOURCE_DB . '.core_achievement.achievement_id,
    ' . SOURCE_DB . '.core_achievement.achievement_name,
    ' . SOURCE_DB . '.core_achievement.achievement_image_name
	from core_achievement;
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
	OPEN cur1;
    
    read_loop: loop
    
		FETCH cur1 INTO achievement_id, achievement_name, achievement_image_name;
        set duplicate_key = 0;
                
        BEGIN
			DECLARE EXIT HANDLER FOR 1086 SET duplicate_key = 1;
                    
			SET @query = CONCAT(
			"SELECT ' . SOURCE_DB . '.core_achievement.achievement_image 
            FROM ' . SOURCE_DB . '.core_achievement
            WHERE ' . SOURCE_DB . '.core_achievement.achievement_id = ", achievement_id, 
			" INTO DUMPFILE \'' . DUMP_ACHIEVEMENT_TYPES . '/",achievement_image_name,"\'");
			PREPARE statement FROM @query;
			EXECUTE statement;
			                     
			SELECT CURRENT_TIMESTAMP() into @time_stamp; 
            
            INSERT INTO ' . TARGET_DB . '.site_user_achievement_type(`title`,`file_name`, `created`, `modified`) 
			VALUES(achievement_name, achievement_image_name, time_stamp, time_stamp );

        END;
        
        IF done THEN
		LEAVE read_loop;
                
    END IF;
     
    END LOOP;

	CLOSE cur1;

END';

$result = $conn_tawny->real_query("DROP PROCEDURE IF EXISTS `dump_user_achievement_images`;");
if ($result) {
    echo "Dropped Stored Procedure: dump_user_achievement_images OK\n";
} else {
    echo "FAILED to drop dump_user_images... Migration halted...\n" . $conn_tawny->error . "\n";
    exit();
}

$result = $conn_tawny->real_query($create_dump_user_achievement_images_sql);

if ($result) {
    echo "Stored Procedure dump_user_achievement_images installed OK\n";
} else {
    echo "FAILED ... Migration halted...\n" . $conn_tawny->error . "\n";
    exit();
}
