<?php
/**
 * Created by PhpStorm.
 * User: Richard Sinclair
 * Date: 19/10/2016
 * Time: 12:58
 */


return "INSERT INTO " . TARGET_DB . ".league(name, position, link) 
SELECT 
" . SOURCE_DB . ".rivals_league.league_name as name, 
" . SOURCE_DB . ".rivals_league.league_position as position,
LOWER(" . SOURCE_DB . ".rivals_league.league_name) as link
FROM " . SOURCE_DB . ".rivals_league 
where " . SOURCE_DB . ".rivals_league.league_id > 0 ";