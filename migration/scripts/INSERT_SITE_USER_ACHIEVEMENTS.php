<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 22/06/2017
 * Time: 17:22
 */

list($eTime, $uTime) = explode(".", strval(microtime(true)));
$time_stamp = date("Y:m:d H:i:s", $eTime) . "." . $uTime;


return "INSERT INTO " . TARGET_DB . ".site_user_achievement(site_user_id, site_user_achievement_type_id, created, modified)"
    . "select " . TARGET_DB . ".site_user.id, " . TARGET_DB . ".site_user_achievement_type.id, '" . $time_stamp . "', '" . $time_stamp . "' "
    . "from " . SOURCE_DB . ".core_achievement_users "
    . "join " . SOURCE_DB . ".core_achievement on " . SOURCE_DB . ".core_achievement.achievement_id = " . SOURCE_DB . ".core_achievement_users.achievement_id "
    . "join " . TARGET_DB . ".site_user on " . TARGET_DB . ".site_user.login_id = " . SOURCE_DB . ".core_achievement_users.user_id "
    . "join " . TARGET_DB . ".site_user_achievement_type on " . TARGET_DB . ".site_user_achievement_type.title = " . SOURCE_DB . ".core_achievement.achievement_name ";