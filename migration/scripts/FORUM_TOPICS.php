<?php

return
    "REPLACE " . TARGET_DB . ".forum_topic(forum_id, user_id, title, description, status, created, modified, last_updated, view_type)
select " . TARGET_DB . ".forum.id AS forum_id,
" . TARGET_DB . ".site_user.id as user_id,
" . SOURCE_DB . ".forum_thread.thread_name as title, 
" . SOURCE_DB . ".forum_thread.thread_description as description,
IF(" . SOURCE_DB . ".forum_thread.thread_lock = 1, 'L', 'A') as status,
date_format(" . SOURCE_DB . ".forum_thread.thread_date, '%Y-%m-%d %T.000') as created,
date_format(" . SOURCE_DB . ".forum_thread.thread_date, '%Y-%m-%d %T.000') as modified,
date_format(" . SOURCE_DB . ".forum_thread.thread_modified, '%Y-%m-%d %T.000') as last_updated,
IF(" . SOURCE_DB . ".forum_section.forum_lock = 1, 'A', 'P') as view_type
from " . SOURCE_DB . ".forum_section
join " . SOURCE_DB . ".forum_thread on " . SOURCE_DB . ".forum_thread.forum_id = " . SOURCE_DB . ".forum_section.forum_id
join " . TARGET_DB . ".forum on " . SOURCE_DB . ".forum_section.forum_name = " . TARGET_DB . ".forum.title
join " . SOURCE_DB . ".core_user on " . SOURCE_DB . ".forum_thread.user_id = " . SOURCE_DB . ".core_user.user_id  
join " . TARGET_DB . ".site_user on " . TARGET_DB . ".site_user.login_id = " . SOURCE_DB . ".core_user.user_id";