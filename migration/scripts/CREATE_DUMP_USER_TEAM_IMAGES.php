<?php

$create_dump_user_team_images_sql = 'CREATE DEFINER=`root`@`localhost` PROCEDURE `dump_user_team_images`()
BEGIN

	DECLARE group_id INT;
    DECLARE done BOOLEAN;
 
    DECLARE group_name VARCHAR(250);
    DECLARE file_name VARCHAR(250);
    DECLARE nickname VARCHAR(255);
    DECLARE website VARCHAR(255);
    DECLARE league_name VARCHAR(255);
    DECLARE duplicate_key INT DEFAULT 0;
    DECLARE league_id INT DEFAULT 0;
	
    DECLARE cur1 CURSOR FOR 
    select
    ' . SOURCE_DB . '.rivals_team.team_id,
    ' . SOURCE_DB . '.rivals_team.team_name,
    ' . SOURCE_DB . '.rivals_team.team_image_name,
    ' . SOURCE_DB . '.rivals_team.team_nickname,
    ' . SOURCE_DB . '.rivals_team.team_site,
    convert(cast(convert(' . SOURCE_DB . '.rivals_league.league_name using latin1) as binary) using utf8) as league_name
	from ' . SOURCE_DB . '.rivals_team
	join ' . SOURCE_DB . '.rivals_league 
	on ' . SOURCE_DB . '.rivals_league.league_id = ' . SOURCE_DB . '.rivals_team.league_id;
    
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN cur1;
    
    read_loop: loop
    
		FETCH cur1 INTO group_id, group_name, file_name, nickname, website, league_name;
        set duplicate_key = 0;
                
        BEGIN
			DECLARE EXIT HANDLER FOR 1086 SET duplicate_key = 1;
                    
			SET @query = CONCAT(
			"SELECT ' . SOURCE_DB . '.rivals_team.team_image 
            FROM rivals_team 
            WHERE ' . SOURCE_DB . '.rivals_team.team_id = ", group_id, 
			" INTO DUMPFILE \'' . DUMP_USER_TEAM_IMGS . '",file_name,"\'");
			PREPARE statement FROM @query;
			EXECUTE statement;
			
            set league_id = (SELECT ' . TARGET_DB . '.league.id
            FROM ' . TARGET_DB . '.league 
            where ' . TARGET_DB . '.league.name = league_name );            
            
            INSERT INTO ' . TARGET_DB . '.team(`name`,`image_name`,`nickname`,`website`,`league_id`) 
			VALUES(group_name, file_name, nickname, website, league_id );

        END;
        
        IF done THEN
		LEAVE read_loop;
                
    END IF;
     
    END LOOP;

	CLOSE cur1;

END';

$result = $conn_tawny->real_query("DROP PROCEDURE IF EXISTS `dump_user_team_images`;");
if ($result) {
    echo "Dropped Stored Procedure: dump_user_team_images OK\n";
} else {
    echo "FAILED to drop dump_user_images... Migration halted...\n" . $conn_tawny->error . "\n";
    exit();
}

$result = $conn_tawny->real_query($create_dump_user_team_images_sql);

if ($result) {
    echo "Stored Procedure dump_user_team_images installed OK\n";
} else {
    echo "FAILED ... Migration halted...\n" . $conn_tawny->error . "\n";
    exit();
}
