<?php
# IN MySqldeveloper use this query then export to json file
# Using Sublime Text 2 Find replace with the the following regex. Find: 'users' : "(.*)”, Replace: 'users' : [$1]
# Then import into mongo db with the following command: mongoimport --db twelfthman --upsert --jsonArray --collection topic_view_counter_stats < thread_views.json

return '
SELECT
' . TARGET_DB . '.forum_topic.forum_id as forum_id, 
' . TARGET_DB . '.forum_topic.id as topic_id,
date_format(CURDATE(), "%Y-%m-%d %T.000") as created,
date_format(CURDATE(), "%Y-%m-%d %T.000") as modified,
group_concat(' . TARGET_DB . '.site_user.id) as users
FROM ' . SOURCE_DB . '.forum_thread_user
join ' . TARGET_DB . '.site_user on ' . SOURCE_DB . '.forum_thread_user.user_id = ' . TARGET_DB . '.site_user.login_id
join ' . SOURCE_DB . '.forum_thread on ' . SOURCE_DB . '.forum_thread.thread_id = ' . SOURCE_DB . '.forum_thread_user.thread_id
join ' . TARGET_DB . '.forum_topic on ' . SOURCE_DB . '.forum_thread.thread_name = ' . TARGET_DB . '.forum_topic.title
group by ' . SOURCE_DB . '.forum_thread_user.thread_id';
