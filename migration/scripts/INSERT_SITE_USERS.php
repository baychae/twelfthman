<?php

return "INSERT INTO " . TARGET_DB . ".site_user (password, login_id, avatar_name, first_name, last_name, email, password_legacy, active, modified, created, level, signature_bb)
SELECT 'legacy',
" . SOURCE_DB . ".core_user.user_id as login_id, 
IF(" . SOURCE_DB . ".core_user.user_display_name is null OR " . SOURCE_DB . ".core_user.user_display_name = '', null, " . SOURCE_DB . ".core_user.user_display_name) as avatar_name, 
IF(" . SOURCE_DB . ".core_user.user_first_name is null OR " . SOURCE_DB . ".core_user.user_first_name = '' , null, " . SOURCE_DB . ".core_user.user_first_name) as first_name, 
IF(" . SOURCE_DB . ".core_user.user_surname is null OR " . SOURCE_DB . ".core_user.user_surname = '', null, " . SOURCE_DB . ".core_user.user_surname ) as last_name, 
IF(" . SOURCE_DB . ".core_user.user_email is null OR " . SOURCE_DB . ".core_user.user_email = '', CONCAT( SUBSTRING(MD5(RAND()) FROM 1 FOR 6), \"_\", \"blank@owlsonline.com\"), " . SOURCE_DB . ".core_user.user_email) as email, 
" . SOURCE_DB . ".core_user.user_password as password, 
IF( " . SOURCE_DB . ".core_user.user_locked = 0, 'Y', 'N' ) as active, 
IF(" . SOURCE_DB . ".core_user.user_first_use IS NULL or  " . SOURCE_DB . ".core_user.user_first_use= '' or  " . SOURCE_DB . ".core_user.user_first_use='0000-00-00 00:00:00', '1000-01-01 00:00:00', " . SOURCE_DB . ".core_user.user_first_use) as modified, 
IF(" . SOURCE_DB . ".core_user.user_first_use IS NULL or  " . SOURCE_DB . ".core_user.user_first_use= '' or  " . SOURCE_DB . ".core_user.user_first_use='0000-00-00 00:00:00', '1000-01-01 00:00:00', " . SOURCE_DB . ".core_user.user_first_use) as created,
IF(" . SOURCE_DB . ".core_user.user_level = 4, 'sysadmin',
	IF(" . SOURCE_DB . ".core_user.user_level = 3, 'admin', 
		IF(" . SOURCE_DB . ".core_user.user_level = 2, 'poweruser', 
			'user')
	)  
) as level,
CAST(" . SOURCE_DB . ".core_user.user_signature AS CHAR(255) CHARACTER SET utf8) as signature_bb
from " . SOURCE_DB . ".core_user
where " . SOURCE_DB . ".core_user.user_first_name != 'Guest'
order by " . SOURCE_DB . ".core_user.user_id asc
ON DUPLICATE KEY UPDATE email= CONCAT(SUBSTRING(MD5(RAND()) FROM 1 FOR 6),\"_\",email,\"_\",\"dupe@owlsonline.com\");";



