<?php

return "
INSERT INTO " . TARGET_DB . ".forum (
" . TARGET_DB . ".forum.forum_type_id, 
" . TARGET_DB . ".forum.TITLE, 
" . TARGET_DB . ".forum.DESCRIPTION, 
" . TARGET_DB . ".forum.view_type, 
" . TARGET_DB . ".forum.STATUS,
" . TARGET_DB . ".forum.created,
" . TARGET_DB . ".forum.modified)

select (
select " . TARGET_DB . ".forum_type.id 
from " . TARGET_DB . ".forum_type  
where " . SOURCE_DB . ".forum_type.type_name = " . TARGET_DB . ".forum_type.title
) as forum_type_id
, " . SOURCE_DB . ".forum_section.forum_name as title 
, " . SOURCE_DB . ".forum_section.forum_description
, IF(" . SOURCE_DB . ".forum_section.forum_lock = 1, 'A', 'P') as view_type
, 'A' as status
, '2000-01-01 00:00:00.0000'
, '2000-01-01 00:00:00.0000'
from " . SOURCE_DB . ".forum_section
join " . SOURCE_DB . ".forum_type on " . SOURCE_DB . ".forum_type.type_id = " . SOURCE_DB . ".forum_section.type_id";
