<?php
/**
 * Created by PhpStorm.
 * User: baychae@gmail.com
 * Date: 30/10/2017
 * Time: 14:53
 */

return "INSERT INTO " . TARGET_DB . ".site_user_friend(site_user_id, friend_site_user_id, created, modified, notified, confirmed)
SELECT su1.id AS site_user_id,
su2.id AS friend_site_user_id,
DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:%s.0000') AS created,
DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:%s.0000') AS modified,
" . SOURCE_DB . ".core_user_friends.friend_notify AS notified,
" . SOURCE_DB . ".core_user_friends.friend_confirmed AS confirmed
FROM " . SOURCE_DB . ".core_user_friends
JOIN " . TARGET_DB . ".site_user su1 ON su1.login_id = " . SOURCE_DB . ".core_user_friends.user_id
JOIN " . TARGET_DB . ".site_user su2 ON su2.login_id = " . SOURCE_DB . ".core_user_friends.friend_user_id";
