<?php
/**
 * AUTHOR: Richard Sinclair <rich.sinclair@gmail.com>
 * Date: 07/11/2016
 * Time: 14:32
 */


echo "Parse Getty Images in news articles" . PHP_EOL;

$result = $conn_twelfthman->query("Select * from " . TARGET_DB . ".news_article ");

while ($row = $result->fetch_assoc()) {
    $new_title = htmlspecialchars_decode($row["title"], ENT_QUOTES);
    $conn_twelfthman->query("UPDATE news_article SET title = \"" . $new_title . "\" where id = " . intval($row["id"]));
}

////////////////////
/* Convert news article BB to html using twelfthmans method
 *
 */
$node_script_convert_news_article_bb = "node " . SCRIPT_DIR . "/news_article_bb_to_html.js";
echo shell_exec($node_script_convert_news_article_bb);

$result = $conn_twelfthman->query("Select * from " . TARGET_DB . ".news_article ");

while ($row = $result->fetch_assoc()) {
    $new_html = html_entity_decode($row["content_html"], ENT_QUOTES);

    $pattern = '/&lt;div class="getty embed image"(.*)&lt;\/div&gt;/';

    $new_html = preg_replace_callback($pattern, function ($m) {
        return "<div class='wysibb-image-embed'>" . html_entity_decode($m[0]) . "</div>";
    }, $row["content_html"], -1);

    $stmt = $conn_twelfthman->prepare("UPDATE news_article SET content_html = ? where id = " . intval($row["id"]));
    $stmt->bind_param("s", $new_html);

    if (!$stmt->execute()) {
        echo "FAILED ... Migration halted..." . printf("Errormessage: %s\n", $conn_twelfthman->error);
        exit();
    } else {
        echo "OK...";
    }

    echo ".";

}

echo PHP_EOL;

if ($result) {
    echo "OK\n";
} else {
    echo "FAILED ... Migration halted...\n";
    exit();
}