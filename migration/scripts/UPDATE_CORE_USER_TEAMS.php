<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 27/05/2016
 * Time: 16:16
 */


return "update " . TARGET_DB . ".site_user
        join " . SOURCE_DB . ".core_user on " . SOURCE_DB . ".core_user.user_id = " . TARGET_DB . ".site_user.login_id 
	    join " . SOURCE_DB . ".core_user_group on " . SOURCE_DB . ".core_user_group.group_id = " . SOURCE_DB . ".core_user.group_id 
	    join " . TARGET_DB . ".team on " . TARGET_DB . ".team.name = " . SOURCE_DB . ".core_user_group.group_name 
        set 
	    " . TARGET_DB . ".site_user.team_id = " . TARGET_DB . ".team.id ";