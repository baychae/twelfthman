<?php


define("SCRIPT_DIR", realpath('scripts') . '/');

include "./../db_config.php";

//This comes last as matches then teams will need deleting
echo "\nDELETING LEAGUES... ";
$delete_leagues = "DELETE FROM league where league.id > 0";
$result = $conn_twelfthman->query($delete_leagues);

if ($result) {
    echo "OK\n";
} else {
    echo "FAILED: " . $result->error . "\n";
    exit();
}

echo "Migrating Leagues... ";

$migrate_leagues = include "./INSERT_LEAGUES.php";
$result = $conn_tawny->real_query($migrate_leagues);

if ($result) {
    echo "OK\n";
} else {
    echo "FAILED: " . $conn_tawny->error . "\n";
    exit();
}

//Post insert processing of leagues -- Convert link to pretty url

echo "Updating Leagues with link... " . PHP_EOL;

$get_leagues = "SELECT * FROM league WHERE id > 0";
$result = $conn_twelfthman->query($get_leagues, MYSQLI_STORE_RESULT);

if ($result) {

    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {

        $link = preg_replace('/[\s-]+/', '-', strtolower($row["link"]));

        $update_link_sql = "UPDATE " . TARGET_DB . ".league set link = '" . $link . "' WHERE id = " . $row["id"];

        if ($conn_twelfthman->query($update_link_sql) === TRUE) {
            echo "Successfully created link for " . $row["name"] . ": " . $link . PHP_EOL;
        } else {
            echo "Error updating record: " . $conn_twelfthman->error;
        }

    }


} else {
    echo "FAILED: " . $conn_twelfthman->error . "\n";
    exit();
}