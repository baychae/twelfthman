<?php

$create_dump_user_images_sql = '
CREATE DEFINER = \'root\'@\'localhost\' PROCEDURE dump_user_images()
BEGIN

	DECLARE article_id INT;
    DECLARE done BOOLEAN;
 
    DECLARE user_id VARCHAR(250);
    DECLARE file_type VARCHAR(250);
    DECLARE file_name VARCHAR(250);
    DECLARE new_image_file_type VARCHAR(250);
    DECLARE duplicate_key INT DEFAULT 0;
	
    DECLARE cur1 CURSOR FOR 
    select ' . SOURCE_DB . '.core_user.user_id, ' . SOURCE_DB . '.core_user.user_avatar_type 
	from ' . SOURCE_DB . '.core_user
    where ' . SOURCE_DB . '.core_user.user_avatar_type != \'text/html\'
	and ' . SOURCE_DB . '.core_user.user_avatar_type !=\'application/octet-stream\'
	and ' . SOURCE_DB . '.core_user.user_avatar is not null;
    
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN cur1;
    
    read_loop: loop
    
		FETCH cur1 INTO user_id, new_image_file_type;
        set duplicate_key = 0;
        
        set file_name = REPLACE(user_id, \' \', \'%20\');
        set file_type = SUBSTRING_INDEX(new_image_file_type, \'/\', -1);
        
        BEGIN
			DECLARE EXIT HANDLER FOR 1086 SET duplicate_key = 1;
                    
			SET @query = CONCAT(
			"SELECT core_user.user_avatar FROM core_user WHERE core_user.user_id = \'",user_id,"\'
			INTO DUMPFILE \'' . DUMP_USER_IMGS . '",user_id,".",file_type,"\'");
			PREPARE statement FROM @query;
			EXECUTE statement;
            
            UPDATE ' . TARGET_DB . '.site_user 
            SET ' . TARGET_DB . '.site_user.avatar_file_name = CONCAT(file_name,".",file_type)
            WHERE ' . TARGET_DB . '.site_user.login_id = user_id;
            
                                    
        END;
        
        IF done THEN
		LEAVE read_loop;
                
    END IF;
     
    END LOOP;

	CLOSE cur1;

END';

$result = $conn_tawny->real_query("DROP PROCEDURE IF EXISTS `dump_user_images`;");
if ($result) {
    echo "Dropped Stored Procedure: dump_user_images OK\n";
} else {
    echo "FAILED to drop dump_user_images... Migration halted...\n" . $conn_tawny->error . "\n";
    exit();
}

$result = $conn_tawny->real_query($create_dump_user_images_sql);

if ($result) {
    echo "Stored Procedure dump_user_images installed OK\n";
} else {
    echo "FAILED ... Migration halted...\n" . $conn_tawny->error . "\n";
    exit();
}
