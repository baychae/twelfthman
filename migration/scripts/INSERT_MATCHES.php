<?php
/**
 * AUTHOR: Richard Sinclair <rich.sinclair@gmail.com>
 * Date: 26/10/2016
 * Time: 14:57
 */

return "INSERT INTO " . TARGET_DB . ".match(home_team_id, away_team_id, home_score, away_score, preview_article_id, report_article_id, start, modified, created)
select 
tw_home_team.id as home_team_id,
tw_away_team.id as away_team_id,
zadmin_match.match_home_team_score as home_score,
zadmin_match.match_away_team_score as away_score,
IF(tw_match_preview.id IS NULL or tw_match_preview.id = '', NULL, tw_match_preview.id) as preview_article_id,
IF(tw_match_report.id IS NULL or tw_match_report.id = '', NULL, tw_match_report.id) as report_article_id,
IF(zadmin_match.match_start IS NULL or zadmin_match.match_start = '' or zadmin_match.match_start ='0000-00-00 00:00:00', '1000-01-01 00:00:00', zadmin_match.match_start) as start,
IF(zadmin_match.match_start IS NULL or zadmin_match.match_start = '' or zadmin_match.match_start ='0000-00-00 00:00:00', '1000-01-01 00:00:00', zadmin_match.match_start) as modified,
IF(zadmin_match.match_start IS NULL or zadmin_match.match_start = '' or zadmin_match.match_start ='0000-00-00 00:00:00', '1000-01-01 00:00:00', zadmin_match.match_start) as created
from " . SOURCE_DB . ".upnext_match zadmin_match
join " . SOURCE_DB . ".upnext_team t_home_team on t_home_team.team_id = zadmin_match.match_home_team_id
join " . SOURCE_DB . ".upnext_team t_away_team on t_away_team.team_id = zadmin_match.match_away_team_id
join " . TARGET_DB . ".team tw_home_team on tw_home_team.name = t_home_team.team_name
join " . TARGET_DB . ".team tw_away_team on tw_away_team.name = t_away_team.team_name
left join " . SOURCE_DB . ".news_data match_preview on match_preview.news_id = zadmin_match.match_preview_url
left join " . TARGET_DB . ".news_article tw_match_preview on tw_match_preview.published_first = match_preview.news_date
left join " . SOURCE_DB . ".news_data match_report on match_report.news_id = zadmin_match.match_report_url
left join " . TARGET_DB . ".news_article tw_match_report on tw_match_report.published_first = match_report.news_date
order by zadmin_match.match_id desc";