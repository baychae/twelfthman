<?php
/**
 * Created by PhpStorm.
 * User: baychae@gmail.com
 * Date: 17/08/2017
 * Time: 14:02
 */

return 'select f.id as forum_id, '
    . 'ft.id as topic_id, '
    . 'ftp1.id as post_id, '
    . 'count( ftp2.id ) as post_count, '
    . 'ftp1.created as modified, '
    . 'ftp1.created as created '
    . 'from forum_topic ft '
    . 'join (select ft2.id , max(ftp1.id) as last_post_id 
            from forum_topic_post ftp1 
            join forum_topic ft2 on ft2.id = ftp1.topic_id 
            group by topic_id 
            order by 2 desc) max_p 
            on max_p.id = ft.id '
    . 'join forum_topic_post ftp1 on ftp1.id = max_p.last_post_id '
    . 'join forum_topic_post ftp2 on ftp2.topic_id = ft.id '
    . 'join forum f on f.id = ft.forum_id '
    . 'join forum_type ftype on ftype.id = f.forum_type_id '
    . 'join site_user su on su.id = ftp1.user_id '
    . 'group by ft.id '
    . 'order by max_p.last_post_id asc';
