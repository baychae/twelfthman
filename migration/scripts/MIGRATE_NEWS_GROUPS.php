<?php
/**
 * Created by PhpStorm.
 * User: baychae@gmail.com
 * Date: 25/08/2017
 * Time: 14:11
 */

return "INSERT INTO " . TARGET_DB . ".news_group (link, label, priority, image_name, image_type)" .
    "SELECT " .
    "SUBSTRING_INDEX(" . SOURCE_DB . ".news_group.news_group_image_name, '.', 1)  AS link, " .
    SOURCE_DB . ".news_group.news_group_name AS label, " .
    SOURCE_DB . ".news_group.news_group_priority AS priority, " .
    SOURCE_DB . ".news_group.news_group_image_name AS image_name, " .
    SOURCE_DB . ".news_group.news_group_image_type AS iamge_type " .
    "FROM " . SOURCE_DB . ".news_group " .
    "ORDER BY " . SOURCE_DB . ".news_group.news_group_id ASC";
