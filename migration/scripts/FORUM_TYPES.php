<?php

return "
INSERT INTO " . TARGET_DB . ".forum_type ( 
" . TARGET_DB . ".forum_type.title, 
" . TARGET_DB . ".forum_type.display_order, 
" . TARGET_DB . ".forum_type.view_type,
" . TARGET_DB . ".forum_type.created,
" . TARGET_DB . ".forum_type.modified)
select distinct
" . SOURCE_DB . ".forum_type.type_name as title,
IF(" . SOURCE_DB . ".forum_type.type_priority = 0, 3,
	IF(" . SOURCE_DB . ".forum_type.type_priority = 1, 2, 1)) as display_order,
IF(" . SOURCE_DB . ".forum_section.forum_lock = 1, 'A', 'P') as view_type,
'2000-01-01 00:00:00.0000' as created,
'2000-01-01 00:00:00.0000' as modified
from " . SOURCE_DB . ".forum_type
join " . SOURCE_DB . ".forum_section
    on " . SOURCE_DB . ".forum_section.type_id = " . SOURCE_DB . ".forum_type.type_id";

