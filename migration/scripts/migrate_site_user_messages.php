<?php
////////////////////
/* Migrate Site User Messages
*
*/


//$mongo = new \MongoClient();
//$mongodb = $mongo->selectDB('twelfthman');

///////////////////////////////////////////////////
//Inbox
echo "Migrate Site User Messages... ";
$migrate_site_user_messages = include "site_user_messages_inbox.php";
$result = $conn_tawny->query($migrate_site_user_messages);

while ($row = $result->fetch_assoc()) {
    $myMessagesArray[] = $row;
}

if ($result) {
    echo "Fetched Inbox Messages...OK\n";
} else {
    echo "FAILED ... Migration halted...\n";
    exit();
}

$my_messages_inbox_json = json_encode($myMessagesArray);
$decoded_messsages_json_inbox = json_decode($my_messages_inbox_json);

//TODO - FOR EACH MESSAGE CREATE ONE IN THE OUTBOX

echo "Processing inbox.. ";

foreach ($decoded_messsages_json_inbox as $message) {
    $message->recipient_read = intval($message->recipient_read);

    //get To user id and set
    $result = $conn_twelfthman->query("SELECT id, login_id, avatar_name FROM site_user WHERE login_id = '"
        . $message->to . "'");
    $to_user = $result->fetch_assoc();
    $message->to_site_user_id = $to_user["id"];

    //get From user id and set
    $result = $conn_twelfthman->query("SELECT id, login_id, avatar_name FROM site_user WHERE login_id = '"
        . $message->from . "'");
    $from_user = $result->fetch_assoc();
    $message->from_site_user_id = $from_user["id"];

    //Make plaintext from the bbcode
    $text = $message->body_bb;
    $embed_stripped = preg_replace('/\[embed\].*\[\/embed\]/', '', $text);
    $youtube_stripped = preg_replace('/\[youtube\].*\[\/youtube\]/', '', $embed_stripped);
    $video_stripped = preg_replace('/\[video\].*\[\/video\]/', '', $youtube_stripped);
    $no_bb = preg_replace('/\[.*?]/', '', $video_stripped); //strip bb codes
    $result = preg_replace('/\<.*?\>(.*)\<\/.*\>/', '', $no_bb); // strip html content
    $message->body_plaintext = $result;

    $message->is_copy = 0;


    //Lower case everything
    $string = strtolower($message->subject);
    //Make alphanumeric (removes all other characters)
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    //Clean up multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s_]/", "-", $string);

    $message->uri = $string . "-" . uniqid();

    //If the message subject is a friend request then replace old style with new style
    if ($message->subject == "New Friend Request" && $from_user["id"] != null) {
        $display_name = $from_user["avatar_name"] === null ? $from_user["avatar_name"] : $from_user["login_id"];

        $new_friend_message =
            "The user [b]"
            . $display_name
            . "[/b] has requested you to be their friend. Please confirm or deny this request. "
            . PHP_EOL
            . "<a "
            . "title='Accept Request' "
            . "alt='Accept Request' "
            . "href='" . "messages/friend-request-accept/" . urlencode($message->from) . "'>"
            . "<img "
            . "class='button accept' "
            . "src='" . "img/buttons/friend.png" . "'>"
            . "<span>Accept Request</span>"
            . "</a>"
            . "<a "
            . "title='Deny Request' "
            . "alt='Deny Request' "
            . "href='" . "messages/friend-request-deny/" . urlencode($message->from) . "'>"
            . "<img "
            . "class='button deny' "
            . "src='" . "img/buttons/friend.png" . "'>"
            . "<span>Deny Request</span>"
            . "</a>";

        $message->body_bb = $new_friend_message;
    }

}
print PHP_EOL;

///////////////////////////////////////////////////
//Outbox


echo "Migrate Site User Messages Outbox... ";
$migrate_site_user_messages_outbox = include "site_user_messages_outbox.php";
$result = $conn_tawny->query($migrate_site_user_messages_outbox);

while ($row = $result->fetch_assoc()) {
    $myMessagesArrayOutbox[] = $row;
}

if ($result) {
    echo "Fetched Outbox Messages...OK\n";
} else {
    echo "FAILED ... Migration halted...\n";
    exit();
}

$my_messages_outbox_json = json_encode($myMessagesArrayOutbox);
$decoded_messsages_json_outbox = json_decode($my_messages_outbox_json);

//TODO - FOR EACH MESSAGE CREATE ONE IN THE OUTBOX

echo "Processing outbox... ";
foreach ($decoded_messsages_json_outbox as $message) {
    //get To user id and set
    $result = $conn_twelfthman->query("SELECT id FROM site_user WHERE login_id = '" . $message->to . "'");
    $to_user = $result->fetch_assoc();
    $message->to_site_user_id = $to_user["id"];

    //get From user id and set
    $result = $conn_twelfthman->query("SELECT id FROM site_user WHERE login_id = '" . $message->from . "'");
    $from_user = $result->fetch_assoc();
    $message->from_site_user_id = $from_user["id"];

    //Make plaintext from the bbcode
    $text = $message->body_bb;
    $embed_stripped = preg_replace('/\[embed\].*\[\/embed\]/', '', $text);
    $youtube_stripped = preg_replace('/\[youtube\].*\[\/youtube\]/', '', $embed_stripped);
    $video_stripped = preg_replace('/\[video\].*\[\/video\]/', '', $youtube_stripped);
    $no_bb = preg_replace('/\[.*?]/', '', $video_stripped); //strip bb codes
    $result = preg_replace('/\<.*?\>(.*)\<\/.*\>/', '', $no_bb); // strip html content
    $message->body_plaintext = $result;

    $message->is_copy = 1;


    //Lower case everything
    $string = strtolower($message->subject);
    //Make alphanumeric (removes all other characters)
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    //Clean up multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s_]/", "-", $string);
    $message->uri = $string . "-" . uniqid();
}
///////////////////////////////////////////////////
//MONGODB BIT


//Prep inbox file the import into mongodb
echo "Export Messages to mongo database\n";

echo "dumping inbox to json file...";
$file_result = file_put_contents(
    'dump_mongo/site_user_messages_inbox.json',
    json_encode($decoded_messsages_json_inbox, JSON_PRETTY_PRINT)
);

echo "importing inbox json to mongodb";
$import_site_user_message_inbox_json_to_mongo =
    "mongoimport --db twelfthman --upsert --jsonArray --collection site_user_message "
    . "< dump_mongo/site_user_messages_inbox.json";
echo `$import_site_user_message_inbox_json_to_mongo`;

//Prep outbox file the import into mongodb
echo "dumping outbox to json file...";
file_put_contents(
    'dump_mongo/site_user_messages_outbox.json',
    json_encode($decoded_messsages_json_outbox, JSON_PRETTY_PRINT)
);

echo "importing outbox json to mongodb";
$import_site_user_message_outbox_json_to_mongo =
    "mongoimport --db twelfthman --upsert --jsonArray --collection site_user_message "
    . "< dump_mongo/site_user_messages_outbox.json";
echo `$import_site_user_message_outbox_json_to_mongo`;
