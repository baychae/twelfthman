<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 22/06/2017
 * Time: 15:30
 */

define("SOURCE_DB", "zadmin_tawny_test");
define("TARGET_DB", "dev_twelfthman_test");

define("PUBLIC_IMGS", "/Users/mrsinclair/Sites/twelfthman/public/img/");

define("DUMP_USER_IMGS", PUBLIC_IMGS . "user-avatars/");
define("DUMP_USER_TEAM_IMGS", PUBLIC_IMGS . "teams/");
define("DUMP_NEWS_IMGS", PUBLIC_IMGS . "news/articles/");
define("DUMP_ACHIEVEMENT_TYPES", PUBLIC_IMGS . "user-achievements/");
define("MYSQL_DIR_PERMISSIONS", "mrsinclair:staff");

$source_db_name = 'zadmin_tawny_test';
$source_db_username = 'root';

$target_db = "dev_twelfthman_test";
$target_db_username = 'root';

$conn_tawny = mysqli_init();
//mysqli_options($conn_tawny, MYSQLI_SET_CHARSET_NAME, 'utf8');


if (!$conn_tawny->real_connect('localhost', 'root', 'root', $source_db_name)) {
    echo "Error Tawny: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}
//mysqli_set_charset('latin1', $conn_tawny);

$conn_twelfthman = mysqli_init();
//mysqli_options($conn_twelfthman, MYSQLI_SET_CHARSET_NAME, 'utf8');


if (!$conn_twelfthman->real_connect('localhost', 'root', 'root', $target_db)) {
    echo "Error Twelfthman: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}
//mysqli_set_charset('utf8', $conn_twelfthman);

$conn_root = mysqli_init();
//mysqli_options($conn_root, MYSQLI_SET_CHARSET_NAME, 'utf8');

if (!$conn_root->real_connect('localhost', 'root', 'root', "")) {
    echo "Error Twelfthman: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

$conn_root->real_query('GRANT FILE ON *.* TO \'root\'@\'localhost\';');

echo "\nRemoving Achievement Type images...\n";
$delete_achievement_types = 'find ' . realpath('../public/img/user-achievements/') . ' -type f -delete';
echo exec($delete_achievement_types);

echo "\nDELETING SITE USERS ACHIEVEMENT TYPES... ";
$delete_site_users_achievement_type = "DELETE FROM site_user_achievement_type where id > 0";
$result = $conn_twelfthman->query($delete_site_users_achievement_type);

if ($result) {
    echo "OK\n";
} else {
    echo "FAILED\n";
    exit();
}


$result = $conn_tawny->real_query("DROP PROCEDURE IF EXISTS `dump_user_achievement_images`;");
if ($result) {
    echo "Dropped Stored Procedure: dump_user_achievement_images OK\n";
} else {
    echo "FAILED to drop dump_user_images... Migration halted...\n" . $conn_tawny->error . "\n";
    exit();
}

include "CREATE_DUMP_ACHIEVEMENT_TYPES.php";

$result = $conn_tawny->real_query("call dump_user_achievement_images();");

if ($result) {
    echo "OK\n";
} else {
    echo "FAILED ... dump_user_achievement_images..." . $conn_tawny->error . PHP_EOL;
    exit();
}

$conn_twelfthman->close();
$conn_tawny->close();