<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 22/03/2016
 * Time: 17:04
 */

return "SELECT 
" . SOURCE_DB . ".core_user_message.message_sender_id as 'from',
" . SOURCE_DB . ".core_user_message.message_title as 'subject',
" . SOURCE_DB . ".core_user_message.message_contents as 'body_bb',
'A' as 'status',
" . SOURCE_DB . ".core_user_message.message_timestamp as 'created',
" . SOURCE_DB . ".core_user_message.message_timestamp as 'modified',
" . SOURCE_DB . ".core_user_message.message_id as 'this_message_id',
IF(
(SELECT COUNT(*) 
 FROM " . SOURCE_DB . ".core_user_message 
 join " . SOURCE_DB . ".core_user_message_alert on " . SOURCE_DB . ".core_user_message_alert.message_id = " . SOURCE_DB . ".core_user_message.message_id
 WHERE " . SOURCE_DB . ".core_user_message_alert.message_id = this_message_id) > 1,\"All Users\",
 (SELECT distinct user_id 
 FROM " . SOURCE_DB . ".core_user_message_alert 
 join " . SOURCE_DB . ".core_user_message on " . SOURCE_DB . ".core_user_message_alert.message_id = " . SOURCE_DB . ".core_user_message.message_id
 WHERE " . SOURCE_DB . ".core_user_message_alert.message_id = this_message_id)
 ) as 'to'
FROM " . SOURCE_DB . ".core_user_message
where " . SOURCE_DB . ".core_user_message.message_id in(
select message_id from " . SOURCE_DB . ".core_user_message_alert
)";