// "use strict";
Error.stackTraceLimit = Infinity;

var TARGET_DB = "dev_twelfthman_test";

var striptags = require('striptags'); //For extracting plaintext
var each = require('async-each-series'); // Used for async each qurey https://www.npmjs.com/package/async-each-series

var mysql = require('mysql');
var news_articles = [];
var q = 'SELECT * FROM ' + TARGET_DB + '.news_article ' +
    'where content_plaintext is null ' +
    'and content_html is null ';
var connection = mysql.createConnection({host: 'localhost', user: 'root', password: 'root', database: TARGET_DB});
connection.connect();
var query = connection.query(q);

query
    .on('error', function (err) {
        // Handle error, an 'end' event will be emitted after this as well
        console.log("There has been a problem: " + err)
        connection.end();
    })
    .on('fields', function (fields) {
        // the field packets for the rows to follow
        console.log("NODE SCRIPT==================CONVERT NEWS ARTICLE BBCODE TO HTML");
    })
    .on('result', function (row) {
        // Pausing the connnection is useful if your processing involves I/O
        connection.pause();

        news_articles.push(row);

        process.stdout.write(".");
        connection.resume();
    })
    .on('end', function () {
        // all rows have been received
        console.log("Fetched");
        connection.end();

        var cwd = process.cwd();

        var html_markup = '<!DOCTYPE html>' +
            '<html lang="en">' +
            '<head>' +
            '<title>News</title>' +
            '<script defer> requirejs(["entry-module"], function () { window.onModulesLoaded(); }); </script>' +
            '<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" async></script>' +
            '<link rel="stylesheet" type="text/css" href="http://localhost/twelfthman/js/wysibb/theme/twelfthman/twelfthman.css"/>' +
            '<script type="text/javascript" src="'+cwd+'/../public/js/wysibb/jquery.wysibb.js" async></script>' +
            '<script type="text/javascript" src="'+cwd+'/../public/js/wysibb-options.js" defer></script>' +
            '<script type="text/javascript" src="' + cwd + '/../public/js/wysibb-image-helper.js" defer></script>' +
            '<script type="text/javascript" src="'+cwd+'/../public/js/jquery.cookie.js"></script>' +
            '<script type="text/javascript" src="'+cwd+'/../public/js/jquery.scroller.js"></script>' +
            '</head>' +
            '<body>' +
            '<textarea id="editor"  style="width:100%"></textarea><input type="hidden" name="content_html" id="content_html" value="">' +
            '</body>' +
            '</html>';


        const jsdom = require("jsdom");
        var window = jsdom.jsdom(html_markup).defaultView;

        window.document.onload = function () {

            console.log("Processing articles: ");
            each(news_articles, function(el, next) {
                setTimeout(function () {
                    // console.log(el.content_bb);
                    window.$("#editor").val(el.content_bb);
                    var x = window.$("#editor").htmlcode();
                    x = x.replace(/www.youtube.com\/embed\/https:\/\/youtu\.be\/([A-Za-z0-9]*)"/g, "www.youtube.com/embed/$1" + '"');
                    // console.log( entities.decode(encoded_x) );

                    process.stdout.write(".");

                    var connection2 = mysql.createConnection({
                        host: 'localhost',
                        user: 'root',
                        password: 'root',
                        database: TARGET_DB
                    });

                    connection2.connect();
                    var query = connection2.query("UPDATE news_article SET content_html = " + connection2.escape(x) + ", " +
                                                    "content_plaintext = " + connection2.escape(striptags(x)) +
                                                    " WHERE id = " + el.id );
                    connection2.end();

                    next(); // Roll onto next item with callback
                },  1);

            }, function (err) {
                console.log('Finished Processing ' + news_articles.length +' articles.');
            });

        };


        // window.$("#editor").html(row.content_bb);
        // console.log(arg1.$("#editor").html());

        // if (document.getElementById("article-feature-image")) {
        //     var article_feature_image = document.getElementById("article-feature-image");
        //     article_feature_image.parentNode.removeChild(article_feature_image);
        // }


        // window.close();
        // delete window;


    });


