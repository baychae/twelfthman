<?php

$create_dump_user_images_sql = 'CREATE DEFINER=`root`@`localhost` PROCEDURE `dump_news_images`()
BEGIN

	DECLARE article_id INT;
    DECLARE done BOOLEAN;
    DECLARE image_name VARCHAR(250);
    DECLARE image_name_no_whitespace VARCHAR(250);
    DECLARE image_name_no_whitespace_dupe VARCHAR(250);
    DECLARE image_name_no_whitespace_dupe_concat VARCHAR(250);
    DECLARE new_image_name VARCHAR(250);
    DECLARE new_image_file_type VARCHAR(250);
    DECLARE image_ID INT;
    DECLARE duplicate_key INT DEFAULT 0;
	
    DECLARE cur1 CURSOR FOR 
    select news_data.news_id, news_data.news_image_name 
	from ' . SOURCE_DB . '.news_data 
    where ' . SOURCE_DB . '.news_data.news_image_type != \'text/html\'
	and ' . SOURCE_DB . '.news_data.news_image_type !=\'application/octet-stream\'
	and ' . SOURCE_DB . '.news_data.news_image is not null
	and news_data.news_image_name is not null;
    
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN cur1;
    
    read_loop: loop
    
		FETCH cur1 INTO image_id, image_name;
        set duplicate_key = 0;
        
        BEGIN
			DECLARE EXIT HANDLER FOR 1086 SET duplicate_key = 1;
            
            set image_name_no_whitespace = REPLACE(image_name, \' \', \'\');

			SET @query = CONCAT(
			"SELECT news_data.news_image FROM news_data WHERE news_data.news_id = ",image_id," 
			INTO DUMPFILE \'/' . DUMP_NEWS_IMGS . '",image_name_no_whitespace,"\'");
			PREPARE statement FROM @query;
			EXECUTE statement;
            
            update news_data
			set news_data.news_image_name = image_name_no_whitespace
			WHERE news_data.news_id = image_id;
            
        END;
        
        if duplicate_key = 1 then
        
		-- set new image name here
        set image_name_no_whitespace_dupe = REPLACE(image_name, \' \', \'\');
        set new_image_file_type = SUBSTRING_INDEX(image_name, \'.\', -2);
		set image_name_no_whitespace_dupe_concat = CONCAT("dupe_",FLOOR(1 + (RAND() * (100000-1)) ),".", new_image_file_type);
        
        
        update news_data
        set news_data.news_image_name = image_name_no_whitespace_dupe_concat
        WHERE news_data.news_id = image_id;
        
        
         SET @query = CONCAT(
			"SELECT news_data.news_image FROM news_data WHERE news_data.news_id = ",image_id," 
			INTO DUMPFILE \'/' . DUMP_NEWS_IMGS . '",image_name_no_whitespace_dupe_concat,"\'");
			PREPARE statement FROM @query;
            EXECUTE statement;
            
        END IF;
        
        IF done THEN
		LEAVE read_loop;
                
    END IF;
     
    END LOOP;

	CLOSE cur1;

END';

$result = $conn_tawny->real_query("DROP PROCEDURE IF EXISTS `dump_news_images`;");
if ($result) {
    echo "Dropped Stored Procedure: dump_news_images OK\n";
} else {
    echo "FAILED to drop dump_news_images... Migration halted...\n" . $conn_tawny->error . "\n";
    exit();
}

$result = $conn_tawny->real_query($create_dump_user_images_sql);

if ($result) {
    echo "Stored Procedure dump_news_images installed OK\n";
} else {
    echo "FAILED ... Migration halted...\n" . $conn_tawny->error . "\n";
    exit();
}
