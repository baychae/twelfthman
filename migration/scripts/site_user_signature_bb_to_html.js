// "use strict";
Error.stackTraceLimit = Infinity;

var striptags = require('striptags'); //For extracting plaintext
var each = require('async-each-series'); // Used for async each qurey https://www.npmjs.com/package/async-each-series

var TARGET_DB = 'dev_twelfthman_test';

var mysql = require('mysql');
var site_users = [];
var q = 'SELECT * FROM ' + TARGET_DB + '.site_user ' +
    'where signature_bb <> "" ';
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: TARGET_DB
});
connection.connect();
var query = connection.query(q);

query
    .on('error', function (err) {
        // Handle error, an 'end' event will be emitted after this as well
        console.log("There has been a problem: " + err)
        connection.end();
    })
    .on('fields', function (fields) {
        // the field packets for the rows to follow
        console.log("NODE SCRIPT==================CONVERT USERS SIGNATURE BBCODE TO HTML");
    })
    .on('result', function (row) {
        // Pausing the connnection is useful if your processing involves I/O
        connection.pause();

        site_users.push(row);

        process.stdout.write(".");
        connection.resume();
    })
    .on('end', function () {
        // all rows have been received
        console.log("Fetched");
        connection.end();

        var cwd = process.cwd();

        var html_markup = '<!DOCTYPE html>' +
            '<html lang="en">' +
            '<head>' +
            '<title>News</title>' +
            '<script defer> requirejs(["entry-module"], function () { window.onModulesLoaded(); }); </script>' +
            '<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" async></script>' +
            '<link rel="stylesheet" type="text/css" href="http://localhost/twelfthman/js/wysibb/theme/twelfthman/twelfthman.css"/>' +
            '<script type="text/javascript" src="' + cwd + '/../public/js/wysibb/jquery.wysibb.js" async></script>' +
            '<script type="text/javascript" src="' + cwd + '/../public/js/wysibb-options.js" defer></script>' +
            '<script type="text/javascript" src="' + cwd + '/../public/js/wysibb-image-helper.js" defer></script>' +
            '<script type="text/javascript" src="' + cwd + '/../public/js/jquery.cookie.js"></script>' +
            '<script type="text/javascript" src="' + cwd + '/../public/js/jquery.scroller.js"></script>' +
            '</head>' +
            '<body>' +
            '<textarea id="editor"  style="width:100%"></textarea><input type="hidden" name="content_html" id="content_html" value="">' +
            '</body>' +
            '</html>';


        const jsdom = require("jsdom");
        var window = jsdom.jsdom(html_markup).defaultView;

        window.document.onload = function () {

            console.log("Processing site user signatures: ");
            each(site_users, function (el, next) {
                setTimeout(function () {
                    // console.log(el.content_bb);
                    window.$("#editor").val(el.signature_bb);
                    var x = window.$("#editor").htmlcode();

                    // console.log( entities.decode(encoded_x) );

                    process.stdout.write(".");

                    var connection2 = mysql.createConnection({
                        host: 'localhost',
                        user: 'root',
                        password: 'root',
                        database: TARGET_DB
                    });

                    connection2.connect();
                    var query = connection2.query("UPDATE site_user SET signature_html = " + connection2.escape(x) +
                        " WHERE id = " + el.id);
                    connection2.end();

                    next(); // Roll onto next item with callback
                }, 1);

            }, function (err) {
                console.log('Finished Processing ' + site_users.length + ' signatures.');
            });

        };


    });


