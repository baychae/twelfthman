<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 22/03/2016
 * Time: 17:04
 */

return "SELECT  core_user_message_alert.user_id as 'to',
core_user_message.message_sender_id as 'from',
core_user_message.message_title as 'subject',
core_user_message.message_contents as body_bb,
core_user_message_alert.message_read as recipient_read,
IF(core_user_message_alert.message_deleted = 1, 'D', 'A') as 'status',
core_user_message.message_timestamp as created,
core_user_message.message_timestamp as modified
FROM core_user_message
join core_user_message_alert on core_user_message_alert.message_id = core_user_message.message_id";