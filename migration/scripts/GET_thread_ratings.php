<?php
return "SELECT
" . TARGET_DB . ".forum_topic.forum_id as forum_id,
" . TARGET_DB . ".forum_topic.id  as topic_id,
group_concat(distinct " . TARGET_DB . ".site_user.id)  as users
FROM " . SOURCE_DB . ".forum_thread_rating
join " . TARGET_DB . ".site_user on " . SOURCE_DB . ".forum_thread_rating.user_id = " . TARGET_DB . ".site_user.login_id
join " . SOURCE_DB . ".forum_thread on " . SOURCE_DB . ".forum_thread.thread_id = " . SOURCE_DB . ".forum_thread_rating.thread_id
join " . TARGET_DB . ".forum_topic on " . TARGET_DB . ".forum_topic.title = " . SOURCE_DB . ".forum_thread.thread_name
group by " . SOURCE_DB . ".forum_thread.thread_name
order by 1 desc";
