<?php

define("SOURCE_DB", "zadmin_tawny_test");
define("TARGET_DB", "dev_twelfthman_test");

define("PUBLIC_IMGS", "/Users/mrsinclair/Sites/twelfthman/public/img/");

define("DUMP_USER_IMGS", PUBLIC_IMGS . "user-avatars/");
define("DUMP_USER_TEAM_IMGS", PUBLIC_IMGS . "teams/");
define("DUMP_NEWS_IMGS", PUBLIC_IMGS . "news/articles/");
define("DUMP_ACHIEVEMENT_TYPES", PUBLIC_IMGS . "user-achievements");
define("MYSQL_DIR_PERMISSIONS", "mrsinclair:staff");

$chown_user_imgs = 'sudo chown -R ' . MYSQL_DIR_PERMISSIONS . ' ' . DUMP_USER_IMGS;
$output = shell_exec($chown_user_imgs);
echo "$output";

$chown_user_imgs = 'sudo chown -R ' . MYSQL_DIR_PERMISSIONS . ' ' . DUMP_USER_TEAM_IMGS;
$output = shell_exec($chown_user_imgs);
echo "$output";

$chown_user_imgs = 'sudo chown -R ' . MYSQL_DIR_PERMISSIONS . ' ' . DUMP_NEWS_IMGS;
$output = shell_exec($chown_user_imgs);
echo "$output";

$db_servername = '127.0.0.1';


$source_db_name = 'zadmin_tawny_test';
$source_db_username = 'root';

$target_db = "dev_twelfthman_test";
$target_db_username = 'root';

$mongo = new \Phalcon\Db\Adapter\MongoDB\Client();
$mongodb = $mongo->selectDatabase('twelfthman');


$conn_tawny = mysqli_init();
//mysqli_options($conn_tawny, MYSQLI_SET_CHARSET_NAME, 'utf8');


if (!$conn_tawny->real_connect('localhost', 'root', 'root', $source_db_name)) {
    echo "Error Tawny: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}
//mysqli_set_charset('latin1', $conn_tawny);

$conn_twelfthman = mysqli_init();
//mysqli_options($conn_twelfthman, MYSQLI_SET_CHARSET_NAME, 'utf8');


if (!$conn_twelfthman->real_connect('localhost', 'root', 'root', $target_db)) {
    echo "Error Twelfthman: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}
//mysqli_set_charset('utf8', $conn_twelfthman);

$conn_root = mysqli_init();
//mysqli_options($conn_root, MYSQLI_SET_CHARSET_NAME, 'utf8');

if (!$conn_root->real_connect('localhost', 'root', 'root', "")) {
    echo "Error Twelfthman: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

$conn_root->real_query('GRANT FILE ON *.* TO \'root\'@\'localhost\';');

echo("any error: " . $conn_root->error . "\n");