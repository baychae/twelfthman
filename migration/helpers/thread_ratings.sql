SELECT  
dev_twelfthman.forum_topic.forum_id as forum_id,
dev_twelfthman.forum_topic.id  as topic_id, 
group_concat(distinct dev_twelfthman.site_user.id)  as users
FROM zadmin_tawny.forum_thread_rating
join dev_twelfthman.site_user on zadmin_tawny.forum_thread_rating.user_id = dev_twelfthman.site_user.login_id
join zadmin_tawny.forum_thread on zadmin_tawny.forum_thread.thread_id = zadmin_tawny.forum_thread_rating.thread_id
join dev_twelfthman.forum_topic on dev_twelfthman.forum_topic.title = zadmin_tawny.forum_thread.thread_name
group by zadmin_tawny.forum_thread.thread_name
order by 1 desc
