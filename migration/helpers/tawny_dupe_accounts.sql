

select zadmin_tawny.core_user.user_id, zadmin_tawny.core_user.user_email
from zadmin_tawny.core_user
where zadmin_tawny.core_user.user_email
in (

	select zadmin_tawny.core_user.user_email
	from zadmin_tawny.core_user
	where zadmin_tawny.core_user.user_email != ""
	group by zadmin_tawny.core_user.user_email 
	HAVING count(zadmin_tawny.core_user.user_id) > 1

)
order by 2 asc