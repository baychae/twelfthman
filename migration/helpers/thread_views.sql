-- IN MySqldeveloper use this query then export to json file
-- Using Sublime Text 2 Find replace with the the following regex. Find: 'users' : "(.*)”, Replace: 'users' : [$1]
-- Then import into mongo db with the following command: mongoimport --db twelfthman --upsert --jsonArray --collection topic_view_counter_stats < thread_views.json 

SELECT 
dev_twelfthman.forum_topic.forum_id as forum_id, 
dev_twelfthman.forum_topic.id as topic_id,
date_format(CURDATE(), "%Y-%m-%d %T.000") as created,
date_format(CURDATE(), "%Y-%m-%d %T.000") as modified,  
group_concat(dev_twelfthman.site_user.id) as users
FROM zadmin_tawny.forum_thread_user
join dev_twelfthman.site_user on zadmin_tawny.forum_thread_user.user_id = dev_twelfthman.site_user.login_id
join zadmin_tawny.forum_thread on zadmin_tawny.forum_thread.thread_id = zadmin_tawny.forum_thread_user.thread_id
join dev_twelfthman.forum_topic on zadmin_tawny.forum_thread.thread_name = dev_twelfthman.forum_topic.title
group by zadmin_tawny.forum_thread_user.thread_id
