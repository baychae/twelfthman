

select zadmin_tawny.core_user.user_id, zadmin_tawny.core_user.user_email, zadmin_tawny.forum_post.post_date
from zadmin_tawny.core_user
join zadmin_tawny.forum_post on zadmin_tawny.forum_post.user_id = zadmin_tawny.core_user.user_id
where zadmin_tawny.core_user.user_email
in (

	select zadmin_tawny.core_user.user_email
	from zadmin_tawny.core_user
	where zadmin_tawny.core_user.user_email != ""
	group by zadmin_tawny.core_user.user_email 
	HAVING count(zadmin_tawny.core_user.user_id) > 1

)
group by zadmin_tawny.core_user.user_id
having max(zadmin_tawny.forum_post.post_date)
order by 2 asc, zadmin_tawny.forum_post.post_date desc