<?php

$target_db = "twelfthman";

define("HOME_DIR", realpath('.') . '/');


////////////////////
/* Migrate Forum Topic View Counters
*
*/

echo "Migrate topic view counters... ";
$migrate_thread_views_sql = include HOME_DIR . "thread_views.php";
$result = $conn_tawny->query($migrate_thread_views_sql);

while($row = $result->fetch_assoc()) {
$thread_views_Array[] = $row;
}

$thread_views_encoded_json = json_encode($thread_views_Array);
$thread_views_decoded_json = json_decode($thread_views_encoded_json);

foreach($thread_views_decoded_json as $row) {
    $row->forum_id = intval($row->forum_id);
    $row->topic_id = intval($row->topic_id);
    $row->users = array_map('intval', explode(",", $row->users));
}

$thread_views_reencoded_json = json_encode($thread_views_decoded_json, JSON_PRETTY_PRINT);

file_put_contents('topic_views_export.json', $thread_views_reencoded_json);

$import_topic_ratings_json_to_mongo = "mongoimport --db twelfthman --upsert --jsonArray --collection topic_view_counter_stats < topic_views_export.json";
echo `$import_topic_ratings_json_to_mongo`;