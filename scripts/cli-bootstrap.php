<?php

ini_set('display_errors', 1);
define("SITE_ROOT", realpath('..') . '/');

try {

    include SITE_ROOT . 'vendor/autoload.php';

    /**
     * Include loader
     */
    require_once SITE_ROOT . 'apps/config/loader.php';

    /**
     * Include services
     */
    require_once SITE_ROOT . 'apps/config/services.php';

    /**
     * Create the application handler
     */
    $application = new \Phalcon\Mvc\Application();

    /**
     * Assign the DI provided by services.php
     */
    $application->setDI($di);


} catch (Phalcon\Exception $e) {
    echo $e->getMessage();
} catch (PDOException $e) {
    echo $e->getMessage();
}