<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 02/06/2017
 * Time: 12:19
 */

require 'cli-bootstrap.php';

class CronExamplesTask extends Phalcon\DI\Injectable
{
    public function run()
    {
        $file_names = `grep -rl *.php .`;
        $file_names = str_replace("\n", '', $file_names);
        $files = array_filter(explode("./", rtrim($file_names)));

        $index = array_search("cli-bootstrap.php", $files);
        unset($files[$index]);

        $index = array_search("php_errors.log", $files);
        unset($files[$index]);

        $index = array_search(basename($_SERVER['PHP_SELF']), $files);
        unset($files[$index]);

        echo "SCRIPTS GENERATED. Execute chmod +x *.sh to activate files. EXAMPLE CRON VALUES" . PHP_EOL;

        $php_command = `which php`;
        $php_command = str_replace("\n", '', $php_command);

        foreach ($files as $file) {

            $file_name = str_replace(".php", "", $file);

            $logfile = SITE_ROOT . "scripts/log/" . $file_name . ".log";
            $scriptfile = SITE_ROOT . "scripts/" . $file;

            $script = "#!/bin/sh" . PHP_EOL
                . "if ps -ef | grep -v grep | grep " . $file . " ; then" . PHP_EOL
                . "exit 0" . PHP_EOL
                . "else" . PHP_EOL
                . $php_command . " " . $scriptfile . " > " . $logfile . " &" . PHP_EOL
                . "exit 0" . PHP_EOL
                . "fi;" . PHP_EOL;


            file_put_contents($file_name . ".sh", $script);
            touch("log/" . $file_name . ".log");

            $exe_dir = SITE_ROOT . "scripts";
            echo "5 * * * *  cd " . $exe_dir . " && ./" . $file_name . ".sh > " . $logfile . " 2>&1" . PHP_EOL;

        }

    }
}

try {
    $task = new CronExamplesTask($config);
    $task->run();
} catch (Exception $e) {
    echo $e->getMessage(), PHP_EOL;
    echo $e->getTraceAsString();
}