<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 26/05/2017
 * Time: 12:24
 */


require 'cli-bootstrap.php';

class SolrForumSearchDihTask extends Phalcon\DI\Injectable
{
    public function run()
    {
        $updater = new \Common\Library\Solr\UpdateDih();
        $updater->update();
    }
}

try {
    $task = new SolrForumSearchDihTask($config);
    $task->run();
} catch (Exception $e) {
    echo $e->getMessage(), PHP_EOL;
    echo $e->getTraceAsString();
}


