<?php
/**
 * User: Richard Sinclair
 * Date: 04/05/2016
 * Time: 15:47
 */

namespace Multiple\Frontend\Forms\Account;

class MyAccount extends \Phalcon\Forms\Form
{


    public function initialize()
    {

        $first_name_element = new \Phalcon\Forms\Element\Text("first_name");
        $first_name_element->setLabel("<label for='first_name' >First Name: </label>");
        $this->add($first_name_element);

        $last_name_element = new \Phalcon\Forms\Element\Text("last_name");
        $last_name_element->setLabel("<label for='last_name' >Last Name: </label>");
        $this->add($last_name_element);

        $email = new \Phalcon\Forms\Element\Text("email");
        $email->setLabel("<label for='email' >Email Address: </label>");
        $this->add($email);

        $email = new \Phalcon\Forms\Element\Text("avatar_name");
        $email->setLabel("<label for='avatar_name' >Displayed Name: </label>");
        $this->add($email);

        $signature_element = new \Phalcon\Forms\Element\Text("signature");
        $signature_element->setLabel("<label for='signature' >Signature:</label>");
        $this->add($signature_element);

        $avatar = new \Phalcon\Forms\Element\File("upload_avatar", ["id" => "upload-new-image-file", "class" => "upload-new-image-file"]);
        $avatar->setLabel("<label>Avatar: </label>");
        $this->add($avatar);

        $image_upload_overlay = new \Phalcon\Forms\Element\Submit("upload_avatar_overlay", ["id" => "upload-new-image-overlay", "class" => "form-button", "value" => "Upload Avatar"]);
        $this->add($image_upload_overlay);

        $delete_avatar = new \Phalcon\Forms\Element\Submit("delete_avatar", ["id" => "delete-image", "class" => "form-button", "name" => "delete_avatar", "value" => "Delete Avatar"]);
        $this->add($delete_avatar);

        $image_upload_update = new \Phalcon\Forms\Element\Submit("update_avatar", ["value" => "Image Upload Update", "name" => "upload_avatar_action", "class" => "form-button"]);
        $this->add($image_upload_update);

        $update_my_profile_submit = new \Phalcon\Forms\Element\Submit("update_my_profile", ["value" => "Update", "id" => "update_my_account", "name" => "update_profile", "class" => "form-button"]);
        $this->add($update_my_profile_submit);

        $content = new \Phalcon\Forms\Element\TextArea("bbcode_field", array("id" => "editor", "name" => "bbcode_field", "style" => "width:100%"));
        $content->setLabel("<legend>Contents</legend> ");
        $this->add($content);

        $favourite_away_ground = new \Phalcon\Forms\Element\Text("favourite_away_ground");
        $favourite_away_ground->setLabel("<label for='favourite_away' >Favourite Away Ground:</label>");
        $this->add($favourite_away_ground);

        $favourite_all_time_player = new \Phalcon\Forms\Element\Text("favourite_all_time_player");
        $favourite_all_time_player->setLabel("<label for='favourite_all_time_player' >Favourite All Time Player:</label>");
        $this->add($favourite_all_time_player);

        $favourite_current_player = new \Phalcon\Forms\Element\Text("favourite_current_player");
        $favourite_current_player->setLabel("<label for='favourite_current_player' >Favourite Current Player:</label>");
        $this->add($favourite_current_player);

        $content_html = new \Phalcon\Forms\Element\Hidden("content_html");
        $this->add($content_html);

        $team_params = [
            'useRmpty' => false,
            'using' => ['id', 'name']
        ];

        $team = new \Phalcon\Forms\Element\Select('team_id', \Common\Models\Team::find(), $team_params);
        $team->setLabel("<label for='team_id'>Home Team:</label>");
        $this->add($team);
    }

}