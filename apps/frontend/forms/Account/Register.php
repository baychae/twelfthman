<?php
/**
 * Created by PhpStorm.
 * User: Richard Sinclair
 * Date: 08/07/2016
 * Time: 17:30
 */

namespace Multiple\Frontend\Forms\Account;

class Register extends \Phalcon\Forms\Form
{

    public function initialize()
    {

        $password_length_validator = new \Phalcon\Validation\Validator\StringLength([
            'max' => 20,
            'min' => 8,
            'messageMaximum' => 'Woah. How long!!??',
            'messageMinimum' => 'Password needs at least 8 characters'
        ]);

        $email_validator = new \Phalcon\Validation\Validator\Email([
            'message' => "Must be an email! "
        ]);

        $user_name_length_validator = new \Phalcon\Validation\Validator\StringLength([
            'max' => 20,
            'min' => 3,
            'messageMaximum' => 'Woah. How long!!?? Under 20 characters please.',
            'messageMinimum' => 'Your user/login name needs at least 3 characters'
        ]);

        $first_name_element = new \Phalcon\Forms\Element\Text('first_name');
        $first_name_element->setLabel('<label for="first_name" >First Name:</label>');
        $first_name_element->addFilter("string");
        $this->add($first_name_element);

        $last_name_element = new \Phalcon\Forms\Element\Text('last_name');
        $last_name_element->setLabel('<label for="last_name" >Last Name:</label>');
        $last_name_element->addFilter("string");
        $this->add($last_name_element);

        $email = new \Phalcon\Forms\Element\Text('email');
        $email->setLabel('<label for="email" >Email Address:<sup class="required"><strong>*required</strong></sup></label>');
        $email->addFilter("email");
        $email->addValidator(
            new \Phalcon\Validation\Validator\PresenceOf(
                array(
                    'message' => 'The email is required. '
                )
            )
        );
        $email->addValidator($email_validator);
        $this->add($email);

        $user_name = new \Phalcon\Forms\Element\Text('login_id');
        $user_name->setLabel('<label for="login_id">User Name:<sup class="required"><strong>*required</strong></sup></label>');
        $user_name->addFilter("email");
        $user_name->addValidator(
            new \Phalcon\Validation\Validator\PresenceOf(
                array(
                    'message' => 'The User Name is required. '
                )
            )
        );
        $user_name->addValidator($user_name_length_validator);
        $this->add($user_name);

        $password = new \Phalcon\Forms\Element\Password('password');
        $password->setLabel('<label for="password">Password:<sup class="required"><strong>*required</strong></sup></label>');
        $password->addFilter("email");
        $password->addValidator(
            new \Phalcon\Validation\Validator\PresenceOf(
                array(
                    'message' => 'The Password is required. '
                )
            )
        );
        $password->addValidator($password_length_validator);
        $this->add($password);

        $password_repeat = new \Phalcon\Forms\Element\Password('password_repeat');
        $password_repeat->setLabel('<label for="password_repeat">Repeat Password:<sup><strong>*required</strong></sup></label>');
        $password_repeat->addFilter("email");
        $password_repeat->addValidator(
            new \Phalcon\Validation\Validator\PresenceOf(
                array(
                    'message' => 'The Repeat Password is required. '
                )
            )
        );
        $password_repeat->addValidator(new \Phalcon\Validation\Validator\Identical([
                'value' => $password->getValue(),
                'message' => 'Passwords must match. '
            ])

        );
        $this->add($password_repeat);
    }
}
