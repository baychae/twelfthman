<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 31/05/2017
 * Time: 14:26
 */

namespace Multiple\Frontend\Forms\Forum;


use Phalcon\Validation\Validator\PresenceOf;

class ForumSearchForm extends \Phalcon\Forms\Form
{

    public function initialize()
    {

        $search_element = new \Phalcon\Forms\Element\Text("search-term");
        $search_element->setLabel("<label for='title' >Search for: </label>");
        $search_element->setFilters("string");
        $search_element->addValidator(new \Phalcon\Validation\Validator\StringLength(
            [
                "min" => 2,
                "messageMinimum" => "The search term is too short.",
                "max" => 50,
                "messageMaximum" => "The search term is too long."
            ]
        ));
        $this->add($search_element);

        $search_by_topic_title = new \Phalcon\Forms\Element\Check("forum-search-by-title", ["value" => 1]);
        $search_by_topic_title->setLabel("<label for='forum-search-by-title' >Search by Topic Title:</label>");
        $this->add($search_by_topic_title);

        $search_by_topic_content = new \Phalcon\Forms\Element\Check("forum-search-by-content", ["value" => 1]);
        $search_by_topic_content->setLabel("<label for='forum-search-by-title' >Search by Topic Content:</label>");
        $this->add($search_by_topic_content);

        $add_submit = new \Phalcon\Forms\Element\Submit('add', array("value" => "Search", "class" => "form-button"));
        $this->add($add_submit);

    }

}
