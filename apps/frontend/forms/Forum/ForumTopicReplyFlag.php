<?php

namespace Multiple\Frontend\Forms\Forum;

class ForumTopicReplyFlag extends \Phalcon\Forms\Form {

    public function initialize() {
        $reply_content = new \Phalcon\Forms\Element\TextArea("bbcode_field", array("id" => "editor", "name" => "bbcode_field", "style" => "width:100%"));
        $this->add($reply_content);

        $content_html = new \Phalcon\Forms\Element\Hidden("content_html");
        $this->add($content_html);

        $reply_button = new \Phalcon\Forms\Element\Submit("send", array("name" => "action", "value" => "Send", "class" => "form-button"));
        $this->add($reply_button);

        $update_button = new \Phalcon\Forms\Element\Submit("cancel", array("name" => "action", "value" => "Cancel", "class" => "form-button"));
        $this->add($update_button);

    }

}