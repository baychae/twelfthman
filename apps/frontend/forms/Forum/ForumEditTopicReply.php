<?php
/**
 * User: Richard Sinclair
 * Date: 08/10/2015
 * Time: 12:10
 */

namespace Multiple\Frontend\Forms\Forum;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\TextArea,
    Phalcon\Forms\Element\Hidden,
    Phalcon\Forms\Element\Submit;

class ForumEditTopicReply extends Form {

    public function initialize() {
        $reply_content = new TextArea("content_bb", array("id" => "editor", "name" => "bbcode_field", "style" => "width:100%"));
        $this->add($reply_content);

        $content_html = new Hidden("content_html");
        $this->add($content_html);

        $view_types = array("P" =>"Public", "M" => "Members", "A" => "Admin");
        $topic_view_types = new \Phalcon\Forms\Element\Select("view_type", $view_types);
        $topic_view_types->setLabel("<label for='view_type'>Reply Visibity:</label>");
        $this->add($topic_view_types);

        $update_button = new Submit("update_button", array("name" => "action", "value" => "Update", "class" => "form-button"));
        $this->add($update_button);

        $move_button = new Submit("move_button", array("name" => "action", "value" => "Move", "class" => "form-button"));
        $this->add($move_button);

        $delete_button = new \Phalcon\Forms\Element\Submit("delete_button", array("name" => "action", "value" => "Delete", "class" => "form-button"));
        $this->add($delete_button);

    }

}