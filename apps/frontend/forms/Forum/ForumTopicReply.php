<?php

namespace Multiple\Frontend\Forms\Forum;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\TextArea,
    Phalcon\Forms\Element\Hidden,
    Phalcon\Forms\Element\Submit;
      
class ForumTopicReply extends Form {

    public function initialize() {
        $reply_content = new TextArea("bbcode_field", array("id" => "editor", "name" => "bbcode_field", "style" => "width:100%"));
        $this->add($reply_content);
        
        $content_html = new Hidden("content_html");
        $this->add($content_html);
        
        $reply_button = new Submit("reply", array("name" => "action", "value" => "Reply", "class" => "form-button"));
        $this->add($reply_button);
        
        $update_button = new Submit("update", array("name" => "action", "value" => "Update", "class" => "form-button"));
        $this->add($update_button);
        
    }

}
