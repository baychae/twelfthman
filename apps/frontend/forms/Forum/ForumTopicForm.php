<?php

namespace Multiple\Frontend\Forms\Forum;

use Phalcon\Forms\Form,
        Phalcon\Forms\Element\Text,
        Phalcon\Forms\Element\TextArea,
    Phalcon\Forms\Element\Submit;

class ForumTopicForm extends Form {

    public function initialize() {
        
        $title_element = new Text("title");
        $title_element->setLabel("<label for='title' >Title: </label>");
        $this->add($title_element);
        
        $description_element = new Text("description");
        $description_element->setLabel("<label for='description' >Description: </label>");
        $this->add($description_element);
        
        $content_element = new TextArea("bbcode_field", array("id" => "editor", "name" => "bbcode_field", "style" => "width:100%"));
        $content_element->setLabel("<legend>Post #1</legend> ");
        $this->add($content_element);
        
        $content_html = new \Phalcon\Forms\Element\Hidden("content_html");
        $this->add($content_html);
        
        $add_submit = new Submit('add', array("name" => "action", "value" => "Add", "class" => "form-button"));
        $this->add($add_submit);
        
        $modify_submit = new Submit('modify', array("name" => "action", "value" => "Modify", "class" => "form-button"));
        $this->add($modify_submit);
        
    }

}
