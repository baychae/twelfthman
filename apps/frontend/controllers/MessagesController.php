<?php

namespace Multiple\Frontend\Controllers;

use Common\Library\DateTime;
use Phalcon\Exception;

class MessagesController extends ControllerBase
{

    public function initialize()
    {
        $nav_menu = [
            "New Message" => "new-message",
            "Inbox" => "inbox",
            "Outbox" => "outbox",
            "Friends" => "friends",
            "Members" => "members"
        ];
        $this->view->setVar("nav_menu_items", $nav_menu);
    }

    public function indexAction()
    {
    }

    public function inboxAction()
    {
        \Phalcon\Tag::setTitle("Message Inbox");

        $auth = $this->session->get("auth");
        $site_user_login_id = $auth["name"];
        $site_user_email = $auth["email"];
        $site_user_id = \Common\Models\SiteUser::getSiteMemberUserId($site_user_login_id, $site_user_email);

        $messages = \Common\Models\SiteUserMessage::find([
            [
                "to_site_user_id" => $site_user_id,
                "status" => "A", "is_copy" => 0
            ],
            "sort" => [
                "created" => -1
            ]
        ]);

        $paginated_page = \Common\Library\CollectionPaginator::paginate($this, $messages);

        $this->view->setVar("page", $paginated_page);
    }

    public function outboxAction()
    {
        \Phalcon\Tag::setTitle("Message Outbox");

        $auth = $this->session->get("auth");
        $site_user_login_id = $auth["name"];
        $site_user_email = $auth["email"];
        $site_user_id = \Common\Models\SiteUser::getSiteMemberUserId($site_user_login_id, $site_user_email);

        $messages = \Common\Models\SiteUserMessage::find([
            [
                "from_site_user_id" => $site_user_id,
                "status" => "A", "is_copy" => 1
            ],
            "sort" => [
                "created" => -1
            ]
        ]);

        $paginated_page = \Common\Library\CollectionPaginator::paginate($this, $messages);

        $this->view->setVar("page", $paginated_page);
    }

    public function newAction()
    {
        \Phalcon\Tag::setTitle("New Message");

        //If request from the forum page then mail_to will be in the dispatcher
        $referrer = $this->request->getHTTPReferer();

        if (!preg_match('[new-message|reply]', $referrer)) {
            $this->session->remove("new-message-to");
            $this->session->remove("new-message-subject");
            $this->session->remove("new-message-body");
        }

        if ($this->session->get("new-message-to")) {
            $mail_to = $this->session->get("new-message-to");
            $this->session->remove("new-message-to");
        } else {
            $mail_to = $this->request->has("mail_to") ? $this->request->get("mail_to") : false  ;
        }

        if ($this->session->get("new-message-subject")) {
            $subject = $this->session->get("new-message-subject");
            $this->session->remove("new-message-subject");
        } else {
            $subject = null;
        }

        if ($this->session->get("new-message-body")) {
            $body_bb = $this->session->get("new-message-body");
            $this->session->remove("new-message-body");
        } else {
            $body_bb = null;
        }

        $this->view->setVar("subject", $subject);
        $this->view->setVar("mail_to", $mail_to);
        $this->view->setVar("body_bb", $body_bb);
    }

    public function sendAction()
    {
        $auth = $this->session->get('auth');

        $new_message = new \Common\Models\SiteUserMessage();
        $new_message->to = $this->request->getPost("to", "string");
        $new_message->from = $this->filter->sanitize($auth["name"], "string");
        $new_message->subject = $this->request->getPost("subject", "string");
        $new_message->body_bb = $this->request->getPost("body"); // TODO - Sanitize BB content
        $new_message->body_plaintext = $this->request->getPost("body", "stripBbTagsHtmlContent");
        $new_message->uri = $this->request->getPost("subject", "seoUrl");
        $success = $new_message->send();

        if ($success) {
            $this->flashSession->success("Your message has been sent");
            $this->response->redirect("/messages/inbox");
        } else {
            foreach ($new_message->getMessages() as $message) {
                $this->flashSession->error($message);
                $this->response->redirect("/messages/new-message");
            }
        }
    }

    public function viewAction()
    {
        $this->assets->addJs("js/wysibb/jquery.wysibb.js");
        $this->assets->addJs("js/wysibb-options.js");

        $uri = $this->dispatcher->getParam("uri", "string");

        $message = \Common\Models\SiteUserMessage::findFirst([["uri" => $uri]]);

        $this->view->setVar("message", $message);
    }

    public function replyAction()
    {
        $this->assets->addJs("js/wysibb/jquery.wysibb.js");
        $this->assets->addJs("js/wysibb-options.js");

        $uri = $this->dispatcher->getParam("uri", "string");

        $message = \Common\Models\SiteUserMessage::findFirst([["uri" => $uri]]);

        $this->view->setVar("message", $message);
    }

    public function deleteAction()
    {
        $uri = $this->dispatcher->getParam("uri", "string");
        $message = \Common\Models\SiteUserMessage::findFirst([["uri" => $uri]]);

        if ($message->delete() == false) {
            foreach ($message->getMessages() as $message) {
                $this->flashSession->error($message);
                $this->response->redirect("/messages/view/".$uri);
            }
        } else {
            $this->flashSession->success("Message deleted!");
            $this->response->redirect("/messages/inbox");
        }
    }

    public function membersAction()
    {
        return $this->response->redirect("members");
    }

    public function friendsAction()
    {
        #TODO
    }

    public function friendRequestAction()
    {
        $member_name = $this->request->get("friend", "safeChars");
        $member_ident = $this->filter->sanitize($member_name, "string");

        //Get the user by avatar or login_id
        $conditions = "if(avatar_name is null, login_id, avatar_name) = :ident:";
        $params = ["ident" => $member_ident];

        $member = \Common\Models\SiteUser::findFirst(
            [
                $conditions,
                "bind" => $params
            ]
        );

        if (!isset($member) == true) {              //If no user then return error that this user is not in the database
            $this->flashSession->success(
                "We could not find the member to make the request please contact "
                . $this->config->frontend->admin->contact
            );
            return $this->response->redirect($this->request->getHTTPReferer());
        } else {                                    //If the user is found then
            // get the current user from session
            $auth = $this->session->get("auth");
            $email = $this->filter->sanitize($auth["email"], "email");
            $conditions = "email = :email_ident: ";
            $parameters = ["email_ident" => $email];
            $logged_in_user = \Common\Models\SiteUser::findFirst([$conditions, "bind" => $parameters]);
            $logged_in_user_avatar = isset($logged_in_user->avatar_name)
                ? $logged_in_user->avatar_name : $logged_in_user->login_id;

            //Check that request has not been made within the last week
            $site_user_friend_provisional = \Common\Models\SiteUserFriend::findFirst(
                [
                    "site_user_id = " . $logged_in_user->id
                ]
            );

            if (!isset($site_user_friend_provisional->id) == true) {
                $site_user_friend_provisional = new \Common\Models\SiteUserFriend();
            }

            if (isset($site_user_friend_provisional->confirmed)) { //Check to see if already a friend
                if ($site_user_friend_provisional->confirmed == 1) {
                    $this->flashSession->warning("This member is already a friend!");
                    return $this->response->redirect($this->request->getHTTPReferer());
                }
            }

            if (isset($site_user_friend_provisional->modified)) {   //Check that within a week
                $timezone = new \DateTimeZone("Europe/London");
                $last_week = new \DateTime("7 days ago", $timezone);

                $request_last_sent = new \DateTime($site_user_friend_provisional->modified, $timezone);
                $less_than_week_old = $last_week->diff($request_last_sent)->invert ? false : true;

                if ($less_than_week_old) {
                    $this->flashSession->warning("You already have an outstanding friend request!");
                    return $this->response->redirect($this->request->getHTTPReferer());
                }

            }

            $site_user_friend_provisional->site_user_id = $logged_in_user->id;
            $site_user_friend_provisional->friend_site_user_id = $member->id;
            $site_user_friend_provisional->notified = 1;
            $site_user_friend_provisional->confirmed = 0;

            $friend_request_body =
                "The user [b]"
                . $logged_in_user_avatar
                . "[/b] has requested you to be their friend. Please confirm or deny this request. "
                . PHP_EOL
                . "<a "
                . "title='Accept Request' "
                . "alt='Accept Request' "
                . "href='" . $this->url->get(
                    "messages/friend-request-accept/"
                    . $this->escaper->escapeUrl($logged_in_user_avatar)
                )
                . "'>"
                . "<img "
                . "class='button accept' "
                . "src='" . $this->url->get("img/buttons/friend.png") . "'>"
                . "<span>Accept Request</span>"
                . "</a>"
                . "<a "
                . "title='Deny Request' "
                . "alt='Deny Request' "
                . "href='" . $this->url->get(
                    "messages/friend-request-deny/"
                    . $this->escaper->escapeUrl($logged_in_user_avatar)
                )
                . "'>"
                . "<img "
                . "class='button deny' "
                . "src='" . $this->url->get("img/buttons/friend.png") . "'>"
                . "<span>Deny Request</span>"
                . "</a>";

            //Build the friend request with all details.
            $new_message = new \Common\Models\SiteUserMessage();
            $new_message->to = $member->login_id;
            $new_message->from = $logged_in_user->login_id;
            $new_message->subject = "New Friend Request from " . $logged_in_user_avatar;
            $new_message->body_bb = $friend_request_body;
            $new_message->body_plaintext = $this->filter->sanitize($friend_request_body, "stripBbTagsHtmlContent");
            $new_message->uri = $this->filter->sanitize(
                "New Friend Request from " . $logged_in_user_avatar,
                'seoUrl'
            );
            $sent_message = $new_message->send();

            if ($sent_message === true) {                             //Put on email notification queue
                $notification = new \Common\Models\Notification();
                $notification->user_id = $member->id;
                $notification->type = "friend-request";

                if ($notification->save() == true) {
                    $to_notify[$member->id] = (string)$notification->getId();

                    try {
                        $this->queue->choose("email");
                        $this->queue->put($to_notify);
                    } catch (Exception $e) {
                        $this->flashSession->error(
                            "Cannot send email please contact "
                            . $this->config->frontend->admin->contact
                            . "."
                        );
                    }

                    if (!$site_user_friend_provisional->save()) {
                        foreach ($site_user_friend_provisional->getMessages() as $message) {
                            $this->flashSession->error("Could not save Provisional Record.: " . $message);
                        }
                    }
                }

                $this->response->redirect("/messages/inbox");
            } else {
                foreach ($sent_message->getMessages() as $message) {
                    $this->flashSession->error($message);
                    $this->response->redirect("/messages/new-message");
                }
            }

            //Save the request to friend request store and place on email message queue
            //On successfull request send redirect with flashSession message success redirect to reWriteUri

            $this->flashSession->success("You made a friends request to " . $member_ident . ". ");

            return $this->response->redirect($this->request->getHTTPReferer());
        }
    }

    public function friendRequestsAction()
    {
    }


    public function friendRequestAcceptAction()
    {
        $sender = $this->dispatcher->getParam("sender");

        $friend_requestee = \Common\Models\SiteUser::findFirst(
            [
                "conditions" => "login_id = ?1 OR avatar_name = ?1",
                "bind" => [1 => $sender]
            ]
        );

        if (isset($friend_requestee->id) == true) {
            $friend_request = $friend_requestee->getSiteUserFriend();

            if (isset($friend_request->id)) {
                $friend_request->confirmed = 1;

                if (!$friend_request->save()) {
                    foreach ($friend_request->getMessages() as $message) {
                        $this->flashSession->warning($message);
                    }
                    return $this->response->redirect($this->request->getHTTPReferer());
                } else {
                    $this->flashSession->success("You have added " . $sender . " to your friends list.");
                    return $this->response->redirect($this->request->getHTTPReferer());
                }
            } else {
                $this->flashSession->error(
                    "The friend request could not be found! Please contact "
                    . $this->config->frontend->admin->contact
                    . "."
                );
                return $this->response->redirect($this->request->getHTTPReferer());
            }
        } else {
            $this->flashSession->error(
                "The requestee could not be found. Please contact "
                . $this->config->frontend->admin->contact
                . "."
            );
            return $this->response->redirect($this->request->getHTTPReferer());
        }

        $this->flashSession->success(
            "Something went wrong! Please contact "
            . $this->config->frontend->admin->contact
            . "."
        );
        $this->response->redirect($this->request->getHTTPReferer());
    }

    public function friendRequestDenyAction()
    {
        $sender = $this->dispatcher->getParam("sender");

        $friend_requestee = \Common\Models\SiteUser::findFirst(
            [
                "conditions" => "login_id = ?1 OR avatar_name = ?1",
                "bind" => [1 => $sender]
            ]
        );

        if (isset($friend_requestee->id) == true) {
            $friend_request = $friend_requestee->getSiteUserFriend();

            if (isset($friend_request->id)) {
                $friend_request->confirmed = 0;

                if (!$friend_request->save()) {
                    foreach ($friend_request->getMessages() as $message) {
                        $this->flashSession->warning($message);
                    }
                    return $this->response->redirect($this->request->getHTTPReferer());
                } else {
                    $this->flashSession->success("You have denied " . $sender . " from your friends list.");
                    return $this->response->redirect($this->request->getHTTPReferer());
                }
            } else {
                $this->flashSession->error(
                    "The friend request could not be found! Please contact "
                    . $this->config->frontend->admin->contact
                    . "."
                );
                return $this->response->redirect($this->request->getHTTPReferer());
            }
        } else {
            $this->flashSession->error(
                "The requestee could not be found. Please contact "
                . $this->config->frontend->admin->contact
                . "."
            );
            return $this->response->redirect($this->request->getHTTPReferer());
        }

        $this->flashSession->success(
            "Something went wrong! Please contact "
            . $this->config->frontend->admin->contact
            . "."
        );
        $this->response->redirect($this->request->getHTTPReferer());
    }

}

