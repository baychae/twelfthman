<?php

namespace Multiple\Frontend\Controllers;

use Phalcon\Tag as Tag;

class IndexController extends ControllerBase
{

    public function initialize()
    {
    }


    public function indexAction()
    {
        $this->response->redirect("news");
    }

}

