<?php

namespace Multiple\Frontend\Controllers;

use Phalcon\Tag as Tag;

class MembersController extends ControllerBase
{

    public function initialize()
    {
    }

    public function indexAction()
    {
        Tag::setTitle('Members');

        $this->assets->addJs("js/jquery.cookie.js");
        $this->assets->addJs("js/jquery.paginater.js");

        $index = $this->request->get("index", "email");
        $search = $this->request->get("search", "safeChars");

        if ($this->dispatcher->wasForwarded() == true) {
            $this->view->pick("messages/members");
        }

        if ($search == true) {
            $members = \Common\Models\SiteUser::find(
                "if(avatar_name is null, login_id, avatar_name) like '%"
                . $search
                . "%'"
            );
            $paginated_page = \Common\Library\ModelPaginator::paginate($this, $members);
            $this->view->setVar("paginator_label", "Members  in search: " . $search);
            $this->view->setVar("indexed_by", "search=" . $search);
        } elseif ($index == true) {
            $members = \Common\Models\SiteUser::find(
                "if(avatar_name is null, login_id, avatar_name) like '"
                . $index
                . "%'"
            );
            $paginated_page = \Common\Library\ModelPaginator::paginate($this, $members);
            $this->view->setVar("paginator_label", "Members in Tab: " . $index);
            $this->view->setVar("indexed_by", "index=" . $index);
        } else {
            $members = \Common\Models\SiteUser::find();
            $paginated_page = \Common\Library\ModelPaginator::paginate($this, $members);
        }

        $site_user = new \Common\Models\SiteUser();
        $members_index = $site_user->getSiteMembersIndex();

        $this->view->setVar("page", $paginated_page);
        $this->view->setVar("alpha_index_array", $members_index);
    }

    public function profileAction()
    {
        //Get current logged in user
        $auth = $this->session->get("auth");
        $email = $this->filter->sanitize($auth["email"], "email");
        $conditions = "email = :email_ident: ";
        $parameters = ["email_ident" => $email];

        $logged_in_user = \Common\Models\SiteUser::findFirst([$conditions, "bind" => $parameters]);

        //Get member details
        $member_ident = $this->request->get("user", "safeChars");
        $member_ident = $this->filter->sanitize($member_ident, "string");

        $conditions = "if(avatar_name is null, login_id, avatar_name) = :ident:";
        $params = ["ident" => $member_ident];

        $view_member = \Common\Models\SiteUser::findFirst([$conditions, "bind" => $params]);

        //Search for site user friend
        $site_user_friend = \Common\Models\SiteUserFriend::findFirst(
            [
                "((site_user_id = " . $logged_in_user->id . " AND friend_site_user_id = " . $view_member->id . ") "
                . "OR (site_user_id = " . $view_member->id . " AND friend_site_user_id = " . $logged_in_user->id . ")) "
                . "AND confirmed = 1"
            ]
        );

        //Get post count
        $topic_post_count = $view_member->countForumTopicPost();

        $this->view->setVar("member", $view_member);
        $this->view->setVar("posts", $topic_post_count);
        $this->view->setVar("site_user_friend", $site_user_friend);
    }

    public function unfriendAction()
    {
        $friend_name = $this->dispatcher->getParam("friend");

        $friend = \Common\Models\SiteUser::findFirst(
            [
                "conditions" => "login_id = ?1 OR avatar_name = ?1",
                "bind" => [1 => $friend_name]
            ]
        );

        if (isset($friend->id) == true) {
            $friend_request = $friend->getSiteUserFriend();

            if (isset($friend_request->id)) {
                $friend_request->confirmed = 0;

                if (!$friend_request->save()) {
                    foreach ($friend_request->getMessages() as $message) {
                        $this->flashSession->warning($message);
                    }
                    return $this->response->redirect($this->request->getHTTPReferer());
                } else {
                    $this->flashSession->success("You have unfriended " . $friend_name . " from your friends list.");
                    return $this->response->redirect($this->request->getHTTPReferer());
                }
            } else {
                $this->flashSession->error(
                    "The friend request could not be found! Please contact "
                    . $this->config->frontend->admin->contact
                    . "."
                );
                return $this->response->redirect($this->request->getHTTPReferer());
            }
        } else {
            $this->flashSession->error(
                "The requestee could not be found. Please contact "
                . $this->config->frontend->admin->contact
                . "."
            );
            return $this->response->redirect($this->request->getHTTPReferer());
        }

        $this->flashSession->success(
            "Something went wrong! Please contact "
            . $this->config->frontend->admin->contact
            . "."
        );
        $this->response->redirect($this->request->getHTTPReferer());
    }
}

