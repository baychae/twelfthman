<?php

namespace Multiple\Frontend\Controllers;

use Phalcon\Tag as Tag;

define("AVATAR_IMG_MAX_SIZE", $config->frontend->my_account->avatar_img_max_size);
define("AVATAR_IMG_DIR", $config->frontend->my_account->avatar_img_dir);
define("AVATAR_ALLOWED_TYPES", serialize($config->frontend->my_account->avatar_allowed_types));

class MyAccountController extends ControllerBase
{
    public function initialize(){
        Tag::setTitle('My Account');
    }

    public function indexAction()
    {
        $auth = $this->session->get("auth");
        $email = $this->filter->sanitize($auth["email"], "email");

        $conditions = "email = :email_ident: ";
        $parameters = ["email_ident" => $email];

        $user = \Common\Models\SiteUser::findFirst([$conditions, "bind" => $parameters]);

        $this->view->setVar("avatar_file_name", $user->avatar_file_name);
        $this->view->setVar("login_id", $user->login_id);

        if ($user->email == $this->session->get("auth")["email"]) {

            $form = new \Multiple\Frontend\Forms\Account\MyAccount($user);
            $this->view->setVar("form", $form);

        } else {
            $this->flashSession->error("Your account appears to be experiencing difficulties. Please contact admin.");
            $this->session->destroy();
            $this->response->redirect("session");
        }

    }

    public function updateAction()
    {
        $auth = $this->session->get("auth");

        $email = $this->filter->sanitize($auth["email"], "email");

        $conditions = "email = :email_ident: ";
        $parameters = ["email_ident" => $email];

        $user = \Common\Models\SiteUser::findFirst([$conditions, "bind" => $parameters]);

        $user->first_name = $this->request->getPost("first_name", "string");
        $user->last_name = $this->request->getPost("last_name", "string");
        $user->email = $this->request->getPost("email", "email", $user->email);
        $user->avatar_name = $this->request->getPost("avatar_name", "safeChars");

        $user->bbcode_field = $this->escaper->escapeHtml($this->request->getPost("bbcode_field"));
        $user->signature_html = $this->escaper->escapeHtml($this->request->getPost("content_html"));
        $user->team_id = $this->request->getPost("team_id", "int");
        $user->favourite_away_ground = $this->request->getPost("favourite_away_ground", "string");
        $user->favourite_all_time_player = $this->request->getPost("favourite_all_time_player", "string");
        $user->favourite_current_player = $this->request->getPost("favourite_current_player", "string");

        if ($this->session->get("auth") == true) {

            $delete_avatar_img = $this->request->getPost("delete_avatar");


            if ($this->request->hasFiles(true) == true && $user->avatar_file_name == NULL && isset($delete_avatar_img) == false) {//Handle images if present

                $files = $this->request->getUploadedFiles(true);
                $fileType = $files[0]->getRealType();
                $allowed_file_types = $this->getDI()->getConfig()->frontend->my_account->avatar_allowed_types->toArray();

                if (!in_array($fileType, $allowed_file_types)) {
                    $this->flashSession->error("You may only upload jpegs and png's");
                    return $this->response->redirect("my_account");
                }

                if ($files[0]->getSize() > constant("AVATAR_IMG_MAX_SIZE")) {

                    $this->flashSession->error("File size must be under: " . (constant("AVATAR_IMG_MAX_SIZE")) . "KB not" . $files[0]->getSize());
                } else {
                    $image = $files[0];
                    $image->moveTo(constant("AVATAR_IMG_DIR") . $image->getName());
                    $user->avatar_file_name = $image->getName();
                }
            } elseif (isset($delete_avatar_img) == true) {

                $file_location = constant("AVATAR_IMG_DIR") . $user->avatar_file_name;

                if (file_exists($file_location) == true) {
                    $deleted = unlink($file_location);
                }

                if ($deleted == true) {
                    $user->avatar_file_name = null;
                    $this->flashSession->success("Avatar successfully deleted");
                } else {
                    $this->flashSession->error("File was not deleted please alert an admin");
                    return $this->response->redirect("my_account");
                }

            }

            if ($user->save() == false) {
                foreach ($user->getMessages() as $message) {
                    $this->flashSession->success("MESSAGE: " . $message);
                }
                return $this->response->redirect("my_account");
            } else {

                if ($this->request->getPost("email", "email")) {
                    $this->session->set('auth', [
                        'email' => $user->email,
                        'name' => $user->login_id,
                        'privelege' => $user->level
                    ]);
                }

                $this->flashSession->success("Account updated");
                return $this->response->redirect("my_account");
            }

        } else {
            $this->flashSession->error("Your account appears to be experiencing difficulties. Please contact admin.");
            $this->session->destroy();
            $this->response->redirect("session");
        }
    }

}
