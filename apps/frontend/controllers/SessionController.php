<?php

namespace Multiple\Frontend\Controllers;

use Phalcon\Tag as tag;
use Common\Models\SiteUser as SiteUser;

class SessionController extends ControllerBase
{

    public function initialize()
    {
    }

    public function indexAction()
    {
        Tag::setTitle('Log In');

        if ($this->cookies->has("reason")) {
            $reason = $this->cookies->get("reason");

            switch ($reason) {
                case "member-profile":
                    $this->flash->notice("Login or become a member to view members profile!");
                    break;
            }
        }
    }

    /**
     * Register authenticated user into session data
     *
     * @param SiteUser $user
     */
    private function registerSession($user)
    {
        $this->session->set('auth', [
            'email' => $user->email,
            'name' => $user->login_id,
            'privelege' => $user->level
        ]);
    }

    public function startAction()
    {

        if ($this->request->isPost()) {
            $password_posted = $this->request->getPost('password');
            $login = $this->request->getPost('login', 'email');

            $redirect_url = 'news/';

            if ($this->cookies->has("reason")) {
                $reason_cookie = $this->cookies->get("reason");

                switch ($reason_cookie) {
                    case "member-profile":
                        $profile_cookie = $this->cookies->get("profile-id");
                        $redirect_url = "members/profile?user=" . $profile_cookie;
                        $reason_cookie->delete();
                        $profile_cookie->delete();
                        break;
                }

            }

            $user = SiteUser::findFirst("login_id='$login' AND active='Y'");

            if (!$user == true) {
                $user = SiteUser::findFirst("email='$login' AND active='Y'");
            }

            if ($user == true && $user->password != '' && $user->password_legacy == 'deprecated') {
                if ($this->security->checkHash($password_posted, $user->password)) {
                    $this->registerSession($user);
                    return $this->response->redirect($redirect_url);
                } else {
                    $this->security->hash(rand());
                }

            } elseif ($user == true && $user->password == 'legacy' && $user->password_legacy != 'deprecated') { //Then uses old password
                $password_posted_md5 = md5($password_posted); //#TODO - Make this dead code

                if ($user->password_legacy == $password_posted_md5) {
                    $new_password_hash = $this->security->hash($password_posted);
                    $user->password = $new_password_hash;
                    $user->password_legacy = 'deprecated';

                    if ($user->save() == true) {
                        $this->registerSession($user);
                        return $this->response->redirect($redirect_url);
                    } else {
                        foreach ($user->getMessages() as $message) {
                            $this->flashSession->error((string)$message);
                        }
                    }
                }
            }

            $this->flashSession->error('Wrong email/password');
        }

        return $this->response->redirect('session/index');
    }

    public function registerAction()
    {
        $auth = $this->session->get("auth");

        if ($auth) {
            $this->flashSession->warning("You are already logged in!");
            return $this->response->redirect("session/logout");
        } else {
            $request = $this->request;

            $form = new \Multiple\Frontend\Forms\Account\Register();
            $this->view->setVar('form', $form);

            $recaptcha = new \ReCaptcha\ReCaptcha($this->config->common->recaptcha->secret);

            if ($request->isPost() === true) {
                $g_recaptcha_response = $this->request->getPost('g-recaptcha-response');
                $remote_add = $this->request->getServer('REMOTE_ADDR');

                if ($this->security->checkToken()) {
                    //Captcha part
                    $resp = $recaptcha->verify($g_recaptcha_response, $remote_add);

                    if (!$resp->isSuccess()) {
                        foreach ($resp->getErrorCodes() as $code) {
                            echo '<kbd>', $code, '</kbd> ';
                            $this->flashSession->error($code);
                        }

                        // Captcha is invalid

                        #TODO - LOG $resp->error incorrect-captcha-sol
                        return $this->response->redirect('session/register');
                    } else {                                                                    // Captcha is valid
                        $site_user = new \Common\Models\SiteUser();
                        $form->bind($_POST, $site_user);

                        $site_user->password = $this->security->hash($_POST['password']);
                        $site_user->password_legacy = "deprecated";
                        $site_user->avatar_name = $this->request->getPost("login_id");
                        $site_user->level = 'user';
                        $site_user->active = 'Y';

                        if (!$form->isValid()) {
                            return true;
                        } else {
                            if ($site_user->save() == false) {
                                foreach ($site_user->getMessages() as $message) {
                                    $this->flashSession->warning($message);
                                }
                                return $this->response->redirect('session/register');
                            } else {
                                $this->flashSession->success('Your account has been created!');
                                $this->flashSession->success('Please login!');

                                return $this->response->redirect('session');
                            }
                        }

                    }                                                                           //Captcha block end
                } else {
                    #TODO - LOG CSRF ATTACK & NOTIFY WEBMASTER WITH IP
                    return $this->response->redirect('session/register');                    //
                }

            }
        }
    }

    public function logoutAction()
    {
        Tag::setTitle('Log Out');
    }

    public function endAction()
    {
        $this->session->destroy();
        $this->response->redirect('news/');
    }

    public function lostPasswordAction()
    {
        $ident = $this->request->getPost("ident", "email");
        $ident = $this->filter->sanitize($ident, "safeName");

        $bind_params = [
            "conditions" => "email = :ident: OR login_id = :ident: ",
            "bind" => [
                "ident" => $ident
            ]
        ];

        if ($this->request->isPost() && !empty($ident)) {
            $user = \Common\Models\SiteUser::findFirst($bind_params);

            //Work on flash messages

            if (!$user) { //If user not found then log attempt and IP ban IP on 5 failed attempts to find user
                //#TODO - Log failed attempt
                //#TODO - On 5 failed attempts ban IP

                $this->flashSession->success("Please check your email and follow the instructions provided.");
                $this->response->redirect("session");
            } else {
                //Send notification
                $notification = new \Common\Models\Notification();
                $notification->user_id = $user->id;
                $notification->type = "pswd-reset";


                if ($notification->save() == true) {
                    $to_notify[$user->id] = (string)$notification->getId();
                    $this->queue->choose("email");
                    $this->queue->put($to_notify);

                    $this->flashSession->success("Please check your email and follow the instructions provided.");
                    $this->response->redirect("session");
                }

            }

        }
    }

    public function passwordResetAction()
    {
        $this->session->destroy();
        $uuid = $this->dispatcher->getParam("uuid");

        $reminder = \Common\Models\PasswordReminderReceipt::findFirst([
            ["uuid" => $uuid]
        ]);

        if ($reminder->used == "Y") {
            if ($this->session->get("auth")) {
                $this->session->remove("auth");
            }
            $this->flashSession->error("HTTP REF: " . $this->request->getHTTPReferer());
            //TODO - Count tries using deadlink

            $this->flashSession->error("Only one password reset allowed per email.");
        }


        $this->view->setVar("uuid", $uuid);
    }

    public function confirmedPasswordResetAction()
    {

        $uuid = $this->request->getPost("uuid");
        $new_password = $this->request->getPost("password");
        $new_password = $this->security->hash($new_password);

        $reminder = \Common\Models\PasswordReminderReceipt::findFirst([
            ["uuid" => $uuid]
        ]);

        if ($reminder->used == "Y") {
            if ($this->session->get("auth")) {
                $this->session->remove("auth");
            }
            return $this->response->redirect("session/password-reset/" . $uuid);
        }
        //TODO - Handle genuine attempt
        //TODO - Count genuine failed attempts

        if (isset($reminder->uuid)) {
            $reminder->used = "Y";

            if (!$reminder->save()) {
                foreach ($reminder->getMessages() as $message) {
                    $this->flashSession->error($message);
                }

                return $this->response->redirect("session/password-reset/" . $uuid);
            } else {
                $notification_id = $reminder->notification_id;

                $notification = \Common\Models\Notification::findById($notification_id);

                if (!isset($notification->_id)) {
                    foreach ($notification->getMessages() as $message) {
                        $this->flashSession->error($message);
                    }
                    return $this->response->redirect("session/password-reset/" . $uuid);
                } else {
                    if ($notification->sent == "Y" && $notification->user_id == $reminder->user_id) {
                        $user = \Common\Models\SiteUser::findFirstById($reminder->user_id);
                        $user->password = $new_password;
                        $user->password_legacy = 'deprecated';

                        if (!$user->update()) {
                            foreach ($user->getMessages() as $message) {
                                $this->flashSession->error($message);
                            }
                        } else {
                            $this->flashSession->success(
                                "You have successfully reset your password. You can now login."
                            );
                            return $this->response->redirect("/session");
                        }


                    }
                }

            }
        }
    }
}
