<?php

namespace Multiple\Frontend\Controllers;

define("ARTICLE_LIMIT", 5);
define("TEXT_LIMIT", 400);

class NewsController extends ControllerBase
{
    private $admin_section;
    private $article_limit;
    private $text_limit;
    
    public function initialize()
    {
        \Phalcon\Tag::appendTitle(' - Owlsonline News');
        $this->article_limit = $this->getArticleLimit();
        $this->text_limit = $this->getTextLimit();
    }
    
    private function getArticleLimit()
    {
        $settings = \Common\Models\AdminSiteSection::getNewsSettings($this->admin_section);
        $article_limit = $settings->getAdminSiteSectionData([
            "conditions" => "name = 'article_limit'"
        ]);
        
        return $article_limit[0]->value ?: constant("ARTICLE_LIMIT");
    }
    
    private function getTextLimit()
    {
        $admin_section = \Common\Models\AdminSiteSection::getNewsSettings($this->admin_section);
        $text_limit = $admin_section->getAdminSiteSectionData([
            "conditions" => "name = 'text_limit'"
        ]);
        
        return $text_limit[0]->value ?: constant("TEXT_LIMIT");
    }
   
   
    public function indexAction($requested_group = null)
    {
        $requested_group = preg_replace("|/|", "", $requested_group);
        $news_group_list = [];
        $all_news_groups = \Common\Models\NewsGroup::find(
            [
                "order" => "priority, id asc"
            ]
        );

        foreach ($all_news_groups as $news_group) { //Label then sanitized link
            $news_group_list[$news_group->label] = $news_group->link;
        }

        if ($requested_group == null) { // If no group then in main page
            \Phalcon\Tag::prependTitle("Sheffield Wednesday");
            $news_articles = \Common\Models\NewsArticle::find(
                [
                    "conditions" => "publish=1",
                    "limit" => $this->article_limit,
                    "order" => "id DESC"
                ]
            );
        } else {
            \Phalcon\Tag::prependTitle(ucwords($requested_group));
            $this->view->setTemplateBefore('news_group');
            $news_group = \Common\Models\NewsGroup::findFirst(["conditions" => "link ='" . $requested_group . "'"]);
            $news_articles = $news_group->getNewsArticle([
                "conditions" => "publish=1",
                "limit" => $this->article_limit
            ]);
        }
        
        $this->view->setVar("news_groups", $news_group_list);
        $this->view->setVar("news_articles", $news_articles);
        $this->view->setVar("copy_text_limit", $this->text_limit);
    }

    public function articleAction()
    {
        $article_id = $this->dispatcher->getParam("article_id");
        $all_news_groups = \Common\Models\NewsGroup::find(["order" => "priority, id asc"]);
        $article = \Common\Models\NewsArticle::findFirst($article_id);
        $settings = new \Common\Models\Entities\NewsArticleSettings($this);

        \Phalcon\Tag::setTitle($article->title);

        $current_news_group_articles = $article->getNewsGroup()
            ->getNewsArticle(
                [
                    'id <> :id:',
                    "order" => 'id desc',
                    "limit" => $settings->other_news_limit,
                    "bind" => [
                        "id" => $article->id
                    ]
                ]
            );


        foreach ($all_news_groups as $news_group) { //Label then sanitized liink
            $news_group_list[$news_group->label] = $news_group->link;
        }

        if (isset($article->image_upload) == true) {
            $art_url = $this->url->get(
                $this->config->frontend->news->article->image_dir . rawurlencode($article->image_upload)
            );
            $this->view->setVar("art_url", $art_url);
        } elseif (isset($article->image_embed)) {
            $art_embed = $article->image_embed;
            $this->view->setVar("art_embed", $art_embed);
        } else {
            $art_url = $this->url->get(
                $this->config->frontend->news->article->settings->image_dir . "default-article-image.png"
            );
        }

        $auth = $this->session->get('auth') != null ? true : false;                                 //Read Tracking
        $token_login_id = $this->filter->sanitize($this->session->get('auth')["name"], "string");
        $token_email = $this->filter->sanitize($this->session->get('auth')["email"], "email");

        if ($auth) {
            $user_id = \Common\Models\SiteUser::getSiteMemberUserId($token_login_id, $token_email);
            $tracker = \Common\Models\ArticleUserReadTrackStats::findFirstByUserId($user_id);
            $articles_read = $tracker->articles;
            $this->view->setVar("articles_read", $articles_read);
            $tracker->updateArticleRead($article_id);
        }


        if (isset($art_url)) {
            $art_meta_url = $art_url;
        } else {
            $art_meta_url = $this->url->get(
                $this->config->frontend->news->article->settings->image_dir . "default-article-image.png"
            );
        }

        $metaData = '<meta name="twitter:card" content="summary">';
        $metaData .= '<meta name="twitter:site" content="@' . $this->config->site->name . '">';
        $metaData .= '<meta name="twitter:title" content="' . $article->title . '">';
        $metaData .= '<meta name="twitter:description" content="' . $article->content_plaintext . '">';
        $metaData .= '<meta name="twitter:creator" content="@' . $this->config->site->name . '">';
        $metaData .= '<meta name="twitter:image" content="' . $art_meta_url . '">';


        $metaData .= '<meta name="og:site_name" content="' . $this->config->site->name . '">';
        $metaData .= '<meta name="og:type" content="article" >';
        $metaData .= '<meta name="og:locale" content="en_GB" >';
        $metaData .= '<meta name="og:title" content="' . $article->title . '">';
        $metaData .= '<meta name="og:description" content="' . $article->content_plaintext . '">';
        $metaData .= '<meta name="og:url" content="' . $this->url->get($this->router->getRewriteUri()) . '">';
        $metaData .= '<meta name="og:image" content="' . $art_meta_url . '">';
        $metaData .= '<meta name="og:image:width" content="300">';
        $metaData .= '<meta name="og:image:height" content="200">';

        $this->view->setVar("meta_data", $metaData);
        $this->view->setVar("current_news_group_articles", $current_news_group_articles);
        $this->view->setVar("news_groups", $news_group_list);
        $this->view->setVar("article", $article);
    }
    
}
