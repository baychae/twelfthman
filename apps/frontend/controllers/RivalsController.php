<?php

namespace Multiple\Frontend\Controllers;

use Phalcon\Tag as Tag;

class RivalsController extends ControllerBase
{

    public function initialize()
    {
        Tag::setTitle('Rivals');
    }

    public function leaguesAction()
    {
        $leagues = \Common\Models\League::find(["order" => "position"]);

        $this->view->setVar("leagues", $leagues);
    }

    public function leagueAction()
    {
        $league_link = $this->dispatcher->getParam("league");

        $league = \Common\Models\League::findFirst("link = '" . $league_link . "'");

        $matches = $this->modelsManager->createBuilder()
            ->addFrom("\\Common\\Models\\Match", 'm')
            ->join("\\Common\\Models\\Team", 'm.home_team_id = ht.id', 'ht')
            ->join("\\Common\\Models\\Team", 'm.home_team_id = at.id', 'at')
            ->join("\\Common\\Models\\League", 'hl.id = ht.league_id', 'hl')
            ->join("\\Common\\Models\\League", 'al.id = at.league_id', 'al')
            ->where(" hl.link = '" . $league_link . "'")
            ->orWhere(" al.link = '" . $league_link . "'")
            ->orderBy('m.start desc')
            ->getQuery()
            ->execute();

        $this->view->setVar("league_name", $league->name);
        $this->view->setVar("league_link", $league->link);
        $this->view->setVar("matches", $matches);
    }

}

