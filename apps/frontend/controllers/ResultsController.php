<?php

namespace Multiple\Frontend\Controllers;

use Phalcon\Tag as Tag;

class ResultsController extends ControllerBase
{

    public function initialize()
    {
        Tag::setTitle('Results');
    }
    
    public function indexAction()
    {
        $team_id = $this->dispatcher->getParam("team_id", "int");

        if (isset($team_id)) {
            $matches = \Common\Models\Match::find([
                "conditions" => "home_team_id = " . $team_id . " OR away_team_id = " . $team_id,
                "order" => "start desc"
            ]);

        } else {
            $matches = \Common\Models\Match::find([
                "order" => "start desc"
            ]);
        }

        $this->view->setVar("matches", $matches);
    }

}

