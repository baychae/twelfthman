<?php

namespace Multiple\Frontend\Controllers;

use Phalcon\Tag as Tag;

class DisclaimerController extends ControllerBase
{

    public function initialize()
    {
        Tag::setTitle('Disclaimer');
    }
    
    public function indexAction()
    {
        $disclaimer_section = \Common\Models\AdminSiteSection::findFirst("name ='disclaimer'");
        $disclaimer = $disclaimer_section->getAdminSiteSectionData()->getFirst();


        $this->view->setVar("disclaimer", $disclaimer->value);
    }

}

