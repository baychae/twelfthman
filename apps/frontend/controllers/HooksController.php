<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 08/06/2017
 * Time: 12:51
 */

namespace Multiple\Frontend\Controllers;

class HooksController extends ControllerBase
{

    public function index()
    {

    }

    public function upNextSmsUpdateScoreAction() //Route post to this controller
    {

        $this->view->disable();
        $this->response->setContentType('text/xml', 'utf-8');

        $id = $this->request->getPost("id", "string");
        $to = $this->request->getPost("to", "int");
        $from = $this->request->getPost("from", "int");
        $keyword = $this->request->getPost("keyword", "string");
        $content = $this->request->getPost("content", "string");


        $userAgent = $this->request->getUserAgent();
        $userAgentCorrect = $userAgent === "Clockwork HTTP Forwarding" ? true : false; // If userAgent === "Clockwork HTTP Forwarding"

        $request_ip_addr = $this->request->getClientAddress();
        $request_ip_addr_val = ip2long($request_ip_addr);
        $ip_within_range = \Common\Library\Helpers::isIpInRange($request_ip_addr_val, $this->config->sms->ip);

        $settings = new \Common\Models\Entities\UpNextSettings($this);

        $settings_live_score_active = $settings->getLiveScoreSmsActive();
        $settings_live_score_end_point = $settings->getLiveScoreEndpointSms();
        $settings_live_score_expect_from = $settings->getLiveScoreSmsExpectFrom();

        if ($settings_live_score_active == "0" || !$ip_within_range || !$userAgentCorrect || $to != $settings_live_score_end_point || $settings_live_score_expect_from != $from) {

            if (!$ip_within_range) {
                $this->logger->log("IP NOT WITHIN RANGE: " . $request_ip_addr);
            }

            if (!$userAgentCorrect) {
                $this->logger->log("INCORRECT USER AGENT: " . $userAgent);
            }

            if ($to != $settings_live_score_end_point) {
                $this->logger->log("INCORRECT SMS SERVICE PROVIDER NUMBER: " . $to);
            }
            if ($settings_live_score_expect_from != $from) {
                $this->logger->log("REQUEST FROM INCORRECT NUMBER: " . $from);
            }

            if ($settings_live_score_active == "0") {
                $this->logger->log("=================LIVE SMS SWITCHED OFF!!!");
                return $this->response->setStatusCode(503, "Service Unavailable");
            }


            return $this->response->setStatusCode(403, "Forbidden");
        } else { //Request Valid

            $match = \Common\Models\Match::getLiveMatch();

            if (isset($match->id)) {//Live exists

                $live_results_raw = explode('-', $content);
                $live_results_mapped = array_map("intval", $live_results_raw);
                $live_results = sizeof($live_results_mapped) == 2 ? $live_results_mapped : FALSE;

                if (!$live_results) {
                    $this->logger->log("=================BAD REQUEST");
                    $this->logger->log($id . " - Message Content: " . $content);
                    return $this->response->setStatusCode(400, "Bad Request");
                }

                $match->home_score = $live_results[0];
                $match->away_score = $live_results[1];

                if ($match->save() != true) {

                    $this->logger->log("=================VALID REQUEST" . $settings_live_score_active);
                    $this->logger->log("client ipAddress: " . $request_ip_addr);

                    $this->logger->log($id . " - id: " . $id);
                    $this->logger->log($id . " - keyword: " . $keyword);
                    $this->logger->log($id . " - content: " . $content);
                    $this->logger->log($id . " - User-Agent: " . $userAgent);
                    $this->logger->log($id . "FAILED TO UPDATE MATCH");

                    foreach ($match->getMessages() as $message) {
                        $this->logger->log($id . " - " . $message);
                    }

                } else {
                    $this->logger->log("=================VALID REQUEST");
                    $this->logger->log("client ipAddress: " . $request_ip_addr);

                    $this->logger->log($id . " - id: " . $id);
                    $this->logger->log($id . " - keyword: " . $keyword);
                    $this->logger->log($id . " - content: " . $content);
                    $this->logger->log($id . " - User-Agent: " . $userAgent);
                    $this->logger->log($id . " - MATCH SCORE UPDATED");
                }


            } else {


                $this->logger->log("client ipAddress: " . $request_ip_addr);
                $this->logger->log($id . " - id: " . $id);
                $this->logger->log($id . " - keyword: " . $keyword);
                $this->logger->log($id . " - content: " . $content);
                $this->logger->log($id . " - User-Agent: " . $userAgent);
                return $this->response->setStatusCode(503, "Service Unavailable");
            }


        }


    }

}