<?php

namespace Multiple\Frontend\Controllers;

use Common\Models\ForumTopicPostsFlagged;
use Common\Models\SiteUser;
use Phalcon\Tag as Tag;

class ForumController extends ControllerBase
{

    public function initialize()
    {

        $this->response->setHeader("Cache-Control", "no-store, must-revalidate, max-age=0");
        $this->response->setHeader("Pragma", "no-cache");
        $this->response->setHeader("Expires", "Wed, 4 May 2016 05:00:00 GMT");

        $this->assets->addJs("js/jquery.cookie.js");
        $this->assets->addJs("js/forum.js");

        $pageSize = $this->cookies->get('pageSize');

        $current_match = \Common\Models\Match::getLiveMatch();
        $this->view->setVar("current_match", $current_match);

        if (isset($pageSize)) {
            if ($pageSize->getValue() === 'undefined') {
                $this->cookies->set("pageSize", "15");
            }
        } else {
            $this->cookies->set("pageSize", "15");
        }
    }

    public function indexAction()
    {
        $this->assets->addJs("js/forum-views.js");

        Tag::setTitle('Forum');

        $auth = $this->session->get('auth');

        if ($auth) {
            $this->view->pick('forum/index');
        } else {
            $this->view->pick('forum/index_public');
        }

        //Total replies for each forum
        $forum_replies_doc = \Common\Models\ForumTopicPostStats::aggregate([
            [
                '$group' => [
                    '_id' => ['forum_id' => '$forum_id'],
                    'forum_replies' => ['$sum' => '$post_count']
                ]
            ]
        ]);

        foreach ($forum_replies_doc as $record) {
            $forum_id = $record["_id"]["forum_id"];
            $post_count = $record["forum_replies"];
            $forum_reply_stats[$forum_id] = $post_count;
        }

        //Total topics for each forum
        $forum_topics_doc = \Common\Models\ForumTopicPostStats::aggregate([
            [
                '$group' => [
                    '_id' => ['forum_id' => '$forum_id'],
                    'count' => ['$sum' => 1]
                ]
            ]
        ]);

        foreach ($forum_topics_doc as $result) {
            $forum_id = $result["_id"]["forum_id"];
            $topic_count = $result["count"];
            $forum_topic_stats[$forum_id] = $topic_count;
        }

        //Last post for each forum
        $forum_last_post = \Common\Models\ForumTopicPostStats::aggregate([
            [
                '$group' => [
                    '_id' => ['forum_id' => '$forum_id'],
                    'last_post_id' => ['$max' => '$post_id']
                ]
            ]
        ]);

        foreach ($forum_last_post as $last_forum_post) {
            $forum_id = $last_forum_post["_id"]["forum_id"];
            $post = \Common\Models\ForumTopicPost::findFirstById($last_forum_post["last_post_id"]);
            $user = \Common\Models\SiteUser::findFirstById($post->user_id);

            if ($post) {
                $page_size = $this->cookies->get('pageSize')->getValue() != null ?
                    $this->cookies->get('pageSize')->getValue() : 15;
                $post_url = $post->getUrl($page_size);
                $last_post[$forum_id]["post_url"] = $post_url;
                $last_post[$forum_id]["title"] = $post->getForumTopic()->title;
                $last_post[$forum_id]["created"] = $post->created;
                $last_post[$forum_id]["avatar_name"] = $user->avatar_name;
            }
        }


        $auth = $this->session->get('auth') != null ? true : false;
        $token_login_id = $this->filter->sanitize($this->session->get('auth')["name"], "string");
        $token_email = $this->filter->sanitize($this->session->get('auth')["email"], "email");

        if ($auth) {
            $user_id = \Common\Models\SiteUser::getSiteMemberUserId($token_login_id, $token_email);
            $this->view->setVar("user_id", $user_id);
            $tracker = \Common\Models\ForumUserReadTrackStats::findFirstByUserId($user_id);
            $forums_read = $tracker->forums;
            $this->view->setVar("forums_read", $forums_read);
        }

        $forum_types = \Common\Models\ForumType::find(
            ["view_type != 'A'  ",
                "order" => "display_order ASC"
            ]
        );

        $this->view->setVar("unread", true);
        $this->view->setVar("logged_in", $auth);
        $this->view->setVar("forum_types", $forum_types);
        $this->view->setVar("forum_replies", $forum_reply_stats);
        $this->view->setVar("forum_topic_count", $forum_topic_stats);
        $this->view->setVar("last_activity", $last_post);
    }

    public function forumMarkReadAction()
    {
        $forum_id = $this->dispatcher->getParam("forum_id");
        $token_login_id = $this->filter->sanitize($this->session->get('auth')["name"], "string");
        $token_email = $this->filter->sanitize($this->session->get('auth')["email"], "email");

        $user_id = \Common\Models\SiteUser::getSiteMemberUserId($token_login_id, $token_email);

        $tracker = \Common\Models\ForumUserReadTrackStats::findFirstByUserId($user_id);
        $set_read = $tracker->setForumRead($forum_id);

        if ($set_read) {
            return $this->response->redirect("/forum/");
        } else {
            $this->flashSession->warning("Cannot mark forum read. Please email admin.");
            return $this->response->redirect("forum/");
        }
    }
    
    public function forumTopicMarkReadAction()
    {
        $forum_id = $this->dispatcher->getParam("forum_id");
        $topic_id = $this->dispatcher->getParam("topic_id");
        $forum_page_num = $this->dispatcher->getParam("forum_page_num");

        $token_login_id = $this->filter->sanitize($this->session->get('auth')["name"], "string");
        $token_email = $this->filter->sanitize($this->session->get('auth')["email"], "email");

        $user_id = \Common\Models\SiteUser::getSiteMemberUserId($token_login_id, $token_email);

        $tracker = \Common\Models\ForumUserReadTrackStats::findFirstByUserId($user_id);
        $set_read = $tracker->setTopicAllRead($topic_id);

        if ($set_read) {
            return $this->response->redirect("forum/" . $forum_id . "/" . $forum_page_num);
        } else {
            $this->flashSession->warning("Cannot mark forum read. Please email admin.");
            return $this->response->redirect("forum/");
        }
    }

    public function forumAction()
    {
        $this->assets->addJs("js/jquery.paginater.js");

        $page_size = $this->cookies->get('pageSize')->getValue() != null ?
            $this->cookies->get('pageSize')->getValue() : 15;

        $auth = $this->session->get('auth') != null ? true : false;
        $sysadmin = $this->session->get('auth')["privelege"] == 'sysadmin' ? true : false;

        //Build paginator
        $forum_id = $this->dispatcher->getParam("forum");
        $forum = \Common\Models\Forum::findFirstById($forum_id);
        $forum_type_title = $forum->getForumType()->title;
        Tag::setTitle($forum->title);

        //Get Pinned Topics
        $pinned_topics = \Common\Models\ForumTopic::getPinnedForumView($forum_id);
        $pinned_topics = count($pinned_topics) > 0 ? $pinned_topics : null;

        //Get topics in current page and build post urls
        if (isset($pinned_topics)) {
            $pinned_topic_ids = array_map(function ($item) {
                return intval($item['topic_id']);
            }, $pinned_topics);

            $pinned_topic_post_stats = \Common\Models\ForumTopicPostStats::find([ //Get forum stats for current page
                [
                    "topic_id" => [
                        '$in' => $pinned_topic_ids
                    ]
                ]
            ]);

            foreach ($pinned_topic_post_stats as $stat) {
                $page = floor($stat->post_count / $page_size) + 1;
                $post_url = "forum/" . $forum->id . "/" . $stat->topic_id . "/" . $page . "#" . $stat->post_count;
                $pinned_topic_post_stats[$stat->topic_id]['post_url'] = $post_url;
                $pinned_topic_post_stats[$stat->topic_id]['post_count'] = $stat->post_count;
            }

            $this->view->setVar('pinned_topic_post_stats', $pinned_topic_post_stats);
        }

        //Get Paginated Topics
        $forumTopicsViewData = \Common\Models\ForumTopic::getForumView($forum_id);

        $paginated = new \Phalcon\Paginator\Adapter\NativeArray(
            [
                "data" => $forumTopicsViewData,
                "limit" => 15,
                "page" => $this->dispatcher->getParam("page_requested", "int")
            ]
        );

        $paginated_page = $paginated->getPaginate();



        $topic_stats = \Common\Models\TopicViewCounterStats::aggregate([[
            '$project' => [
                '_id' => 0,
                'topic_id' => 1,
                'view_count' => ['$size' => '$users']
            ]
        ]]);

        foreach ($topic_stats as $topic_view) {
            $topic_views[$topic_view["topic_id"]] = $topic_view["view_count"];
        }
        
        // Get Forum Topic Read Tracker
        $token_login_id = $this->session->get('auth')["name"];
        $token_login_email = $this->session->get('auth')["email"];

        $user_rating = \Common\Models\TopicRatingStats::getForumTopicRatings($paginated_page->items);

        if (isset($pinned_topics)) {
            $user_rating_pinned = \Common\Models\TopicRatingStats::getForumTopicRatings($pinned_topics);
            $this->view->setVar('user_rating_pinned', $user_rating_pinned);
        }

        //Get topics in current page and build post urls
        $topic_ids = array_map(function ($item) {
            return intval($item['topic_id']);
        }, $paginated_page->items);


        $forum_topic_post_stats = \Common\Models\ForumTopicPostStats::find([ //Get forum stats for current page
            [
                "topic_id" => [
                    '$in' => $topic_ids
                ]
            ]
        ]);

        foreach ($forum_topic_post_stats as $stat) {
            $page = floor($stat->post_count / $page_size) + 1;
            $post_url = "forum/" . $forum->id . "/" . $stat->topic_id . "/" . $page . "#" . $stat->post_count;
            $topic_post_stats[$stat->topic_id]['post_url'] = $post_url;
            $topic_post_stats[$stat->topic_id]['post_count'] = $stat->post_count;
        }

        if ($auth) {
            //Get the logged in user
            $logged_in_user_id = \Common\Models\SiteUser::getSiteMemberUserId($token_login_id, $token_login_email);

            $this->view->setVar('logged_in_user_id', $logged_in_user_id);

            //Read tracker generates next to read urls.
            $topic_read_tracker = \Common\Models\ForumUserReadTrackStats::findFirstByUserId($logged_in_user_id);

            $users_read_topic_urls = $topic_read_tracker->getLastReadPostUrls($paginated_page->items, $topic_ids);
            if (isset($pinned_topics)) {
                $users_read_pinned_topic_urls = $topic_read_tracker->getLastReadPostUrls(
                    $pinned_topics,
                    $pinned_topic_ids
                );
                $this->view->setVar('user_read_pinned_topic_urls', $users_read_pinned_topic_urls);
            }
            $this->view->setVar('user_read_topic_urls', $users_read_topic_urls);
        }

        $this->view->setVar('topic_post_stats', $topic_post_stats);
        $this->view->setVar("forum_type_title", $forum_type_title);
        $this->view->setVar("pinned_topics", $pinned_topics);
        $this->view->setVar('user_rating', $user_rating);
        $this->view->setVar("sysadmin", $sysadmin);
        $this->view->setVar("topic_views", $topic_views);
        $this->view->setVar("auth", $auth);
        $this->view->setVar("page", $paginated_page);
        $this->view->setVar("forum", $forum);
    }

    public function topicAction()
    {
        $this->assets->addJs("js/jquery.paginater.js");
//        $this->assets->addJs("js/wysibb/jquery.wysibb.js");
//        $this->assets->addJs("js/wysibb-options.js");

        $page_size = $this->cookies->get('pageSize')->getValue() != null ?
            $this->cookies->get('pageSize')->getValue() : 15;

        //Is user logged in?
        $auth = $this->session->get('auth') != null ? true : false;
        $topic_rated = $this->dispatcher->getParam("rate") == 'rate' ? true : false; // Rate Topic
        $topic_id = $this->dispatcher->getParam("topic"); // This Topic
        $page_requested = $this->dispatcher->getParam("page_requested");

        $topic = \Common\Models\ForumTopic::findFirstById($topic_id);
        Tag::setTitle($topic->title);

        //Get all Posts for topic with bespoke view
        $aTopic_forum_posts = $topic->getPostsForView();

        $paginated = new \Phalcon\Paginator\Adapter\NativeArray(
            [
                "data" => $aTopic_forum_posts,
                "limit" => $page_size,
                "page" => $this->dispatcher->getParam("page_requested", "int")
            ]
        );
        $paginated_page = $paginated->getPaginate();
        $page_base = ($paginated_page->current - 1) * $paginated_page->limit;

        //Get Site Site User Achievements = Return array of array[$user_id] = achievment_file_name
        $site_user_ids_in_page = array_map(function ($item) {
            return intval($item['user_id']);
        }, $paginated_page->items);

        $site_user_achievements = \Common\Models\SiteUser::getAchievementImageNamesForUsers($site_user_ids_in_page);


        //Topic View Counter #TODO - FACTOR OUT
        $auth = $this->session->get('auth');

        if ($auth) {
            $user = \Common\Models\SiteUser::findFirstByLoginId($this->session->get("auth")["name"]);
            $user_id = $user->id;
            $this->view->setVar("user_id", $user_id);

            $token_login_id = $this->session->get('auth')["name"];
            $token_login_email = $this->session->get('auth')["email"];

            if ($auth) {
                $logged_in_user_id = \Common\Models\SiteUser::getSiteMemberUserId($token_login_id, $token_login_email);
                $this->view->setVar('logged_in_user_id', $logged_in_user_id);
            }
            
            //Tracker will update if not the read up to the post
            $read_tracker = \Common\Models\ForumUserReadTrackStats::findFirstByUserId($user_id);
            $aReadTrackerTopics = (Array)$read_tracker->topics;

            $previously_read_post_ordinal = isset($aReadTrackerTopics[$topic_id]) == true ?
                $aReadTrackerTopics[$topic_id] : false;
            $this->view->setVar("previously_read_post_date", $previously_read_post_ordinal);

            $last_post_in_page = end($paginated_page->items)['topic_ordinal'];
            $read_tracker->updateTopicRead($topic_id, $last_post_in_page);

            //If the topic counter doesn't exist then create it
            $topic_counter = \Common\Models\TopicViewCounterStats::findFirst([
                ["topic_id" => intval($topic->id)]
            ]);

            if (isset($user_id) == true && $topic_counter == false) {
                $new_topic_counter = new \Common\Models\TopicViewCounterStats();
                $new_topic_counter->forum_id = intval($topic->forum_id);
                $new_topic_counter->topic_id = intval($topic->id);
                $new_topic_counter->users = [intval($user_id)];

                if ($new_topic_counter->save() == false) {
                    foreach ($new_topic_counter->getMessages() as $message) {
                        echo "Warning: View counter broken please report to admin or support";
                    }
                }
            } elseif (isset($user_id) == true && $topic_counter->topic_id == $topic->id) {
                $user_found = array_search($user_id, $topic_counter->users);

                if ($user_found === false) {//must use object equality
                    array_push($topic_counter->users, intval($user_id));
                    if ($topic_counter->save() == false) {
                        foreach ($new_topic_counter->getMessages() as $message) {
                            echo "Warning: View counter broken please report to admin or support";
                        }
                    }
                }
            }

            $form = new \Multiple\Frontend\Forms\Forum\ForumTopicReply();
            $this->view->setVar("form", $form);
        }

        if ($auth != null && $auth["privelege"] != 'guest' && $topic_rated == 1) {
            $user = \Common\Models\SiteUser::findFirstByLoginId($this->session->get("auth")["name"]);
            $user_id = $user->id;

            $back_to_forum = "/forum/" . $topic->forum_id . "/" . $topic->id . "/" . $page_requested;

            $topic_rating = \Common\Models\TopicRatingStats::findFirst([
                ["topic_id" => intval($topic->id)]
            ]);

            if (!$topic_rating) {
                $new_rating = new \Common\Models\TopicRatingStats();
                $new_rating->forum_id = intval($topic->forum_id);
                $new_rating->topic_id = intval($topic->id);
                $new_rating->users = [intval($user->id)];

                if ($new_rating->save() == false) {
                    foreach ($new_rating->getMessages() as $message) {
                        $this->flashSession->warning("Error: There is an error in Topic Rating. 
                        Please report to admin.");
                    }

                    return $this->response->redirect($back_to_forum);
                } else {
                    $this->flashSession->success("You have rated this topic.");
                    return $this->response->redirect($back_to_forum);
                }
            } elseif (!in_array($user_id, $topic_rating->users)) {
                array_push($topic_rating->users, $user->id);

                if ($topic_rating->save() == false) {
                    foreach ($topic_rating->getMessages() as $message) {
                        $this->flashSession->warning("Error: There is an error in Topic Rating. 
                        Please report to admin.");
                    }

                    return $this->response->redirect($back_to_forum);
                } else {
                    $this->flashSession->success("You have rated this topic.");
                    return $this->response->redirect($back_to_forum);
                }
            } else {
                $this->flashSession->warning("You have already rated this topic.");
                return $this->response->redirect($back_to_forum);
            }
        }

        $user_rating = \Common\Models\TopicRatingStats::getTopicRating($topic_id);
        $this->view->setVar('user_rating', $user_rating);

        $privelege = $this->session->get('auth')["privelege"];
        $is_admin = $privelege === 'sysadmin' || $privelege === 'admin' ? true : false;

        $this->view->setVar('site_user_achievements', $site_user_achievements);
        $this->view->setVar("editable", $is_admin);
        $this->view->setVar('auth', $auth);
        $this->view->setVar('page_base_num', $page_base); //Used to calculate Topic used in page jump functionality
        $this->view->setVar('topic', $topic);
        $this->view->setVar("page", $paginated_page);
    }

    public function addTopicAction()
    {
//        $this->assets->addJs("js/wysibb/jquery.wysibb.js");
//        $this->assets->addJs("js/wysibb-options.js");
        $this->assets->addJs("js/forum-topic-helpers.js");

        $forum_id = $this->dispatcher->getParam("forum");

        $forum = \Common\Models\Forum::findFirstById($forum_id);

        $this->view->setVar("forum", $forum);

        $form = new \Multiple\Frontend\Forms\Forum\ForumTopicForm();
        $this->view->setVar('form', $form);


        if ($this->request->isPost()) {
            $topic = new \Common\Models\ForumTopic();
            $original_post = new \Common\Models\ForumTopicPost();
            $login_id = $this->session->get('auth')["name"];

            $forum_id = $this->dispatcher->getParam("forum");
            $user = \Common\Models\SiteUser::findFirstByLoginId($login_id);

            $original_post->content_bb = $this->request->getPost('bbcode_field');
            $original_post->content_html = $this->request->getPost('content_html');
            $original_post->view_type = $forum->view_type;
            $original_post->user_id = $user->id;
//            $original_post->forumTopic = $topic; //Magic setter

            $topic->forum_id = $forum_id;
            $topic->user_id = $user->id;
            $topic->title = $this->request->getPost('title');
            $topic->description = $this->request->getPost('description');
            $topic->status = "A"; //Checked by trait MySoftDeletable
            $topic->view_type = $forum->view_type; //Default view type is that of parent forum.
            $topic->forumTopicPost = $original_post;

            if ($topic->save() == false) {
                foreach ($topic->getMessages() as $message) {
                    $this->flashSession->warning("Warning: " . $message);
                }
                return;
            } else {
                $this->flashSession->success("Topic " . $topic->title . " has been saved successfully.");
            }

            return $this->response->redirect('forum/' . $forum_id . '/1');
        }
    }

    public function topicReplyAction()
    {

//        $this->assets->addJs("js/wysibb/jquery.wysibb.js");
//        $this->assets->addJs("js/wysibb-options.js");
        $this->assets->addJs("js/forum-topic-helpers.js");

        $topic_id = $this->dispatcher->getParam('topic');
        $page_num = $this->dispatcher->getParam('page');

        if ($post_id = $this->dispatcher->getParam('post')) {
            $post = \Common\Models\ForumTopicPost::findFirst($post_id);
            $post_quote_text = $post->content_bb;

            $quote_text = "[quote]" . $post_quote_text . "[/quote]";
            $this->view->setVar("quote_text", $quote_text);

        }

        $form = new \Multiple\Frontend\Forms\Forum\ForumTopicReply();

        $topic = \Common\Models\ForumTopic::findFirstById($topic_id);
        $original_post = \Common\Models\ForumTopicPost::findFirstByTopicId($topic_id);
        $this->view->setVar('form', $form);
        $this->view->setVar('topic', $topic);
        $this->view->setVar('original_post_content', $original_post->content_bb);
    }

    public function addTopicReplyAction()
    {
        $reply_bb = $this->request->getPost("bbcode_field");
        $reply_html = $this->request->getPost("content_html");
        $action = $this->request->getPost("action");
        $topic_id = $this->dispatcher->getParam('topic');

        $topic = \Common\Models\ForumTopic::findFirstById($topic_id);

        $session_login_id = $this->session->get("auth")["name"];
        $user = \Common\Models\SiteUser::findFirstByLoginId($session_login_id);

        if ($action == "Reply") {
            $reply = new \Common\Models\ForumTopicPost();
            $reply->user_id = $user->id;
            $reply->content_bb = $reply_bb;
            $reply->content_html = $reply_html;
            $reply->forumTopic = $topic;
            $reply->editor_user_id = null;
            $reply->view_type = "P";


            if ($reply->save() == false) {
                foreach ($reply->getMessages() as $message) {
                    $this->flashSession->warning("<b>" . $message . "</b>");
                }
            } else {
                $this->flashSession->success("Reply to " . $topic->title . " successful");
            }
        } elseif ($action == "Update") {
            //Do update
        } else {
            //something went wrong
        }
        $page_size = $this->cookies->get('pageSize')->getValue() != null ?
            $this->cookies->get('pageSize')->getValue() : 15;
        $redirect_url = $reply->getUrl($page_size);
        $this->response->redirect($redirect_url);
    }

    public function flagTopicReplyAction()
    {
//        $this->assets->addJs("js/wysibb/jquery.wysibb.js");
//        $this->assets->addJs("js/wysibb-options.js");
        $this->assets->addJs("js/navigation.js");

        $post_id = $this->dispatcher->getParam("post_id");

        $post = \Common\Models\ForumTopicPost::findFirstById($post_id);

        $offending_post_content = $post->content_bb;

        $form = new \Multiple\Frontend\Forms\Forum\ForumTopicReplyFlag();

        $this->view->setVar('offending_post_content', $offending_post_content);
        $this->view->setVar('form', $form);
        $this->view->setVar('post', $post);

        if ($this->request->isPost()) {
            $forum_id = $this->dispatcher->getParam("forum_id");
            $topic_id = $this->dispatcher->getParam("topic_id");
            $post_id = $this->dispatcher->getParam("post_id");
            $content_bb = $this->request->getPost("bbcode_field");

            $session_user_email = $this->session->get('auth')["email"];
            $session_user_login = $this->session->get('auth')["name"];

            $user = SiteUser::findFirst([
                'email = "' . $session_user_email . '" AND login_id = "' . $session_user_login . '"'
            ]);

            $already_flagged = ForumTopicPostsFlagged::findFirst([
                'user_id = "' . $user->id . '" AND post_id = "' . $post_id . '"'
            ]);

            if (!$already_flagged) {
                $flag_post = new ForumTopicPostsFlagged();
                $flag_post->user_id = $user->id;
                $flag_post->topic_id = $topic_id;
                $flag_post->post_id = $post_id;
                $flag_post->content_bb = $content_bb;

                if ($flag_post->save() == false) {
                    foreach ($flag_post->getMessages() as $message) {
                        $this->flashSession->warning("Warning: " . $message);
                    }
                    return;
                } else {
                    $this->flashSession->success("<b>Post has been flagged to admin</b>");
                    return $this->response->redirect("forum/" . $forum_id . "/" . $topic_id . "/1");
                }

            } else {
                $this->flashSession->success("<b>You have already flagged this post to admin</b>");
                return $this->response->redirect("forum/" . $forum_id . "/" . $topic_id . "/1");
            }
        }
    }

    public function editTopicReplyAction()
    {

//        $this->assets->addJs("js/wysibb/jquery.wysibb.js");
//        $this->assets->addJs("js/wysibb-options.js");

        $post_id = $this->dispatcher->getParam("post_id");

        $post = \Common\Models\ForumTopicPost::findFirst($post_id);
        $topic = $post->getForumTopic();
        $page_size = $this->cookies->get('pageSize')->getValue() != null ?
            $this->cookies->get('pageSize')->getValue() : 15;

        $post_url = $this->url->get("forum/") . $post->getUrl($page_size);

        $form = new \Multiple\Frontend\Forms\Forum\ForumEditTopicReply($post);

        $this->view->setVar("form", $form);
        $this->view->setVar("topic", $topic);
        $this->view->setVar("post", $post);
        $this->view->setVar("post_url", $post_url);
    }

    public function updateTopicReplyAction() //Does not have view redirect from this action controller
    {
        $auth = $this->session->get("auth");
        $forum_id = $this->dispatcher->getParam("forum_id");
        $topic_id = $this->dispatcher->getParam("topic_id");
        $post_id = $this->dispatcher->getParam("post_id");
        $page_size = $this->cookies->get('pageSize')->getValue() != null ?
            $this->cookies->get('pageSize')->getValue() : 15;

        $action = $this->request->getPost("action");

        if ($action) {
            switch ($action) {
                case "Delete":
                    return $this->dispatcher->forward([
                        "action" => "deleteTopicReply",
                        "params" => ["post_id" => $post_id]
                    ]);
                    break;
            }
        }

        $post = \Common\Models\ForumTopicPost::findFirstById($post_id);
        $authorised_user = $post->getAuthorisedUser($auth);
        $edit_topic_reply_url = "forum/" . $forum_id . "/" . $topic_id . "/editreply/" . $post_id;

        $http_post = $this->request->getPost();
        $content_bb = $http_post["bbcode_field"];
        $view_type = $http_post["view_type"];
        $action = $http_post["action"];

        $the_same_content = $post->content_bb === $content_bb;

        if ($this->request->isPost() && $authorised_user && !$the_same_content) {
            switch ($action) {
                case "Update":
                    $post->content_bb = $content_bb;
                    $post->view_type = $view_type;
                    $post->editor_user_id = $authorised_user->id;

                    if ($post->update() == false) {
                        foreach ($post->getMessages() as $message) {
                            $this->flashSession->warning("Error: The reply could not be  " . $message);
                        }
                        return $this->response->redirect($edit_topic_reply_url);
                    } else {
                        $this->flashSession->success("Reply Updated.");
                        return $this->response->redirect("forum/" . $post->getUrl($page_size));
                    }

                    break;
            }
        } else {
            $this->flashSession->warning("Reply not edited.");
            return $this->response->redirect($edit_topic_reply_url);
        }
    }

    public function deleteTopicReplyAction()
    {
        $auth = $this->session->get("auth");
        $post_id = $this->dispatcher->getParam("post_id");
        $page_size = $this->cookies->get('pageSize')->getValue() != null ?
            $this->cookies->get('pageSize')->getValue() : 15;

        $post = \Common\Models\ForumTopicPost::findFirstById($post_id);
        $authorised_user = $post->getAuthorisedUser($auth);

        if ($authorised_user) {
            $post->editor_id = $authorised_user->id;

            if (!$post->update()) {
                foreach ($post->getMessages() as $message) {
                    $this->flashSession->error("Post not deleted: " . $message);
                    return $this->response->redirect("forum/" . $post->getUrl($page_size));
                }
            } else {
                echo "<BR> IN AFTER SAVE";
                if (!$post->delete()) {
                    foreach ($post->getMessages() as $message) {
                        $this->flashSession->error("Post not deleted: " . $message);
                        return $this->response->redirect("forum/" . $post->getUrl($page_size));
                    }
                } else {
                    $this->flashSession->success("Post deleted: ");
                    return $this->response->redirect("forum/" . $post->getUrl($page_size));
                }
            }
        }
    }

    public function allTopicsAction()
    {
        \Phalcon\Tag::setTitle("All Topics");

        $this->assets->addJs("js/jquery.paginater.js");

        $page_size = $this->cookies->get('pageSize')->getValue() != null ?
            $this->cookies->get('pageSize')->getValue() : 15;

        $auth = $this->session->get('auth') != null ? true : false;
        $sysadmin = $this->session->get('auth')["privelege"] == 'sysadmin' ? true : false;

        //Get Pinned Topics
        $pinned_topics = \Common\Models\ForumTopic::getAllPinnedTopicsView();
        $pinned_topics = count($pinned_topics) > 0 ? $pinned_topics : null;

        if (isset($pinned_topics)) {
            //Get topics in current page and build post urls
            $pinned_topic_ids = array_map(function ($item) {
                return intval($item['topic_id']);
            }, $pinned_topics);

            $pinned_topic_post_stats = \Common\Models\ForumTopicPostStats::find([ //Get forum stats for current page
                [
                    "topic_id" => [
                        '\"$in\"' => $pinned_topic_ids
                    ]
                ]
            ]);

            foreach ($pinned_topic_post_stats as $stat) {
                $page = floor($stat->post_count / $page_size) + 1;
                $post_url = "forum/" . $stat->forum_id . "/" . $stat->topic_id . "/" . $page . "#" . $stat->post_count;
                $pinned_topic_post_stats[$stat->topic_id]['post_url'] = $post_url;
                $pinned_topic_post_stats[$stat->topic_id]['post_count'] = $stat->post_count;
            }
        }

        //Get Paginated Topics
        $forumTopicsViewData = \Common\Models\ForumTopic::getAllTopicsView();

        $paginated = new \Phalcon\Paginator\Adapter\NativeArray(
            [
                "data" => $forumTopicsViewData,
                "limit" => 15,
                "page" => $this->dispatcher->getParam("page_requested", "int")
            ]
        );

        $paginated_page = $paginated->getPaginate();


        $topic_stats = \Common\Models\TopicViewCounterStats::aggregate([
            [
                '$project' => [
                    '_id' => 0,
                    'topic_id' => 1,
                    'view_count' => ['$size' => '$users']
                ]
            ]
        ]);

        foreach ($topic_stats as $topic_view) {
            $topic_views[$topic_view["topic_id"]] = $topic_view["view_count"];
        }

        // Get Forum Topic Read Tracker
        $token_login_id = $this->session->get('auth')["name"];
        $token_login_email = $this->session->get('auth')["email"];

        $user_rating = \Common\Models\TopicRatingStats::getForumTopicRatings($paginated_page->items);

        if (isset($pinned_topics)) {
            $user_rating_pinned = \Common\Models\TopicRatingStats::getForumTopicRatings($pinned_topics);
        }

        //Get topics in current page and build post urls
        $topic_ids = array_map(function ($item) {
            return intval($item['topic_id']);
        }, $paginated_page->items);


        $forum_topic_post_stats = \Common\Models\ForumTopicPostStats::find([ //Get forum stats for current page
            [
                "topic_id" => [
                    '$in' => $topic_ids
                ]
            ]
        ]);

        foreach ($forum_topic_post_stats as $stat) {
            $page = floor($stat->post_count / $page_size) + 1;
            $post_url = "forum/" . $stat->forum_id . "/" . $stat->topic_id . "/" . $page . "#" . $stat->post_count;
            $topic_post_stats[$stat->topic_id]['post_url'] = $post_url;
            $topic_post_stats[$stat->topic_id]['post_count'] = $stat->post_count;
        }

        if ($auth) {
            //Get the logged in user
            $logged_in_user_id = \Common\Models\SiteUser::getSiteMemberUserId($token_login_id, $token_login_email);

            $this->view->setVar('logged_in_user_id', $logged_in_user_id);

            //Read tracker generates next to read urls.
            $topic_read_tracker = \Common\Models\ForumUserReadTrackStats::findFirstByUserId($logged_in_user_id);

            $users_read_topic_urls = $topic_read_tracker->getLastReadPostUrls($paginated_page->items, $topic_ids);

            if (isset($pinned_topics)) {
                $users_read_pinned_topic_urls =
                    $topic_read_tracker->getLastReadPostUrls($pinned_topics, $pinned_topic_ids);
            }
            $this->view->setVar('user_read_topic_urls', $users_read_topic_urls);
            $this->view->setVar('user_read_pinned_topic_urls', $users_read_pinned_topic_urls);
        }

        $this->view->setVar('topic_post_stats', $topic_post_stats);
        $this->view->setVar('pinned_topic_post_stats', $pinned_topic_post_stats);
        $this->view->setVar("pinned_topics", $pinned_topics);
        $this->view->setVar('user_rating', $user_rating);
        $this->view->setVar('user_rating_pinned', $user_rating_pinned);
        $this->view->setVar("sysadmin", $sysadmin);
        $this->view->setVar("topic_views", $topic_views);
        $this->view->setVar("auth", $auth);
        $this->view->setVar("page", $paginated_page);
    }

    public function searchAction()
    {
        $search_term = $this->request->getPost("search-term", "string");
        $search_by_title = $this->request->getPost("forum-search-by-title", "int") === "1" ? 1 : 0;
        $search_by_content = $this->request->getPost("forum-search-by-content", "int") === "1" ? 1 : 0;

        $search_form = new \Multiple\Frontend\Forms\Forum\ForumSearchForm();

        $search_by_title_results = false;
        $search_by_title_content = false;

        if ($search_by_title && $search_by_content) {
            $search_by_title_results = \Common\Models\ForumTopic::find([
                "conditions" => 'title like "%' . $search_term . '%"',
                "order" => "id desc",
                "limit" => 18
            ]);

            $search_by_title_content = \Common\Models\ForumTopicPost::find([
                "conditions" => 'content_bb like "%' . $search_term . '%"',
                "order" => "id desc",
                "limit" => 18
            ]);
        } elseif ($search_by_content) {
            $search_by_title_content = \Common\Models\ForumTopicPost::find([
                "conditions" => 'content_bb like "%' . $search_term . '%"',
                "order" => "id desc",
                "limit" => 18
            ]);
        } elseif ($search_by_title) {
            $search_by_title_results = \Common\Models\ForumTopic::find([
                "conditions" => 'title like "%' . $search_term . '%"',
                "order" => "id desc",
                "limit" => 18
            ]);
        } else {
            $search_form->get("forum-search-by-title")->setDefault(1);
            $search_by_title_results = \Common\Models\ForumTopic::find([
                "conditions" => 'title like "%' . $search_term . '%"',
                "order" => "id desc",
                "limit" => 18
            ]);
        }


        $this->view->setVar("title_results", $search_by_title_results);

        $this->view->setVar("content_results", $search_by_title_content);

        $this->view->setVar("form", $search_form);
        $this->view->setVar("search_term", $search_term);
    }


}