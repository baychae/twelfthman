<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 15/06/2016
 * Time: 17:12
 */

namespace Multiple\Frontend\Controllers;

use Phalcon\Tag as Tag;

class ErrorsController extends ControllerBase
{

    public function initialize()
    {
        Tag::setTitle('Home');
    }


    public function show404Action()
    {

    }

    public function show500Action()
    {

    }

    public function show403Action()
    {
        $this->response->setStatusCode(403, "403 Forbidden");
        $this->response->setContent("Sorry, the page doesn't exist");
    }


}