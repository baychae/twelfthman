<main id="forum">
    <header>
        <h1>Rivals</h1>
    </header>
    <section>
        <h2>Leagues</h2>
        {% for league in leagues %}
            <div class="table-row">
               <span class="league-name">
                   <a href="rivals/{{ league.name|prettyUrl }}">
                       <span>{{ league.name }}</span>
                       <span>
                           <img
                                   title="{{ league.name }}"
                                   alt="{{ league.name }}"
                                   src="{{ url.get( config.common.leagues.image_dir ~ league.name|prettyUrl ) }}.png"
                           >
                       </span>
                    </a>
               </span>
            </div>
        {% endfor %}
    </section>
</main>