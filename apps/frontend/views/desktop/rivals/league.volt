{{ content() }}
<main id="forum">
    <header>
        <h1>Rivals - {{ league_name }}</h1>

    </header>
    <section id="league-rivals">
        <h2><a href="rivals/">Leagues</a> >> {{ league_name }}</h2>
        <img
                title="{{ league_name }}"
                alt="{{ league_name }}"
                class="league-main-img"
                src="{{ url.get( config.common.leagues.image_dir ~ league_name|prettyUrl ) }}.png"
        >
        <img
                title="{{ league_name }}"
                alt="{{ league_name }}"
                class="league-main-img"
                src="{{ url.get( config.common.leagues.image_dir ~ league_name|prettyUrl ) }}.png"
        >
        {% for match in matches %}
            <div class="table-row">
                <span>
                    <a {% if match.getHomeTeam().website  is not empty %}
                        href="//{{ match.getHomeTeam().website }}"
                        target="_blank"
                    {% endif %} >
                    <img
                            title="Take me to {{ match.getHomeTeam().name }} forum."
                            alt="{{ match.getHomeTeam().name }}"
                            src="img/teams/{{ match.getHomeTeam().image_name }}"
                    >
                </a>
                </span>
                <span>
                    {{ match.getHomeTeam().name }}
                </span>
                <span>
                    <span>{{ match.start|datedecode }}</span>
                    <span>{{ match.home_score }} - {{ match.away_score }}</span>
                </span>
                <span>
                {{ match.getAwayTeam().name }}

            </span>
                <span>
                    <a {% if match.getAwayTeam().website  is not empty %}
                        href="//{{ match.getAwayTeam().website }}"
                        target="_blank"

                    {% endif %} >
                        <img
                                title="Take me to the {{ match.getAwayTeam().name }} forum"
                                alt="{{ match.getAwayTeam().name }}"
                                src="img/teams/{{ match.getAwayTeam().image_name }}"
                        >
                    </a>
                </span>
            </div>
        {% endfor %}

    </section>
</main>
