<h1 class="new-message-h1">
    {% if dispatcher.wasForwarded() %}
        {{ dispatcher.getpreviouscontrollername()|capitalize }} - {{ dispatcher.getpreviousactionname()|capitalize }}
    {% else %}
        {{ view.getControllerName()|capitalize }} - {{ view.getActionName()|capitalize }}
    {% endif %}
</h1>
<nav id="nav-vertical" class="messages-nav">
    {{ elements.buildVerticalNav( nav_menu_items, "img/buttons" ) }}
</nav>
{{ content() }}