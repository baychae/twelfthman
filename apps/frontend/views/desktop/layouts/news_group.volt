<nav id="news-nav-horizontal"> {{ elements.buildHorizontalNav(news_groups, config.frontend.news.group.image_dir ) }} </nav>

<table class="news-group-table">
    <tbody>
        <tr>
            <td> {% include "partials/news_articles.volt" %} </td>
            <td class="news-sidebar-right">RIGHT</td>
        </tr>
    </tbody>
</table>
