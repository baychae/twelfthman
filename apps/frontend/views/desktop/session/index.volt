<h1>Log In </h1>


{{ form('session/start', 'id': 'form') }}
<fieldset>
    <legend>
        Your Login Details {{ flashSession.output() }} {{ content() }}
    </legend>
    <div class="control-group">
        <label for="login">Username/Email</label>
        {{ text_field('login') }}
    </div>
    <div class="control-group">
        <label for="password">Password</label>
        {{ password_field('password') }}
    </div>
    <div>
        <label></label>
        <span>If you need to create a new account, <a href="session/register/">click here</a>.</span>
    </div>
    <div>
        <label></label>
        <span>If you have forgotten your username or password, <a href="session/lost-password">click here</a> to reset your details or email <a
                    href="mailTo: {{ config.frontend.admin.contact }}">{{ config.frontend.admin.contact }}</a> </span>
    </div>

</fieldset>
{{ submit_button('Login', 'class': 'form-button') }}