
{{ content() }}

<h1>Log Out</h1>
<section>
    <img
            title="Warning!"
            alt="Warning!"
            class="warning button"
            src="img/buttons/exclamation.png"
    >
    <span>Are you sure you want to log out of {{ config.site.name }}?</span>
</section>
<p>
    <a id="logout" class="form-button" href="session/end">Logout</a>
    <a id="cancel" class="form-button" href="{{ request.getHTTPReferer() }}">Cancel</a>
</p>