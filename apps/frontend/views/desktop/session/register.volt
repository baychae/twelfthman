{{ content() }}
{% do assets.addJs("https://www.google.com/recaptcha/api.js", false, null, ['async': 'async', 'defer':'defer'] ) %}
<h1>Create New Account</h1>


{{ form('session/register', 'id': 'form', 'class': 'form-horizontal', 'onbeforesubmit': 'return false') }}
<fieldset>
    <legend>New Account Details {{ flashSession.output() }}</legend>
    <div>
        {{ form.getLabel('first_name') }}
        {{ form.get('first_name') }}
        <sup>*(Optional)</sup>
    </div>
    <div>
        {{ form.getLabel('last_name') }}
        {{ form.get('last_name') }}
        <sup>*(Optional)</sup>
    </div>
    <div>
        {{ form.getLabel('email') }}
        {{ form.get('email') }}
        {% for message in form.getMessagesFor('email') %}
            {% include 'partials/form_error_message.volt' %}
        {% endfor %}
    </div>
</fieldset>
<fieldset>
    <legend>Display Details</legend>
    <div>
        {{ form.getLabel('login_id') }}
        {{ form.get('login_id') }}
        {% for message in form.getMessagesFor('login_id') %}
            {% include 'partials/form_error_message.volt' %}
        {% endfor %}
    </div>

    <div>
        {{ form.getLabel('password') }}
        {{ form.get('password') }}
        {% for message in form.getMessagesFor('password') %}
            {% include 'partials/form_error_message.volt' %}
        {% endfor %}
    </div>
    <div>
        {{ form.getLabel('password_repeat') }}
        {{ form.get('password_repeat') }}
        {% for message in form.getMessagesFor('password_repeat') %}
            {% include 'partials/form_error_message.volt' %}
        {% endfor %}
    </div>
    <input type="hidden" name="{{ security.getTokenKey() }}" value="{{ security.getToken() }}">

</fieldset>
<div class="g-recaptcha" data-sitekey="{{ config.common.recaptcha.data_site_key }}"></div>
{{ submit_button('Register', 'class': 'btn btn-primary btn-large') }}
</form>
