<h1>Password Reset</h1>

{{ content() }}

{{ form('session/password-reset', 'id': 'form') }}
<fieldset>
    <legend>
        Your New Password {{ flashSession.output() }}
    </legend>
    <div class="control-group">
        <label for="password">Password</label>
        {{ password_field('password') }}
    </div>
    <div class="control-group">
        <label for="password">Repeat Password</label>
        {{ password_field('repeast-password') }}
    </div>
    <input type="hidden" name="uuid" value="{{ uuid }}">

</fieldset>
{{ submit_button('Reset Password', 'class': 'form-button') }}