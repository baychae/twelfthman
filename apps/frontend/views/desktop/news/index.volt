{{ content() }}

<div class="inline-table">
    <nav id="nav-vertical" class="inline-table">
        {{ elements.buildVerticalNav(news_groups, config.frontend.news.group.image_dir ) }}
        <div>
            {% include "partials/adspace/news-vertical-nav-bar.volt" %}
        </div>
    </nav>
    <section id="news-splash-blurbs" class="container inline-table">
        {% include "partials/adspace/news-blurbs-small.volt" %}
        <h1>Sheffield Wednesday News</h1>
        {% include "partials/news_articles.volt" %} 
    </section>

    <aside id="news-sidebar-right" class="inline-table">

        {{ elements.getLastMatchPreviewPromo() }}
        {{ elements.getMatchDetails() }}
    </aside>
</div>