{{ content() }}


<link rel="stylesheet" type="text/css" href="js/Font-Awesome/css/font-awesome.css"/>
<link rel="stylesheet" type="text/css" href="js/jssocials/dist/jssocials.css"/>
<link rel="stylesheet" type="text/css" href="js/jssocials/dist/jssocials-theme-minima.css"/>

<main id="news-main-article" itemscope itemtype="http://schema.org/NewsArticle">
    <nav id="news-nav-horizontal"> {{ elements.buildHorizontalNav(news_groups,config.frontend.news.group.image_dir ) }} </nav>
    <article id="main-article" itemprop="mainEntity">
    <h1 itemprop="headline">{{ article.title }}</h1>
        <div id="share" style="display: inline-block; vertical-align: top;"></div>

        <script src="js/jssocials/dist/jssocials.js"></script>
        <script>
            $("#share").jsSocials({
                shares: [
                    {
                        share: "twitter",
                        hashtags: "{{ article.title|camelcaseWhitespace }}"
                    }, "facebook"
                ],
                url: "{{ url.get( router.getReWriteUri() ) }}"
            });
        </script>
    <span itemprop="articleSection" hidden>{#{{ article.getNewsGroup().label }}#}</span>
    <div itemprop="articleBody">
        {% if art_url is defined %}

            {% if article.image_caption|length > 0 %} 
                <div class="article-feature-art-caption">{{article.image_caption}}</div>
                <img
                        title="{{ article.image_caption }}"
                        alt="{{ article.image_caption }}"
                        class="article-feature-art"
                        src="{{ art_url }}"
                        itemprop="image"
                >
            {% else %}
                <img
                        title="{{ article.title }}"
                        alt="{{ article.title }} Image"
                        class="article-feature-art"
                        src="{{ art_url }}"
                        itemprop="image"
                >
            {% endif %}

        {% elseif art_embed is defined%}
            <div class="article-feature-art">
                {{ art_embed }}
            </div>
        {% endif %}
        <!-- Article Start --> {# For syndicated news scrapers #}
        {{ article.content_html }}
        <!-- Article End -->
        <time property="datePublished" datetime="{{ article.created|iso8601 }}">{{ article.created|datedecode }}</time>
    </div>        
    </article>
</main>
<aside id="news-main-article-aside">

    <div id="news-main-article-aside-owner">
        <div id="news-main-article-aside-tools">
            {% if articles_read[ article.id ] is defined %}
                <img
                        title="You have read this article"
                        alt="You have read this article"
                        class="button info"
                        src="{{ url.get('img/buttons/text.png') }}"
                >
            {% else %}
                <img
                        title="You have read this article"
                        alt="You have read this article"
                        class="button success"
                        src="{{ url.get('img/buttons/text.png') }}"
                >
            {% endif %}
            <img
                    alt="New Message"
                    class="button warning"
                    src="{{ url.get('img/buttons/new-message.png') }}"
            >
        </div>
        <div id="news-main-article-aside-author">
            <a id="author" href="{{ url.get('members/profile?user=' ~ article.getSiteUser().avatar_name) }}">

                {{ article.getSiteUser().avatar_name }}

            </a>
            {% if article.getSiteUser().avatar_file_name is defined %}
                <img
                        alt="Avatar Image"
                        src="{{ url.get('img/user-avatars/' ~ article.getSiteUser().avatar_file_name) }}"
                >
            {% else %}
                <img
                        alt="Default Avatar Image"
                        src="{{ url.get('img/user-avatars/default-avatar.png') }}"
                >
            {% endif %}
            <time datetime="{{ article.created|iso8601 }}">{{ article.created|datedecode }}</time>

        </div>
    </div>
    <div id="news-main-article-aside-other">
        <h2>Other {{ article.getNewsGroup().label }} News</h2>
        <ul>
            {% for article in current_news_group_articles %}
                <li>
                    <a href="{{ url.get('news/article/' ~ article.id) }}">
                        <span>{{ article.title }}</span>
                        <img
                                alt="{{ article.title }}"
                                src="img/buttons/arrow_right.png"
                        >
                    </a>
                </li>
            {% endfor %}
        </ul>
    </div>

</aside>