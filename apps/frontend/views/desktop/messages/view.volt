<section id="form" class="message-form" method="post" enctype="multipart/form-data">
    <fieldset>
        <legend>Messages >> View Message {{ flashSession.output() }}</legend>
        <div>
            <label>From:</label> <span>{{ message.from }}</span>
        </div>
        <div>
            <label>Subject:</label> <span>{{ message.subject }}</span>
        </div>
        <div>
            <label>Sent:</label> <span>{{ message.modified|datedecode }}</span>
        </div>
    </fieldset>

    <fieldset>
        <legend>Message Body</legend>
        <div>
            <p>{{ message.body_bb|parsebbtohtml|nl2br }}</p>
        </div>
    </fieldset>

    <a class="form-button" href="messages/inbox">Close</a>
    <a class="form-button" href="messages/reply/{{ message.uri }}">Reply</a>
    <a class="form-button" href="messages/delete/{{ message.uri }}"
       onclick="return confirm('Are you sure you would like to delete this message?')">
        Delete
    </a>
</section>