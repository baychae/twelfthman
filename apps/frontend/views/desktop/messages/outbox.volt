<div class="messages-section" >
    <h2>Outbox
        <div class="paginator">{{ elements.getPaginator(page, "Messages") }} </div>
    </h2> {{ flashSession.output() }}

    {% include "partials/messages-outbox.volt" %}

</div>