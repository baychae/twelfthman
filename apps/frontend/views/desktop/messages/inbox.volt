{{ content() }}
<div class="messages-section">
    <h2>Inbox {{ flashSession.output() }}
        <div class="paginator">{{ elements.getPaginator(page, "Messages") }} </div>
    </h2>

    {% include "partials/messages-inbox.volt" %}

</div>