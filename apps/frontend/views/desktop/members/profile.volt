{% if member.avatar_name is defined %}
    {% set display_name = member.avatar_name %}
{% else %}
    {% set display_name = member.login_id %}
{% endif %}

{{ setTitle( display_name ~ "'s Profile") }}

<main id="member-profile">
    <header>
        <h1>
            {{ display_name }}'s Profile
        </h1>
    </header>
    <section id="form">
        <fieldset>
            <legend>
                <h2>{{ display_name }}</h2>
            </legend>
            <div>
                <span class="label">Status:</span><span class="input-text">Unknown(#TODO)</span>
            </div>
            <div>
                <span class="label">Home Team:</span>
                {% if member.getTeam().image_name is defined %}
                    <img
                            title="{{ member.getTeam().name }}"
                            alt="{{ member.getTeam().name }}"
                            class="team"
                            src="{{ config.common.team.image.dir ~ member.getTeam().image_name }}"
                    >
                    <span>{{ member.getTeam().name }}</span>
                {% endif %}
            </div>
            <div>
                <span class="label">Avatar:</span>
                {% if member.avatar_file_name is defined %}
                    <img
                            title="{{ display_name }}"
                            alt="{{ display_name }}"
                            src="img/user-avatars/{{ member.avatar_file_name }}"
                    >
                    <span class="input-text"></span>
                {% endif %}
            </div>
            <div>
                <span class="label">First Used:</span>{{ member.created|shortdate }}<span class="input-text"></span>
            </div>
            <div>
                <span class="label">Last Active</span>
                {% if member.getForumTopicPost().getLast().created is defined %}
                    {{ member.getForumTopicPost().getLast().created|datedecode }}
                {% endif %}
                <span class="input-text"></span>
            </div>
            <div>
                <span class="label">Posts:</span><span class="input-text">{{ posts }}</span>
            </div>
            <div>
                <span class="label">Signature:</span> {{ html_entity_decode( member.signature_html ) }}
            </div>
            <div>
                <span class="label">Achievements:</span>
                <span>
                    {% for achievement in member.siteUserAchievementType %}
                        <img
                                title="{{ achievement.title }}"
                                alt="{{ achievement.title }}"
                                class="member-achievement-badge"
                                src="{{ config.backend.members.achievement.image_dir ~ achievement.file_name }}"
                        >
                    {% endfor %}
                </span>
            </div>
            <div>
                <span class="label"></span>
                <span>
                    {% if site_user_friend is not null %}
                        <a href="{{ url.get('members/unfriend/' ~ display_name|url_encode ) }}">
                            <img
                                    title="Unfriend {{ display_name }}"
                                    alt="Unfriend {{ display_name }}"
                                    class="button danger"
                                    src="{{ url.get('img/buttons/friend.png') }}"
                            >
                            <span title="Unfriend {{ display_name }}"
                                  alt="Unfriend {{ display_name }}">Unfriend {{ display_name }}</span>
                        </a>
                    {% else %}
                        <a href="{{ url.get('messages/friend-request?friend=' ~ display_name|url_encode ) }}">
                            <img
                                    title="Make friend request to {{ display_name }}"
                                    alt="Make friend request to {{ display_name }}"
                                    class="button warning"
                                    src="{{ url.get('img/buttons/friend.png') }}"
                            >
                        </a>
                    {% endif %}
                </span>
            </div>
        </fieldset>
        {% if member.favourite_away_ground is not empty or member.favourite_all_time_player is not empty or member.favourite_current_player is not empty %}
            <fieldset>
                <legend>
                    <h2>Favourites</h2>
                </legend>
                {% if member.favourite_away_ground is not empty %}
                    <div>
                        <span class="label">Favorite Away Ground:</span><span
                                class="input-text">{{ member.favourite_away_ground }}</span>
                    </div>
                {% endif %}
                {% if member.favourite_all_time_player is not empty %}
                    <div>
                        <span class="label">All-Time Favourite Player:</span><span
                                class="input-text">{{ member.favourite_all_time_player }}</span>
                    </div>
                {% endif %}
                {% if member.favourite_current_player is not empty %}
                    <div>
                        <span class="label">Favourite Current Player:</span><span
                                class="input-text">{{ member.favourite_current_player }}</span>
                    </div>
                {% endif %}
            </fieldset>
        {% endif %}

    </section>
</main>