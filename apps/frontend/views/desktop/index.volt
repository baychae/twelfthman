<!DOCTYPE html>
<html lang="en">
    <head>
            {{ get_title() }}

        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ url('img/icons/apple-touch-icon.png') }}">
            {{ stylesheetLink("css/style.css") }}
            {{ stylesheetLink("js/wysibb/theme/twelfthman/twelfthman.css") }}
        {{ assets.outputInlineJs() }}
            {{ assets.outputJs() }}
        {{ assets.outputCss() }}
        {% include 'partials/adspace/ad-network-script-src.volt' %}
        <meta content="{{ config.social.facebook.app_id }}" property="fb:app_id">
        {#set by the paginator#}
            {% if rel_next is defined %} {{ rel_next }} {% endif %}
            {% if rel_prev is defined %} {{ rel_prev }} {% endif %}
        <base href="{{ url.get() }}"> {#TODO - Introduce redundancy by checking in config if url.get does not work #}
        {% if meta_data is defined %}
            {{ meta_data }}
        {% endif %}
    </head>

    <body>
    <header>
        <nav class="navbar">
            {{ elements.getNavBar() }}
        </nav>
        <div id='header-logo-desktop'></div>
        <div id='header-ad-space'>
            {% include 'partials/adspace/site-header-leaderboard.volt' %}
        </div>
    </header>

    <div id="page-content"> {# Next Level down is index controller view #}
        {{ flashSession.output() }}
            {{ content() }}


        <footer id="footer">
            {% include "partials" %}
            {% include "partials" %}
        </footer>
    </div>
    <!--TODO - Javascript goes here-->
    </body>

</html>