{% for message in page.items %}

    <div class="messages-itemised table-row">
        <div class="messages-itemised-control table-cell" >
            <a href="messages/reply/{{ message.uri }}"> {# TODO - Message Quick Reply #}
                <img alt="Quick Reply" title="Quick Reply" class="button warning" src="img/buttons/new-message.png">
            </a>
        </div>

        <div class="table-cell messages-itemised-details">
            <a href="messages/view/{{ message.uri }}"
               class="table-cell messages-itemised-subject">{{ message.subject }}</a>
            from
            <a href="members/profile?user={{ message.from }}"> {{ message.from }} </a><BR> {# TODO : get the user profile url. Build Members section #}
            sent on <a href="messages/view/{{ message.uri }}"> {{ message.modified|datedecode }} </a>
        </div>
        <div class="table-cell messages-itemised-snippet"><a
                    href="messages/view/{{ message.uri }}">{{ message.body_plaintext|substrtows(298) }}...</a></div>
    </div>


{% endfor %}