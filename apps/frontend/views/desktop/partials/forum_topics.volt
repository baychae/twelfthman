{{ content() }}
<div class="table-row-group">
    <div class="table-header table-row">
        <span></span><span>Topic</span> <span>Replies/Views</span><span>Last Update</span><span></span>
    </div>

    {% for topic in page.items %}
        <div class="table-row">

            {% if auth is false %}
                <span>
                    <a class="auth-false"
                       href="{{ url.get( 'forum/' ~ topic['forum_id'] ~ '/' ~ topic['topic_id'] ~ '/1' ) }}">
                        <img
                                title="{{ topic["topic_title"] }}"
                                alt="{{ topic["topic_title"] }}"
                                class="button success"
                                src="{{ url.get('img/buttons/text.png') }}"
                        >
                    </a>
                </span>
            {% else %}
                <span>
                  {{ user_read_topic_urls[ topic['topic_id'] ]['html'] }}
                </span>
            {% endif %}

            <div> {##}
                <h3>
                    <a href="{{ url.get('forum/' ~ topic['forum_id'] ~ '/' ~ topic['topic_id'] ~ '/1') }}">{{ topic["topic_title"] }}</a>
                    {% if user_rating[topic['topic_id']] is defined %} {{ elements.getTopicRatingStars(user_rating[topic['topic_id']]) }} {% endif %}
                </h3>
                <p>{% if topic.description is not empty %} {{ topic.description }}, by {% else %} by {% endif %}
                    {% if logged_in_user_id is defined %}
                        <a href="{{ url.get( 'members/profile?user=' ~ topic['last_post_avatar_name']|url_encode ) }}">{{ topic['last_post_avatar_name'] }}</a>
                    {% else %}
                        <a href="{{ url.get('session/') }}">{{ topic['last_post_avatar_name'] }}</a>
                    {% endif %}
                </p>
            </div>
            <div> {##}
                <dl>
                    <dt>Replies:</dt>
                    <dd>{{ topic_post_stats[ topic['topic_id'] ]['post_count'] }}</dd>
                </dl>
                <dl>
                    <dt>Views:</dt>
                    <dd> {{ topic_views[topic['topic_id']] is not defined ? 0 : topic_views[topic['topic_id']] }}
                    <dd>
                </dl>
            </div>
            <div> {##}
                {% if logged_in_user_id is defined %}
                    <a href="{{ url.get('members/profile?user=' ~ topic['last_post_avatar_name']|url_encode ) }}">{{ topic['last_post_avatar_name'] }}</a>
                {% else %}
                    <a href="{{ url.get('session/') }}">{{ topic['last_post_avatar_name'] }}</a>
                {% endif %}
                <p>{{ topic['last_post_created']|datedecode }} </p>
            </div>
            <div>  {##}
                {% if auth is true %}
                    {% if user_read_topic_urls[ topic['topic_id'] ]['unread'] is defined %}
                        <a href="{{ url.get('forum/topic/markread/' ~ topic['forum_id'] ~ "/" ~ topic['topic_id'] ~ "/" ~ page.current) }}">
                            <img
                                    alt="Mark All Topics Read"
                                    title="Mark All Topics Read" class="button success"
                                    src="{{ url.get('img/buttons/tick.png') }}"
                            >
                        </a>
                    {% endif %}
                {% endif %}
            </div>
        </div>
    {% endfor %}

</div>