{% do assets.addJs("https://code.jquery.com/ui/1.12.1/jquery-ui.js", false, null, [ "integrity" : "sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=", "crossorigin" : "anonymous" ]) %}
{% do assets.addCss( "css/forum-topic-search.css" ) %}
{% do assets.addInlineJs("Solr = " ~ config.solr|json_encode ~ ";") %}