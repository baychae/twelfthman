<div id="live-match">

<span>
    <a {% if current_match.getHomeTeam().website  is not empty %}
        href="//{{ current_match.getHomeTeam().website }}"
        target="_blank"
        title="Take me to {{ current_match.getHomeTeam().name }} forum."
    {% endif %} >
        <img src="img/teams/{{ current_match.getHomeTeam().image_name }}">
    </a>
    <span>{{ current_match.getHomeTeam().name }}</span>
</span>
    <span>{{ current_match.home_score }} - {{ current_match.away_score }}</span>
    <span>
    <span>{{ current_match.getAwayTeam().name }}</span>
    <span>
        <a {% if current_match.getAwayTeam().website  is not empty %}
            href="//{{ current_match.getAwayTeam().website }}"
            target="_blank"
            title="Take me to {{ current_match.getAwayTeam().name }} forum."
        {% endif %} >
            <img src="img/teams/{{ current_match.getAwayTeam().image_name }}">
        </a>
    </span>
</span>
    <div><a {% if current_match.getHomeTeam().website  is not empty %}
            href="//{{ current_match.getHomeTeam().website }}"
            target="_blank"
            title="Take me to {{ current_match.getHomeTeam().name }} forum."
        {% endif %} >
            Rivals Site
        </a></div>
</div>