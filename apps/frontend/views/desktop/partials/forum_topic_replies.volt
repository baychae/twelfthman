{% set current_url = router.getReWriteUri() %}

{% for post in page.items %}

    <article class="post">
        {% if auth is true %} {# Topic Reply Tools #}
            <div class="misc-tool">{#topic post tool bar#}
                {% if previously_read_post_date >= post['topic_ordinal'] %}
                    <img
                            title="Read Post"
                            alt="Read Post"
                            class="button info"
                            src="{{ url.get('img/buttons/text.png') }}"> {# css info class = blue - has been read #}
                {% else %}
                    <img
                            title="Unread Post"
                            alt="Unread Post"
                            class="button success"
                            src="{{ url.get('img/buttons/text.png') }}"> {# css success class = green - is a new post #}
                {% endif %}

                <a href="{{ url.get('messages/new-message?mail_to=' ~ post['display_name']|url_encode ) }}">
                    <img
                            title="Message {{ post['display_name'] }}"
                            alt="Message {{ post['display_name'] }}"
                            class="button warning"
                            src="{{ url.get('img/buttons/new-message.png') }}"></a>
                <a href="{{ url.get('forum/' ~ post['forum_id'] ~ '/' ~ post['topic_id'] ~ '/flagreply/' ~ post['id']) }}">
                    <img
                            title="Flag Post"
                            alt="Flag Post"
                            class="button info"
                            src="{{ url.get('img/buttons/flag.png') }}"
                    >
                </a>
                <img
                        title="Scroll to Top of Page"
                        alt="Scroll to Top of Page"
                        class="button success"
                        src="{{ url.get('img/buttons/arrow_up.png') }}"
                        onclick="window.scrollTo(0,0)"
                >
                <a href="{{ url.get('messages/friend-request?member=' ~ post['display_name']|url_encode ) }}">
                    <img
                            title="Make friend request to {{ post['display_name'] }}"
                            alt="Make friend request to {{ post['display_name'] }}"
                            class="button warning"
                            src="{{ url.get('img/buttons/friend.png') }}"
                    >
                </a>
            </div>
        {% endif %}
        <div class="user-details">
            {% if post['display_name'] is defined %}
                {% if logged_in_user_id is defined %}
                    <a href="{{ url.get('members/profile?user=' ~ post['display_name']|url_encode ) }}">{{ post['display_name'] }}</a>
                {% else %}
                    <a href="{{ url.get('session/') }}">{{ post['display_name'] }}</a>
                {% endif %}
            {% else %}
                <a href="{{ url.get('members/profile?user=' ~ post['display_name']|url_encode ) }}">{{ post['display_name'] }}</a>
            {% endif %}
            {% if post['avatar_file_name'] is defined %}
                <img
                        title="{{ post['display_name'] }}"
                        alt="{{ post['display_name'] }}"
                        src="{{ url.get('img/user-avatars/' ~ post['avatar_file_name']) }}"
                >
            {% else %}
                <img
                        title="{{ post['display_name'] }}"
                        alt="{{ post['display_name'] }}"
                        src="{{ url.get('img/user-avatars/default-avatar.png') }}"
                >
            {% endif %}
            <p>Posting since: <BR>{{ post['user_created_date']|shortdate }}</p>
            {% if post['user_team_image_name'] is defined %}
                <img
                        title="{{ post['display_name'] ~ '`s team is ' ~ post['user_team_name'] }}"
                        alt="{{ post['display_name'] ~ '`s team is ' ~ post['user_team_name'] }}"
                        class="team"
                        src="{{ url.get( config.common.team.image.dir ~ post['user_team_image_name']) }}">
            {% endif %}

            {% if site_user_achievements[ post['user_id'] ] is defined %}
                {% for achievement in site_user_achievements[ post['user_id'] ] %}

                    <img
                            title="{{ achievement['title'] }} Achievement"
                            alt="{{ achievement['title'] }} Achievement"
                            class="member-achievement-badge"
                            src="{{ url.get( config.backend.members.achievement.image_dir ~ achievement['file_name']) }}"
                    >
                {% endfor %}
            {% endif %}
        </div>
        <div class="post-details">{#post section#}
            <h2 class="reset-this"><a
                        href="{{ url.get( current_url ~ "#" ~ post['topic_ordinal'] ) }}" {#Calculating post id for page jump#}
                        id="{{ post['topic_ordinal'] }}">
                    #{{ post['topic_ordinal'] }}
                </a>{#post details#}</h2>
            <time datetime="{{ post['created']|iso8601 }}"> {{ post['created']|datedecode }} </time> {#Filtering time into seo and pretty formats#}
        </div>
        <div class="post-content">
            {% if post['status'] is not "D" %}

                {{ post['content_bb']|parsebbtohtml|nl2br }}

                {% if post['editor_display_name'] is defined %} <p class="post-content-edited-by"> Post
                    edited {{ post['modified']|datedecode }} by {{ post['editor_display_name'] }}</p> {% endif %}
            {% else %}
                <p class="post-content-edited-by">Post delete by admin {{ post['modified']|datedecode }}. </p>
            {% endif %}
        </div>
        <div class="table-row">
            {% if auth is true %}
                <span></span>
            {% endif %}
            <span></span>
            <div class="user-signature">
                <p>{{ html_entity_decode( post['user_signature_html']) }}</p>
                <div>
                    {% if user_id is defined %}
                        {% if editable or post['user_id'] == user_id %}
                            <a href="{{ url.get('forum/' ~ post['forum_id'] ~ '/' ~ topic.id ~ '/editreply/' ~ post['id'] ) }}">
                                <img
                                        title="Edit Post"
                                        alt="Edit Post"
                                        class="button success"
                                        src="{{ url.get('img/buttons/edit.png') }}"
                                >
                            </a>
                        {% endif %}
                    {% endif %}

                    <a href="{{ url.get('forum/' ~ post['forum_id'] ~ '/' ~ topic.id ~ '/addreply/' ~ post['id'] ) }}">
                        <img
                                title="Reply Quoting This Reply"
                                alt="Reply Quoting This Reply"
                                class="button warning"
                                src="{{ url.get('img/buttons/quotes.png') }}"
                        >
                    </a>
                    <a>
                        <img
                                title="Reply Quoting All Replies"
                                alt="Reply Quoting All Replies"
                                class="button warning"
                                src="{{ url.get('img/buttons/quotes_multiple.png') }}"
                        >
                    </a>
                </div>
            </div>
        </div>
    </article>
{% endfor %}