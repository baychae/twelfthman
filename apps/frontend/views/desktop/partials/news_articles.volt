{% for news_article in news_articles %}
    <article itemscope itemtype="http://schema.org/NewsArticle">
        <span itemprop="articleSection" hidden>{{ news_article.getNewsGroup().label }}</span>
        <h2 itemprop="headline"><a itemprop="url"
                                   href='news/article/{{ news_article.id }}'> {{ news_article.title }}</a>
        </h2>
        <p itemprop="description">{{ news_article.content_plaintext|substrtows(copy_text_limit) }} ...</p>
    </article>
{% endfor %}        
