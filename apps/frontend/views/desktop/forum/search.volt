<main id="forum" class="forum">
    <header>
        <h1>Search</h1>
    </header>
    {{ content() }}
    <form id="form" method="post">
        <fieldset>
            <legend>Forum Search</legend>
            <div>
                {#<label for="forum-search-term">Search for:</label>#}
                {{ form.getLabel("search-term") }}
                {{ form.get("search-term") }}
                {% for message in form.getMessagesFor('search-term') %}
                    {% include 'partials/form_error_message.volt' %}
                {% endfor %}
            </div>
            <div>
                {{ form.getLabel("forum-search-by-title") }}
                {{ form.get("forum-search-by-title") }}
                {% for message in form.getMessagesFor('forum-search-by-title') %}
                    {% include 'partials/form_error_message.volt' %}
                {% endfor %}
            </div>
            <div>
                {{ form.getLabel("forum-search-by-content") }}
                {{ form.get("forum-search-by-content") }}
                {% for message in form.getMessagesFor('forum-search-by-content') %}
                    {% include 'partials/form_error_message.volt' %}
                {% endfor %}
            </div>
        </fieldset>
        <input type="submit" value="Search" class="form-button">
    </form>

    {% if title_results is not empty %}
        <section>
            <h2><span id="forum-search-toggle-title-results"
                      class="forum-search-results-max"> Topic Title Results</span></h2>
            <div id="forum-topic-title-results" class="table-row-group">
                <div class="table-header table-row">
                    <span class="forum-search-results-half-width">Forum</span>
                    <span class="forum-search-results-half-width">Topic Title</span>
                </div>
                {% for forum_topic in title_results %}
                    <div class="table-row">
                        <a href="{{ url.get("forum/" ~ forum_topic.forum_id ~ "/" ~ forum_topic.id ~ "/1") }}">
                            <div>{{ forum_topic.getForum().title }}</div>
                        </a>
                        <a href="{{ url.get("forum/" ~ forum_topic.forum_id ~ "/" ~ forum_topic.id ~ "/1") }}">
                            <div>{{ forum_topic.title|highlightWord( search_term ) }}</div>
                        </a>
                        </a>
                    </div>
                {% endfor %}
            </div>
        </section>
    {% endif %}


    {% if content_results is not empty %}
        <section>
            <h2><span id="forum-search-toggle-content-results"
                      class="forum-search-results-max"> Topic Content Results</span></h2>
            <div id="forum-topic-content-results" class="table-row-group">
                <div class="table-header table-row">
                    <span class="forum-search-results-half-width">Forum</span>
                    <span class="forum-search-results-half-width">Topic Title</span>
                    <span class="forum-search-results-half-width">Poster</span>
                    <span class="forum-search-results-half-width">Content</span>
                </div>
                {% for forum_topic_post in content_results %}
                    <div class="table-row">
                        <a href="{{ url.get("forum/") ~ forum_topic_post.getUrl(15) }}">
                            <div>{{ forum_topic_post.getForumTopic().getForum().title }}</div>
                        </a>
                        <a href="{{ url.get("forum/") ~ forum_topic_post.getUrl(15) }}">
                            <div>{{ forum_topic_post.getForumTopic().title }}</div>
                        </a>
                        <a href="{{ url.get("forum/") ~ forum_topic_post.getUrl(15) }}">
                            <div>{{ forum_topic_post.getSiteUser().getDisplayName() }}</div>
                        </a>
                        <a href="{{ url.get("forum/") ~ forum_topic_post.getUrl(15) }}">
                            <div id="forum-search-results-post-content">
                                {{ forum_topic_post.content_bb|parseBbToText|subStrStartWithLimit( search_term, 30) }}
                            </div>
                        </a>
                    </div>
                {% endfor %}
            </div>
        </section>

    {% endif %}

</main>