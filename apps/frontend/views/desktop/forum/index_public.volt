{{ content() }}
<main id="forum">
    <header>
        <h1>Forum</h1>
    </header>
    {% if current_match.id is not empty %}
        {% include 'partials/current-match-result.volt' %}
    {% endif %}
    {% include 'partials/adspace/forum-leaderboard.volt' %}
{% for forum_type in forum_types %}
	{% if forum_type.getForum(['status = "A" and view_type="P"']).count() != 0 %}
	    <section>
	        {{ flashSession.output() }}
	        <h2> {{forum_type.title}} </h2>
	        <div class="table-row-group">
	            <div class="table-header table-row">
					<span class="replies-col"></span>
					<span class="name-col">Name</span>
					<span class="topics-posts-col">Topics/Posts</span>
					<span class="activity-col">Last Activity</span>
	            </div>
	            {% for forum in forum_type.getForum(['status = "A" and view_type="P"']) %}
	                <div class="table-row">
	                    <span>
	                        <a href="forum/{{ forum.id }}/1">
	                        	<img
										title="Go to the {{ forum.title }} forum"
										alt="Go to the {{ forum.title }} forum"
										class="button success"
										src="img/buttons/text.png"
								>
	                        </a>
	                    </span>
	                    
	                    <div>
							<h3><a href="forum/{{ forum.id }}/1">{{ forum.title }}</a>
							</h3> {#TODO - use display: flex and change margin-top to override default styling#}
	                        <p> {{ forum.description }} </p>
	                    </div>
	                    
	                    <div>
	                        <dl><dt>Topics:</dt><dd>{{ forum_topic_count[forum.id] is defined ? forum_topic_count[forum.id] : "0" }}</dd></dl>
	                        <dl><dt>Posts:</dt><dd>{{ forum_replies[forum.id] is defined ? forum_replies[forum.id] : "0" }}</dd></dl>
	                    </div>
	                    
	                    <div>
							<h3>
								<a title='Go to the latest post in "{{ last_activity[forum.id]["title"] }}"'
								   href="{{ last_activity[forum.id]["post_url"] }}"> {{ last_activity[forum.id]["title"] is defined ? last_activity[forum.id]["title"] : "" }} </a>
							</h3>
							<p>{{ last_activity[forum.id]["created"] is defined ? last_activity[forum.id]["created"]|datedecode : "" }}
								<BR>
                                {{ last_activity[forum.id]["avatar_name"] is defined ? "by <a href='session/'>" ~ last_activity[forum.id]["avatar_name"] ~ "</a>" : "" }}
							</p>
	                    </div>
	                </div>
	            {% endfor %}
	        </div>
	    </section>
    {% endif %}
{% endfor %}
</main>