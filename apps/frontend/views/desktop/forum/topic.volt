{{ content() }}

{% include 'partials/forum-search-assets.volt' %}

{% do assets.addJs("js/wysibb/jquery.wysibb.js") %}
{% do assets.addJs("js/wysibb-options.js") %}

<article class="forum-topic"> {#TODO - will need to factor out style that both topic and forum style share and style they don't.#}
    <header>
        <h1>{{ topic.getForum().title }} - {{ topic.title }}</h1>
        {{ elements.getForumSearch() }}
        {% include 'partials/adspace/forum-leaderboard.volt' %}
    </header>
    {% if current_match.id is not empty %}
        {% include 'partials/current-match-result.volt' %}
    {% endif %}

    <nav>
        <h2>
            <a href="{{ url.get('forum/') }}">{{ topic.getForum().getForumType().title }}</a> >>
            <a href="{{ url.get('forum/' ~ topic.getForum().id ~ '/1') }}">{{ topic.getForum().title }}</a>
            >> {{ topic.title }}
            {{ elements.getTopicRatingStars(user_rating) }}
            {{ flashSession.output() }}
        </h2>
    </nav>

    <div>
        <div class="forum-topic-post-toolbar">
        <div class="paginator left">{{ elements.getPaginator(page, "Posts") }} </div>

        {% if auth is true%}
            <div class="right">
                <a href="{{ url.get('forum/' ~ topic.getForum().id  ~ '/' ~ topic.id ~ '/' ~ page.current ~ '/rate') }}">
                    <img
                            title="Rate Topic"
                            alt="Rate Topic"
                            class="button web-orange"
                            src="{{ url.get('img/buttons/star.png') }}"
                    > Rate Topic
                </a>
                <a href="{{ url.get('forum/' ~ topic.getForum().id ~ '/' ~ topic.id ~ '/' ~ page.current ~ '/addreply') }}">
                    <img
                            title="Add Reply"
                            alt="Add Reply"
                            class="button success"
                            src="{{ url.get('img/buttons/plus.png') }}"> Add Reply
                </a>
                {%if editable%}
                    <a href="{{ url.get('admin/forum/topic/' ~ topic.id) }}">
                        <img
                                title="Edit Topic"
                                alt="Edit Topic"
                                class="button web-orange"
                                src="{{ url.get('img/buttons/edit.png') }}"> Edit Topic
                    </a>
                {% endif %}
            </div>
        {% endif %}

        </div>

        {% include "partials/forum_topic_replies.volt" %}

        {% if page.items|length > 2 %}
        <div class="forum-topic-post-toolbar">
        {% if page.total_pages > 1 %}
            <div class="paginator left">{{ elements.getPaginator(page, "Posts") }} </div>
        {% endif %}

        {% if auth is true%}
            <div class="right">
                <a href="{{ url.get('forum/' ~ topic.getForum().id ~ '/' ~ topic.id ~ '/' ~ page.current ~ '/rate') }}">
                    <img
                            title="Rate Topic"
                            alt="Rate Topic"
                            class="button tool"
                            src="{{ url.get('img/buttons/star.png') }}"
                    > Rate Topic
                </a>
                <a href="{{ url.get('forum/' ~ topic.getForum().id ~ '/' ~ topic.id ~ '/' ~ page.current ~ '/addreply') }}">
                    <img
                            title="Add Reply"
                            alt="Add Reply"
                            class="button success"
                            src="{{ url.get('img/buttons/plus.png') }}"> Add Reply
                </a>
                {%if editable%}
                    <a href="{{ url.get('admin/forum/topic/' ~ topic.id) }}">
                        <img
                                title="Edit Topic"
                                alt="Edit Topic"
                                class="button web-orange"
                                src="{{ url.get('img/buttons/edit.png') }}"> Edit Topic
                    </a>
                {% endif %}
            </div>
        {% endif %}

        </div>
        {% endif %}
    </div>

    <nav>
        <a href="{{ url.get('forum/') }}">{{ topic.getForum().getForumType().title }}</a> >>
        <a href="{{ url.get('forum/' ~ topic.getForum().id ~ '/1') }}">{{ topic.getForum().title }}</a>
        >> {{ topic.title }}
        {{ elements.getTopicRatingStars(user_rating) }}
        {{ flashSession.output() }}
    </nav>

    {% if auth is true %}
        <form id="form" method='post' enctype='multipart/form-data'>
            <fieldset>
                <legend>Quick Reply</legend>
                {% if quote_text is defined %}
                    {{ form.get("bbcode_field").setDefault(quote_text) }}
                {% else %}
                    {{ form.get("bbcode_field") }}
                {% endif %}
                {{ form.get("content_html") }}
            </fieldset>

            {{ form.get("reply") }}

        </form>
    {% endif %}

</article>

