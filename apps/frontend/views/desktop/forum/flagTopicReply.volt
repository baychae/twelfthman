{% do assets.addJs("js/wysibb/jquery.wysibb.js") %}
{% do assets.addJs("js/wysibb-options.js") %}


<header>
        <h1>{{ post.getForumTopic().getForum().title ~ " - " ~ post.getForumTopic().title }}</h1>
    {% include 'partials/adspace/forum-leaderboard.volt' %}
    </header>

    <form id="form" method='post' enctype='multipart/form-data'>
        <fieldset>
            <legend>
                <a href="forum/{{ post.getForumTopic().getForum().id }}">
                    {{ post.getForumTopic().getForum().title }}
                </a>
                >>
                <a href="forum/{{ post.getForumTopic().getForum().id }}/{{ post.getForumTopic().id }}/1">
                    {{ post.getForumTopic().title }}
                </a>
                >> Flag Post
                {{ flashSession.output() }}
            </legend>
        </fieldset>

        <fieldset>
            <legend>Flagged Post</legend>
            <p>{{ offending_post_content|parsebbtohtml }}</p>
        </fieldset>

        <fieldset>
            <legend>Flag Reason</legend>
            {% if quote_text is defined %}
                {{ form.get("bbcode_field") }}
            {% else %}
                {{ form.get("bbcode_field") }}
            {% endif %}
            {{ form.get("content_html") }}
        </fieldset>

        {{ form.get("send") }}
        <button id="go-back" type="button" class="form-button" >Cancel</button>
        {#{{ form.get("cancel") }}#}

    </form>
