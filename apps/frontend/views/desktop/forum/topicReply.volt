{% do assets.addJs("js/wysibb/jquery.wysibb.js") %}
{% do assets.addJs("js/wysibb-options.js") %}

<main class="forum"> {#TODO - will need to factor out style that both topic and forum style share and style they don't.#}
    <header>
        <h1>{{ topic.getForum().title }} - {{ topic.title }}</h1>
        {% include 'partials/adspace/forum-leaderboard.volt' %}
    </header>

    <form id="form" method='post' enctype='multipart/form-data'>
        <fieldset>
            <legend>
                <a href="forum/{{ topic.getForum().id }}">
                    {{ topic.getForum().title }}
                </a>
                >>
                <a href="forum/{{ topic.getForum().id }}/{{ topic.id }}/1">
                    {{ topic.title }}
                </a>
                >> Reply
            </legend>
        </fieldset>

        <fieldset>
            <legend>Original Post</legend>
            <p>{{ original_post_content|parsebbtohtml }}</p>
        </fieldset>

        <fieldset>
            <legend>Reply</legend>
            {% if quote_text is defined %}
                {{ form.get("bbcode_field").setDefault(quote_text) }}
            {% else %}
                {{ form.get("bbcode_field") }}
            {% endif %}
            {{ form.get("content_html") }}
        </fieldset>

        {{ form.get("reply") }}

    </form>

</main>