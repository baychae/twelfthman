{#TODO - will need to factor out style that both topic and forum style share and style they don't.#}
{% include 'partials/forum-search-assets.volt' %}
    <header>
        <h1>{{ forum.title }}</h1>
        {{ elements.getForumSearch() }}
        {% include 'partials/adspace/forum-leaderboard.volt' %}
    </header>
{% if current_match.id is not empty %}
    {% include 'partials/current-match-result.volt' %}
{% endif %}

    {% if pinned_topics is not empty %}{#TOD - Pinned topics#}
        <section class="forum-topics">
                <h2>
                    <a href="{{ url.get('forum/') }}">{{ forum_type_title }}</a> >> {{ forum.title }} >> Pinned Topics
                </h2>

            {% include "partials/forum_topics_pinned.volt" %}
            
        </section>
    {% endif %}

<section class="forum-topics">
            <h2>
                <a href="{{ url.get('forum/') }}">{{ forum_type_title }}</a> >> {{ forum.title }}
            </h2>
        <header>
            {% if auth is true%}
                <a href="{{ url.get('forum/addtopic/' ~ forum.id ) }}">
                    <img
                            title="Add New Topic"
                            alt="Add New Topic"
                            class="button success"
                            src="{{ url.get('img/buttons/plus.png') }}"
                    >
                    <span>Add New Topic</span>
                </a>
                {%if sysadmin%}
                    <a href="{{ url.get('admin/forum/' ~ forum.id ~ '/1') }}">
                        <img
                                title="Edit Forum"
                                alt="Edit Forum"
                                class="button web-orange"
                                src="{{ url.get('img/buttons/edit.png') }}"
                        >
                        <span>Edit Forum</span>
                    </a>
                {% endif %}
            {% endif %}
            <div class="paginator">{{ elements.getPaginator(page, "Topics") }} </div>
        </header>

        {% include "partials/forum_topics.volt" %}
    </section>
{{ content() }}