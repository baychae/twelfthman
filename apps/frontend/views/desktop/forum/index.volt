{% include 'partials/forum-search-assets.volt' %}


<main id="forum" class="forum">
    <header>
        <h1>Forum</h1>
        {{ elements.getForumSearch() }}
        <button id="view-topics" class="forum button" onclick="window.location='{{ url.get('forum/all-topics') }}'">View
            All Topics
        </button>
        {% include 'partials/adspace/forum-leaderboard.volt' %}
    </header>
    {% if current_match.id is not empty %}
        {% include 'partials/current-match-result.volt' %}
    {% endif %}

{% for forum_type in forum_types %}
    <section>
        {{ flashSession.output() }}
        <h2> {{forum_type.title}} </h2>
        <div class="table-row-group">
            <div class="table-header table-row">
                <span class="replies-col"></span>
                <span class="name-col">Name</span>
                <span class="topics-posts-col">Topics/Posts</span>
                <span class="activity-col">Last Activity</span>
                {% if logged_in %}<span class="mark-read-col"></span>{% endif %}
            </div>
            {% for forum in forum_type.getForum(['status = "A"']) %}
                <div class="table-row">
                    <span>
                        <a href="{{ url.get('forum/'~ forum.id ~ '/1') }}">
                        	{% if forum.hasNewReplies(user_id) %}
                                <img
                                        title='New replies in "{{ forum.title }}"'
                                        alt='New replies in "{{ forum.title }}"'
                                        class="button success"
                                        src="{{ url.get('img/buttons/text.png') }}"
                                >
                        	{% else %}
                                <img
                                        title='New replies in "{{ forum.title }}"'
                                        alt='New replies in "{{ forum.title }}"'
                                        class="button info"
                                        src="{{ url.get('img/buttons/text.png') }}">
                        	{% endif %}
                        </a>
                    </span>
                    <div>
                        <h3><a title='View forum "{{ forum.title }}"'
                               href="{{ url.get('forum/' ~ forum.id ~ '/1') }}">{{ forum.title }}</a>
                        </h3> {#TODO - use display: flex and change margin-top to override default styling#}
                        <p> {{ forum.description }} </p>
                    </div>
                    <div>
                        <dl><dt>Topics:</dt><dd>{{ forum_topic_count[forum.id] is defined ? forum_topic_count[forum.id] : "0" }}</dd></dl>
                        <dl><dt>Posts:</dt><dd>{{ forum_replies[forum.id] is defined ? forum_replies[forum.id] : "0" }}</dd></dl>
                    </div>
                                        {#<span>#TODO</span>{#Posts#}
                    <div>
                        <h3>
                            <a title='Go to the latest post in "{{ last_activity[forum.id]["title"] }}"'
                               href="{{ last_activity[forum.id]["post_url"] }}">{{ last_activity[forum.id]["title"] is defined ? last_activity[forum.id]["title"] : "" }}</a>
                        </h3> {# TODO - LINK TO LAST POST #}
                        <p> {{ last_activity[forum.id]["created"] is defined ? last_activity[forum.id]["created"]|datedecode : ""}} <BR>
                            {% if last_activity[forum.id]["avatar_name"] is defined %}
                                by <a title="View {{ last_activity[forum.id]["avatar_name"] }}&#39;s profile"
                                      href="{{ url.get("members/profile?user=" ~ last_activity[forum.id]["avatar_name"]) }}">
                                {{ last_activity[forum.id]["avatar_name"] }}
                            </a>
                            {% endif %}
                        </p>
                    </div>
                    {% if logged_in %}
                        <span>
							{% if forum.hasNewReplies(user_id) %}
                                <a href="{{ url.get('forum/markread/' ~ forum.id) }}">
                                    <img
                                            title="Mark All Topics Read"
                                            alt="Mark All Topics Read"
                                            class="button success"
                                            src="{{ url.get('img/buttons/tick.png') }}">
                                </a>
                            {% endif %}
                        </span>
                    {% endif %}
                </div>
            {% endfor %}
        </div>
    </section>
{% endfor %}
    
</main>
