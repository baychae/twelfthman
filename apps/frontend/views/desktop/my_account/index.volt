{% do assets.addJs('js/wysibb/jquery.wysibb.js') %}
{% do assets.addJs("js/wysibb-options.js") %}
{% do assets.addJs("js/wysibb-image-helper.js") %}
{% do assets.addJs("js/wysibb-ihelper-options-my-account.js") %}

<h1> My Account </h1>
<form id="form" enctype="multipart/form-data" method="post">
    <fieldset>
        <legend>Personal Details <b>{{ flashSession.output() }}</b></legend>
        <div>{{ form.getLabel("first_name") }} {{ form.get("first_name") }}</div>
        <div>{{ form.getLabel("last_name") }} {{ form.get("last_name") }}</div>
        <div>{{ form.getLabel("email") }} {{ form.get("email") }}</div>
    </fieldset>
    <fieldset>
        <legend>Display Details</legend>
        <div>{{ form.getLabel("avatar_name") }} {{ form.get("avatar_name").setAttribute("placeholder", login_id) }}

            {% if form.get("avatar_name").getValue() is empty %}<span> * Defaults to your login id. Enter text to add your own.</span>{% endif %}
        </div>
        <div>
            {{ form.getLabel("team_id") }} {{ form.get("team_id") }}
        </div>
        <div>
            {{ form.getLabel("upload_avatar") }}
            <div class="image-toolset">
                {% if avatar_file_name is not defined %}
                    <div id="image-preview" class="no-select"></div>
                    {{ form.get("upload_avatar") }}
                    {{ form.get("upload_avatar_overlay") }}
                {% else %}
                    <img
                            title="Avatar Image"
                            alt="Avatar Image"
                            class="avatar-img"
                            src="img/user-avatars/{{ avatar_file_name }}"
                    >
                    {{ form.get("delete_avatar") }}
                {% endif %}

            </div>
    </fieldset>
    <fieldset>
        <legend>Signature</legend>
        {{ form.get("bbcode_field") }}
        {{ form.get("content_html") }}
    </fieldset>
    <fieldset>
        <legend>My Favourites</legend>
        <div>{{ form.getLabel("favourite_away_ground") }}  {{ form.get("favourite_away_ground") }}</div>
        <div>{{ form.getLabel("favourite_all_time_player") }}  {{ form.get("favourite_all_time_player") }}</div>
        <div>{{ form.getLabel("favourite_current_player") }}  {{ form.get("favourite_current_player") }}</div>
    </fieldset>
    {{ form.get("update_my_profile") }}
</form>