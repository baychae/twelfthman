{{ content() }}
<main id="forum">
    <header>
        <h1>Results</h1>
    </header>
    <section id="results">
        <h2>{{ config.site.team }} Results</h2>

        {% for match in matches %}
            <div class="table-row">
                <span>
                    <a {% if match.getHomeTeam().website  is not empty %}
                        href="//{{ match.getHomeTeam().website }}"
                        target="_blank"

                    {% endif %} >
                    <img
                            title="Take me to {{ match.getHomeTeam().name }} forum."
                            alt="{{ match.getHomeTeam().name }}"
                            src="img/teams/{{ match.getHomeTeam().image_name }}"
                    >
                </a>
                </span>
                <span>
                    {{ match.getHomeTeam().name }}
                </span>
                <span>
                    <span>{{ match.start|datedecode }}</span>
                    <span {% if match.preview_article_id is not defined %} class="hidden" {% endif %}>
                            <a href="news/article/{{ match.preview_article_id }}">Preview</a>
                    </span>
                    <span>{{ match.home_score }} - {{ match.away_score }}</span>
                    <span {% if match.report_article_id is not defined %} class="hidden" {% endif %}>
                            <a href="news/article/{{ match.report_article_id }}">Report</a>
                    </span>
                </span>
                <span>
                {{ match.getAwayTeam().name }}
            </span>
                <span>
                    <a {% if match.getAwayTeam().website  is not empty %}
                        href="//{{ match.getAwayTeam().website }}"
                        target="_blank"
                    {% endif %} >
                        <img
                                title="Take me to {{ match.getAwayTeam().name }} forum."
                                alt="{{ match.getAwayTeam().name }}"
                                src="img/teams/{{ match.getAwayTeam().image_name }}"
                        >
                    </a>
                </span>
            </div>
        {% endfor %}
    </section>
</main>