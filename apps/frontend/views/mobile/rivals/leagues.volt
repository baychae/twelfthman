<main id="forum">
    <header>
        <h1>Rivals</h1>
    </header>
    <section>
        <h2>Leagues</h2>
        {% for league in leagues %}
            <div class="table-row">
               <span class="league-name">
                   <a href="rivals/{{ league.name|prettyUrl }}">
                        {{ league.name }}
                    </a>
               </span>
            </div>
        {% endfor %}
    </section>
</main>