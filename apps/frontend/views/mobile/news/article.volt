<link rel="stylesheet" type="text/css" href="js/Font-Awesome/css/font-awesome.css"/>
<link rel="stylesheet" type="text/css" href="js/jssocials/dist/jssocials.css"/>
<link rel="stylesheet" type="text/css" href="js/jssocials/dist/jssocials-theme-minima.css"/>

<main id="news-article">
    <article itemprop="mainEntity" itemscope itemtype="//schema.org/NewsArticle">
        <h1 itemprop="headline">{{ article.title }}</h1>
        <div id="share" style="display: inline-block; vertical-align: top;"></div>

        <script src="js/jssocials/dist/jssocials.js"></script>
        <script>
            $("#share").jsSocials({
                shares: ["twitter", "facebook", "whatsapp"],
                url: "https://owlsonline.com"
            });
        </script>
        <span itemprop="articleSection" hidden>{#{{ article.getNewsGroup().label }}#}</span>
        <!-- Article Start --> {# For syndicated news scrapers #}
        <p itemprop="articleBody">
            {{ article.content_html }}
        </p>
        <!-- Article End -->
    </article>
</main>