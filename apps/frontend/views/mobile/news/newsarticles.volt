{{ content() }}

<table>
    <caption>News Articles <span></span></caption>

    <th colspan=2 class="paginator">
        {{ elements.getPaginator(page, "News Articles") }}
    </th>
    <tr>
        <th>Title</th>
        <th class="forty-percent-wide">Date</th>
    </tr>

    {% for article in page.items %}
        <tr>
            <td>
                <a href="../../news/modifynewsarticle/{{ article.id }}"> {{ article.title }} </a> {{ flashSession.output() }}
            </td>
            <td><a href="../../news/modifynewsarticle/{{ article.id }}"> {{ article.created|datedecode }} </a></td>
        </tr>
    {% endfor %}

</table>
