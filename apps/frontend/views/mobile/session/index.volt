<h1>Log In </h1>

{{ content() }}

{{ form('session/start', 'id': 'login-form') }}
<fieldset class="form-section">
    <legend class="page-section-header">
        Your Login Details
    </legend>
    <div class="control-group">
        <label for="login">Username/Email</label>
        {{ text_field('login') }}
    </div>
    <div class="control-group">
        <label for="password">Password</label>
        {{ password_field('password') }}
    </div>
    <div>
        {{ flashSession.output() }}
        <p>If you need to create a new account, <a href="session/register/">click here</a>.</p>
        <p>Forgotten your username or password? <a href="session/lost-password">click here</a>.</p>
        <p>To reset your details or email <a
                    href="mailTo: {{ config.frontend.admin.contact }}">{{ config.frontend.admin.contact }}</a></p>
    </div>


</fieldset>
<p>
    {{ submit_button('Login', 'class': 'btn btn-large form-button') }}
</p>