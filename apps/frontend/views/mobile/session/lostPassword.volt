<h1>Password Reset</h1>


{{ content() }}

{{ form('session/lost-password', 'id': 'form') }}
<fieldset>
    <legend>
        Please enter your email/login for identification {{ flashSession.output() }}
    </legend>
    <div class="control-group">
        <label for="password">Email/Login Id</label>
        {{ text_field('ident') }}
    </div>
</fieldset>
{{ submit_button('Send Email', 'class': 'form-button') }}