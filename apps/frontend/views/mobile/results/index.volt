<main id="result" class="page-content-container">
    <header>
        <h1>Results</h1>
    </header>

    <h2 class="page-section-header caption-header">{{ config.site.team }} Results</h2>

    <table id="results">
        <thead>
        <th class="center-align">Home Team</th>
        <th class="center-align">Score</th>
        <th class="center-align">Away Team</th>
        </thead>

        {% for match in matches %}
            <tr>
                <td class="center-align">
                    <img src="img/teams/{{ match.getHomeTeam().image_name }}">
                    <div>{{ match.getHomeTeam().name }}</div>
                </td>
                <td class="center-align">
                    {#<span>{{ match.start|datedecode }}</span>#}

                    <strong>{{ match.home_score }} - {{ match.away_score }}</strong>


                </td>
                <td class="center-align">
                    <img src="img/teams/{{ match.getAwayTeam().image_name }}">
                    <div>{{ match.getAwayTeam().name }}</div>

                </td>
            </tr>
        {% endfor %}
    </table>
</main>