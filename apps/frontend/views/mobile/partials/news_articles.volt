{% for news_article in news_articles %}
    <article class="page-section" itemscope itemtype="http://schema.org/NewsArticle">
        <span itemprop="articleSection" hidden>{{ news_article.getNewsGroup().label }}</span>
        <h2 itemprop="headline"><a itemprop="url"
                                   href='{{ url.get('news/article/' ~ news_article.id) }}'> {{ news_article.title }}</a>
        </h2>
        <a href="{{ url.get('news/article/' ~ news_article.id) }}"> {# HTML5 Block level link is legal #}
            <p itemprop="description">{{ news_article.content_plaintext|substrtows(copy_text_limit) }} ...</p>
        </a>
    </article>
{% endfor %}        
