{{ content() }}
<h2>
    <form method="get">
        <label for="search">Search:</label><input id="search" name="search" type="text">
        <input type="submit" value="Go">
    </form>
    {{ flashSession.output() }}
</h2>

<div class="center">
    {{ elements.getAlphaNumPaginator(alpha_index_array) }}
</div>

{% if indexed_by is defined %}
    <div class="paginator center">
        {{ elements.getIndexedPaginator(page, paginator_label, indexed_by) }}
    </div>
{% else %}
    <div class="paginator center">
        {{ elements.getIndexedPaginator(page, "Members") }}
    </div>
{% endif %}

<br>
{% for member in page.items %}

    {% set user_name = member.avatar_name ? member.avatar_name|url_encode : member.login_id|url_encode %}

    <div class="user-details">
        <a title="View Member {{ user_name }}" href="members/profile?user={{ user_name|e }}">
            {% if member.avatar_name is defined %}
                {{ member.avatar_name }}
            {% else %}
                {{ member.login_id }}
            {% endif %}
        </a>

        <a title="Send New Message to {{ user_name }}"
           href="messages/new-message?mail_to={{ user_name|e }}">
            {% if member.avatar_file_name is defined %}
                <img src="img/user-avatars/{{ member.avatar_file_name }}">
            {% else %}
                <img src="img/user-avatars/default-avatar.png">
            {% endif %}
        </a>
        <p>
            <a title="View Member {{ user_name }}" href="members/profile?user={{ user_name|e }}">
                Joined: {{ member.modified|shortdate }}
            </a>
        </p>
    </div>


{% endfor %}
