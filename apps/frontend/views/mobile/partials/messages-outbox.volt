{% do assets.addJs("js/jquery.paginater.js") %}
{{ content() }}
<div class="paginator">{{ elements.getPaginator(page, "Messages") }} </div>

{% for message in page.items %}

    <div class="messages-itemised">
        {#<div class="messages-itemised-control table-cell">#}
        {#<a href="messages/reply/{{ message.uri }}"> #}{# TODO - Message Quick Reply #}
        {#<img alt="Quick Reply" title="Quick Reply" class="button web-orange" src="img/buttons/new-message.png">#}
        {#</a>#}
        {#</div>#}

        <div class="messages-itemised-details">
            <a href="members/profile?user={{ message.to }}"> <strong>{{ message.to }}</strong>
            </a> {# TODO : get the user profile url. Build Members section #}
            <a href="messages/view/{{ message.uri }}"
               class="messages-itemised-date"> {{ message.modified|emaildate }} </a>
            <a href="messages/view/{{ message.uri }}" class="messages-itemised-subject">{{ message.subject }}</a>

        </div>
        <div class="table-cell messages-itemised-snippet"><a
                    href="messages/view/{{ message.uri }}">{{ message.body_plaintext|substrtows(298) }}...</a></div>
    </div>

    <div class="divider"></div>
{% endfor %}