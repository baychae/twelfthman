{% for post in page.items %}
    <article class="post">
        {% if auth is true %} {# Topic Reply Tools #}
            <div class="misc-tool right-align">{#topic post tool bar#}
                {% if previously_read_post_date >= post['modified'] %}
                    <img class="button info"
                         src="img/buttons/text.png"> {# css info class = blue - has been read #}
                {% else %}
                    <img class="button success"
                         src="img/buttons/text.png"> {# css success class = green - is a new post #}
                {% endif %}

                <a href="messages/new-message?mail_to={{ post['display_name']|url_encode }}"><img
                            class="button web-orange"
                            src="img/buttons/new-message.png"></a>
                <a href="forum/{{ post['forum_id'] }}/{{ post['topic_id'] }}/flagreply/{{ post['id'] }}">
                    <img class="button info" src="img/buttons/flag.png">
                </a>
                <img class="button success" src="img/buttons/arrow_up.png" onclick="window.scrollTo(0,0)">
            </div>
            <div class="divider"></div>
        {% endif %}
        <div class="user-details">
            <a href="{{ url.get( router.getRewriteUri() ) }}#{{ post['topic_ordinal'] }}" {#Calculating post id for page jump#}
               name="{{ post['topic_ordinal'] }}">
                #{{ post['topic_ordinal'] }}
            </a>
            {% if post['display_name'] is defined %}
                {% if logged_in_user_id is defined %}
                    <a href="{{ url.get('members/profile?user=' ~ post['display_name']|url_encode ) }}">{{ post['display_name'] }}</a>
                {% else %}
                    <a href="{{ url.get('session/') }}">{{ post['display_name'] }}</a>
                {% endif %}
            {% else %}
                <a href="{{ url.get('members/profile?user=' ~ post['display_name']|url_encode ) }}">{{ post['display_name'] }}</a>
            {% endif %}

            {#post details#}
            <time datetime="{{ post['created']|iso8601 }}"> {{ post['created']|datedecode }} </time> {#Filtering time into seo and pretty formats#}
            {% if post['user_team_image_name'] is defined %}
                <img class="team"
                     src="{{ url.get(config.backend.members.achievement.image_dir ~ post['user_team_image_name']) }}">
            {% endif %}
        </div>
        <div class="divider"></div>
        <div class="post-content section">
            {% if post['status'] is not "D" %}
                {{ post['content_bb']|parsebbtohtml|nl2br }}
                {% if post['editor_display_name']is defined %} <p class="post-content-edited-by"> Post
                    edited {{ post['modified']|datedecode }} by {{ post['editor_display_name'] }}</p> {% endif %}
            {% else %}
                <p class="post-content-edited-by">Post delete by admin {{ post['modified']|datedecode }}. </p>
            {% endif %}
        </div>

    </article>
{% endfor %}