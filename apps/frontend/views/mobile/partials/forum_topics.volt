{{ content() }}
<table class="table-container">
<tr class="table-header table-row">
    <th></th>
    <th>Topic</th>
    <th>Stats</th>
</tr>

{% for topic in page.items %}

    <tr class="table-row">
        <td>
            {% if auth is false %}
                <a href="{{ url.get( 'forum/' ~ topic['forum_id'] ~ '/' ~ topic['topic_id'] ~ '/1' ) }}">
                <img class="button success" src="img/buttons/text.png">
            </a>
            {% else %} {# Completely Unread get first post #}
                {{ user_read_topic_urls[ topic['topic_id'] ]['html'] }}
            {% endif %}
            {% if auth is true %}
                {% if user_read_topic_urls[ topic['topic_id'] ]['unread'] is defined %}
                    <a href="{{ url.get('forum/topic/markread/' ~ topic['forum_id'] ~ "/" ~ topic['topic_id'] ~ "/" ~ page.current) }}"><img
                                title="Mark All Topics Read" class="button success"
                                src="{{ url.get('img/buttons/tick.png') }}"></a>
                {% endif %}
            {% endif %}

        </td>
        <td> {#Topic#}
            {% if user_rating[ topic['topic_id'] ] is defined %}
                <div id="forum-topic-recommended" title="Rated Topic!">
                    {{ elements.getTopicRatingStars( user_rating[ topic['topic_id'] ] ) }}
                </div>
            {% endif %}
            <h3>
                <a href="{{ url.get('forum/' ~ topic['forum_id'] ~ '/' ~ topic['topic_id'] ~ '/1') }}">{{ topic["topic_title"] }}</a>
            </h3>
            {% if topic.description is not empty %}<span>{{ topic.description }},</span>{% endif %}
                {% if logged_in_user_id is defined %}
                    <div>by
                        <a href="{{ url.get('members/profile?user=' ~ topic['last_post_avatar_name']) }}">{{ topic['last_post_avatar_name'] }}</a>
                    </div>
                {% else %}
                    <div>by <a href="{{ url.get('session/') }}">{{ topic['last_post_avatar_name'] }}</a></div>
                {% endif %}
            </span>

        </td>
        <td> {#Replies/Views#}
            <dl>
                <dt>Replies</dt>
                <dd>{{ topic_post_stats[ topic['topic_id'] ]['post_count'] }}</dd>
            </dl>
            <dl>
                <dt>Views</dt>
                <dd> {{ topic_views[ topic['topic_id'] ] is not defined ? 0 : topic_views[ topic['topic_id'] ] }}
                <dd>
            </dl>
        </td>

    </tr>
{% endfor %}

</table>