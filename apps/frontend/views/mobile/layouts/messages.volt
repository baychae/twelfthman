<nav id="mobile-nav-horizontal">

    <ul>
        {{ elements.buildHorizontalNav( nav_menu_items, url.get("img/buttons") ) }}
        {#<span class="stretcher"></span>#}
    </ul>


</nav>
<h1>
    {% if dispatcher.wasForwarded() %}
        {{ dispatcher.getpreviouscontrollername()|capitalize }} -> {{ dispatcher.getpreviousactionname()|capitalize }}
    {% else %}
        {{ view.getControllerName()|capitalize }} -> {{ view.getActionName()|capitalize }}
    {% endif %}
</h1>
{{ content() }}