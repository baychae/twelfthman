{{ content() }}
<div class="messages-section">
    <h2>Inbox {{ flashSession.output() }}</h2>

    {% include "partials/messages-inbox.volt" %}

</div>