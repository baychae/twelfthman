{% do assets.addJs("js/wysibb/jquery.wysibb.js") %}
{% do assets.addJs("js/wysibb-mobile-options.js") %}

<form class="message-form" id="form" method='post' enctype='multipart/form-data'>
    <fieldset class="page-section form-section">
        <legend class="page-section-header">New Message {{ flashSession.output() }}</legend>
        <div>
            <label for="mailTo">TO:</label>
            <input type="text" id="mailTo" name="to" value="{{ mail_to }}">
        </div>
        <div>
            <label for="subject">Subject:</label>
            <input type="text" id="subject" name="subject" value="{{ subject }}">
        </div>
        <div>
            <textarea id="editor" name="body" style="width: 100%">{{ body_bb }}</textarea>
        </div>

    </fieldset>
    <input class="form-button" type="submit" value="Send">
    <a class="form-button" href="messages/inbox/">Cancel</a>
</form>
