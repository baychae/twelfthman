<form id="form" class="message-form" method="post" enctype="multipart/form-data">
    <fieldset class="page-section form-section">
        <legend class="page-section-header">Messages >> Reply</legend>
        <div>
            <label>To:</label> <input name="to" type="text" value="{{ message.from }}" readonly>
        </div>
        <div>
            <label>Subject:</label> <input name="subject" type="text" value="RE: {{ message.subject }}"></input>
        </div>

    </fieldset>

    <fieldset class="page-section form-section">
        <legend class="page-section-header">Message Body</legend>
        <div class="wysibb-text">
            <textarea id="editor" name="body" style="width: 100%">[quote]{{ message.body_bb }}[/quote]
 </textarea> {# This has been indented in this way so that the caret returns to a new line #}
        </div>
    </fieldset>

    <input type="submit" value="Reply" class="form-button">
</form>