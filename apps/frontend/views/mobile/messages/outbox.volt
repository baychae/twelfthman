<div class="messages-section">
    <h2>Outbox</h2> {{ flashSession.output() }}

    {% include "partials/messages-outbox.volt" %}

</div>