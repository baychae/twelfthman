<main id="member-profile">
    <header>
        <h1>
            <a href="members/"> {{ router.getControllerName()|capitalize }}</a>
            >> {{ router.getActionName()|capitalize }}
        </h1>
    </header>
    <section id="form">
        <fieldset>
            <legend>
                <h2>{% if member.avatar_name is defined %} {{ member.avatar_name }} {% else %} {{ member.login_id }} {% endif %}</h2>
            </legend>
            <div>
                <span class="label">Status:</span><span class="input-text">Unknown(#TODO)</span>
            </div>
            <div>
                <span class="label">Home Team:</span>
                {% if member.getTeam().image_name is defined %}
                    <img
                            title="{{ member.getTeam().image_name }}"
                            alt="{{ member.getTeam().image_name }}"
                            class="team"
                            src="{{ config.common.team.image.dir ~ member.getTeam().image_name }}"
                    >
                    <span>{{ member.getTeam().name }}</span>
                {% endif %}
            </div>
            <div>
                <span class="label">Avatar:</span>
                {% if member.avatar_file_name is defined %}
                <img src="img/user-avatars/{{ member.avatar_file_name }}"><span class="input-text">
                {% endif %}</span>
            </div>
            <div>
                <span class="label">First Used:</span>{{ member.created|shortdate }}<span class="input-text"></span>
            </div>
            <div>
                <span class="label">Last Active</span>
                {% if member.getForumTopicPost().getLast().created is defined %}
                    {{ member.getForumTopicPost().getLast().created|datedecode }}
                {% endif %}
                <span class="input-text"></span>
            </div>
            <div>
                <span class="label">Posts:</span><span class="input-text">{{ posts }}</span>
            </div>
            <div>
                <span class="label">Signature:</span> {{ html_entity_decode( member.signature_html ) }}
            </div>
        </fieldset>
        {% if member.favourite_away_ground is not empty or member.favourite_all_time_player is not empty or member.favourite_current_player is not empty %}
            <fieldset>
                <legend>
                    <h2>Favourites</h2>
                </legend>
                {% if member.favourite_away_ground is not empty %}
                    <div>
                        <span class="label">Favorite Away Ground:</span><span
                                class="input-text">{{ member.favourite_away_ground }}</span>
                    </div>
                {% endif %}
                {% if member.favourite_all_time_player is not empty %}
                    <div>
                        <span class="label">All-Time Favourite Player:</span><span
                                class="input-text">{{ member.favourite_all_time_player }}</span>
                    </div>
                {% endif %}
                {% if member.favourite_current_player is not empty %}
                    <div>
                        <span class="label">Favourite Current Player:</span><span
                                class="input-text">{{ member.favourite_current_player }}</span>
                    </div>
                {% endif %}
            </fieldset>
        {% endif %}

    </section>
</main>