<main id="forum"> {#TODO - will need to factor out style that both topic and forum style share and style they don't.#}
    <h1>{{ forum.title }}</h1>


    {% if pinned_topics is not empty %}{#TOD - Pinned topics#}
        <section class="forum-topics">

            <h2><a href="forum/">{{ forum_type_title }}</a> >> {{ forum.title }} >> Pinned Topics</h2>

            {% include "partials/forum_topics_pinned.volt" %}

        </section>
    {% endif %}

    <div class="forum-topics page-content-container">
        <h2 class="page-section-header"><a href="forum/">{{ forum_type_title }}</a> >> {{ forum.title }} </h2>
        <header>
            {% if auth is true %}
                <div class="forum-topic-tools">
                    <a href="forum/addtopic/{{ forum.id }}">
                        <img
                                title="Add New Topic"
                                alt="Add New Topic"
                                class="button success"
                                src="img/buttons/plus.png"
                        >
                        <span>Add New Topic</span>
                    </a>
                    {% if sysadmin %}
                        <a href="admin/forum/{{ forum.id }}/1">
                            <img
                                    title="Edit Forum"
                                    alt="Edit Forum"
                                    class="button web-orange"
                                    src="img/buttons/edit.png"
                            >
                            <span>Edit Forum</span>
                        </a>
                    {% endif %}
                </div>
            {% endif %}
            <div class="paginator">{{ elements.getPaginator(page, "Topics") }} </div>
        </header>

        {% include "partials/forum_topics.volt" %}


    </div>

</main>


{{ content() }}