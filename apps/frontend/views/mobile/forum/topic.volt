{% do assets.addJs("js/wysibb/jquery.wysibb.js") %}
{% do assets.addJs("js/wysibb-mobile-options.js") %}

<section
        id="forum-topic"> {#TODO - will need to factor out style that both topic and forum style share and style they don't.#}
    <header>
        <h1>{{ topic.title }}</h1>
        {#TODO - FORUM SEARCH TOOLS#}
    </header>


    <div>
        <nav>
            <a href="forum/{{ topic.getForum().id }}/1">{{ topic.getForum().title }}</a>
            >> {{ topic.title }}
            {{ elements.getTopicRatingStars(user_rating) }}
            {{ flashSession.output() }}
        </nav>

        <div class="forum-topic-tools">

            {% if auth is true %}
                <a href="forum/{{ topic.getForum().id }}/{{ topic.id }}/{{ page.current }}/rate"><img
                            class="button web-orange" src="img/buttons/star.png" alt="Rate Topic"> Rate Topic</a>
                <a href="forum/{{ topic.getForum().id }}/{{ topic.id }}/{{ page.current }}/addreply"><img
                            class="button success" src="img/buttons/plus.png">Reply</a>
                {% if editable %}
                    <a href="admin/forum/topic/{{ topic.id }}">
                        <img
                                title="Edit Topic"
                                alt="Edit Topic"
                                class="button web-orange"
                                src="img/buttons/edit.png"
                        > Edit Topic
                    </a>
                {% endif %}
            {% endif %}

        </div>
        <div class="paginator">{{ elements.getPaginator(page, "Posts") }} </div>
        {% include "partials/forum_topic_replies.volt" %}

        {% if page.items|length > 2 %}
            <div class="forum-topic-post-toolbar">
                {% if page.total_pages > 1 %}
                    <div class="paginator center">{{ elements.getPaginator(page, "Posts") }} </div>
                {% endif %}

                {% if auth is true %}
                    <div class="right-align">
                        <a href="">
                            <img
                                    title="Rate Topic"
                                    alt="Rate Topic"
                                    class="button web-orange"
                                    src="img/buttons/star.png"
                            > Rate Topic
                        </a>
                        <a href="forum/{{ topic.getForum().id }}/{{ topic.id }}/{{ page.current }}/addreply"><img
                                    class="button success" src="img/buttons/plus.png">Reply</a>
                        {% if editable %}
                            <a href="admin/forum/topic/{{ topic.id }}">
                                <img
                                        title="Edit Topic"
                                        alt="Edit Topic"
                                        class="button web-orange"
                                        src="img/buttons/edit.png"
                                > Edit Topic
                            </a>
                        {% endif %}
                    </div>
                {% endif %}

            </div>
        {% endif %}
    </div>


    {% if auth is true %}
        <form id="form" method='post' enctype='multipart/form-data'>
            <fieldset class="page-section form-section">
                <legend class="page-section-header">Quick Reply</legend>
                {% if quote_text is defined %}
                    {{ form.get("bbcode_field").setDefault(quote_text) }}
                {% else %}
                    {{ form.get("bbcode_field") }}
                {% endif %}
                {{ form.get("content_html") }}
            </fieldset>

            {{ form.get("reply") }}

        </form>
    {% endif %}

    <nav>
        <a href="forum/">{{ topic.getForum().getForumType().title }}</a> >>
        <a href="forum/{{ topic.getForum().id }}/1">{{ topic.getForum().title }}</a>
        >> {{ topic.title }}
        {{ elements.getTopicRatingStars(user_rating) }}
        {{ flashSession.output() }}
    </nav>

</section>

