<div id="forum" class="forum">
    <h1>Forum</h1>


    {% for forum_type in forum_types %}
        <div class="page-content-container">
            {{ flashSession.output() }}
            <h2 class="page-section-header"> {{ forum_type.title }} </h2>
            <table>
            <tr class="table-header">
                <th id="replies-col"></th>
                <th id="name-col">Name</th>
                <th id="topics-posts-col">Topics</th>
                {% if logged_in %}<span id="mark-read-col"></span>{% endif %}
            </tr>
            {% for forum in forum_type.getForum(['status = "A" AND view_type = "P"']) %}
                <tr class="table-row">
                    <td>
                        <a href="forum/{{ forum.id }}/1">
                            <img title="Go to the {{ forum.title }} forum" class="button success"
                                 src="img/buttons/text.png">
                        </a>
                    </td>
                    <td>
                        <h3><a title='View forum "{{ forum.title }}"'
                               href="forum/{{ forum.id }}/1">{{ forum.title }}</a>
                        </h3> {#TODO - use display: flex and change margin-top to override default styling #}
                        <a title='View forum "{{ forum.title }}"'
                           href="forum/{{ forum.id }}/1">
                            {{ forum.description }}
                        </a>
                    </td>
                    <td>{{ forum_topic_count[forum.id] is defined ? forum_topic_count[forum.id] : "0" }}</td>
                    {#<span>#TODO</span>{#Posts#}
                </tr>
            {% endfor %}
            </table>
        </div>
    {% endfor %}

</div>
