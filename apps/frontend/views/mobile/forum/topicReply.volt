{% do assets.addJs("js/wysibb/jquery.wysibb.js") %}
{% do assets.addJs("js/wysibb-mobile-options.js") %}

<main class="forum forum-topic-reply"> {#TODO - will need to factor out style that both topic and forum style share and style they don't.#}
    <header>
        <h1>{{ topic.getForum().title }}</h1>
    </header>

    <form id="form" method='post' enctype='multipart/form-data'>
        <fieldset>
            <legend class="page-section-header">
                <a href="forum/{{ topic.getForum().id }}/{{ topic.id }}/1">
                    {{ topic.title }}
                </a>
                >> Reply
            </legend>
        </fieldset>

        <fieldset class="page-section form-section">
            <legend class="page-section-header">Original Post</legend>
            <p>{{ original_post_content|parsebbtohtml }}</p>
        </fieldset>

        <fieldset class="page-section form-section">
            <legend class="page-section-header">Reply</legend>
            {% if quote_text is defined %}
                {{ form.get("bbcode_field").setDefault(quote_text) }}
            {% else %}
                {{ form.get("bbcode_field") }}
            {% endif %}
            {{ form.get("content_html") }}
        </fieldset>

        {{ form.get("reply") }}

    </form>

</main>
