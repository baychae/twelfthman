{% do assets.addJs("js/wysibb/jquery.wysibb.js") %}
{% do assets.addJs("js/wysibb-mobile-options.js") %}

<main class="forum forum-topic-reply">
    <header>
        <h1>{{ post.getForumTopic().getForum().title }}</h1>
    </header>

    <form id="form" method='post' enctype='multipart/form-data'>
        <fieldset>
            <legend class="page-section-header">
                <a href="forum/{{ post.getForumTopic().getForum().id }}/{{ post.getForumTopic().id }}/1">
                    {{ post.getForumTopic().title }}
                </a>
                >> Flag Post
                {{ flashSession.output() }}
            </legend>
        </fieldset>

        <fieldset class="page-section form-section">
            <legend class="page-section-header">Flagged Post</legend>
            <p>{{ offending_post_content|parsebbtohtml }}</p>
        </fieldset>

        <fieldset class="page-section form-section">
            <legend class="page-section-header">Flag Reason</legend>
            {% if quote_text is defined %}
                {{ form.get("bbcode_field") }}
            {% else %}
                {{ form.get("bbcode_field") }}
            {% endif %}
            {{ form.get("content_html") }}
        </fieldset>

        {{ form.get("send") }}
        <button id="go-back" type="button" class="form-button">Cancel</button>
        {#{{ form.get("cancel") }}#}

    </form>

</main>