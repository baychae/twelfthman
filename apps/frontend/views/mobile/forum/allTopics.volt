<main class="forum"> {#TODO - will need to factor out style that both topic and forum style share and style they don't.#}
    <header>
        <h1>All Topics</h1>
        <a href="forum/">
            <button id="view-forums" class="forum button">View Forums</button>
        </a>
    </header>

    {% if pinned_topics is not empty %}{#TOD - Pinned topics#}
        <section class="forum-topics">

            <h2><a></a> All Pinned Topics</h2>

            {% include "partials/forum_topics_pinned.volt" %}

        </section>
    {% endif %}

    <section class="forum-topics">
        <h2>All Topics</h2>
        <header>
            {% if auth is true %}
                <a href="#">
                    <img
                            title="Add New Topic"
                            alt="Add New Topic"
                            class="button success"
                            src="img/buttons/plus.png">
                    <span>Add New Topic</span>
                </a>
                {% if sysadmin %}
                    <a href="#">
                        <img
                                title="Edit Forum"
                                alt="Edit Forum"
                                class="button web-orange"
                                src="img/buttons/edit.png"
                        >
                        <span>Edit Forum</span>
                    </a>
                {% endif %}
            {% endif %}
            <div class="paginator">{{ elements.getPaginator(page, "Topics") }} </div>
        </header>

        {% include "partials/forum_topics.volt" %}


    </section>

</main>


{{ content() }}