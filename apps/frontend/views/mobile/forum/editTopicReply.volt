{{ content() }}


<main class="forum">

    <div class="forum-topic">

        <h1>Edit Topic</h1>

        <form id="form" method='post' enctype='multipart/form-data'>


            <fieldset>
                <legend>{{ topic.getForum().getForumType().title }}
                    >> {{ topic.getForum().title }}
                    >> <a href="{{ post_url }}">{{ topic.title }} </a>
                    {{ flashSession.output() }}
                    >> Edit Reply
                    {{ flashSession.output() }}
                </legend>

                <div>
                    {{ form.get("content_bb") }}
                </div>

            </fieldset>

            <fieldset>
                <legend>Options</legend>
                <div>
                    {{ form.get("view_type").getLabel() }}
                    {{ form.get("view_type") }}
                </div>
            </fieldset>


            {{ form.get("update_button") }}
            {{ form.get("move_button") }}
            {{ form.get("delete_button") }}

        </form>

    </div>

</main>
