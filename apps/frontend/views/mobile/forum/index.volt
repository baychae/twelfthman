<div id="forum" class="forum">
    <h1>Forum</h1>


    {% for forum_type in forum_types %}
        <div class="page-content-container">
            {{ flashSession.output() }}
            <h2 class="page-section-header">{{ forum_type.title }}</h2>
            <table>
            <tr class="table-header">
                <th id="replies-col"></th>
                <th id="name-col">Name</th>
                <th id="topics-posts-col">Topics</th>
                {% if logged_in %}<span id="mark-read-col"></span>{% endif %}
            </tr>
            {% for forum in forum_type.getForum(['status = "A"']) %}
                <tr class="table-row">
                    <td>
                        <a href="forum/{{ forum.id }}/1">
                            {% if forum.hasNewReplies(user_id) %}
                                <img title='New replies in "{{ forum.title }}"' class="button success"
                                     src="img/buttons/text.png">
                            {% else %}
                                <img title='New replies in "{{ forum.title }}"' class="button info"
                                     src="img/buttons/text.png">
                            {% endif %}
                        </a>
                        {% if logged_in %}
                            {% if forum.hasNewReplies(user_id) %}
                                <a href="forum/markread/{{ forum.id }}">
                                    <img title="Mark All Topics Read" class="button info" src="img/buttons/tick.png">
                                </a>
                            {% endif %}
                        {% endif %}
                    </td>
                    <td>
                        <h3>
                            <a title='View forum "{{ forum.title }}"' href="forum/{{ forum.id }}/1">
                                {{ forum.title }}
                            </a>
                        </h3> {#TODO - use display: flex and change margin-top to override default styling #}
                        <a title='View forum "{{ forum.title }}"' href="forum/{{ forum.id }}/1">
                            {{ forum.description }}
                        </a>
                    </td>
                    <td class="topics-td">
                        {{ forum_topic_count[forum.id] is defined ? forum_topic_count[forum.id] : "0" }}
                    </td>
                    {#<span>#TODO</span>{#Posts#}
                </tr>
            {% endfor %}
            </tbody>
        </table>
    {% endfor %}

</div>
