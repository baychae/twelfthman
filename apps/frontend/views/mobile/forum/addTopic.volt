{% do assets.addJs("js/wysibb/jquery.wysibb.js") %}
{% do assets.addJs("js/wysibb-mobile-options.js") %}

<h1>Add Topic to {{ forum.title }}</h1>

<div class="forum forum-topic-reply">
    <form id='form' method='post' enctype='multipart/form-data'>
        <fieldset class="page-section form-section">
            <legend class="page-section-header">Topic Details {{ flashSession.output() }}</legend>
            <div>
                {{ form.getLabel("title") }}
                {{ form.get("title") }}
            </div>
            <div>
                {{ form.getLabel("description") }}
                {{ form.get("description") }}
            </div>
        </fieldset>
        <fieldset class="page-section form-section">
            {{ form.getLabel("bbcode_field") }}
            {{ form.get("bbcode_field") }}
            {{ form.get("content_html") }}
        </fieldset>

        {#TODO if no article if then button is add
        {{ form.get('modify') }}#}

        {{ form.get('add') }}


    </form>
</div>
