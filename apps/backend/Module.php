<?php

namespace Multiple\Backend;

use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\DiInterface;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\ModuleDefinitionInterface;

class Module implements ModuleDefinitionInterface
{
    /**
     * Register a specific autoloader for the module
     */
    public function registerAutoloaders(DiInterface $dependencyInjector = null)
    {

        $config = include SITE_ROOT . 'apps/config/config.php';

        $loader = new Loader();

        $loader->registerNamespaces(
            [
                'Multiple\\Backend\\Controllers' => SITE_ROOT . 'apps/backend/controllers/',
                'Multiple\\Backend\\Forms' => SITE_ROOT . 'apps/backend/forms',
                'Common\\Models' => SITE_ROOT . "apps/common/models",
                'Common\\Library' => SITE_ROOT . 'apps/common/library/',
                'Common\\Models\\Traits' => SITE_ROOT . "apps/common/models/traits",
                'Common\\Models\\Entities' => SITE_ROOT . "apps/common/models/entities",
                "Common\\Plugins" => SITE_ROOT . 'apps/common/plugins',
                "JBBCode" => $config->vendorDir . "jBBCode-1.3.0/JBBCode/",
//                "Clockwork" => SITE_ROOT . "apps/common/library/Sms/Clockwork.php",
                "BespokeJBBCodeVisitors"  => $config->common->libraryDir . "BespokeJBBCodeVisitors/",
                "BespokeJBBCodeDefinitions" => $config->common->libraryDir . "BespokeJBBCodeDefinitions/",
            ]
        );

        include_once SITE_ROOT . 'vendor/autoload.php';

        $loader->register();
    }

    /**
     * Register specific services for the module
     */
    public function registerServices(DiInterface $di)
    {

        $config = include SITE_ROOT . 'apps/config/config.php';

        // Registering a dispatcher
        $di->set('dispatcher', function () use ($di) {
            $eventsManager = $di->getShared('eventsManager');
            $security = new \Common\Plugins\Security($di);
            //#TODO set a custom work factor
            //    //$security->setWorkFactor(12);
            //    //Listen for events produced in the dispatcher using the Security plugin
            $eventsManager->attach('dispatch:beforeExecuteRoute', $security);
            $eventsManager->attach('dispatch:beforeException', new \Common\Plugins\NotFoundPlugin());

            $dispatcher = new Dispatcher();
            $dispatcher->setEventsManager($eventsManager);
            $dispatcher->setDefaultNamespace("Multiple\Backend\Controllers");

            return $dispatcher;
        });

        /**
         * Setting up volt as a service
         */
        $di->set('voltService', function ($view, $di) use ($config) {

            $volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);

            $volt->setOptions([
                "compiledPath" => $config->backend->cacheDir,
                "compiledExtension" => ".compiled",
                'compiledSeparator' => '_'
            ]);

            $compiler = $volt->getCompiler();

            // format number
            $compiler->addFilter('substr', function ($resolvedArgs, $exprArgs) {
                return 'Common\Library\Helpers::substr(' . $resolvedArgs . ');';
            });

            $compiler->addFilter('substrtows', function ($resolvedArgs, $exprArgs) {
                return 'Common\Library\Helpers::substrtows(' . $resolvedArgs . ');';
            });

            $compiler->addFilter('htmldecode', function ($resolvedArgs, $exprArgs) {
                return 'Common\Library\Helpers::htmldecode(' . $resolvedArgs . ');';
            });

            $compiler->addFilter('datedecode', function ($resoledArgs, $exprArgs) {
                return 'Common\Library\Helpers::datedecode(' . $resoledArgs . ')';
            });

            $compiler->addFilter('iso8601', function ($resoledArgs, $exprArgs) {
                return 'Common\Library\Helpers::iso8601(' . $resoledArgs . ')';
            });

            $compiler->addFilter('parsebbtohtml', function ($resolvedArgs, $exprArgs) {
                return 'Common\Library\Helpers::parsebbtohtml(' . $resolvedArgs . ')';
            });

            $compiler->addFilter('ordinal', function ($resoledArgs, $exprArgs) {
                return 'Common\Library\Helpers::ordinal(' . $resoledArgs . ')';
            });

            $compiler->addFilter('ukDate', function ($resoledArgs, $exprArgs) {
                return 'Common\Library\Helpers::ukDate(' . $resoledArgs . ')';
            });

            $compiler->addFilter('shortdate', function ($resoledArgs, $exprArgs) {
                return 'Common\Library\Helpers::shortdate(' . $resoledArgs . ')';
            });

            return $volt;
        });

        /**
         * Setting up the view component
         */
        //Register Volt as template engine
        $di->set('view', function () use ($config) {

            $view = new View();

            $detect = new \Mobile_Detect();
            $deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'desktop');


            if ($deviceType === 'desktop' || $deviceType === 'tablet') {
                $view->setViewsDir($config->backend->views->desktop);
            } else {
                define('MAX_PAGES_TO_LIST', $config->site->paginator->mobile->max_pages);
                define('PAGINATOR_VIEW_STATS', 0);
                define('PAGINATOR_VIEW_WORD_PAGE', 0);
                $view->setViewsDir($config->backend->views->mobile);
            }

            $view->registerEngines([
                ".volt" => 'voltService'
            ]);

            return $view;
        });
    }

}
