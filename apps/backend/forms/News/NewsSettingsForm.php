<?php

namespace Multiple\Backend\Forms\News;

use Phalcon\Forms\Form;

class NewsSettingsForm extends Form {
    
    public function initialize($news_settings=null) {
        $this->add(new \Phalcon\Forms\Element\Text("maxBlurbs"));
        $this->add(new \Phalcon\Forms\Element\Text("other_news_limit"));
        $this->add(new \Phalcon\Forms\Element\File("upload_image_file", array("id" => "upload-new-image-file", "class" => "upload-new-image-file")));
        $this->add(new \Phalcon\Forms\Element\Submit("Upload New Image", array("id" => "upload-new-image-overlay")));
        $this->add(new \Phalcon\Forms\Element\Submit("upload_image", array("value" => "Upload Image", "name" => "image_action")));
        $this->add(new \Phalcon\Forms\Element\Submit("delete_image", array("value" => "Delete Image", "name" => "image_action")));
        $this->add(new \Phalcon\Forms\Element\Submit("Submit", array("value" => "Save Settings")));
    }
    
}