<?php

namespace Multiple\Backend\Forms\News;

use Phalcon\Forms\Form;

class ModifyNewsGroupForm extends Form {
    
    private $_news_group_label;
    
    public function initialize($news_group)
    {
        $this->_news_group_label = $news_group->label;
        
        //NAME
        $name_element = new \Phalcon\Forms\Element\Text("Name");
        $name_element->setLabel("<label for='Name'>Name:</label> ");
        $name_element->setDefault($this->_news_group_label);
        
        //IMAGE FILE OR IMAGE IF FOUND
        $image_file_element = new \Phalcon\Forms\Element\File("ImageFile");
        $image_file_element->setLabel("<label for='ImageFile'>Image:</label> ");
        $image_file_element->setAttribute("onchange", "readSRC(this, '#img-admin-newsgroup')");

        $image_element_maxsize = new \Phalcon\Forms\Element\Hidden("MAX_FILE_SIZE", ["value" => constant("MAX_ICON_IMAGE_SIZE")]);
        
        //PRIORITY
        $priority_element = new \Phalcon\Forms\Element\Text("Priority");
        $priority_element->setLabel("<label for='Priority'>Priority:</label>");
        $priority_element->setDefault($news_group->priority);

        $update_image_button = new \Phalcon\Forms\Element\Submit("UpdateImage", ["name" => "command", "class" => "form-button", "value" => "Update Image"]);
        $delete_image_button = new \Phalcon\Forms\Element\Submit("DeleteImage", ["name" => "command", "class" => "form-button", "value" => "Delete Image", "onclick" => "confirm('Are you sure you want to delete?') "]);
        $cancel_image_button = new \Phalcon\Forms\Element\Submit("CancelImage", ["name" => "command", "class" => "form-button", "value" => "Cancel Update"]);

        $modify_button = new \Phalcon\Forms\Element\Submit("Update", ["name" => "command", "value" => "Update", "class" => "form-button", "onclick" => "confirm('Update Record?') "]);
        $delete_button = new \Phalcon\Forms\Element\Submit("Delete", ["name" => "command", "value" => "Delete", "class" => "form-button", "onclick" => "confirm('Are you sure you want to delete the news group?') "]);
               
        $this->add($name_element);
        $this->add($image_element_maxsize);
        $this->add($image_file_element);
        $this->add($priority_element);
        $this->add($update_image_button);
        $this->add($delete_image_button);
        $this->add($cancel_image_button);
        $this->add($modify_button);
        $this->add($delete_button);
    }
    
        public function renderSelf()
    {

        
    }
    
}

