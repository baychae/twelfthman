<?php

namespace Multiple\Backend\Forms\News;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Submit;

class AddNewsGroupForm extends Form {
    
    public function initialize($admin_section_data)
    {
        
        $name_element = new \Phalcon\Forms\Element\Text("Name");
        $name_element->setLabel("<label for='Name'>Name:</label> ");
        $submit_button = new Submit("Submit", ["value" => "Add", "class" => "form-button"]);
        
        $this->add($submit_button);
        $this->add($name_element);
    }
    
}

