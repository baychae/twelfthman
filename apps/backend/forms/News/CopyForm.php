<?php

namespace Multiple\Backend\Forms\News;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\Submit;

class CopyForm extends Form
{
    
    public function initialize()
    {
        $this->add(new Text("text_limit"));
        $this->add(new Text("article_limit"));
        $this->add(new Submit("Submit"));
    }
    
}