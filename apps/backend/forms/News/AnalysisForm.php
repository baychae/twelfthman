<?php

namespace Multiple\Backend\Forms\News;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\Submit;

use Phalcon\Validation\Validator\PresenceOf;

class AnalysisForm extends Form
{
    
    public function initialize()
    {
        
        $article_limit = new Text("article_limit");
        $article_limit->addValidator(new PresenceOf(array(
            'message' => '<span class="validation-message">Please add a value for item limit</span>'
        )));
        $this->add($article_limit);
        
        $text_limit = new Text("text_limit");
        $text_limit->addValidator(new PresenceOf(array(
            'message' => '<span class="validation-message">Please add a value for text limit</span>'
        )));
        $this->add($text_limit);

        $this->add(new Submit("Submit"));
    }
    
}