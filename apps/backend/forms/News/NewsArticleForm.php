<?php

namespace Multiple\Backend\Forms\News;

use Phalcon\Forms\Form;

class NewsArticleForm extends Form
{
    
    public function initialize($news_article=null)
    {
        self::setUserOption ("class", "admin-form");
        
        $title = new \Phalcon\Forms\Element\Text("title");
        $title->setLabel("<label for='Title' >Title: </label>");
        $title->addValidator(new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'The article title is required'
        )));

        $image_upload_file = new \Phalcon\Forms\Element\File("Upload New Image File", array("id" => "upload-new-image-file", "class" => "upload-new-image-file"));
        $image_upload_file->setLabel("<label>Image: </label>");
        $image_upload_overlay = new \Phalcon\Forms\Element\Submit("Upload New Image", array("id" => "upload-new-image-overlay", "class" => "form-button"));
        $image_upload_update = new \Phalcon\Forms\Element\Submit("Update Image", array("value" => "Image Upload Update", "name" => "image_action", "class" => "form-button"));
        $image_embed_submit = new \Phalcon\Forms\Element\Submit("Embed New Image", array("name" => "embed-new-image", "onclick" => "" , "class" => "form-button"));
        $image_embed_text_area = new \Phalcon\Forms\Element\TextArea("image_embed", array("id" => "embed-image-text", "name" => "image_embed", "spellcheck" => "false"));
        
        $image_caption = new \Phalcon\Forms\Element\Text("image_caption");
        $image_caption->setLabel("<label for='Image Caption' >Image Caption: </label>");


        $news_groups = \Common\Models\NewsGroup::find(array('columns' => 'id,label'));
        $news_group = new \Phalcon\Forms\Element\Select("news_group_id", $news_groups, ["using" => ["id", "label"]]); //GET AND SET NEWS GROUPS
        $news_group->setLabel("<label for='news_group_id' >News Group: </label>");
        
        $publish_news_article = new \Phalcon\Forms\Element\Check("publish", array("onchange" => "unPublishAlert()") );
        $publish_news_article->setLabel("<label for='publish' >Publish News Article: </label>");

        $authors = \Common\Models\SiteUser::find(array("conditions" => "level != 'user' OR level != NULL "));
        $author = new \Phalcon\Forms\Element\Select("user_id", $authors, ["using" => ["id", "login_id"]]); //GET AND SET NEWS GROUPS
        $author->setLabel("<label for='user_id' >Author: </label>");
        
        $content = new \Phalcon\Forms\Element\TextArea("bbcode_field", array("id" => "editor", "name" => "bbcode_field", "style" => "width:100%"));
        $content->setLabel("<legend class='page-section-header'>Contents</legend> ");
        
        $content_html = new \Phalcon\Forms\Element\Hidden("content_html");
        
        $submit_button = new \Phalcon\Forms\Element\Submit("Submit", array("id" =>"update-news-article", "value" => "Update", "class" => "form-button"));
                  
        //Add elements to the form. Default values automatic.
        $this->add($title);
        $this->add($image_upload_file);
        $this->add($image_upload_overlay);
        $this->add($image_upload_update);
        $this->add($image_embed_submit);
        $this->add($image_embed_text_area);
        $this->add($image_caption);
        $this->add($news_group);
        $this->add($publish_news_article);
        $this->add($author);
        $this->add($content);
        $this->add($content_html);
        $this->add($submit_button);
    }
    

}