<?php
/**
 * AUTHOR: Richard Sinclair <rich.sinclair@gmail.com>
 * Date: 31/10/2016
 * Time: 09:57
 */

namespace Multiple\Backend\Forms\UpNext;

use Phalcon\Forms\Form;

class MatchForm extends Form
{
    public function initialize()
    {
        $teams = \Common\Models\Team::find(["order" => "name"]);

        $previews = \Common\Models\NewsGroup::findFirst("link = 'preview' ");
        $reports = \Common\Models\NewsGroup::findFirst("link = 'match-report' ");


        $home_team = new \Phalcon\Forms\Element\Select("home_team_id", $teams,
            [
                "useEmpty" => true,
                "emptyText" => "Choose home team ...",
                "using" => ["id", "name"]
            ]
        );
        $home_team->setLabel("<label for='home_team_id'>Home Team:</label>");

        $away_team = new \Phalcon\Forms\Element\Select("away_team_id", $teams,
            [
                "useEmpty" => true,
                "emptyText" => "Choose away team ...",
                "using" => ["id", "name"]
            ]
        );
        $away_team->setLabel("<label for='away_team_id'>Away Team:</label>");

        $match_date = new \Phalcon\Forms\Element\Date("kickoff_date");
        $match_date->setLabel("<label for='kickoff_date'>Kickoff Date: (DD/MM/YYYY)</label>");

        $match_time = new \Common\Library\FormElements\TimeInput("kickoff_time");
        $match_time->setLabel("<label for='kickoff_time'>Kickoff Time:</label>");

        $update_button = new \Phalcon\Forms\Element\Submit("Update", ["value" => "Update Match", "name" => "action", "class" => "form-button"]);
        $delete_button = new \Phalcon\Forms\Element\Submit("Delete", ["value" => "Delete Match", "name" => "action", "class" => "form-button"]);
        $add_button = new \Phalcon\Forms\Element\Submit("Add", ["value" => "Add New Match", "name" => "action", "class" => "form-button"]);
        $save_button = new \Phalcon\Forms\Element\Submit("Save", ["value" => "Save New Match", "name" => "action", "class" => "form-button"]);

        $home_score = new \Phalcon\Forms\Element\Text("home_score", ["size" => 2, "maxlength" => 2]);
        $home_score->setLabel("<label>Home Score</label>");

        $away_score = new \Phalcon\Forms\Element\Text("away_score", ["size" => 2, "maxlength" => 2]);
        $away_score->setLabel("<label>Away Score</label>");

        $match_preview = new \Phalcon\Forms\Element\Select("preview_article_id",
            $previews->getNewsArticle(["order" => "published_first desc"]),
            [
                "using" => ["id", "title"],
                "useEmpty" => true,
                "emptyText" => "Choose match preview..."
            ]
        );
        $match_preview->setLabel("<label for='preview_article_id'>Match Preview:</label>");

        $match_report = new \Phalcon\Forms\Element\Select("report_article_id",
            $reports->getNewsArticle(["order" => "published_first desc"]),
            [
                "using" => ["id", "title"],
                "useEmpty" => true,
                "emptyText" => "Choose match report..."
            ]
        );
        $match_report->setLabel("<label for='report_article_id'>Match Report:</label>");

        $this->add($home_team);
        $this->add($away_team);
        $this->add($match_date);
        $this->add($match_time);
        $this->add($update_button);
        $this->add($delete_button);
        $this->add($home_score);
        $this->add($away_score);
        $this->add($match_preview);
        $this->add($match_report);
        $this->add($add_button);
        $this->add($save_button);

    }
}