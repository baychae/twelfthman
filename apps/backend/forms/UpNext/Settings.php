<?php

namespace Multiple\Backend\Forms\UpNext;

class Settings extends \Phalcon\Forms\Form
{

    public function initialize($news_settings = null)
    {

        $live_score_endpoint_sms = new \Phalcon\Forms\Element\Text("liveScoreEndpointSms");
        $live_score_endpoint_sms->setLabel("<label for='liveScoreEndpointSms'>Live Score End Point #:</label>");
        $this->add($live_score_endpoint_sms);

        $live_score_sms_expect_from = new \Phalcon\Forms\Element\Text("liveScoreSmsExpectFrom");
        $live_score_sms_expect_from->setLabel("<label for='liveScoreSmsExpectFrom'>Live Score Admin #:</label>");
        $this->add($live_score_sms_expect_from);

        $live_score_sms_active = new \Phalcon\Forms\Element\Check("liveScoreSmsActive", ["value" => 1]);
        $live_score_sms_active->setLabel("<label for='liveScoreSmsActivea'>Live Score SMS Active:</label>");
        $this->add($live_score_sms_active);

        $live_score_balance = new \Phalcon\Forms\Element\Text("live-score-sms-balance");
        $live_score_balance->setLabel("<label for='live-score-sms-balance'>SMS Balance:</label>");
        $this->add($live_score_balance);

        $this->add(new \Phalcon\Forms\Element\Submit("Submit", array("value" => "Update")));
    }

}