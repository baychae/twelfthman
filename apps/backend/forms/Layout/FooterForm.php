<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 27/09/2016
 * Time: 12:34
 */

namespace Multiple\Backend\Forms\Layout;


class FooterForm extends \Phalcon\Forms\Form
{

    public function initialize($escaped_footer_html)
    {
        $content = new \Phalcon\Forms\Element\TextArea("bbcode_field", array("id" => "editor", "name" => "bbcode_field", "style" => "width:100%"));
        $content_html = new \Phalcon\Forms\Element\Hidden("content_html");
        $submit_button = new \Phalcon\Forms\Element\Submit("Submit", array("id" => "save-footer", "class" => "form-button"));

        $this->add($content);
        $this->add($content_html);
        $this->add($submit_button);
    }
}
