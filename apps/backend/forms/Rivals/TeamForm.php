<?php
/**
 * Created by PhpStorm.
 * User: Richard Sinclair <rich.sinclair@gmail.com>
 * Date: 24/10/2016
 * Time: 15:39
 */

namespace Multiple\Backend\Forms\Rivals;

class TeamForm extends \Phalcon\Forms\Form
{

    public function initialize()
    {


        $name_element = new \Phalcon\Forms\Element\Text("name");
        $name_element->setLabel("<label for='name'>Name</label>");

        $nickname_element = new \Phalcon\Forms\Element\Text("nickname");
        $nickname_element->setLabel("<label for='position'>Nickname</label>");

        $website_element = new \Phalcon\Forms\Element\Text("website");
        $website_element->setLabel("<label for='position'>Website</label>");

        $image_upload_file = new \Phalcon\Forms\Element\File("FlagFile", array("id" => "upload-new-image-file", "class" => "upload-new-image-file"));
        $image_upload_file->setLabel("<label>Flag: </label>");

        $image_upload_update = new \Phalcon\Forms\Element\Submit("UpdateFlag", array("value" => "Update Flag", "name" => "image_action", "class" => "form-button"));

        $update_button = new \Phalcon\Forms\Element\Submit("Update", array("value" => "Update Team", "name" => "action", "class" => "form-button"));
        $this->add($update_button);

        $delete_button = new \Phalcon\Forms\Element\Submit("Delete", array("value" => "Delete Team", "name" => "action", "class" => "form-button"));
        $this->add($delete_button);


        $leagues_select = new \Phalcon\Forms\Element\Select("league_id", \Common\Models\League::find(),
            [
                "using" => ["id", "name"]
            ]
        );
        $leagues_select->setLabel("<label>League</label>");


        $this->add($name_element);
        $this->add($nickname_element);
        $this->add($website_element);
        $this->add($image_upload_file);
        $this->add($image_upload_update);
        $this->add($leagues_select);
        $this->add(new \Phalcon\Forms\Element\Submit("Submit"));
    }

}