<?php

namespace Multiple\Backend\Forms\Rivals;

class LeagueForm extends \Phalcon\Forms\Form
{

    public function initialize()
    {
        $name_element = new \Phalcon\Forms\Element\Text("name");
        $name_element->setLabel("<label for='name'>Title</label>");

        $position_element = new \Phalcon\Forms\Element\Text("position");
        $position_element->setLabel("<label for='position'>Position</label>");

        $update_button = new \Phalcon\Forms\Element\Submit("Update", array("value" => "Update League", "name" => "action", "class" => "form-button"));
        $this->add($update_button);

        $delete_button = new \Phalcon\Forms\Element\Submit("Delete", array("value" => "Delete League", "name" => "action", "class" => "form-button"));
        $this->add($delete_button);


        $this->add($name_element);
        $this->add($position_element);
        $this->add(new \Phalcon\Forms\Element\Submit("Submit"));
    }

}