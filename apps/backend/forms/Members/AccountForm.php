<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 16/06/2017
 * Time: 13:37
 */

namespace Multiple\Backend\Forms\Members;

class AccountForm extends \Phalcon\Forms\Form
{
    public function initialize()
    {
        $first_name_element = new \Phalcon\Forms\Element\Text("first_name");
        $first_name_element->setLabel("<label for='first_name' title='First Name'>First Name:</label>");
        $this->add($first_name_element);

        $last_name_element = new \Phalcon\Forms\Element\Text("last_name");
        $last_name_element->setLabel("<label for='last_name' title='Last Name'>Last Name:</label>");
        $this->add($last_name_element);

        $login_id = new \Phalcon\Forms\Element\Text("login_id");
        $login_id->setLabel("<label for='login_id' title='Login I.D.'>Login Id:</label>");
        $this->add($login_id);

        $email = new \Phalcon\Forms\Element\Text("email");
        $email->setLabel("<label for='email' title='Email Address'>Email Address:</label>");
        $this->add($email);

        $email = new \Phalcon\Forms\Element\Text("avatar_name");
        $email->setLabel("<label for='avatar_name' title='Displayed Name'>Displayed Name:</label>");
        $this->add($email);

        $priv_levels = [
            'sysadmin' => 'System Admin',
            'admin' => 'Admin',
            'poweruser' => 'Power User',
            'user' => 'User'
        ];
        $user_privlege = new \Phalcon\Forms\Element\Select("level", $priv_levels);
        $user_privlege->setLabel("<label for='level' title='Level'>Level:</label>");
        $this->add($user_privlege);

        $user_locked = new \Phalcon\Forms\Element\Check("active", ["value" => "L"]);
        $user_locked->setLabel("<label for='active' title='User Locked'>User Locked:</label>");
        $this->add($user_locked);

        $signature_element = new \Phalcon\Forms\Element\Text("signature");
        $signature_element->setLabel("<label for='signature' title='Signiture'>Signature:</label>");
        $this->add($signature_element);

        $avatar = new \Phalcon\Forms\Element\File("upload_avatar",
            [
                "id" => "upload-new-image-file",
                "class" => "upload-new-image-file"
            ]
        );
        $avatar->setLabel("<label title='Avatar'>Avatar:</label>");
        $this->add($avatar);

        $image_upload_overlay = new \Phalcon\Forms\Element\Submit("upload_avatar_overlay",
            [
                "id" => "upload-new-image-overlay",
                "class" => "form-button",
                "value" => "Upload Avatar",
                "title" => "Upload Avatar"
            ]
        );
        $this->add($image_upload_overlay);

        $delete_avatar = new \Phalcon\Forms\Element\Submit("delete_avatar",
            [
                "id" => "delete-image",
                "class" => "form-button",
                "name" => "delete_avatar",
                "value" => "Delete Avatar",
                "title" => "Delete Avatar"
            ]
        );
        $this->add($delete_avatar);

        $image_upload_update = new \Phalcon\Forms\Element\Submit("update_avatar",
            [
                "value" => "Image Upload Update",
                "name" => "upload_avatar_action",
                "class" => "form-button",
                "title" => "Image Upload Update"
            ]
        );
        $this->add($image_upload_update);

        $update_my_profile_submit = new \Phalcon\Forms\Element\Submit("update_my_profile",
            [
                "value" => "Update",
                "id" => "update_my_account",
                "name" => "update_profile",
                "class" => "form-button",
                "title" => "Update Profile"
            ]
        );
        $this->add($update_my_profile_submit);

        $content = new \Phalcon\Forms\Element\TextArea("bbcode_field",
            [
                "id" => "editor",
                "name" => "bbcode_field",
                "style" => "width:100%"
            ]
        );
        $content->setLabel("<legend title='Contents'>Contents</legend> ");
        $this->add($content);

        $favourite_away_ground = new \Phalcon\Forms\Element\Text("favourite_away_ground");
        $favourite_away_ground->setLabel("<label for='favourite_away' title='Favourite Away Ground'>Favourite Away Ground:</label>");
        $this->add($favourite_away_ground);

        $favourite_all_time_player = new \Phalcon\Forms\Element\Text("favourite_all_time_player");
        $favourite_all_time_player->setLabel("<label for='favourite_all_time_player' title='Favourite All Time Player'>Favourite All Time Player:</label>");
        $this->add($favourite_all_time_player);

        $favourite_current_player = new \Phalcon\Forms\Element\Text("favourite_current_player");
        $favourite_current_player->setLabel("<label for='favourite_current_player' title='Favourite Current Player'>Favourite Current Player:</label>");
        $this->add($favourite_current_player);

        $content_html = new \Phalcon\Forms\Element\Hidden("content_html");
        $this->add($content_html);

        $team_params = [
            'useRmpty' => false,
            'using' => ['id', 'name']
        ];

        $team = new \Phalcon\Forms\Element\Select('team_id', \Common\Models\Team::find(), $team_params);
        $team->setLabel("<label for='team_id' title='Home Team'>Home Team:</label>");
        $this->add($team);
    }


}