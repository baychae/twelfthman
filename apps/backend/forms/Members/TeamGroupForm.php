<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 07/06/2016
 * Time: 15:22
 */

namespace Multiple\Backend\Forms\Members;


class TeamGroupForm extends \Phalcon\Forms\Form
{

    public function initialize()
    {
        $group_name_element = new \Phalcon\Forms\Element\Text("name");
        $group_name_element->setLabel("<label for='title'>Group Name:</label>");
        $this->add($group_name_element);

        $image_upload_file = new \Phalcon\Forms\Element\File("upload_image_file", ["id" => "upload-new-image-file", "class" => "upload-team-group-image-file"]);
        $image_upload_file->setLabel("<label>Image: </label>");
        $this->add($image_upload_file);

        $image_upload_update = new \Phalcon\Forms\Element\Submit("update_image",
            [
                "value" => "Update",
                "name" => "update_image",
                "class" => "form-button"
            ]
        );
        $this->add($image_upload_update);

        $image_delete_file = new \Phalcon\Forms\Element\Submit("delete_image",
            [
                "value" => "Delete",
                "name" => "delete_image",
                "class" => "form-button"
            ]
        );
        $this->add($image_delete_file);

        $image_upload_overlay = new \Phalcon\Forms\Element\Submit("Upload New Image",
            [
                "id" => "upload-new-image-overlay",
                "class" => "form-button"
            ]
        );
        $this->add($image_upload_overlay);

        $update_form = new \Phalcon\Forms\Element\Submit("update_group",
            [
                "value" => "Update Group",
                "name" => "update_group",
                "class" => "form-button"
            ]
        );
        $this->add($update_form);

        $delete = new \Phalcon\Forms\Element\Submit("delete_group",
            [
                "value" => "Delete Group",
                "name" => "delete_group",
                "class" => "form-button"
            ]
        );
        $this->add($delete);


    }

}

