<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 19/06/2017
 * Time: 15:24
 */

namespace Multiple\Backend\Forms\Members;

class AchievementForm extends \Phalcon\Forms\Form
{
    public function initialize()
    {
        $title = new \Phalcon\Forms\Element\Text("title");
        $title->setLabel("<label for='title' >Title:</label>");
        $this->add($title);

        $avatar = new \Phalcon\Forms\Element\File("upload_ach_image",
            [
                "id" => "upload-new-image-file",
                "class" => "upload-new-image-file"
            ]
        );
        $avatar->setLabel("<label  for='upload-new-image-file'>Achievement Image:</label>");
        $this->add($avatar);

        $image_upload_overlay = new \Phalcon\Forms\Element\Submit("upload_ach_image_overlay",
            [
                "id" => "upload-new-image-overlay",
                "class" => "form-button",
                "value" => "Upload Image"
            ]
        );
        $this->add($image_upload_overlay);

        $delete_avatar = new \Phalcon\Forms\Element\Submit("delete_ach_image",
            [
                "id" => "delete-image",
                "class" => "form-button",
                "name" => "delete_image",
                "value" => "Delete Image"
            ]
        );
        $this->add($delete_avatar);


        $save = new \Phalcon\Forms\Element\Submit('save', ['class' => 'form-button', 'value' => 'Save']);
        $this->add($save);

        $update = new \Phalcon\Forms\Element\Submit('update', ['value' => 'Update']);
        $this->add($update);

    }


}