<?php

namespace Multiple\Backend\Forms\Forum;

use Phalcon\Forms\Form;

class ForumTypeForm extends Form {
    
    public function initialize()
    {
       
        $title_element = new \Phalcon\Forms\Element\Text("title");
        $title_element->setLabel("<label for='Name'>Title:</label>");
        $this->add($title_element);

        $display_order_list = [
            "1" => "The Very Top",
            "2" => "Top",
            "3" => "Middle",
            "4" => "Bottom",
            "5" => "Below The Bottom"
        ];
        
        $display_order_element = new \Phalcon\Forms\Element\Select("display_order", $display_order_list);
        $display_order_element->setLabel("<label for='display_order'>Display Order:</label>");
        $this->add($display_order_element);

        $submit_button = new \Phalcon\Forms\Element\Submit("addType", ["value" => "Add Type", "class" => "form-button"]);
        $this->add($submit_button);

        $modify_button = new \Phalcon\Forms\Element\Submit("modify", ["name" => "action", "value" => "Update", "class" => "form-button"]);
        $this->add($modify_button);

        $delete_button = new \Phalcon\Forms\Element\Submit("delete", ["name" => "action", "value" => "Delete", "class" => "form-button"]);
        $this->add($delete_button);
        
    }
    
    
}

