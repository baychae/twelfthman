<?php

namespace Multiple\Backend\Forms\Forum;

use Phalcon\Forms\Form;

class ForumSettings extends Form {
    
    public function initialize()
    {
        $reset_forum_stats_button = new \Phalcon\Forms\Element\Submit("Reset", ["value" => "Reset", "name" => "action", "class" => "form-button"]);
        $this->add($reset_forum_stats_button);

        $modify_button = new \Phalcon\Forms\Element\Submit("Save", ["value" => "Save", "name" => "action", "class" => "form-button"]);
        $this->add($modify_button);
    }
    
}
