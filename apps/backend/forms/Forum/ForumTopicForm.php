<?php

namespace Multiple\Backend\Forms\Forum;

use Phalcon\Forms\Form;

class ForumTopicForm extends Form
{

    public function initialize()
    {
        
        $title_element = new \Phalcon\Forms\Element\Text("title");
        $title_element->setLabel("<label for='title' >Title: </label>");
        $this->add($title_element);

        $description_element = new \Phalcon\Forms\Element\TextArea("description", ["cols" => "60", "rows" => "4"]);
        $description_element->setLabel("<label for='description' >Description: </label>");
        $this->add($description_element);

        $params = [
            'using' => ['id', 'title'],
            'class' => 'browser-default'
        ];
        
        $forum_element = new \Phalcon\Forms\Element\Select('forum_id', \Common\Models\Forum::find(), $params);
        $forum_element->setLabel("<label for='forum_id' >Forum: </label>");
        $this->add($forum_element);

        $view_types = ["P" => "Public", "M" => "Members", "A" => "Admin"];
        $topic_view_types = new \Phalcon\Forms\Element\Select("view_type", $view_types, ['class' => 'browser-default']);
        $topic_view_types->setLabel("<label for='forumType'>Topic Visibity:</label>");
        $this->add($topic_view_types);

        $pinned_topic = new \Phalcon\Forms\Element\Check(
            "pinned",
            [
                'value' => "1",
                'class' => 'filled-in'
            ]
        );
        $pinned_topic->setLabel("<label for='pinned' >: Pinned Topic</label>");
        $this->add($pinned_topic);

        $modify_button = new \Phalcon\Forms\Element\Submit(
            "update",
            [
                "value" => "Update",
                "name" => "action",
                "class" => "form-button"
            ]
        );
        $this->add($modify_button);

        $close_button = new \Phalcon\Forms\Element\Submit(
            "close",
            [
                "value" => "Close",
                "name" => "action",
                "class" => "form-button"
            ]
        );
        $this->add($close_button);
        
        $delete_button = new \Phalcon\Forms\Element\Submit(
            "delete",
            [
                "value" => "Delete",
                "name" => "action",
                "class" => "form-button"
            ]
        );
        $this->add($delete_button);
        
        $back_button = new \Phalcon\Forms\Element\Submit(
            "back",
            [
                "value" => "Back",
                "name" => "action",
                "class" => "form-button"
            ]
        );
        $this->add($back_button);
        
        $goto_button = new \Phalcon\Forms\Element\Submit(
            "goto",
            [
                "value" => "Go to topic",
                "name" => "action",
                "class" => "form-button"
            ]
        );
        $this->add($goto_button);
    }

}
