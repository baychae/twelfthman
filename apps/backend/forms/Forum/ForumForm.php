<?php

namespace Multiple\Backend\Forms\Forum;

use Phalcon\Forms\Form;

class ForumForm extends Form {
    
    public function initialize()
    {
        $title_element = new \Phalcon\Forms\Element\Text("title");
        $title_element->setLabel("<label for='title'>Title:</label>");
        $this->add($title_element);

        $description_element = new \Phalcon\Forms\Element\TextArea("description", ["cols" => "60", "rows" => "4"]);
        $description_element->setLabel("<label for='description'>Description:</label>");
        $this->add($description_element);

        $forum_type_select_element = new \Phalcon\Forms\Element\Select(
            "forum_type_id",
            \Common\Models\ForumType::find(),
            [
                'using' => ['id', 'title'],
                'class' => 'browser-default'
            ]
        );
        $forum_type_select_element->setLabel("<label for='forumType'>Forum Type:</label>");
        $this->add($forum_type_select_element);

        $view_types = ["P" => "Public", "M" => "Members", "A" => "Admin"];
        $forum_view_types = new \Phalcon\Forms\Element\Select(
            "view_type",
            $view_types,
            [
                'class' => 'browser-default'
            ]
        );
        $forum_view_types->setLabel("<label for='forumType'>Forum Visibity:</label>");
        $this->add($forum_view_types);

        $modify_button = new \Phalcon\Forms\Element\Submit(
            "Update",
            [
                "value" => "Update Forum",
                "name" => "action",
                "class" => "form-button"
            ]
        );
        $this->add($modify_button);

        $submit_button = new \Phalcon\Forms\Element\Submit(
            "Create",
            [
                "value" => "Create",
                "class" => "form-button"
            ]
        );
        $this->add($submit_button);

        $delete_button = new \Phalcon\Forms\Element\Submit(
            "Delete",
            [
                "value" => "Delete Forum",
                "name" => "action",
                "class" => "form-button"
            ]
        );
        $this->add($delete_button);

        $delete_topic_button = new \Phalcon\Forms\Element\Submit(
            "DeleteTopics",
            [
                "value" => "Delete Selected Topics",
                "name" => "action",
                "class" => "form-button"
            ]
        );
        $this->add($delete_topic_button);
    }
    
}

