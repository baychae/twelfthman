

<form id="form" method='post' enctype='multipart/form-data'>
    <fieldset>
        <legend>Modify Forum Type</legend>
        
        <div>
            {{ form.get("title").getLabel() }}
            {{ form.get("title") }}
        </div>
        
        <div>
            {{ form.get("display_order").getLabel() }}
            {{ form.get("display_order") }}
        </div>
        
    </fieldset>
    
    {{ form.get("modify") }}
    {{ form.get("delete") }}
    
</form>


{{ content() }}