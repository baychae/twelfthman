{{ content() }}

<form id="form" method='post' enctype='multipart/form-data'>
    <fieldset>

        <legend>
            <a href="{{ url.get('admin/forums') }}">Forums</a>
            >>
            <a href="{{ url.get('admin/forum/') ~ topic.getForum().id }}/1">Edit {{ topic.getForum().title }}</a>
            >>
            Edit {{ topic.title }}
            {{ flashSession.output() }}
        </legend>

        <div>
            {{ form.get("title").getLabel() }}
            {{ form.get("title") }}
        </div>

        <div>
            {{ form.get("description").getLabel() }}
            {{ form.get("description") }}
        </div>

        <div>
            {{ form.get("forum_id").getLabel() }}
            {{ form.get("forum_id") }}
        </div>

        <div>
            {{ form.get("view_type").getLabel() }}
            {{ form.get("view_type") }}
        </div>

        <div>
            {{ form.get("pinned").getLabel() }}
            {{ form.get("pinned") }}
        </div>

        {{ form.get("update") }}
        {{ form.get("close") }}
        {{ form.get("delete") }}
        <a href="{{ url.get('admin/forum') ~ '/' ~ topic.getForum().id  ~ '/1' }}" class="a-button form-button">Back</a>
        <a href="{{ url.get('forum') ~ '/' ~ topic.getForum().id  ~ '/' ~ topic.id ~ '/1' }}"
           class="a-button form-button">Go to Topic in Forum</a>

    </fieldset>


</form>


