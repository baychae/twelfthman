<table>


    <caption>Forums{{ flashSession.output() }}</caption>
    
    <th colspan=2 class="paginator">
        {{ elements.getPaginator(page, "Forums") }}
    </th>
    <tr>
        <th>Title</th>
        <th class="forty-percent-wide">Last Modified Date/Time</th>
    </tr>
    
    {% for forum in page.items %}
        <tr>
            <td> <a href="{{ url.get("admin/forum") }}/{{ forum.id }}/1"> {{ forum.title }} </a> {{ flashSession.output() }} </td>
            <td> <a href="{{ url.get("admin/forum") }}/{{ forum.id }}/1"> {{ forum.modified|datedecode }} </a> </td>
        </tr>
    {% endfor %}
        
</table>




{{ content() }}