{{ content() }}
<form id="form" method='post' enctype='multipart/form-data'>
    <fieldset>


        <legend>Add Forum</legend>

        <div>
            {{ form.get("title").getLabel() }}
            {{ form.get("title") }}
        </div>

        <div>
            {{ form.get("description").getLabel() }}
            {{ form.get("description")  }} <span class="note">*nb max 250 characters.</span>
        </div>

        <div>
            {{ form.get("forum_type_id").getLabel() }}
            {{ form.get("forum_type_id") }}
        </div>

        <div>
            {{ form.get("view_type").getLabel() }}
            {{ form.get("view_type") }}
        </div>

    </fieldset>

    {{ form.get("Create") }}

</form>
