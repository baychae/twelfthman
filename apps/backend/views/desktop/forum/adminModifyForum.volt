{{ content() }}
<form id="form" method='post' enctype='multipart/form-data'>
    <fieldset>

        {% if form.get("title").getValue() is empty %}
        <legend>Add Forum</legend>
        {% else %}    
        <legend><a href="{{ url.get('admin/forums') }}">Forums</a> >> Edit Forum {{ flashSession.output() }}</legend>
        {% endif %}

        <div>
            {{ form.get("title").getLabel() }}
            {{ form.get("title") }}
        </div>

        <div>
            {{ form.get("description").getLabel() }}
            {{ form.get("description")  }} <span class="note">*nb max 250 characters.</span>
        </div>

        <div>
            {{ form.get("forum_type_id").getLabel() }}
            {{ form.get("forum_type_id") }}
        </div>

        <div>
            {{ form.get("view_type").getLabel() }}
            {{ form.get("view_type") }}
        </div>
        
    </fieldset>


    <section class="forum-topics">
        <h2>{{ forum.title }} Topics</h2>    
        <div class="paginator">{{ elements.getPaginator(page, "Topics") }}</div>
    </section>

    {% include "partials/admin_forum_topics.volt" %}
    <BR>
        {{ form.get("Update") }}
        {{ form.get("Delete") }}
        {{ form.get("DeleteTopics") }}

</form>

