{{ content() }}
<table>

    <caption>Flagged Posts <b>{{ flashSession.output() }}</b></caption>

    {% if page.total_pages > 1 %}
        <div colspan=2 class="paginator">
            {{ elements.getPaginator(page, "Flagged Posts") }}
        </div>
    {% endif %}

    <tr id="admin-forum-flagged-posts-header">
        <th>Post Date</th>
        <th>Post in Thread</th>
        <th>Reported User</th>
        <th>Thread Id</th>
        <th>Post Id</th>
    </tr>

    {% for flagged_post in page.items %}
            <tr>
                <td><a href="{{ url.get("admin/forum/flaggedpost/") ~ flagged_post.id }}">{{ flagged_post.getForumTopicPost().created|datedecode }}</a></td>
                <td><a href="{{ url.get("admin/forum/flaggedpost/") ~ flagged_post.id }}">{{ flagged_post.getForumTopic().title }}</a></td>
                <td><a href="{{ url.get("admin/forum/flaggedpost/") ~ flagged_post.id }}">{{ flagged_post.getSiteUser().login_id }}</a></td>
                <td><a href="{{ url.get("admin/forum/flaggedpost/") ~ flagged_post.id }}">{{ flagged_post.topic_id }}</a></td>
                <td><a href="{{ url.get("admin/forum/flaggedpost/") ~ flagged_post.id }}">{{ flagged_post.post_id }} {% if flagged_post.status is "D" %}<b>Deleted</b>{% endif %}</a></td>
            </tr>
        </a>
    {% endfor %}

</table>

<form method="post">
    {% if not deleted %}
        <input type="submit" class="form-button" value="Show Deleted Flags" name="action"></input>
    {% else %}
        <input type="submit" class="form-button" value="Hide Deleted Flags" name="action"></input>
    {% endif %}
</form>