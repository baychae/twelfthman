{{ content() }}


<table>
    <caption>Leagues<span></span>{{ flashSession.output() }}</caption>
    <th colspan=2 class="paginator">
        {{ elements.getPaginator(page, "Teams") }}
    </th>

    <tr>
        <th>Title</th>
        <th class="forty-percent-wide">Flag</th>
    </tr>

    {% for team in page.items %}
        <tr>
            <td><a href="{{ url.get('admin/rivals/team/') ~ team.id }}"> {{ team.name }} </a></td>
            <td>
                <a href="{{ url.get('admin/rivals/team/') ~ team.id }}">
                    {% if team.image_name is defined %}
                        <img src="{{ url( "img/teams/" ~ team.image_name ) }}">
                    {% endif %}
                </a>
            </td>
        </tr>
    {% endfor %}

</table>
