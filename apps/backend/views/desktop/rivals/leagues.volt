{{ content() }}


<table>
    <caption>Leagues<span></span>{{ flashSession.output() }}</caption>

    <tr>
        <th>Title</th>
        <th class="forty-percent-wide">Rank</th>
    </tr>

    {% for league in leagues %}
        <tr>
            <td><a href="{{ url.get('admin/rivals/') ~ league.id }}"> {{ league.name }} </a></td>
            <td><a href="{{ url.get('admin/rivals/') ~ league.id }}"> {{ league.position|ordinal }} </a></td>
        </tr>
    {% endfor %}

</table>
