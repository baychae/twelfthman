<table>
    <caption>News Groups {% if flashSession.has("success") %}
        <span class="caption-flash"><b>{{flashSession.getMessages()["success"][0]}}</b></span>{% endif %}
    </caption>
        
{% for news_group in news_groups %}
    <tr>
        <td><a href="news/modifynewsgroup/{{ news_group.link }}">{{ news_group.label }}</a></td>
    </tr>
{% endfor %}
</table>