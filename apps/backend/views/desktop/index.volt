<!DOCTYPE html>
<html lang="en">
    <head>
            {{ get_title() }}
            {{ stylesheetLink("css/style.css") }}
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
            {{ stylesheetLink("js/wysibb/theme/twelfthman/twelfthman.css") }}
            {{ assets.outputJs() }}
        
            {#set by the paginator#}
            {% if rel_next is defined %} {{ rel_next }} {% endif %}
            {% if rel_prev is defined %} {{ rel_prev }} {% endif %}
        <base href="{{ url.get("admin/") }}"> {#TODO - Introduce redundancy by checking in config if url.get does not work #}
    </head>

    <body>
    <nav class="navbar">
            {{ elements.getNavBar() }}
        </nav>

        <header id='header-logo-desktop' class=''>
            <div id='header-banner'> <!-- #TODO Place Banner here --> </div>
        </header>

        <div id="page-content"> {# Next Level down is index controller view #}   
            {{ content() }}
        </div>

        <footer>
            <!--TODO Footer-->
        </footer>
        <!--TODO - Javascript goes here--> 
    </body>

</html>