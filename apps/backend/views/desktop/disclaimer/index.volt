{% do assets.addJs("js/wysibb/jquery.wysibb.js") %}
{% do assets.addJs("js/wysibb-options.js") %}


{{ content() }}
<h1>Disclaimer</h1>

<form id="form" method="post" class="admin-disclaimer">
    <fieldset>
        <legend>Contents {{ flashSession.output() }}</legend>
        <div class="wysibb-text">
            <textarea id="editor"
                      name="bbcode_field">{{ disclaimer_text is not empty ? disclaimer_text : "" }}</textarea>
            <input id="content_html" name="content_html" type="hidden">
        </div>

    </fieldset>
    <input type="submit" class="form-button" value="Save">
</form>
