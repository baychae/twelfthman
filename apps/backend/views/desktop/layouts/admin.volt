{{ elements.getAdminHeading() }}
{{ elements.getAdminNavBar() }}
{{ elements.getAdminSelectBox() }}
<div class="admin-body">
    {{ content() }}
</div>