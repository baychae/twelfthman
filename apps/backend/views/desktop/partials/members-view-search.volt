{{ content() }}
<h2>
    <form method="get">
        <label for="search">Search:</label><input id="search" name="search" type="text">
        <input type="submit" value="Go">
    </form>
    {{ flashSession.output() }}
</h2>

<div class="center">
    {{ elements.getAlphaNumPaginator(alpha_index_array) }}
</div>

{% if indexed_by is defined %}
    <div class="paginator center">
        {{ elements.getIndexedPaginator(page, paginator_label, indexed_by) }}
    </div>
{% else %}
    <div class="paginator center">
        {{ elements.getIndexedPaginator(page, "Members") }}
    </div>
{% endif %}

<br>
{% for member in page.items %}

    {% set user_name = member.avatar_name ? member.avatar_name|url_encode : member.login_id|url_encode %}
    {% set user_id = member.id %}

    <div class="user-details">
        <a title="View Member {{ user_name }}" href="{{ url.get("admin/members/account/" ~ user_id) }}">
            {% if member.avatar_name is defined %}
                {{ member.avatar_name }}
            {% else %}
                {{ member.login_id }}
            {% endif %}
        </a>

        <a class="member-img" title="Send New Message to {{ user_name }}"

           href="{{ url.get("admin/members/account/" ~ user_id) }}">
            {% if member.avatar_file_name is defined %}
                <img src="{{ url.get('img/user-avatars/') ~ member.avatar_file_name }}">
            {% else %}
                <img src="{{ url.get('img/user-avatars/default-avatar.png') }}">
            {% endif %}
            {% if member.active == "L" %}
                <img class="member-locked" src='{{ url.get("img/buttons/closed.png") }}'>
            {% endif %}
        </a>
        <p>
            <a title="View Member {{ user_name }}" href="{{ url.get("admin/members/account/" ~ user_id) }}">
                {{ member.email }}
            </a>
        </p>
    </div>


{% endfor %}
