
<div class="table-row-group">
    <div class="table-header table-row">
        <span>ID</span>
        <span>Title</span>
        <span>Modified</span>
        <span>Delete</span>
    </div>

{% for topic in page.items %}

    <a class="table-row" href="{{ url.get('admin/forum/topic/') ~ topic.id }}">
        <div>{{ topic.id }}</div>
        <div>{{ topic.title }}</div>
        <div>{{ topic.modified|datedecode }}</div>
        <div><input type="checkbox" name="delete_topic[]" value="{{ topic.id }}"></div>
    </a>


{% endfor %}

</div>

{{ content() }}