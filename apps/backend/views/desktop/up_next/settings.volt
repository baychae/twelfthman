{{ content() }}
<h1>Settings</h1>

<div class="admin-body">
    <form id="form" method='post'>
        <fieldset>
            <legend>Live Score Sms Settings<b>{{ flashSession.output() }}</b> {{ flashSession.output() }}</legend>

            <div>
                {{ form.get("liveScoreSmsActive").getLabel() }}
                {{ form.get("liveScoreSmsActive") }}
            </div>

            <div>
                {{ form.get("liveScoreEndpointSms").getLabel() }}
                {{ form.get("liveScoreEndpointSms") }}
            </div>

            <div>
                {{ form.get("liveScoreSmsExpectFrom").getLabel() }}
                {{ form.get("liveScoreSmsExpectFrom") }}
            </div>

            <div>
                {{ form.get("live-score-sms-balance").getLabel() }}
                {{ sms_balance }}
            </div>

            {{ form.get("Submit") }}

        </fieldset>
    </form>
</div>