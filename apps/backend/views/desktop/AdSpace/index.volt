{{ content() }}
<h1>Ad Spaces</h1>

<form id="form" method="post" class="admin-ad-space">
    <fieldset>
        <legend>Ad Space {{ flashSession.output() }}</legend>
        {% for ad in ads %}

            <div>
                <label for="{{ ad["id"] }}">{{ ad["label"] }}:</label>
                <textarea id="{{ ad["id"] }}" type="text" name="{{ ad["id"] }}">{{ ad["code"] }}</textarea>
            </div>

        {% endfor %}
    </fieldset>
    <input type="submit" class="form-button" value="Save">
</form>
