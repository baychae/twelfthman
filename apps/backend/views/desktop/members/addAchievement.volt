{% do assets.addJs("js/wysibb-image-helper.js") %}
{% do assets.addJs("js/wysibb-ihelper-generic-options.js") %}

<form id="form" enctype="multipart/form-data" method="post">
    <fieldset>
        <legend>Add Achievement {{ flashSession.output() }}</legend>
        <div>
            {{ form.getLabel('title') }}
            {{ form.get('title') }}
        </div>
        <div>
            {{ form.getLabel("upload_ach_image") }}
            <div class="image-toolset">
                <div id="image-preview" class="no-select"></div>
                {{ form.get("upload_ach_image") }}
                {{ form.get("upload_ach_image_overlay") }}

            </div>
        </div>
    </fieldset>

    {{ form.get('save') }}

</form>