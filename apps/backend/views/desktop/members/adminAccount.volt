{% do assets.addJs('js/wysibb/jquery.wysibb.js') %}
{% do assets.addJs("js/wysibb-options.js") %}
{% do assets.addJs("js/wysibb-image-helper.js") %}

{{ content() }}

<form id="form" enctype="multipart/form-data" method="post">
    <fieldset>
        <legend>Member - Personal Details<b>{{ flashSession.output() }}</b></legend>
        <div>
            <label title="System I.D.">System Id:</label>
            {{ login_id }}
        </div>
        <div>
            {{ form.getLabel("first_name") }}
            {{ form.get("first_name") }}
        </div>
        <div>
            {{ form.getLabel("last_name") }}
            {{ form.get("last_name") }}
        </div>
        <div>
            {{ form.getLabel("login_id") }}
            {{ form.get("login_id") }}
        </div>
        <div>
            {{ form.getLabel("email") }}
            {{ form.get("email") }}
        </div>
    </fieldset>
    <fieldset>
        <legend>Member - User Type</legend>
        <div>
            {{ form.getLabel("level") }}
            {{ form.get("level") }}
        </div>
        <div>
            {{ form.getLabel("active") }}
            {{ form.get("active") }}
        </div>
    </fieldset>
    <fieldset>
        <legend>Member - Display Details</legend>
        <div>{{ form.getLabel("avatar_name") }} {{ form.get("avatar_name").setAttribute("placeholder", login_id) }}

            {% if form.get("avatar_name").getValue() is empty %}<span> * Defaults to your login id. Enter text to add your own.</span>{% endif %}
        </div>
        <div>
            {{ form.getLabel("team_id") }} {{ form.get("team_id") }}
        </div>
        <div>
            {{ form.getLabel("upload_avatar") }}
            <div class="image-toolset">
                {% if avatar_file_name is not defined %}
                    <div id="image-preview" class="no-select"></div>
                    {{ form.get("upload_avatar") }}
                    {{ form.get("upload_avatar_overlay") }}
                {% else %}
                    <img class="avatar-img" src="{{ url.get( 'img/user-avatars/' ~ avatar_file_name ) }}">
                    {{ form.get("delete_avatar") }}
                {% endif %}

            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Member - Signature</legend>
        {{ form.get("bbcode_field") }}
        {{ form.get("content_html") }}
    </fieldset>
    <fieldset>
        <legend>Member - Favourites</legend>
        <div>{{ form.getLabel("favourite_away_ground") }}  {{ form.get("favourite_away_ground") }}</div>
        <div>{{ form.getLabel("favourite_all_time_player") }}  {{ form.get("favourite_all_time_player") }}</div>
        <div>{{ form.getLabel("favourite_current_player") }}  {{ form.get("favourite_current_player") }}</div>
    </fieldset>
    <fieldset>
        <legend>Member - Achievements</legend>
        {% for achievement_type in achievement_types %}
            <div class="member-achievements">
                <label for="{{ achievement_type.title }}"
                       title="{{ achievement_type.title }}">{{ achievement_type.title }}</label>
                {% if achievement_type.id in achievements %}
                    <input id="{{ achievement_type.title }}" type="checkbox" name="achievements[]"
                           value="{{ achievement_type.id }}" checked>
                {% else %}
                    <input id="{{ achievement_type.title }}" type="checkbox" name="achievements[]"
                           value="{{ achievement_type.id }}">
                {% endif %}
                <img src="{{ url.get(config.backend.members.achievement.image_dir ~ achievement_type.file_name) }}">
            </div>
        {% endfor %}
    </fieldset>
    {{ form.get("update_my_profile") }}
</form>