<table>
    <caption>Achievement Types <b>{{ flashSession.output() }}</b></caption>
    {% if page.total_pages > 1 %}
        <div class="paginator">
            {{ elements.getPaginator(page, "Achievement Types") }}
        </div>
    {% endif %}
    {#<tr>#}
    <th>Title</th>
    <th>Pic</th>

    {% for achievement in page.items %}
        <tr>
            <td>
                <a href="{{ url.get('admin/members/achievement/edit/') ~ achievement.id }}">{{ achievement.title }}</a>
                {{ flashSession.output() }}
            </td>
            <td>
                <a href="{{ url.get('admin/members/achievement/edit/') ~ achievement.id }}">
                    {% if achievement.file_name is not empty %}
                        <img
                                src="{{ url.get( config.backend.members.achievement.image_dir ~ achievement.file_name ) }}">
                    {% endif %}
                </a>
            </td>
        </tr>
    {% endfor %}

</table>