{% do assets.addJs("js/wysibb-image-helper.js") %}
{% do assets.addJs("js/wysibb-ihelper-generic-options.js") %}
{{ content() }}

<form id="form" enctype="multipart/form-data" method="post" action="members/achievement/edit/{{ achievement_id }}">
    <fieldset>
        <legend>Edit Achievement {{ flashSession.output() }}</legend>
        <div>
            {{ form.getLabel('title') }}
            {{ form.get('title') }}
        </div>
        <div>
            {{ form.getLabel("upload_ach_image") }}
            <div class="image-toolset">

                {% if image_url is not empty %}
                    <img id="image-preview" src={{ image_url }}>
                    {{ form.get("delete_ach_image") }}

                {% else %}
                    <div id="image-preview" class="no-select"></div>
                    {{ form.get("upload_ach_image") }}
                    {{ form.get("upload_ach_image_overlay") }}
                {% endif %}


            </div>
        </div>
    </fieldset>

    {{ form.get('save') }}

</form>