<!DOCTYPE html>
<html lang="en">
<head>
    {{ get_title() }}
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('img/icons/apple-touch-icon.png') }}">
    {{ stylesheet_link("css/mobile-style.css") }}
    {#{{ stylesheet_link("css/materialize/materialize.css") }}#}

    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="{{ url.get('css/materialize/materialize.css') }}"
          media="screen,projection"/>


    {{ stylesheet_link("js/wysibb/theme/twelfthman-mobile/twelfthman-mobile.css") }}
    {{ assets.outputCss() }}

    {{ assets.outputInlineJs() }}
    {{ assets.outputJs() }}

    {#set by the paginator#}
    {% if rel_next is defined %} {{ rel_next }} {% endif %}
    {% if rel_prev is defined %} {{ rel_prev }} {% endif %}
    <base href="{{ url.get() }}"> {#TODO - Introduce redundancy by checking in config if url.get does not work #}
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>

<!-- Dropdown Structure -->

<nav id="mobile-navbar">
    <div class="nav-wrapper">
        {#<a href="{{ url.get("") }}" class="brand-logo left">Owlsonline</a>#}
        {#<div id='header-logo-mobile'></div>#}
        <img src="{{ url.get("img/logo/logo.png") }}">
        <!-- Dropdown Trigger -->
        <a class="button-collapse right" href="#!" data-activates="slide-out"><i class="material-icons right">
                menu
            </i>
        </a>
        {{ elements.getMobileMenu() }}
    </div>


</nav>

<div id="page-content"> {# Next Level down is index controller view #}
    {{ content() }}


    <footer id="footer">
        {% include "partials" %}
        {% include "partials" %}
    </footer>
</div>
<!--TODO - Javascript goes here-->

{#<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>#}
<script type="text/javascript" src="js/materialize/materialize.js"></script>

<script type="text/javascript">$(document).ready(function () {
        $('.button-collapse').sideNav({
            menuWidth: 150,
            edge: 'right',
            closeOnClick: true,
            draggable: true
        });

    })</script>
</body>

</html>