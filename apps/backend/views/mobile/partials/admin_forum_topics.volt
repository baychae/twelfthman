<table class="table-container">

    <tr class="table-header table-row">
        <th>ID</th>
        <th>Title</th>
        <th>Modified</th>
        <th>Delete</th>
    </tr>

    {% for topic in page.items %}
        <tr class="table-row">
            <td>
                <a href="{{ url.get('admin/forum/topic/') ~ topic.id }}">
                    {{ topic.id }}
                </a>
            </td>
            <td>
                <a href="{{ url.get('admin/forum/topic/') ~ topic.id }}">
                    {{ topic.title }}
                </a>
            </td>
            <td>
                <a href="{{ url.get('admin/forum/topic/') ~ topic.id }}">
                    {{ topic.modified|datedecode }}
                </a>
            </td>
            <td>
                <input id="{{ topic.id }}" class="filled-in" type="checkbox" name="delete_topic[]"
                       value="{{ topic.id }}">
                <label for="{{ topic.id }}"></label>
            </td>
        </tr>
    {% endfor %}

</table>

{{ content() }}