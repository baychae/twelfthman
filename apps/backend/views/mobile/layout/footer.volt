<form id="form" action="{{ url.get("admin/layout/footer") }}" method='post' enctype='multipart/form-data'>
    <table>
        <fieldset>
            <legend>Footer Text {{ flashSession.output() }}</legend>
            {{ form.get("bbcode_field") }}
            {{ form.get("content_html") }}
        </fieldset>
        {{ form.get("Submit") }}
    </table>
</form>