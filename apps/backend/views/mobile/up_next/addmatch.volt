{{ content() }}
<form id="form" method='post' enctype='multipart/form-data'>
    <fieldset>

        <legend><a href="{{ url.get("admin/upnext/matches") }}">Matches</a> >> Add New Match{{ flashSession.output() }}
        </legend>

        <div>
            {{ form.get("home_team_id").getLabel() }}
            {{ form.get("home_team_id") }}
        </div>


        <div>
            {{ form.get("away_team_id").getLabel() }}
            {{ form.get("away_team_id") }}
        </div>

        <div>
            {{ form.get("kickoff_date").getLabel() }}
            {{ form.get("kickoff_date") }}

        </div>
        <div>
            {{ form.get("kickoff_time").getLabel() }}
            {{ form.get("kickoff_time") }}
        </div>
        <div>
            {{ form.get("home_score").getLabel() }}
            {{ form.get("home_score") }}
        </div>
        <div>
            {{ form.get("away_score").getLabel() }}
            {{ form.get("away_score") }}
        </div>

    </fieldset>
    <fieldset>
        <legend>News Article Links</legend>
        <div>
            {{ form.get("preview_article_id").getLabel() }}
            {{ form.get("preview_article_id") }}
        </div>
        <div>
            {{ form.get("report_article_id").getLabel() }}
            {{ form.get("report_article_id") }}
        </div>

    </fieldset>

    {{ form.get("Save") }}


</form>