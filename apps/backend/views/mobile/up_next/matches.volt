{{ content() }}

<table>
    <caption>Matches<span></span>{{ flashSession.output() }}</caption>
    <th colspan=2 class="paginator">
        {{ elements.getPaginator(page, "Matches") }}
    </th>

    <tr>
        <th>Date</th>
        <th class="forty-percent-wide">Home Team</th>
    </tr>

    {% for match in page.items %}
        <tr>
            <td><a href="{{ url.get('admin/upnext/match/') ~ match.id }}"> {{ match.start|datedecode }} </a></td>
            <td><a href="{{ url.get('admin/upnext/match/') ~ match.id }}"> {{ match.getHomeTeam().name }} </a></td>
        </tr>

    {% endfor %}

</table>
