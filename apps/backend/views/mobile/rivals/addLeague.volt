{{ content() }}

<form id="form" method='post'>
    <fieldset>
        <legend><a href="{{ url.get("/admin/rivals") }}">Rivals</a> -> Add League {{ flashSession.output() }}</legend>
        <div>
            {{ form.get("name").getLabel() }}
            {{ form.get("name") }}
        </div>
        <div>
            {{ form.get("position").getLabel() }}
            {{ form.get("position") }}
        </div>
    </fieldset>


    {{ form.get("Update") }}
</form>