<form id="form" method='post' enctype="multipart/form-data">
    <fieldset>
        <legend><a href="{{ url.get("/admin/rivals/teams") }}">Teams</a> >>
            {% if team.name is defined %}
                {{ team.name }}
            {% else %}
                Add Team
            {% endif %}{{ flashSession.output() }}
        </legend>
        <div>
            {{ form.get("name").getLabel() }}
            {{ form.get("name") }}
        </div>
        <div>
            {{ form.get("FlagFile").getLabel() }}
            <div class="image-toolset">
                {% if team.image_name is defined %}
                    <img class="image-toolset-img" src={{ url.get("img/teams/" ~ team.image_name) }}>
                    <input id="delete-image" class="form-button" name="action" value="Delete Flag" type="submit">
                {% else %}
                    <div id="image-preview" class="no-select"></div>
                    {{ form.get("FlagFile") }}
                    {{ form.get("UpdateFlag") }}
                {% endif %}
            </div>
        </div>
        <div>
            {{ form.get("nickname").getLabel() }}
            {{ form.get("nickname") }}
        </div>
        <div>
            {{ form.get("league_id").getLabel() }}
            {{ form.get("league_id") }}
        </div>
        <div>
            {{ form.get("website").getLabel() }}
            {{ form.get("website") }}
        </div>

    </fieldset>


    {{ form.get("Update") }}
    {{ form.get("Delete") }}
</form>