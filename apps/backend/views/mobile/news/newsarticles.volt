{{ content() }}

<div class="page-content-container">
    <h2 class="page-section-header">Articles</h2>
    <table class="table-container">
        <th colspan=2 class="paginator">
            {{ elements.getPaginator(page, "News Articles") }}
        </th>
        <tr class="table-header">
            <th class="table-header-cell">Title</th>
            <th class="table-header-cell">Date</th>
        </tr>

        {% for article in page.items %}
            <tr class="table-row">
                <td>
                    <a href="{{ url.get('admin/news/modifynewsarticle/' ~ article.id) }}"> {{ article.title }} </a> {{ flashSession.output() }}
                </td>
                <td>
                    <a href="{{ url.get('admin/news/modifynewsarticle/' ~ article.id) }}"> {{ article.created|ukDate }} </a>
                </td>
            </tr>
        {% endfor %}
    </table>

</div>
