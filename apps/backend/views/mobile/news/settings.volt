{{ content() }}

<form method='post' enctype='multipart/form-data'>
    <fieldset>
        <legend>News Settings <b>{{ flashSession.output() }}</b></legend>


        <div>
            <label>Max Blurbs: </label>
            {{ form.get("maxBlurbs") }}
        </div>

        <div>
            <label>Default Image:</label>

            <div style="display: inline-block; width: 300px;">
                {% if image_url is defined %}
                    <div id="image-preview" class="no-select">
                        <img style="display: block; width: 300px;" src={{ image_url }}>
                    </div>
                    {{ form.get("delete_image") }}
                {% endif %}

                {% if image_url is not defined %}
                    <div id="image-preview" class="no-select"></div>
                    {{ form.get("upload_image_file") }}
                    {{ form.get("upload_image") }}

                {% endif %}
            </div>

        </div>

        {{ form.get("Submit") }}


    </fieldset>
</form>