<form id='form' method='post'>
    <fieldset class="page-section form-section">
        <legend class="page-section-header">Add News Group</legend>

        <div>
            {{ form.getLabel("Name") }}
            {{ form.get("Name") }}
        </div>

    </fieldset>
    {{ form.get("Submit") }}
</form>