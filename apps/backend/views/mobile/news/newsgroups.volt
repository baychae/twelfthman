<div class="page-content-container">
    <h2 class="page-section-header">Groups {% if flashSession.has("success") %}
            <span class="caption-flash"><b>{{ flashSession.getMessages()["success"][0] }}</b></span>{% endif %}
    </h2>
    <table>
    {% for news_group in news_groups %}
        <tr class="table-row">
            <td><a href="{{ url.get( 'admin/news/modifynewsgroup/' ~ news_group.link ) }}">{{ news_group.label }}</a>
            </td>
        </tr>
    {% endfor %}
</table>
</div>