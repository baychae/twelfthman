{{ content () }}

<form id="form" method='post' enctype='multipart/form-data'>
    <table>
        <fieldset class="page-section form-section">
            {% if form.get("title").getValue() == null %}
                <legend class="page-section-header">Add News Article <b>{{ flashSession.output() }}</b></legend>
            {% else %}
                <legend class="page-section-header">Modify News Article <b>{{ flashSession.output() }}</b></legend>
            {% endif %}

            <div>
                {{ form.get("title").getLabel() }}
                {{ form.get("title") }}
            </div>
            <div>

                {{ form.get("Upload New Image File").getLabel() }}
                <div class="image-toolset">

                    {% if image_embed_code is defined %}
                        <div id="image-preview"> {{ image_embed_code }} </div>
                        <button id="embed-new-image" type='button' name="image_action" class="form-button">Update Embed
                            Image
                        </button>
                        <button id="embed-image-delete" name="image_action" value="Delete Embed Image" type="button"
                                class="form-button">Delete Embed Image
                        </button>
                    {% endif %}
                    <dialog id="embed-image-dialog"> {# Embed New Image Dialogue #}
                        <div>
                            <h3>Embed This Image</h3>
                            {{ form.get("image_embed") }}
                            <div>
                                <button id="embed-image-cancel" class="embed-image-button label-as-button"
                                        type="button">Cancel
                                </button>
                                <button id="embed-image-ok" class="embed-image-button label-as-button" type="button">
                                    Ok
                                </button>
                            </div>
                        </div>
                    </dialog>
                    {% if image_url is defined %}
                        <img id="image-preview" class="image-toolset-img" src={{ image_url }}>
                        {{ form.get("Upload New Image File") }}
                        {{ form.get("Update Image") }}
                        <button id="delete-news-image" name="image_action" value="Delete Upload Image"
                                type="button" class="form-button">Delete Upload Image
                        </button>

                    {% endif %}

                    {% if image_embed_code is not defined and image_url is not defined %}
                        <div id="image-preview" class="no-select"></div>
                        {{ form.get("Upload New Image File") }}
                        {{ form.get("Upload New Image") }}
                        <button id="embed-new-image" type='button' class="form-button">Embed New Image</button>

                    {% endif %}

                </div>

            </div>

            <div>
                {{ form.get("image_caption").getLabel() }}
                {{ form.get("image_caption") }}
            </div>
            <div>
                {{ form.get("news_group_id").getLabel() }}
                {{ form.get("news_group_id") }}
            </div>
            <div>
                {{ form.get("publish").getLabel() }}
                {{ form.get("publish") }}
            </div>

            <div>
                {{ form.get("user_id").getLabel() }}
                {{ form.get("user_id") }}
            </div>

        </fieldset>

        <fieldset class="page-section form-section">
            {{ form.get("bbcode_field").getLabel() }}
            {{ form.get("bbcode_field") }}
            {{ form.get("content_html") }}
        </fieldset>
        {{ form.get("Submit") }}
    </table>


</form>




