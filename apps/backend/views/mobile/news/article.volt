{{ content() }}
<nav id="news-main-nav-horizontal"> {{ elements.buildVerticalNav(news_groups) }} </nav>
<main>
    <article itemprop="mainEntity" itemscope itemtype="http://schema.org/NewsArticle">
        <h1 itemprop="headline">{{ article.title }}</h1>
        <span itemprop="articleSection" hidden>{{ article.newsGroup.label }}</span>
        <div itemprop="articleBody">
            {% if art_url is defined %}

                {% if article.image_caption|length > 0 %}
                    <div class="article-feature-art-caption">{{ article.image_caption }}</div>
                    <img class="article-feature-art" src={{ art_url }} title="{{ article.image_caption }}"
                         alt="{{ article.image_caption }}">
                {% else %}
                    <img class="article-feature-art" src={{ art_url }}>
                {% endif %}

            {% elseif art_embed is defined %}
                <div class="article-feature-art"> {{ art_embed }} </div>
            {% endif %}
            {{ article.content_html }}
        </div>
    </article>
</main>