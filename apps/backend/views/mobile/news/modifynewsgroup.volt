{{ flashSession.output() }}
<form id="form" method='post' enctype='multipart/form-data'>

    <fieldset class="page-section form-section">
        <legend class="page-section-header">Modify News Group</legend>
        <div>
            {{ form.get("Name").getLabel() }}
            {{ form.render("Name") }}
        </div>

        {% if image_location_url is not true %}
            <div>
                {{ form.get("ImageFile").getLabel() }}
                <img id="img-admin-newsgroup">
                {{ form.render("ImageFile") }}
            </div>
        {% else %}
            <div>
                <label>Image</label>
                <img id="img-admin-newsgroup" src="{{ image_location_url }}">

                {% if view.getVar("UpdatingImage") %}
                    {{ form.render("ImageFile") }}
                    {{ form.render("CancelImage") }}
                {% else %}
                    {{ form.get("UpdateImage") }}
                    {{ form.get("DeleteImage") }}
                {% endif %}
            </div>
        {% endif %}

        <div>
            {{ form.getLabel("Priority") }}
            {{ form.get("Priority") }}
        </div>


    </fieldset>
    {{ form.get("Update") }}
    {{ form.get("Delete") }}

</form> 