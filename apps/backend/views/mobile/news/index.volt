{{ content() }}

<div class="table">
    <nav class="nav-vertical">   {{ elements.buildVerticalNav(news_groups) }} </nav>

    <section class="container">
        <h1>Sheffield Wednesday News</h1>
        {% include "partials/news_articles.volt" %}
    </section>

    <aside class="news-sidebar-right">#get right side bar</aside>
</div>