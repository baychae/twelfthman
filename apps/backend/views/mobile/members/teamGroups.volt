{{ content() }}

<table>
    <caption>Team Groups <b>{{ flashSession.output() }}</b></caption>
    <div class="paginator">
        {{ elements.getPaginator(page, "Team Groups") }}
    </div>
    {#<tr>#}
    <th>Name</th>
    <th>Pic</th>

    {% for group in page.items %}
        <tr>
            <td>
                <a href="{{ url.get('/admin/members/teamgroup/edit/') ~ group.id }}">{{ group.name }}</a>
                {{ flashSession.output() }}
            </td>
            <td>
                <a href="{{ url.get('/admin/members/teamgroup/edit/') ~ group.id }}">
                    {% if group.image_name is not empty %}
                        <img class="team"
                             src="{{ url.get( config.common.teams.image.dir ~ group.image_name ) }}">
                    {% endif %}
                </a>
            </td>
        </tr>
    {% endfor %}

</table>