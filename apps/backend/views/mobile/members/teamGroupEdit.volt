{{ content () }}

<form id="form" method='post' enctype='multipart/form-data'>
    <table>
        <fieldset>
            <legend>Edit Group<b>{{ flashSession.output() }}</b></legend>

            <div>
                {{ form.getLabel("name") }}
                {{ form.get("name") }}
            </div>
            <div>
                {{ form.getLabel("upload_image_file") }}
                <div class="image-toolset">
                    {% if image_name is not empty %}
                        <img id="image-preview" class="image-toolset-img team"
                             src={{ url.get( config.common.teams.image.dir ~ image_name ) }}>
                    {% else %}
                        <div id="image-preview" class="no-select"></div>
                    {% endif %}

                    {{ form.get("upload_image_file") }}
                    {{ form.get("update_image") }}
                    {{ form.get("delete_image") }}
                </div>
            </div>
        </fieldset>
        {{ form.get("update_group") }}
        {{ form.get("delete_group") }}
    </table>


</form>

