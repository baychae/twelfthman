{{ content() }}

<div id="form">
    <fieldset class="page-section form-section">
        <legend class="page-section-header">Flagged Post <b>{{ flashSession.output() }}</b></legend>
        <div>{{ flagged_post.getForumTopicPost().content_bb|parsebbtohtml }}</div>
    </fieldset>
    <fieldset class="page-section form-section">
        <legend class="page-section-header">Flagged Reason</legend>
        <div>{{ flagged_post.content_bb|parsebbtohtml }}</div>
    </fieldset>
    <form method="post">
        <input type="submit" class="form-button" value="Go to Post" name="action"></input>
        <input type="submit" class="form-button" value="Remove Flag" name="action"></input>
    </form>


</div>