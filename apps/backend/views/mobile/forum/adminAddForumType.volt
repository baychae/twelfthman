<form id="form" method='post' enctype='multipart/form-data'>
    <fieldset class="page-section form-section">
        <legend class="page-section-header">Add New Forum Type</legend>

        <div>
            {{ form.get("title").getLabel() }}
            {{ form.get("title") }}
        </div>

        <div>
            {{ form.get("display_order").getLabel() }}
            {{ form.get("display_order") }}
        </div>

    </fieldset>

    {{ form.get("addType") }}

</form>


{{ content() }}