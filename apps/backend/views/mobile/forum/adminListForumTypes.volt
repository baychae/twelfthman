{{ content() }}

<div class="page-content-container">
    <h2 class="page-section-header">Forum Types <b>{{ flashSession.output() }}</b></h2>
    <table>
        <th colspan=2 class="paginator">
            {{ elements.getPaginator(page, "Forum Types") }}
        </th>
        <tr>
            <th>Title</th>
            <th class="forty-percent-wide">Created Date</th>
        </tr>

        {% for forum_type in page.items %}
            <tr class="table-row">
                <td>
                    <a href="../modifyforumtype/{{ forum_type.id }}"> {{ forum_type.title }} </a> {{ flashSession.output() }}
                </td>
                <td><a href="../modifyforumtype/{{ forum_type.id }}"> {{ forum_type.created|datedecode }} </a></td>
            </tr>
        {% endfor %}
    </table>
</div>
