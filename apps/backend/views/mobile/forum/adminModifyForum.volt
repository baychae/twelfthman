{{ content() }}
{% do assets.addJs("js/forum-admin-materialize.js") %}
<form action="{{ url.get( router.getReWriteUri() ) }}" id="form" method='post' enctype='multipart/form-data'>
    <fieldset class="page-section form-section">

        {% if form.get("title").getValue() is empty %}
            <legend class="page-section-header">Add Forum</legend>
        {% else %}
            <legend class="page-section-header"><a href="{{ url.get('admin/forums') }}">Forums</a> >> Edit
                Forum {{ flashSession.output() }}
            </legend>
        {% endif %}

        <div>
            {{ form.get("title").getLabel() }}
            {{ form.get("title") }}
        </div>

        <div>
            {{ form.get("description").getLabel() }}
            {{ form.get("description") }} <span class="note">*nb max 250 characters.</span>
        </div>

        <div>
            {{ form.get("forum_type_id").getLabel() }}
            {{ form.get("forum_type_id") }}
        </div>

        <div>
            {{ form.get("view_type").getLabel() }}
            {{ form.get("view_type") }}
        </div>

    </fieldset>
    <div class="page-content-container">
        <h2 class="page-section-header">{{ forum.title }} Topics</h2>
        <div class="paginator">{{ elements.getPaginator(page, "Topics") }}</div>

        {% include "partials/admin_forum_topics.volt" %}
        <BR>
        {{ form.get("Update") }}
        {{ form.get("Delete") }}
        {{ form.get("DeleteTopics") }}
    </div>
</form>

