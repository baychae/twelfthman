<?php

namespace Multiple\Backend\Controllers;

use Phalcon\Tag as Tag,
    Phalcon\Paginator\Adapter\Model as PaginatorModel;

class ForumController extends \Multiple\Backend\Controllers\ControllerBase
{

    public function initialize()
    {
        Tag::setTitle('Admin Forums');
    }

    public function adminForumsAction($page_requested = null)
    {
        //Using admin layout
        
        $this->view->setTemplateBefore('admin');
        $this->assets->addJs("js/jquery.cookie.js");
        $this->assets->addJs("js/jquery.paginater.js");

        $current_uri = $this->router->getRewriteUri();

        if ($current_uri == '/admin/forum/listforums') {
            return $this->response->redirect("/admin/forum/1");
        }

        $data = \Common\Models\Forum::find(
            [
                    "status != 'D' ",
                    "order" => "id desc"
            ]
        );

        $paginated_page = \Common\Library\ModelPaginator::paginate($this, $data);
        $this->view->setVar('page', $paginated_page);
    }

    public function adminListForumTypesAction($page_requested = 1)
    {
        //Using admin layout
        $this->view->setTemplateBefore('admin');

        $this->assets->addJs("js/jquery.cookie.js");
        $this->assets->addJs("js/jquery.paginater.js");

        //Handle for if cookie not there or blank then sanitize
        $cookie_value = $this->cookies->get('pageSize')->getValue() ? $this->cookies->get('pageSize')->getValue() : 15;
        $sanitized_limit = $this->filter->sanitize($cookie_value, "int");
        $limit = $sanitized_limit > 200 ? 200 : $sanitized_limit; //Limit to avoid cookie exploit

        $forum_type = \Common\Models\ForumType::find(
            [
                                "order" => "id desc"
            ]
        );
        $paginated_page = \Common\Library\ModelPaginator::paginate($this, $forum_type);
        $this->view->setVar('page', $paginated_page);
    }

    public function adminAddForumAction()
    {
        $this->view->setTemplateBefore('admin');

        $form = new \Multiple\Backend\Forms\Forum\ForumForm();
        $this->view->setVar('form', $form);

        if ($this->request->isPost()) {
            $forum_type_id = $this->request->getPost("forum_type_id");

            //add forum to model
            $forum = new \Common\Models\Forum();
            $forum_type = \Common\Models\ForumType::findFirstById($forum_type_id);

            $forum->title = $this->request->getPost("title");
            $forum->description = $this->request->getPost("description");
            $forum->forumType = $forum_type;
            $forum->view_type = $this->request->getPost("view_type");
            
            if ($forum->save() == false) {
                foreach ($forum->getMessages() as $message) {
                    $this->flashSession->warning("MESSAGE: " . $message);
                }
            } else {
                $this->flashSession->success("Forum " . $forum->title . " successfully saved.");
            }

            //Redirect to list forums
            return $this->response->redirect("admin/forums");
        }
    }

    public function adminModifyForumAction()
    {
        $this->view->setTemplateBefore('admin');

        $this->assets->addJs("js/jquery.cookie.js");
        $this->assets->addJs("js/jquery.paginater.js");

        $forum_id = $this->dispatcher->getParam("forum");
        $current_page = $this->dispatcher->getParam("page");

        $forum = \Common\Models\Forum::findFirstById($forum_id);

        if ($forum->status == "D") {//Do not allow access to SoftDelete Forum
            return $this->response->redirect("admin/forums");
        }

        $form = new \Multiple\Backend\Forms\Forum\ForumForm($forum);
        $this->view->setVar('form', $form);

        if ($this->request->isPost()) {
            $request_action = $this->request->getPost("action");

            switch ($request_action) {
                case "Update Forum":
                    $forum_type = \Common\Models\ForumType::findFirstById($this->request->getPost("forum_type_id"));

                    $forum->title = $this->request->getPost("title");
                    $forum->description = $this->request->getPost("description");
                    $forum->forumType = $forum_type;
                    $forum->view_type = $this->request->getPost("view_type");

                    if ($forum->save() == false) {
                        foreach ($forum->getMessages() as $message) {
                            $this->flashSession->warning("WARNING: " . $message);
                        }
                        return $this->response->redirect("admin/forum/" . $forum_id . "/1");
                    } else {
                        $this->flashSession->success("Forum " . $forum->title . " has been successfully updated.");
                        return $this->response->redirect("admin/forum/" . $forum_id . "/1");
                    }
                    break;

                case "Delete Forum":
                    $manager = new \Phalcon\Mvc\Model\Transaction\Manager();
                    $transaction = $manager->get();
                    $forum = \Common\Models\Forum::findFirstById($forum_id);

                    foreach ($forum->getForumTopic() as $topic) {
                        foreach ($topic->getForumTopicPost() as $ftp) {
                            $ftp->delete();
                        }

                        $topic->delete();
                    }

                    $forum->delete();

                    return $this->response->redirect("admin/forum/" . $forum_id . "/1");
                    break;

                case "Delete Selected Topics":
                    break;
            }

            //Redirect to list forums
            return $this->response->redirect("admin/forum/" . $forum_id . "/1");
        }

        //Paginator
        //Handle for if cookie not there or blank then sanitize
        $cookie_value = $this->cookies->get('pageSize')->getValue() ? $this->cookies->get('pageSize')->getValue() : 15;
        $sanitized_limit = $this->filter->sanitize($cookie_value, "int");
        $limit = $sanitized_limit > 200 ? 200 : $sanitized_limit; //Limit to avoid cookie exploit

        $topics = \Common\Models\ForumTopic::find([
                    "forum_id = " . $forum_id . " AND status != 'D' ",
                    "order" => "modified desc"
        ]);
        
        $paginator = new PaginatorModel(
            [
            "data" => $topics,
            "limit" => $limit,
            "page" => $current_page
            ]
        );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $this->view->setVar("forum", $forum);
        $this->view->setVar("page", $page);
    }

    public function adminForumTopicAction()
    {
        $this->view->setTemplateBefore('admin');
        $topic_id = $this->dispatcher->getParam("topic_id", "int");
        $topic = \Common\Models\ForumTopic::findFirstById($topic_id);
        $available_forums = \Common\Models\Forum::find();

        $form = new \Multiple\Backend\Forms\Forum\ForumTopicForm($topic);

        $this->view->setVar("available_forums", $available_forums);
        $this->view->setVar("topic", $topic);
        $this->view->setVar("form", $form);
    }

    public function adminSaveForumTopicAction()
    {
        if ($this->request->getPost("action") == "Update") {
            $topic_id = $this->dispatcher->getParam("topic_id", "int");
            $topic = \Common\Models\ForumTopic::findFirstById($topic_id);

            $topic->title = $this->request->getPost("title", "string");
            $topic->description = $this->request->getPost("description", "string");
            $topic->forum_id = $this->request->getPost("forum_id", "int");
            $topic->view_type = $this->request->getPost("view_type", "string");
            $topic->pinned = $this->request->getPost("pinned") == true ? 1 : 0;

            if (!$topic->save()) {
                foreach ($topic->getMessages() as $message) {
                    $this->flashSession->error("Saving the topic failed " . $message);
                }
                $this->response->redirect("admin/forum/topic/" . $topic->id);
            } else {
                $this->flashSession->success("Topic saved "
                    . $this->filter->sanitize(\date('Y:m:d H:i:s'), "datedecode") . "!");
                $this->response->redirect("admin/forum/topic/" . $topic->id);
            }
        }
    }

    public function adminAddForumTypeAction()
    {
        $this->view->setTemplateBefore('admin');

        $form = new \Multiple\Backend\Forms\Forum\ForumTypeForm();
        $this->view->setVar('form', $form);

        if ($this->request->isPost()) {
            $forum_type = new ForumType();
            $forum_type->title = $this->request->getPost("title");
            $forum_type->order = $this->request->getPost("order");
            $forum_type->save();

            if ($forum_type->save() == false) {
                foreach ($forum_type->getMessages() as $message) {
                    $this->flashSession->warning("MESSAGE: " . $message);
                }
            } else {
                $this->flashSession->success("Forum " . $forum_type->title . " successfully saved.");
            }

            //Redirect to list forums
            return $this->response->redirect("admin/forum/listforumtypes");
        }
    }

    public function adminModifyForumTypeAction($forum_type_id)
    {
        $this->view->setTemplateBefore('admin');

        $forum_type = \Common\Models\ForumType::findFirstById($forum_type_id);
        $form = new \Multiple\Backend\Forms\Forum\ForumTypeForm($forum_type);

        $this->view->setVar('form', $form);

        if ($this->request->isPost()) {
            if ($this->request->getPost("action") == "Update") {
                $forum_type->title = $this->request->getPost("title");
                $forum_type->order = $this->request->getPost("order");
                $forum_type->save();

                if ($forum_type->save() == false) {
                    foreach ($forum_type->getMessages() as $message) {
                        $this->flashSession->warning("MESSAGE: " . $message);
                    }
                } else {
                    $this->flashSession->success("Forum " . $forum_type->title . " successfully saved.");
                }
            } elseif ($this->request->getPost("action") == "Delete") {
                $forum_type_id = $this->router->getParams()[0];
                $forum_type = \Common\Models\ForumType::findFirstById($forum_type_id);

                if ($forum_type->delete() == false) {
                    foreach ($forum_type->getMessages() as $message) {
                        $this->flashSession->warning("Warning: " . $message);
                    }
                } else {
                    $this->flashSession->success("Forum Type " . $forum_type->title . " successfully deleted.");
                }
            }

            //Redirect to list forums
            return $this->response->redirect("admin/forum/listforumtypes");
        }
    }

    public function adminFlaggedPostsAction()
    {
        $this->view->setTemplateBefore('admin');

        $current_page = $this->dispatcher->getParam("page") > 1 ? $this->dispatcher->getParam("page") : 1;

        //Paginator
        //Handle for if cookie not there or blank then sanitize
        $cookie_value = $this->cookies->get('pageSize')->getValue() ? $this->cookies->get('pageSize')->getValue() : 15;
        $sanitized_limit = $this->filter->sanitize($cookie_value, "int");
        $limit = $sanitized_limit > 200 ? 200 : $sanitized_limit; //Limit to avoid cookie exploit

        $this->view->setVar("deleted", false);
        $flagged_posts = \Common\Models\ForumTopicPostsFlagged::find("status != 'D'");


        if ($this->request->isPost()) {
            if ($this->request->getPost("action") == "Show Deleted Flags") {
                $flagged_posts = \Common\Models\ForumTopicPostsFlagged::find();
                $this->session->set("show-deleted-flags", true);
                $this->view->setVar("deleted", true);
            } elseif ($this->request->getPost("action") == "Hide Deleted Flags") {
                    $this->view->setVar("deleted", false);
                    $this->session->set("show-deleted-flags", false);
                    $flagged_posts = \Common\Models\ForumTopicPostsFlagged::find("status != 'D'");
            }

        } else {
            $show_deleted = $this->session->get("show-deleted-flags");

            if ($show_deleted) {
                $flagged_posts = \Common\Models\ForumTopicPostsFlagged::find();
                $this->view->setVar("deleted", true);
            } else {
                $this->view->setVar("deleted", false);
                $flagged_posts = \Common\Models\ForumTopicPostsFlagged::find("status != 'D'");
            }

        }

        $paginator = new PaginatorModel(
            [
                "data" => $flagged_posts,
                "limit" => $limit,
                "page" => $current_page
            ]
        );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $this->view->setVar('page', $page);
    }

    public function adminFlaggedPostAction()
    {
        $this->view->setTemplateBefore('admin');
        $flagged_post_id = $this->dispatcher->getParam('flagged_post_id');
        $post_flag = \Common\Models\ForumTopicPostsFlagged::findFirst($flagged_post_id);
        $original_post = $post_flag->getForumTopicPost();
        $this->view->setVar("flagged_post", $post_flag);

        if ($this->request->isPost()) {
            if ($this->request->getPost("action") == "Go to Post") {
                $page_size = $this->cookies->get('pageSize')->getValue();
                $redirect_url = $original_post->getUrl($page_size);
                $this->response->redirect($redirect_url);

            } elseif ($this->request->getPost("action") == "Remove Flag") {
                if ($post_flag->delete() == false) {
                    foreach ($post_flag->getMessages() as $message) {
                        $this->flashSession->warning("Warning: " . $message);
                    }
                } else {
                    $this->flashSession->success("Flag removed.");
                    $this->response->redirect("admin/forum/flaggedposts");
                }

            }
        }
    }

    public function adminSettingsAction()
    {
        $this->view->setTemplateBefore('admin');

        $form = new \Multiple\Backend\Forms\Forum\ForumSettings();
        $this->view->setVar("form", $form);

        if ($this->request->isPost() == true) {
            if ($this->request->getPost("action") == "Reset") {
                $forum_topic = \Common\Models\ForumTopic::find();

                foreach ($forum_topic as $topic) {
                    $forum_stats_replies = \Common\Models\ForumTopicPostStats::findFirst([
                        ["topic_id" => intval($topic->id)]
                    ]);

                    if (isset($forum_stats_replies->topic_id) == false) {
                        echo "NOT MAKING NEW ONE FOUND!!!!<BR>";
                        $forum_stats_replies = new \Common\Models\ForumTopicPostStats();
                    }

                    $forum_stats_replies->forum_id = $topic->forum_id;
                    $forum_stats_replies->topic_id = $topic->id;
                    $forum_stats_replies->post_id = $topic->getForumTopicPost()->getLast()->id;
                    $forum_stats_replies->post_count = $topic->getForumTopicPost()->count();


                    $forum_stats_replies->save();
                }
            }
        }
    }
}
