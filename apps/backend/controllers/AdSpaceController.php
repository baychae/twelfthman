<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 17/07/2017
 * Time: 16:43
 */

namespace Multiple\Backend\Controllers;

class AdSpaceController extends \Multiple\Backend\Controllers\ControllerBase
{
    public function initialize()
    {
        \Phalcon\Tag::setTitle("Admin Ad Space");

    }

    public function indexAction()
    {
        $this->view->setTemplateBefore('admin');

        $ad_space_dir_path = constant('SITE_ROOT') . 'apps/frontend/views/desktop/partials/adspace/';


        $dir_handle = @opendir($ad_space_dir_path) or $reserror = ('Unable to open ' . $ad_space_dir_path);

        $adsArray = [];

        if (isset($reserror) == FALSE) { //if unable to access direcoty, give error

            while (($file = readdir($dir_handle)) !== false) {
                if ($file != '.' && $file != '..') {
                    $pathinfo = pathinfo($file);

                    if (isset($pathinfo['extension'])) {
                        $ext = strtolower($pathinfo['extension']);
                    } else {
                        $ext = '';
                    }

                    if ($ext == 'volt') { //allow only volt files

                        $file_name = pathinfo($file, PATHINFO_FILENAME);
                        $file_name_sep = preg_replace('/-/', ' ', $file_name);
                        $title = mb_convert_case($file_name_sep, MB_CASE_TITLE, "UTF-8");
                        $file_contents = \file_get_contents($ad_space_dir_path . $file);

                        $array[$file_name]["label"] = $title;
                        $array[$file_name]["id"] = $file_name;
                        $array[$file_name]["code"] = mb_convert_encoding($file_contents, "HTML-ENTITIES", "UTF-8");
                    }

                    array_push($adsArray, $file);
                }
            }

        } else {
//            $this->flashSession->error("Error: " . $reserror);
//            return $this->response->redirect($this->router->getRewriteUri());
        }

        $this->view->setVar('ads', $array);

    }

    public function saveAction()
    {
        $ad_space_dir_path = constant('SITE_ROOT') . 'apps/frontend/views/desktop/partials/adspace/';

        $dir_handle = @opendir($ad_space_dir_path) or $reserror = ('Unable to open ' . $ad_space_dir_path);

        $adsArray = [];

        if (isset($reserror) == FALSE) { //if unable to access direcoty, give error

            while (($file = readdir($dir_handle)) !== false) {
                if ($file != '.' && $file != '..') {
                    $pathinfo = pathinfo($file);

                    if (isset($pathinfo['extension'])) {
                        $ext = strtolower($pathinfo['extension']);
                    } else {
                        $ext = '';
                    }

                    if ($ext == 'volt') { //allow only volt files

                        $file_name = pathinfo($file, PATHINFO_FILENAME);
                        $file_name_sep = preg_replace('/-/', ' ', $file_name);
                        $title = mb_convert_case($file_name_sep, MB_CASE_TITLE, "UTF-8");

                        $data = $this->request->getPost($file_name);
                        $file_contents = \file_put_contents($ad_space_dir_path . $file, $data);

                        $array[$file_name]["label"] = $title;
                        $array[$file_name]["id"] = $file_name;
                        $array[$file_name]["code"] = mb_convert_encoding($file_contents, "HTML-ENTITIES", "UTF-8");
                    }

                    array_push($adsArray, $file);
                }
            }

        } else {
            $this->flashSession->error("Error: " . $reserror);
            return $this->response->redirect("admin/adspace");
        }

        $this->flashSession->success("Adds saved!");
        $this->response->redirect("admin/adspace");

    }


}
