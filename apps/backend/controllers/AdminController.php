<?php 

namespace Multiple\Backend\Controllers;

class AdminController extends \Multiple\Backend\Controllers\ControllerBase
{
    public function initialize(){
        $action = ucwords($this->dispatcher->getActionName());
        $title_stripped = str_replace('_', ' ', $action);
        $title_capped = ucwords($title_stripped);
        
        \Phalcon\Tag::setTitle('Admin - '.$title_capped);
    }
   
    public function indexAction()
    {
        
    }
    
}
