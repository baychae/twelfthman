<?php

namespace Multiple\Backend\Controllers;

class RivalsController extends \Multiple\Backend\Controllers\ControllerBase
{

    public function initialize()
    {
        \Phalcon\Tag::setTitle('Admin Rivals');

    }

    public function leaguesAction()
    {
        $this->view->setTemplateBefore('admin');

        $this->assets->addJs("js/jquery.cookie.js");
        $this->assets->addJs("js/jquery.paginater.js");

        $leagues = \Common\Models\League::find(["order" => "position"]); // Get the leagues

        $this->view->setVar('leagues', $leagues);
    }

    public function modifyLeagueAction()
    {
        $this->view->setTemplateBefore('admin');

        $league_id = $this->dispatcher->getParam("league_id", "int");
        $league = \Common\Models\League::findFirst($league_id);
        $form = new \Multiple\Backend\Forms\Rivals\LeagueForm($league);

        $this->view->setVar("league_name", $league->name);
        $this->view->setVar("form", $form);
    }

    public function updateLeagueAction()
    {
        $league_id = $this->dispatcher->getParam("league_id", "int");

        $league = \Common\Models\League::findFirst(
            [
                "conditions" => "id = ?1",
                "bind" => [
                    1 => $league_id
                ]
            ]
        );

        if ($this->request->getPost("action") == "Update League") {

            $post = $this->request->getPost();

            $result = $league->save(
                $post,
                [
                    "name",
                    "position"
                ]
            );

            if ($result === false) {
                $messages = $league->getMessages();

                foreach ($messages as $message) {
                    echo $this->flashSession->error("Could not update the League" . $message);
                }

                $this->response->redirect("admin/rivals/" . $league_id);
            } else {
                $this->flashSession->success("League Updated!");
                $this->response->redirect("admin/rivals/" . $league_id);
            }
        } else if ($this->request->getPost("action") == "Delete League") {


            if ($league->delete() == false) {
                $messages = $league->getMessages();

                foreach ($messages as $message) {
                    echo $this->flashSession->error("Could not delete the league" . $message);
                }

                $this->response->redirect("admin/rivals/" . $league_id);
            } else {
                $this->flashSession->success("League Deleted!");
                $this->response->redirect("admin/rivals/leagues");
            }

        }

    }

    public function addLeagueAction()
    {
        $this->view->setTemplateBefore('admin');
        $form = new \Multiple\Backend\Forms\Rivals\LeagueForm();

        if ($this->request->isPost() === true) {

            $league = new \Common\Models\League();

            $result = $league->save($_POST, ['name', 'position']);

            if ($result === false) {
                $messages = $league->getMessages();

                foreach ($messages as $message) {
                    echo $this->flashSession->error("Could not add the league " . $message);
                }

                $this->response->redirect("admin/rivals/addleague");
            } else {
                $this->flashSession->success("League Added!");
                $this->response->redirect("admin/rivals/leagues");
            }

        }

        $this->view->setVar("form", $form);
    }

    public function deleteLeagueAction()
    {

    }

    public function teamsAction()
    {
        $this->assets->addJs("js/jquery.cookie.js");
        $this->assets->addJs("js/jquery.paginater.js");

        $this->view->setTemplateBefore('admin');

        $teams = \Common\Models\Team::find(["order" => "name"]);

        $paginated_page = \Common\Library\ModelPaginator::paginate($this, $teams);
        $this->view->setVar('page', $paginated_page);

    }

    public function teamAction()
    {
        $this->assets->addJs("js/wysibb-image-helper.js");
        $this->assets->addJs("js/wysibb-ihelper-options-team.js");

        $team_id = $this->dispatcher->getParam("team_id");

        if ($this->request->isPost() == true) {

            switch ($this->request->getPost("action")) {
                case "Update Team":
                    $this->dispatcher->forward(["action" => "saveTeam", "team_id" => $team_id]);
                    break;
                case "Delete Team":
                    $this->dispatcher->forward(["action" => "deleteTeam", "team_id" => $team_id]);
                    break;
                case "Delete Flag":
                    $this->dispatcher->forward(["action" => "deleteTeamFlag", "team_id" => $team_id]);
                    break;
            }

        }

        $this->view->setTemplateBefore('admin');


        $team = \Common\Models\Team::findFirst($team_id);
        $form = new \Multiple\Backend\Forms\Rivals\TeamForm($team);


        $this->view->setVar("team", $team);
        $this->view->setVar("form", $form);
    }

    public function saveTeamAction()
    {
        $team_id = $this->dispatcher->getParam("team_id");
        $request_has_files = $this->request->hasFiles(true);

        $this->flashSession->success($sFlag_file);

        $to_teams_url = "/admin/rivals/teams";
        $config = $this->config;

        $sTeam_name = $this->request->getPost("name", "string");
        $iTeam_league_id = $this->request->getPost("league_id", "int");
        $sNickname = $this->request->getPost("nickname", "string");
        $sWebsiteUrl = $this->request->getPost("website");

        $save_params = [
            "name" => $sTeam_name,
            "league_id" => $iTeam_league_id,
            "nickname" => $sNickname,
            "website" => $sWebsiteUrl
        ];

        if (!isset($team_id)) {
            $team = new \Common\Models\Team($save_params);
        } else {
            $team = \Common\Models\Team::findFirst($team_id);
        }

        if (isset($team->name) == true && $request_has_files == true) {

            $allowed_types = $config->common->team->image->allowed_types->toArray();
            $max_file_size = $config->common->team->image->max_file_size;
            $image_dir = $config->common->team->image->dir;

            $files = $this->request->getUploadedFiles(true);
            $imageType = $files[0]->getRealType();

            if (!in_array($imageType, $allowed_types)) {

                $this->flashSession->error("You may only upload images of type: " . implode(" ", $allowed_types));
                return $this->response->redirect($to_teams_url);
            } elseif ($max_file_size < $files[0]->getSize()) {

                $this->flashSession->error("File size must be under: " . $max_file_size . " Bytes File is " . $files[0]->getSize() . " Bytes");
                return $this->response->redirect($to_teams_url);
            } else {

                if (!empty($sTeam_name)) {

                    $file_type = preg_replace("/image\//", "", $imageType);

                    $new_image_name = $sTeam_name . "." . $file_type;

                    if (!empty($team->image_name)) {// Delete old file and replace

                        $existing_file_location = $image_dir . $team->image_name;
                        $deleted = unlink($existing_file_location);

                        if (!$deleted) {
                            $this->flashSession("Could not delete old file. Please contact admin!");
                            return $this->response->redirect($to_teams_url);
                        }

                        $image = $files[0];
                        $image->moveTo($image_dir . $new_image_name);
                        $team->image_name = $new_image_name;
                    } else {//Brand new image
                        $image = $files[0];
                        $image->moveTo($image_dir . $new_image_name);
                        $team->image_name = $new_image_name;
                    }
                }
            }
        }

        if ($team->save($save_params) == false) {
            foreach ($team->getMessages() as $message) {
                $this->flashSession->error("Could not save the team: " . $message);
            }
            return $this->response->redirect("admin/rivals/teams");
        } else {
            $this->flashSession->success("Team " . $team->name . " saved!");
            return $this->response->redirect("admin/rivals/team/" . $team->id);
        }
    }

    public function addTeamAction()
    {
        if ($this->request->isPost() == true) {

            if ($this->request->getPost("action") == "Update Team") {
                $this->dispatcher->forward([
                    "action" => "saveTeam"
                ]);
            }

        }

        $this->view->setTemplateBefore('admin');
        $this->view->pick("rivals/team");

        $form = new \Multiple\Backend\Forms\Rivals\TeamForm();
        $this->view->setVar("form", $form);
    }

    public function deleteTeamAction()
    {
        $team_id = $this->dispatcher->getParam("team_id");
        $team = \Common\Models\Team::findFirst($team_id);
        $team_name = $team->name;

        if (!$team->delete()) {

            foreach ($team->getMessages() as $message) {
                $this->flashSession->error("Problem deleting the team: " . $message);
            }

            return $this->response->redirect("admin/rivals/teams");

        } else {
            $this->flashSession->success("Deleted team " . $team_name . "!");
            return $this->response->redirect("admin/rivals/teams");
        }

    }

    public function deleteTeamFlagAction()
    {
        $team_id = $this->dispatcher->getParam("team_id");
        $team = \Common\Models\Team::findFirst($team_id);

        $file_location = $this->config->common->team->image->dir . $team->image_name;

        if (file_exists($file_location) == true) {
            $deleted = unlink($file_location);
        }

        if ($deleted == true) {
            $team->image_name = null;
        } else {
            $this->flashSession->error("File was not deleted please alert an admin");
            return $this->response->redirect("admin/rivals/teams");
        }

        if (!$team->update()) {
            foreach ($team->getMessages() as $message) {
                $this->flashSession->error("Could not update team: " . $message);
                return $this->response->redirect("admin/rivals/teams");
            }
        } else {
            $this->flashSession->success("Team " . $team->name . " flag successfully deleted!");
            return $this->response->redirect("admin/rivals/team/" . $team->id);
        }

    }


}