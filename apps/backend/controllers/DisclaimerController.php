<?php
/**
 * Created by PhpStorm.
 * User: baychae@gmail.com
 * Date: 24/07/2017
 * Time: 11:55
 */

namespace Multiple\Backend\Controllers;

class DisclaimerController extends \Multiple\Backend\Controllers\ControllerBase
{
    public function initialize()
    {
        \Phalcon\Tag::setTitle('Admin - Disclaimer');
    }

    public function indexAction()
    {
        $this->view->setTemplateBefore('admin');

        $disclaimer_text = \Common\Models\AdminSiteSection::findFirst("name ='disclaimer'");
        $disclaimer = $disclaimer_text->getAdminSiteSectionData()->getFirst();


        $this->view->setVar("disclaimer_text", $disclaimer->value);
    }

    public function saveAction()
    {
        $disclaimer_text = \Common\Models\AdminSiteSection::findFirst("name ='disclaimer'");
        $disclaimer = $disclaimer_text->getAdminSiteSectionData()->getFirst();

        $disclaimer->value = $this->request->getPost("bbcode_field");

        if ($disclaimer->save()) {
            $this->flashSession->success("Disclaimer saved!");
            return $this->response->redirect($this->router->getRewriteUri());
        } else {
            foreach ($disclaimer->getMessages() as $message) {
                $this->flashSession->error("Error: " . $message);
            }
            return $this->response->redirect($this->router->getRewriteUri());
        }

    }
}