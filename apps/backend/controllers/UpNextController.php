<?php
/**
 * AUTHOR: Richard Sinclair <rich.sinclair@gmail.com>
 * Date: 25/10/2016
 * Time: 16:56
 */

namespace Multiple\Backend\Controllers;

class UpNextController extends \Multiple\Backend\Controllers\ControllerBase
{

    public function initialize()
    {
        \Phalcon\Tag::setTitle("Admin Up Next");
    }

    public function matchesAction()
    {
        $this->view->setTemplateBefore('admin');

        $this->assets->addJs("js/jquery.cookie.js");
        $this->assets->addJs("js/jquery.paginater.js");

        $matches = \Common\Models\Match::find(["order" => "start desc"]);

        $paginated_page = \Common\Library\ModelPaginator::paginate($this, $matches);
        $this->view->setVar('page', $paginated_page);
    }

    public function matchAction()
    {
        $match_id = $this->dispatcher->getParam("match_id", "int");

        if ($this->request->isPost() == true) {

            switch ($this->request->getPost("action")) {
                case "Update Match":
                    $this->dispatcher->forward(["action" => "updateMatch", "team_id" => $match_id]);
                    break;
                case "Delete Match":
                    $this->dispatcher->forward(["action" => "deleteMatch", "team_id" => $match_id]);
                    break;
                case "Add New Match":
                    $this->response->redirect("admin/upnext/addmatch");
                    break;
            }

        }

        $this->view->setTemplateBefore('admin');

        $this->assets->addJs("js/jquery.cookie.js");
        $this->assets->addJs("js/jquery.paginater.js");



        $match = \Common\Models\Match::findFirst($match_id);

        $form = new \Multiple\Backend\Forms\UpNext\MatchForm($match);

        $this->view->setVar("form", $form);
    }

    public function updateMatchAction()
    {
        if ($this->request->isPost()) {
            $match_id = $this->dispatcher->getParam("match_id", "int");

            if (isset($match_id)) {
                $match = \Common\Models\Match::findFirst($match_id);
                $failed_redirect_url = "admin/upnext/match/" . $match_id;
            } else {
                $match = new \Common\Models\Match();
                $failed_redirect_url = "admin/upnext/addmatch";
            }

            $home_team_id = $this->request->getPost("home_team_id", "int");
            $away_team_id = $this->request->getPost("away_team_id", "int");
            $kickoff_date = $this->request->getPost("kickoff_date", "string");
            $kickoff_time = $this->request->getPost("kickoff_time", "string");
            $home_score = $this->request->getPost("home_score", "int");
            $away_score = $this->request->getPost("away_score", "int");
            $preview_article_id = $this->request->getPost("preview_article_id", "int");
            $report_article_id = $this->request->getPost("report_article_id", "int");

            $match->home_team_id = empty($home_team_id) ? null : $home_team_id;
            $match->away_team_id = empty($away_team_id) ? null : $away_team_id;
            $match->kickoff_date = empty($kickoff_date) ? null : $kickoff_date;
            $match->kickoff_time = empty($kickoff_time) ? null : $kickoff_time;
            $match->home_score = empty($home_score) ? 0 : $home_score;
            $match->away_score = empty($away_score) ? 0 : $away_score;
            $match->preview_article_id = empty($preview_article_id) ? null : $preview_article_id;
            $match->report_article_id = empty($report_article_id) ? null : $report_article_id;

            if ($match->save() == false) {

                foreach ($match->getMessages() as $message) {
                    $this->flashSession->error("Could not save the match " . $message);
                    $this->flashSession->error("Could not save the match " . $match->start);
                }

                $this->response->redirect($failed_redirect_url);
            } else {
                $this->flashSession->success("Saved the " . \Common\Library\Helpers::datedecode($match->start) . " match!");
                $this->response->redirect("admin/upnext/matches");
            }
        }
    }

    public function deleteMatchAction()
    {
        if ($this->request->isPost()) {
            $match_id = $this->dispatcher->getParam("match_id", "int");
            $match = \Common\Models\Match::findFirst($match_id);

            if ($match->delete() == false) {

                foreach ($match->getMessages() as $message) {
                    $this->flashSession->error("Could not delete the match " . $message);
                }

                $this->response->redirect("admin/upnext/match/" . $match_id);
            } else {
                $this->flashSession->success("Deleted the " . \Common\Library\Helpers::datedecode($match->start) . " match!");
                $this->response->redirect("admin/upnext/matches");
            }
        }
    }

    public function addmatchAction()
    {
        $this->view->setTemplateBefore('admin');

        $form = new \Multiple\Backend\Forms\UpNext\MatchForm();
        $this->view->setVar("form", $form);

    }

    public function settingsAction()
    {
        $this->view->setTemplateBefore('admin');

        $balance = $this->sms->checkBalance();
        $balance_string = $balance["symbol"] . $balance["balance"];
        $this->view->setVar("sms_balance", $balance_string);

        $settings = new \Common\Models\Entities\UpNextSettings($this);

        $form = new \Multiple\Backend\Forms\UpNext\Settings($settings);
        $this->view->setVar("form", $form);

    }

    public function saveSettingsAction()
    {
        $liveScoreSmsActive = $this->request->getPost("liveScoreSmsActive", "int");
        $liveScoreEndpointSms = $this->request->getPost("liveScoreEndpointSms", "string");
        $liveScoreSmsExpectFrom = $this->request->getPost("liveScoreSmsExpectFrom", "string");

        $settings = new \Common\Models\Entities\UpNextSettings($this);
        $settings->liveScoreSmsActive = $liveScoreSmsActive == "1" ? "1" : "0";
        $settings->liveScoreEndpointSms = $liveScoreEndpointSms;
        $settings->liveScoreSmsExpectFrom = $liveScoreSmsExpectFrom;

        if ($settings->save()) {
            $this->flashSession->success("Settings Updated.");
            return $this->response->redirect("/admin/upnext/settings");
        } else {
            $this->flashSession->warning("Error: Settings Not Update.");
            return $this->response->redirect("/admin/upnext/settings");
        }


    }

}
