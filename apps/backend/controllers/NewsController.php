<?php

namespace Multiple\Backend\Controllers;

use Phalcon\Tag as Tag;

define("ARTICLE_LIMIT", $config->frontend->news->article_limit);
define("TEXT_LIMIT", $config->frontend->news->text_limit);
define("MAX_ICON_IMAGE_SIZE", $config->frontend->news->max_icon_image_size);
define("MAX_ARTICLE_IMAGE_SIZE", $config->frontend->news->max_article_image_size);
define('NEWS_GROUP_IMAGE_DIR', $config->frontend->news->group->image_dir);
define('NEWS_ARTICLE_IMAGE_DIR', $config->frontend->news->article->image_dir);
define('NEWS_ARTICLE_SETTINGS_IMAGE_DIR', $config->frontend->news->article->settings->image_dir);

class NewsController extends \Multiple\Backend\Controllers\ControllerBase
{

    private $_admin_section;
    private $_article_limit;
    private $_text_limit;

    public function initialize() {
        Tag::setTitle('Admin News');
        $this->_article_limit = $this->getArticleLimit();
        $this->_text_limit = $this->getTextLimit();
    }

    private function getArticleLimit() {
        $settings = \Common\Models\AdminSiteSection::getNewsSettings($this->_admin_section);
        $article_limit = $settings->getAdminSiteSectionData(array(
            "conditions" => "name = 'article_limit'"
        ));

        return $article_limit[0]->value ? : constant("ARTICLE_LIMIT");
    }

    private function getTextLimit() {
        $admin_section = \Common\Models\AdminSiteSection::getNewsSettings($this->_admin_section);
        $text_limit = $admin_section->getAdminSiteSectionData(array(
            "conditions" => "name = 'text_limit'"
        ));

        return $text_limit[0]->value ? : constant("TEXT_LIMIT");
    }

//ADMIN SECTION    
    public function addnewsgroupAction() {
        $this->view->setTemplateBefore('admin');
        $form = new \Multiple\Backend\Forms\News\AddNewsGroupForm();

        if ($this->request->isPost() == true) {

            $raw_label = $this->request->getPost("Name");
            $label_sanitised = $this->filter->sanitize($raw_label, "safeName");
            $this->flashSession->success($label_sanitised);
            $link_sanitised_name = $this->filter->sanitize($raw_label, "link");

            $news_group = new \Common\Models\NewsGroup();
            $news_group->link = $link_sanitised_name;
            $news_group->label = $label_sanitised;

            if ($news_group->create() == false) {
                echo "<span>This News Group name cannot be used. </span>";
                foreach ($news_group->getMessages() as $message) {
                    echo "<span>", $message, "</span>";
                }
            } else {
                $this->flashSession->success("News Group " . $news_group->label . " successfully created!");
                return $this->response->redirect('admin/news/newsgroups');
            }
        }
        
        $this->view->setVar('form', $form);
        
    }

    public function modifynewsgroupAction()
    {

        $news_group_name = $this->dispatcher->getParam("news_group_name", "string");

        $this->assets->addJs("js/wysibb-image-helper.js");
        $this->assets->addJs("js/wysibb-image-helper-options-article.js");
        $this->view->setTemplateBefore('admin');

        $news_group = \Common\Models\NewsGroup::findFirst("link = '" . $news_group_name . "'");

        $form = new \Multiple\Backend\Forms\News\ModifyNewsGroupForm($news_group);
        $this->view->setVar("form", $form);

        $image_location_full = getcwd() . "/" . constant("NEWS_GROUP_IMAGE_DIR") . $news_group->link . ".png";

        if (file_exists($image_location_full) == true && $news_group->image_name == $news_group->link) {
            $image_location_url = $this->url->getBaseUri() . "public/" . $this->url->getBasePath() . constant("NEWS_GROUP_IMAGE_DIR") . $news_group->link . ".png";
            $this->view->setVar('image_location_url', $image_location_url);
        } else {
            $this->view->setVar('image_location_url', false);
        }

        if ($this->request->isPost() == true) {

            $label = $this->request->getPost("Name");
            $label = $this->filter->sanitize($label, "safeName");
            $priority = $this->request->getPost("Priority");
            $command = $this->request->getPost("command");

            switch ($command) {
                case "Update";
                    $news_group->label = $label;
                    $news_group->priority = $priority;

                    if ($this->request->hasFiles() == true) {

                        $files = $this->request->getUploadedFiles();

                        if ($files[0]->getSize() > constant("MAX_ICON_IMAGE_SIZE")) {
                            $this->flashSession->error("FILE SIZE MUST BE UNDER: " . (constant("MAX_ICON_IMAGE_SIZE") / 1000) . "KB");
                        } elseif ($files[0]->getRealType() != "image/png") {
                            $this->flashSession->error("The image must be a .png image.");
                        } else {
                            $icon = $files[0];
                            $result = $icon->moveTo(constant("NEWS_GROUP_IMAGE_DIR") . $news_group->link . ".png");#TODO - refactor to constant
                            $news_group->image_name = $news_group->link;
                            $news_group->image_type = $icon->getType();
                        }
                    }

                    if ($news_group->save() == false) {
                        foreach ($news_group->getMessages() as $message) {
                            $this->flashSession->success("MESSAGE: " . $message);
                        }
                    }

                    return $this->response->redirect($this->router->getRewriteUri());
                    break;

                case "Update Image";
                    $this->view->setVar("UpdatingImage", true);
                    break;

                case "Cancel Update";
                    return $this->response->redirect($this->router->getRewriteUri());
                    break;

                case "Delete Image";
                    if (file_exists($image_location_full) == true) {
                        unlink($image_location_full);
                        $news_group->image_name = null;
                        $news_group->image_type = null;
                        $news_group->save();

                        return $this->response->redirect($this->router->getRewriteUri());
                    } else {
                        $this->flashSession->error("Image file does not exist to delete!");
                    }
                    break;

                case "Delete";
                    if ($news_group->delete() == true) {
                        $this->flashSession->success("News Group " . $news_group->label . " deleted!");
                        return $this->response->redirect('admin/news/newsgroups');
                    } else {
                        $this->flashSession->error("News Group " . $news_group->label . " could not be deleted!");
                        return $this->response->redirect($this->router->getRewriteUri());
                    }
                    break;

                default:
                    break;
            }
        }
    }

    public function newsgroupsAction() {
        $this->view->setTemplateBefore('admin');
        $news_groups = \Common\Models\NewsGroup::find();
        $this->view->setVar("news_groups", $news_groups);
    }

    public function modifyaddnewsarticleAction($article_id = null) {
        $this->assets->addJs("js/wysibb/jquery.wysibb.js");
        $this->assets->addJs("js/wysibb-options.js");
        $this->assets->addJs("js/wysibb-image-helper.js");
        $this->assets->addJs("js/wysibb-ihelper-options-article.js");
        $this->assets->addJs("js/jquery.cookie.js");
        $this->assets->addJs("js/jquery.scroller.js");
        $this->view->setTemplateBefore('admin');
        $this->view->pick('news/newsarticle');


        if ($article_id == true) {
            $news_article = \Common\Models\NewsArticle::findFirst(array(
                        "conditions" => "id = $article_id "
            ));

            if (isset($news_article->image_embed) == true) {
                $this->view->setVar("image_embed_code", $news_article->image_embed);
            }

            if (isset($news_article->image_upload) == true) {
                $this->view->setVar("image_url", $this->url->getBaseUri() . constant("NEWS_ARTICLE_IMAGE_DIR") . $news_article->image_upload);
            }

            $form = new \Multiple\Backend\Forms\News\NewsArticleForm($news_article);
            $this->view->setVar("form", $form);
        } else {
            $news_article = new \Common\Models\NewsArticle();
            $form = new \Multiple\Backend\Forms\News\NewsArticleForm();
            $this->view->setVar("form", $form);
        }

        if ($this->request->isPost() == true) {

            if ($this->request->getPost("image_action") == true && isset($news_article) == true) {
                $action = $this->request->getPost("image_action");

                if ($action == "Delete Upload Image" || $action == "Delete Embed Image") {
                    $news_article->image_upload = null;
                    $news_article->image_embed = null;

                    if ($news_article->save() == false) {
                        foreach ($news_article->getMessages() as $message) {
                            $this->flashSession->error("The news article cannot be saved: " . $message);
                        }
                    } else {
                        $this->flashSession->success("Image has been deleted");
                        return $this->response->redirect("admin/news/modifynewsarticle/" . $article_id);
                    }
                }

                if ($action == "Image Upload Update") {
                    $this->flashSession->success("Image has been updated");
                    return $this->response->redirect("admin/news/modifynewsarticle/" . $article_id);
                }
            } else {
//                #TODO - Logic needs changing when image_upload already exists and no file update so....

                $form->bind($_POST, $news_article); //Set form validation within the form //TODO WTF IS THIS ---- NO VALIDTATION SO WHAT IS IT THERE FOR?????
                $news_article->image_embed = strlen($news_article->image_embed) > 0 ? $news_article->image_embed : null;
                $news_article->publish = $this->request->getPost("publish") != null ? 1 : 0;
                $news_article->content_plaintext = $this->filter->sanitize($this->request->getPost("bbcode_field"), 'stripBbTagsHtmlContent'); //done
                // if this request has no image upload??????
                if ($this->request->hasFiles() == true && $news_article->image_upload == NULL) {//Handle images if present
                    $files = $this->request->getUploadedFiles();

                    if ($files[0]->getSize() > constant("MAX_ARTICLE_IMAGE_SIZE")) {
                        $this->flashSession->error("FILE SIZE MUST BE UNDER: " . (constant("MAX_ARTICLE_IMAGE_SIZE") / 100) . "MB");
                    } else {
                        $image = $files[0];
                        $image->moveTo(constant("NEWS_ARTICLE_IMAGE_DIR") . $image->getName());
                        $news_article->image_upload = $image->getName();
                        $news_article->image_embed = null;
                    }
                }

                if ($news_article->publish == true && $news_article->published_first == true) {
                    $news_article->published_last = date('Y/d/m H:i:s', time());
                } elseif ($news_article->publish == true && $news_article->published_first == null) {
                    $news_article->published_first = date('Y/d/m H:i:s', time());
                }

                if ($news_article->save() == false) {

                    foreach ($news_article->getMessages() as $message) {
                        $this->flashSession->error("The news article cannot be saved: " . $message);
                    }
                } else {
                    if ($article_id == true) {
                        $this->flashSession->success("Article has been saved");
                        return $this->response->redirect("admin/news/modifynewsarticle/" . $article_id);
                    } else {
                        $this->flashSession->success("News article created");
                        return $this->response->redirect('admin/news/newsarticles');
                    }
                }
            }
        }
    }

    public function newsarticlesAction($page_requested = null)
    {
        $this->assets->addJs("js/jquery.cookie.js");
        $this->assets->addJs("js/jquery.paginater.js");

        $this->view->setTemplateBefore('admin');

        $news_articles = \Common\Models\NewsArticle::find(
                        array(
                            "order" => "id desc"
                        )
        );

        $paginated_page = \Common\Library\ModelPaginator::paginate($this, $news_articles);

        $this->view->setVar('page', $paginated_page);
    }

    public function settingsAction() {

        $this->assets->addJs("js/wysibb-image-helper.js");
        $this->view->setTemplateBefore('admin');

        $settings = new \Common\Models\Entities\NewsArticleSettings($this);
        $form = new \Multiple\Backend\Forms\News\NewsSettingsForm($settings);
        $image_url = $settings->getDefaultImage() !== null ? $this->url->getBaseUri() . $settings->getDefaultImage() : null;

        $this->view->setVar("image_url", $image_url);
        $this->view->setVar("form", $form);

        if ($this->request->isPost() == true) {
            $settings->maxBlurbs = $this->request->getPost("maxBlurbs");
            $settings->other_news_limit = $this->request->getPost("other_news_limit");

            if ($this->request->hasFiles() == true) {//Handle images if present
                $files = $this->request->getUploadedFiles();

                if ($files[0]->getSize() > constant("MAX_ARTICLE_IMAGE_SIZE")) {
                    $this->flashSession->error("FILE SIZE MUST BE UNDER: " . (constant("MAX_ARTICLE_IMAGE_SIZE") / 100) . "MB");
                } else {
                    $image = $files[0];
                    $image_mime_type = $image->getRealType();
                    $image_name = "default-article-image";
                    $image_path = constant("NEWS_ARTICLE_SETTINGS_IMAGE_DIR");
                    $is_image = strpos($image_mime_type, 'image') !== false ? true : false;

                    if ($is_image) {
                        $image_type = preg_replace("/image\//", "", $image_mime_type);
                        $image_location = $image_path . $image_name . "." . $image_type;
                        $image->moveTo($image_location);
                    }

                    $settings->defaultImage = $image_location;
                }
            } else {
                if ($this->request->getPost("image_action") == "Delete Image") {
                    $settings->defaultImage = null;
                }
            }

            if ($settings->save() != true) {
                
            } else {
                $this->flashSession->success("Successfully saved settings");
            }

            return $this->response->redirect($this->router->getRewriteUri());
        }
    }

}
