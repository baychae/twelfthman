<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 03/06/2016
 * Time: 10:20
 */


namespace Multiple\Backend\Controllers;

use Common\Library\Helpers;

class MembersController extends \Multiple\Backend\Controllers\ControllerBase
{

    public function initialize()
    {
        \Phalcon\Tag::setTitle('Admin Members');
        $this->view->setTemplateBefore('admin');
    }

    public function indexAction()
    {

    }

    public function teamGroupsAction()
    {
        $this->view->setTemplateBefore('admin');
        $this->assets->addJs("js/jquery.cookie.js");
        $this->assets->addJs("js/jquery.paginater.js");

        $groups = \Common\Models\Team::find(['order' => 'name ASC']);
        $paginated_page = \Common\Library\ModelPaginator::paginate($this, $groups);

        $this->view->setVar("page", $paginated_page);
    }

    public function teamGroupEditAction()
    {
        $this->assets->addJs("js/wysibb-image-helper.js");
        $this->assets->addJs("js/wysibb-ihelper-options-members.js");

        $group_id = $this->dispatcher->getParam("group_id", "int");

        $group = \Common\Models\Team::findFirst($group_id);

        $form = new \Multiple\Backend\Forms\Members\TeamGroupForm($group);


        $this->view->setVar("form", $form);
        $this->view->setVar("image_name", $group->image_name);

    }

    public function teamGroupUpdateAction()
    {
        $group_id = $this->dispatcher->getParam("group_id", "int");
        $delete_action = $this->request->getPost("delete_group");
        $main_redirect_url = "admin/members/teamgroups";


        if (empty($group_id)) {
            $group = new \Common\Models\Team();
        } else {
            $group = \Common\Models\Team::findFirst($group_id);
        }

        if (isset($delete_action) == true) {

            if ($group->delete() == false) {
                foreach ($group->getMessages() as $message) {
                    $this->flashSession->error("The team group cannot be deleted. " . $message);
                    return $this->response->redirect($main_redirect_url);
                }
            } else {
                $this->flashSession->success("Team Group " . $group->name . " has been updated.");
                return $this->response->redirect($main_redirect_url);
            }
        }

        $post_name = $this->request->getPost("name", "string");
        $post_name = str_replace(' ', '_', $post_name);
        $post_name = strtolower($post_name);

        $allowed_types = $this->config->common->teams->image->allowed_types->toArray();
        $max_file_size = $this->config->common->teams->image->max_file_size;
        $image_dir = $this->config->common->teams->image->dir;


        if ($this->request->hasFiles(true) == true) {//Handle images if present

            $files = $this->request->getUploadedFiles(true);
            $imageType = $files[0]->getRealType();

            if (!in_array($imageType, $allowed_types)) {
                $this->flashSession->error("You may only upload images of type: " . implode(" ", $allowed_types));
                return $this->response->redirect($main_redirect_url);
            } elseif ($files[0]->getSize() < $config->backend->members->teams->image->max_file_size) {
                $this->flashSession->error("File size must be under: " . $max_file_size . "KB not " . $files[0]->getSize() . "KB");
                return $this->response->redirect($main_redirect_url);
            } else {

                if (!empty($post_name)) {

                    $file_type = preg_replace("/image\//", "", $imageType);

                    $new_image_name = $post_name . "." . $file_type;
                    $this->flashSession->success($new_image_name);


                    if (!empty($group->image_name)) {// Delete old file and replace

                        $existing_file_location = $image_dir . $group->image_name;
                        $deleted = unlink($existing_file_location);

                        if (!$deleted) {
                            $this->flashSession("Could not delete old file. Please contact admin!");
                            return $this->response->redirect($main_redirect_url);
                        }

                        $image = $files[0];
                        $image->moveTo($image_dir . $new_image_name);
                        $group->image_name = $new_image_name;
                    } else {//Brand new image
                        $image = $files[0];
                        $image->moveTo($image_dir . $new_image_name);
                        $group->image_name = $new_image_name;
                    }

                }
            }
        } elseif ($this->request->getPost("delete_image") == "Delete") {


            $file_location = $image_dir . $group->image_name;

            if (empty($group->image_name)) {
                $this->flashSession->success("There is no Team Image to delete.");
                return $this->response->redirect($main_redirect_url);
            }


            if (file_exists($file_location) == true) {
                $deleted = unlink($file_location);
            } else {
                $this->flashSession->success("There is no Team Image to delete.");
                return $this->response->redirect($main_redirect_url);
            }

            if ($deleted == true) {
                $group->image_name = null;
                $this->flashSession->success("Team group image successfully deleted.");
            } else {
                $this->flashSession->error("File was not deleted please alert an admin : ");
                return $this->response->redirect($main_redirect_url);
            }

        }


        $group->name = $this->request->getPost("name", "string");

        if ($group->save() == false) {
            foreach ($group->getMessages() as $message) {
                $this->flashSession->error("The team group cannot be saved: " . $message);
                return $this->response->redirect($main_redirect_url);
            }
        } else {
            $this->flashSession->success("Team Group " . $group->name . " has been updated.");
            return $this->response->redirect($main_redirect_url);
        }


    }

    public function addTeamGroupAction()
    {
        $this->view->pick("members/teamGroupEdit");
        $form = new \Multiple\Backend\Forms\Members\TeamGroupForm();
        $this->view->setVar("form", $form);
    }

    public function adminAccountsAction()
    {
        $this->assets->addJs("js/jquery.cookie.js");
        $this->assets->addJs("js/jquery.paginater.js");

        $index = $this->request->get("index", "email");
        $search = $this->request->get("search", "safeChars");

        if ($search == true) {
            $members = \Common\Models\SiteUser::find("if(avatar_name is null, login_id, avatar_name) like '%" . $search . "%'");
            $paginated_page = \Common\Library\ModelPaginator::paginate($this, $members);
            $this->view->setVar("paginator_label", "Members  in search: " . $search);
            $this->view->setVar("indexed_by", "search=" . $search);
        } elseif ($index == true) {
            $members = \Common\Models\SiteUser::find("if(avatar_name is null, login_id, avatar_name) like '" . $index . "%'");
            $paginated_page = \Common\Library\ModelPaginator::paginate($this, $members);
            $this->view->setVar("paginator_label", "Members in Tab: " . $index);
            $this->view->setVar("indexed_by", "index=" . $index);
        } else {
            $members = \Common\Models\SiteUser::find();
            $paginated_page = \Common\Library\ModelPaginator::paginate($this, $members);
        }

        $site_user = new \Common\Models\SiteUser();
        $members_index = $site_user->getSiteMembersIndex();

        $this->view->setVar("page", $paginated_page);
        $this->view->setVar("alpha_index_array", $members_index);
    }

    public function adminAccountAction()
    {
        $post_user_id = $this->dispatcher->getParam("account_id");
        $site_user = \Common\Models\SiteUser::findFirst($post_user_id);

        $accountForm = new \Multiple\Backend\Forms\Members\AccountForm($site_user);

        $achievement_types = \Common\Models\SiteUserAchievementType::find();
        $site_user_achievements = $site_user->getSiteUserAchievement();
        $achievement_ids = [];

        foreach ($site_user_achievements as $achievement) {
            $achievement_ids [] = $achievement->site_user_achievement_type_id;
        }

        $this->view->setVar("avatar_file_name", $site_user->avatar_file_name);
        $this->view->setVar("login_id", $site_user->id);
        $this->view->setVar("form", $accountForm);
        $this->view->setVar("achievements", $achievement_ids);
        $this->view->setVar("achievement_types", $achievement_types);

    }

    public function adminSaveAccountAction()
    {
        $site_user_id = $this->dispatcher->getParam("account_id", "int") . PHP_EOL;
        $site_user = \Common\Models\SiteUser::findFirstById($site_user_id);
        $achievements_to_delete = \Common\Models\SiteUserAchievement::find("site_user_id = " . $site_user->id);

        $posted_achievements = $this->request->getPost("achievements");

        foreach ($achievements_to_delete as $achievements) {
            if ($achievements->delete() === false) {
                echo "Sorry, we can't delete the achievement right now: \n";

                $messages = $achievements->getMessages();

                foreach ($messages as $message) {
                    echo $message, "\n";
                }
            }
        }

        if (isset($posted_achievements)) {
            $post_achievement_ids = implode(",", $this->request->getPost("achievements")); // string of comma seperated id's
            $achievement_types = \Common\Models\SiteUserAchievementType::find("id IN(" . $post_achievement_ids . ")")
                ->filter(function ($t) {
                    return $t;
                });
            $site_user->siteUserAchievementType = $achievement_types;
        }

        if ($site_user->save()) {
            $this->flashSession->success("Users Account Updated!");
            return $this->response->redirect($this->router->getRewriteUri());
        } else {
            foreach ($site_user->getMessages as $message) {
                $this->flashSession->error("failed: " . $message);
            }
            return $this->response->redirect($this->router->getRewriteUri());
        }


    }

    public function adminAchievementsAction()
    {
        $achievements = \Common\Models\SiteUserAchievementType::find();
        $paginated_page = \Common\Library\ModelPaginator::paginate($this, $achievements);

        $this->view->setVar('page', $paginated_page);
    }

    public function addAchievementAction()
    {
        $form = new \Multiple\Backend\Forms\Members\AchievementForm();
        $this->view->setVar('form', $form);
    }

    public function saveAchievementAction()
    {
        $error_redirect_url = "/admin/members/addachievement";
        $success_redirect_url = "/admin/members/achievements";

        $achievement_id = $this->dispatcher->getParam("achievement_id"); //If there's an achievement then this is an edit
        $action_delete_img = $this->request->getPost("delete_image");

        //IF EDIT THEN GET THE RECORD if not then create new

        if ($achievement_id) {
            $achievement_type = \Common\Models\SiteUserAchievementType::findFirst($achievement_id);
        } else {
            $achievement_type = new \Common\Models\SiteUserAchievementType();
        }

        if (!empty($action_delete_img) && !empty($achievement_type->id)) { // Delete image from

            $image_location = $this->config->backend->members->achievement->image_dir . $achievement_type->file_name;

            if (file_exists($image_location) == true) {

                if (unlink($image_location)) {
                    $achievement_type->file_name = new \Phalcon\Db\RawValue('NULL');
                }

                if ($achievement_type->save()) {
                    $this->flashSession->success("Deleted Achievement Image.");
                    return $this->response->redirect($this->router->getRewriteUri());
                } else {
                    foreach ($achievement_type->getMessages() as $message) {
                        $this->flashSession->error($message);
                    }
                    return $this->response->redirect($this->router->getRewriteUri());
                }


            } else {
                $this->flashSession->error("Image file does not exist to delete!");
                return $this->response->redirect($this->router->getRewriteUri());
            }
        }


        $title = $this->request->getPost("title", "string");
        $file_prospect = $this->request->getUploadedFiles(); //Does post have multi part form
        //If the multipart form is not empty; Then check to see if file is there; then grab it. Otherwise false in both cases.
        $file = !empty($file_prospect) ? $file_prospect[0]->getSize() > 0 ? $file_prospect[0] : false : false;

        if (empty($title)) {
            $this->flashSession->warning("You need a title!");
            return $this->response->redirect($this->router->getRewriteUri());
        }

        if (!$file && !$achievement_id) { //If no file and not and edit then eject
            $this->flashSession->warning("You need to have an image associated with the achievement!" . $achievement_id);
            return $this->response->redirect($this->router->getRewriteUri());
        }

        if ($file) {

            $file_name = $file->getName();
            $file_size = $file->getSize();
            $file_real_type = $file->getRealType();
            $max_file_size = $this->config->backend->members->achievement->max_file_size;

            if ($file_size > $max_file_size) {
                $this->flashSession->error("FILE SIZE MUST BE UNDER: " . ($max_file_size / 1000) . "KB");
                return $this->response->redirect($error_redirect_url);

            }

            if ($file_real_type != "image/png") {

                $this->flashSession->error("The image must be a .png image.");
                return $this->response->redirect($error_redirect_url);

            }

            $file->moveTo($this->config->backend->members->achievement->image_dir . $file_name);
            $achievement_type->file_name = $file_name;
        }

        $achievement_type->title = $title;

        if ($achievement_type->save()) {
            $this->flashSession->success("Your achievement type has been saved!");
            return $this->response->redirect($success_redirect_url);

        } else {

            foreach ($achievement_type->getMessages() as $message) {
                $this->flashSession->error("Error: " . $message);
            }

            return $this->response->redirect($error_redirect_url);
        }
    }

    public function editAchievementAction()
    {
        $achievement_id = $this->dispatcher->getParam("achievement_id");

        $file_base = $this->config->backend->members->achievement->image_dir;

        $achievement = \Common\Models\SiteUserAchievementType::findFirst($achievement_id);
        $form = new \Multiple\Backend\Forms\Members\AchievementForm($achievement);

        $image_url = isset($achievement->file_name) ? $this->url->get($file_base . $achievement->file_name) : false;

        $this->view->setVar('achievement_id', $achievement_id);
        $this->view->setVar('image_url', $image_url);
        $this->view->setVar('form', $form);


    }

}