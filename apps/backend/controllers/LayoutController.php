<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 27/09/2016
 * Time: 12:15
 */

namespace Multiple\Backend\Controllers;

class LayoutController extends \Multiple\Backend\Controllers\ControllerBase
{

    public function initialize()
    {
        \Phalcon\Tag::setTitle('Admin Layouts');
        $this->view->setTemplateBefore('admin');
    }

    public function indexAction()
    {

    }

    public function footerAction()
    {
        $this->assets->addJs("js/wysibb/jquery.wysibb.js");
        $this->assets->addJs("js/wysibb-options.js");

        $site_section = \Common\Models\AdminSiteSection::findFirst("name = 'layout' AND sub_section = 'footer' ");
        $site_section_data = $site_section->getAdminSiteSectionData("name = 'footer'")->getFirst();
        $escaped_footer_bb = $site_section_data->value;

        $form = new \Multiple\Backend\Forms\Layout\FooterForm();
        $form->get("bbcode_field")->setDefault($escaped_footer_bb);

        $this->view->setVar("form", $form);


    }

    public function saveFooterAction()
    {

        $footer_bb = $this->request->getPost("bbcode_field", "string");

        $site_section = \Common\Models\AdminSiteSection::findFirst("name = 'layout' AND sub_section = 'footer' ");
        $site_section_data = $site_section->getAdminSiteSectionData("name = 'footer'")->getFirst();

        if (!$site_section_data->value) { //Create a new one
            $site_section_data = new \Common\Models\AdminSiteSectionData();
            $site_section_data->name = "footer";
            $site_section_data->AdminSiteSection = $site_section;
            $site_section_data->value = $footer_bb;
        } else {
            $site_section_data->value = $footer_bb;
        }

        if (!$site_section_data->save()) {
            foreach ($site_section_data->getMessages() as $message) {
                $this->flashSession->error($message);
            }
            $this->response->redirect("admin/layout/footer");
        } else {
            $this->flashSession->success("Saved footer!");
            $this->response->redirect("admin/layout/footer");
        }
    }

}