<?php //#TODO - Turn these into short form

$router = new \Phalcon\Mvc\Router(true);
$router->setDefaultModule('frontend');

$router->add(
    "/news/:namespace",
    [
        "controller" => "news",
        "action" => "index",
        "group" => 1
    ]
);

$router->add(
    "/news/article/:int",
    [
        "controller" => "news",
        "action" => "article",
        "article_id" => 1
    ]
);

$router->add(
    "/messages[/]{0,1}",
    [
        "controller" => "messages",
        "action" => "inbox"
    ]
);

$router->addGet("/messages/{page_requested:[0-9]{0,50}}", "frontend::messages::inbox");

$router->addGet("/messages/inbox/{page_requested:[0-9]{0,50}}", "frontend::messages::inbox");

$router->addGet("/messages/outbox/{page_requested:[0-9]{0,50}}", "frontend::messages::outbox");

$router->addPost("/messages/new-message", "Messages::send");


$router->addGet( //Fresh new message request
    "/messages/new-message[/]{0,1}",
    [
        "controller" => "messages",
        "action" => "new",
        "mail_to" => 1
    ]
);


$router->addGet( //Request coming from the forum and contains mail to
    "/messages/new-message/:namespace[/]{0,1}",
    [
        "controller" => "messages",
        "action" => "new",
        "mail_to" => 1
    ]
);

$router->addGet( //Request coming from the forum and contains mail to
    "/messages/view/:namespace[/]{0,1}",
    [
        "controller" => "messages",
        "action" => "view",
        "uri" => 1
    ]
);

$router->addGet( //Request coming from the forum and contains mail to
    "/messages/reply/:namespace[/]{0,1}",
    [
        "controller" => "messages",
        "action" => "reply",
        "uri" => 1
    ]
);

$router->addPost( //Request coming from the forum and contains mail to
    "/messages/reply/:namespace[/]{0,1}",
    [
        "controller" => "messages",
        "action" => "send"
    ]
);

$router->addGet( //Request coming from the forum and contains mail to
    "/messages/delete/:namespace[/]{0,1}",
    [
        "controller" => "messages",
        "action" => "delete",
        "uri" => 1
    ]
);

$router->addGet(
    "/messages/members/:int",
    [
        "controller" => "messages",
        "action" => "members",
        "page_requested" => 1
    ]
);

$router->addGet(
    "/messages/friend-request[/]{0,1}",
    [
        "controller" => "messages",
        "action" => "friendRequest"
    ]
);

$router->addGet(
    "/messages/friend-request-accept/{sender}",
    [
        "module" => "frontend",
        "controller" => "messages",
        "action" => "friendRequestAccept",
    ]
);

$router->addGet(
    "/messages/friend-request-deny/{sender}",
    [
        "module" => "frontend",
        "controller" => "messages",
        "action" => "friendRequestDeny",
    ]
);

$router->addGet(
    "/members/unfriend/{friend}",
    [
        "module" => "frontend",
        "controller" => "members",
        "action" => "unfriend"
    ]
);

$router->addPost("/my_account[/]{0,1}", "MyAccount::update");

$router->addGet(
    "/session/password-reset/{uuid:[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}}",
    [
        "controller" => "session",
        "action" => "passwordReset"
    ]
);

$router->addPost("/session/password-reset[/]{0,1}", "Session::confirmedPasswordReset");

$router->addGet("/session/password-reset-failed[/]{0,1}", "Session::passwordResetFailed");

$router->add("/session/lost-password[/]{0,1}", "Session::lostPassword");

$router->add("/session/{login_from:[a-zA-Z0-9]{0,50}}", "frontend::session::index");

$router->addPost("/session/start[/]{0,1}", "frontend::session::start");

$router->addGet("/session/end[/]{0,1}", "frontend::session::end");

$router->add("/session/register[/]{0,1}", "frontend::session::register");

$router->addGet("/session/logout[/]{0,1}", "frontend::session::logout");

//leagues

$router->addGet("/rivals[/]{0,1}", "frontend::rivals::leagues");

$router->addGet("/rivals/{league:[0-9a-zA-Z-]{1,60}}", "frontend::rivals::league");

$router->addGet("/results/{team_id:[0-9]{0,60}}", "Results::index");

//ADMIN ROUTES
//NEWS


$router->add(
    "/admin[/]{0,1}",
    [
        "module" => "backend",
        "controller" => "admin",
        "action" => "index"
    ]
);


$router->add(
    "/admin/news",
    [
        "module" => "backend",
        "controller" => "news",
        "action" => "newsarticles"
    ]
);

$router->add("/admin/news/newsarticles", "backend::news::newsarticles");

$router->add(
    "/admin/news/newsarticles/:int",
    [
        "module" => "backend",
        "controller" => "news",
        "action" => "newsarticles",
        "page_requested" => 1
    ]
);

$router->add("/admin/news/newsgroups", "backend::news::newsgroups");

$router->add("/admin/news/addnewsgroup", "backend::news::addnewsgroup");

$router->add("/admin/news/modifynewsgroup/:namespace",
    [
        "module" => "backend",
        "controller" => "news",
        "action" => "modifynewsgroup",
        "news_group_name" => 1
    ]
);

$router->add("/admin/news/settings", "backend::news::settings");

$router->add(
    "/admin/news/modifynewsarticle/:params",
    [
        "module" => "backend",
        "controller" => "news",
        "action" => "modifyaddnewsarticle",
        "params" => 1
    ]
);

$router->add(
    "/admin/news/addnewsarticle",
    [
        "module" => "backend",
        "controller" => "news",
        "action" => "modifyaddnewsarticle"
    ]
);

$router->addGet("/admin/members[/]{0,1}",
    [
        "module" => "backend",
        "controller" => "members",
        "action" => "index"
    ]
);

$router->addGet("/admin/members/teamgroups[/]{0,1}",
    [
        "module" => "backend",
        "controller" => "members",
        "action" => "teamGroups"
    ]
);

$router->addGet("/admin/members/teamgroups/:int",
    [
        "module" => "backend",
        "controller" => "members",
        "action" => "teamGroups",
        "page_requested" => 1
    ]
);

$router->addGet("/admin/members/teamgroup/edit/:int",
    [
        "module" => "backend",
        "controller" => "members",
        "action" => "teamGroupEdit",
        "group_id" => 1
    ]
);

$router->addPost("/admin/members/teamgroup/edit/:int",
    [
        "module" => "backend",
        "controller" => "members",
        "action" => "teamGroupUpdate",
        "group_id" => 1
    ]
);

$router->addGet("/admin/members/addteamgroup[/]{0,1}",
    [
        "module" => "backend",
        "controller" => "members",
        "action" => "addTeamGroup"
    ]
);

$router->addPost("/admin/members/addteamgroup",
    [
        "module" => "backend",
        "controller" => "members",
        "action" => "teamGroupUpdate"
    ]
);

$router->addGet("/admin/members/accounts[/]{0,1}", "backend::members::adminAccounts");

$router->addGet("/admin/members/account/{account_id:[0-9]{1,100}}", "backend::members::adminAccount");

$router->addPost("/admin/members/account/{account_id:[0-9]{1,100}}", "backend::members::adminSaveAccount");

$router->addGet('/admin/members/achievements[/]{0,1}', 'backend::members::adminAchievements');

$router->addGet('/admin/members/addachievement[/]{0,1}', 'backend::members::addAchievement');

$router->addPost('/admin/members/addachievement[/]{0,1}', 'backend::members::saveAchievement');

$router->addGet('/admin/members/achievement/edit/{achievement_id:[0-9]{1,100}}', 'backend::members::editAchievement');

$router->addPost('/admin/members/achievement/edit/{achievement_id:[0-9]{1,100}}', 'backend::members::saveAchievement');

$router->addGet("/admin/rivals[/]{0,1}", "backend::rivals::leagues");

$router->addGet("/admin/rivals/leagues[/]{0,1}", "backend::rivals::leagues");

$router->addGet("/admin/rivals/{league_id:[0-9]{1,100}}", "backend::rivals::modifyLeague");

$router->addPost("/admin/rivals/{league_id:[0-9]{1,100}}", "backend::rivals::updateLeague");

$router->add("/admin/rivals/addleague[/]{0,1}", "backend::rivals::addLeague");

$router->addGet("/admin/rivals/teams[/]{0,1}", "backend::rivals::teams");

$router->addGet("/admin/rivals/teams/([0-9]{1,100})",
    [
        "module" => "backend",
        "controller" => "rivals",
        "action" => "teams",
        "page_requested" => 1
    ]
);

$router->add("/admin/rivals/team/([0-9]{1,1000})",
    [
        "module" => "backend",
        "controller" => "rivals",
        "action" => "team",
        "team_id" => 1
    ]
);

$router->add("/admin/rivals/addteam[/]{0,1}", "backend::rivals::addTeam");

$router->add("/admin/upnext[/]{0,1}", "backend::up_next::matches");

$router->add("/admin/upnext/matches[/]{0,1}", "backend::up_next::matches");

$router->addGet("/admin/upnext/{page_requested:[0-9]{1,10000}}", "backend::up_next::matches");

$router->addGet("/admin/upnext/matches/{page_requested:[0-9]{1,10000}}", "backend::up_next::matches");

$router->add("/admin/upnext/match/{match_id:[0-9]{1,10000}}", "backend::up_next::match");

$router->addGet("/admin/upnext/addmatch[/]{0,1}", "backend::up_next::addmatch");

$router->addPost("/admin/upnext/addmatch[/]{0,1}",
    [
        "module" => "backend",
        "controller" => "up_next",
        "action" => "updateMatch"
    ]
);

$router->addGet("/admin/upnext/settings[/]{0,1}", "backend::up_next::settings");

$router->addPost("/admin/upnext/settings[/]{0,1}", "backend::up_next::saveSettings");

$router->add("/hooks/up-next-sms-update-score", "frontend::hooks::upNextSmsUpdateScore");

$router->addGet('/admin/disclaimer[/]{0,1}', 'backend::disclaimer::index');

$router->addPost('/admin/disclaimer[/]{0,1}',
    [
        "module" => "backend",
        "controller" => "disclaimer",
        "action" => "save"
    ]
);

//FORUM

$router->add(
    "/forum/markread/:int",
    [
        "controller" => "forum",
        "action" => "forumMarkRead",
        "forum_id" => 1
    ]
);

$router->add(
    "/forum/topic/markread/:int/:int/:int",
    [
        "controller" => "forum",
        "action" => "forumTopicMarkRead",
        "forum_id" => 1,
        "topic_id" => 2,
        "forum_page_num" => 3
    ]
);

$router->add(
    "/forum/:int/:int",
    [
        "controller" => "forum",
        "action" => "forum",
        "forum" => 1,
        "page_requested" => 2
    ]
);

$router->addGet(
    "/forum/:int/:int/:int[/]{0,1}",
    [
        "controller" => "forum",
        "action" => "topic",
        "forum" => 1,
        "topic" => 2,
        "page_requested" => 3
    ]
);

$router->addPost(
    "/forum/:int/:int/:int[/]{0,1}",
    [
        "controller" => "forum",
        "action" => "addTopicReply",
        "forum" => 1,
        "topic" => 2,
        "page_requested" => 3
    ]
);

$router->add(
    "/forum/:int/:int/:int/(rate)",
    [
        "controller" => "forum",
        "action" => "topic",
        "forum" => 1,
        "topic" => 2,
        "page_requested" => 3,
        "rate" => 4
    ]
);

$router->add(
    "/forum/addtopic/:int",
    [
        "controller" => "forum",
        "action" => "addTopic",
        "forum" => 1

    ]
);

$router->addGet(
    "/forum/:int/:int/:int/addreply[/]{0,1}",
    [
        "controller" => "forum",
        "action" => "topicReply",
        "topic" => 2,
        "page" => 3
    ]
);

$router->addPost(
    "/forum/:int/:int/:int/addreply[/]{0,1}",
    [
        "controller" => "forum",
        "action" => "addTopicReply",
        "topic" => 2,
        "page" => 3
    ]
);

//$router->addGet(
//    "/forum/:int/:int/addreply/:int",
//    [
//        "controller" => "forum",
//        "action" => "topicReply",
//        "topic" => 2,
//        "post" => 3
//    ]
//);

$router->add(
    "/forum/:int/:int/flagreply/:int",
    [
        "controller" => "forum",
        "action" => "flagTopicReply",
        "forum_id" => 1,
        "topic_id" => 2,
        "post_id" => 3
    ]
);

$router->add(
    "/forum/:int/:int/editreply/:int",
    [
        "controller" => "forum",
        "action" => "editTopicReply",
        "forum_id" => 1,
        "topic_id" => 2,
        "post_id" => 3
    ]
);

$router->addPost(
    "/forum/:int/:int/editreply/:int",
    [
        "controller" => "forum",
        "action" => "updateTopicReply",
        "forum_id" => 1,
        "topic_id" => 2,
        "post_id" => 3
    ]
);

$router->addPost(
    "/forum/:int/:int/delete/:int[/]{0,1}",
    [
        "controller" => "forum",
        "action" => "deleteTopicReply",
        "forum_id" => 1,
        "topic_id" => 2,
        "post_id" => 3
    ]
);

$router->addGet(
    "/forum/all-topics[/]{0,1}", "frontend::forum::allTopics"
);

$router->addGet(
    "/forum/all-topics/{page_requested:[0-9]{0,10000}}", "frontend::forum::allTopics"
);

$router->add(
    "/admin/forum",
    [
        "module" => "backend",
        "controller" => "forum",
        "action" => "adminForums",
        "params" => "1"
    ]
);

$router->addGet("/admin/forums[/]{0,1}", "backend::forum::adminForums");

$router->addGet("/forum/search", "frontend::forum::search");

$router->addPost("/forum/search", "frontend::forum::search");

$router->add(
    "/admin/forum/:int/:int",
    [
        "module" => "backend",
        "controller" => "forum",
        "action" => "adminModifyForum",
        "forum" => 1,
        "page" => 2
    ]
);

$router->addGet(
    "/admin/forum/topic/:int",
    [
        "module" => "backend",
        "controller" => "forum",
        "action" => "adminForumTopic",
        "topic_id" => 1,
    ]
);

$router->addPost(
    "/admin/forum/topic/:int",
    [
        "module" => "backend",
        "controller" => "forum",
        "action" => "adminSaveForumTopic",
        "topic_id" => 1,
    ]
);

$router->add(
    "/admin/forum/addforum",
    [
        "module" => "backend",
        "controller" => "forum",
        "action" => "adminAddForum"
    ]
);

$router->add(
    "/admin/forum/addforumtype",
    [
        "module" => "backend",
        "controller" => "forum",
        "action" => "adminAddForumType"
    ]
);

$router->add(
    "/admin/forum/listforumtypes/:params",
    [
        "module" => "backend",
        "controller" => "forum",
        "action" => "adminListForumTypes",
        "params" => 1
    ]
);

$router->add(
    "/admin/forum/modifyforumtype/:params",
    [
        "module" => "backend",
        "controller" => "forum",
        "action" => "adminModifyForumType",
        "params" => 1
    ]
);

$router->add(
    "/admin/forum/flaggedposts",
    [
        "module" => "backend",
        "controller" => "forum",
        "action" => "adminFlaggedPosts",
        "page" => "1"
    ]
);

$router->add(
    "/admin/forum/flaggedposts/:int",
    [
        "module" => "backend",
        "controller" => "forum",
        "action" => "adminFlaggedPosts",
        "page" => 1
    ]
);

$router->add(
    "/admin/forum/flaggedpost/:int",
    [
        "module" => "backend",
        "controller" => "forum",
        "action" => "adminFlaggedPost",
        "flagged_post_id" => 1
    ]);

$router->add(
    "/admin/forum/settings",
    [
        "module" => "backend",
        "controller" => "forum",
        "action" => "adminSettings",
        "params" => 1
    ]
);

$router->addGet('/admin/layout[/]{0,1}',
    [
        "module" => "backend",
        "controller" => "layout",
        "action" => "footer"
    ]
);

$router->add("/admin/layout/footer",
    [
        "module" => "backend",
        "controller" => "layout",
        "action" => "footer"
    ]
);

$router->addPost("/admin/layout/footer",
    [
        "module" => "backend",
        "controller" => "layout",
        "action" => "saveFooter"
    ]
);

$router->addGet('/admin/adspace',
    [
        "module" => "backend",
        "controller" => "adSpace",
        "action" => "index"
    ]
);

$router->addPost('/admin/adspace',
    [
        "module" => "backend",
        "controller" => "adSpace",
        "action" => "save"
    ]
);


return $router;