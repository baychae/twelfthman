<?php

$application->registerModules(array(
        'frontend' => array(
            'className' => 'Multiple\Frontend\Module',
            'path' => SITE_ROOT . 'apps/frontend/Module.php',
        ),
        'backend' => array(
            'className' => 'Multiple\Backend\Module',
            'path' => SITE_ROOT . 'apps/backend/Module.php',
        )
    )
);