<?php

return new \Phalcon\Config([
    'application' => [
        'logPath' => SITE_ROOT . 'apps/logs/'
    ],
    'redis' => [
        'ip' => '192.168.1.3',
        'port' => 6379
    ],
    'sms' => [
        "api_key" => "bee0dc4c5da177d3d613ec46c4d1793d9c904e65",
        "ip" => [
            "derby" => [
                "bottom" => "127.0.0.1", // 89.248.48.192
                "top" => "127.0.0.1"      //89.248.48.223
            ],
            "leeds" => [
                "bottom" => "127.0.0.1", // 89.248.58.16
                "top" => "127.0.0.1"      // 89.248.58.31
            ]
        ]
    ],
    'site' => [
        'name' => 'Owlsonline',
        'team' => 'Sheffield Wednesday',
        'paginator' => [
            'desktop' => [
                'max_pages' => 9
            ],
            'mobile' => [
                'max_pages' => 3
            ]
        ]
    ],
    'database' => [
        'adapter' => 'Mysql',
        'host' => '192.168.1.3',
        'username' => 'root',
        'password' => 'root',
        'dbname' => 'dev_twelfthman_test',
    ],
    'mongodb' => [
        'host' => '192.168.1.3',
        'dbname' => 'twelfthman'
    ],
    'queue' => [
        'host' => '192.168.1.3',
        'port' => 11300,
        'persistent' => false
    ],
    'smtp' => [
        'host' => "smtp.gmail.com",
        'port' => 587,
        'security' => 'tls',
        'username' => 'owlsonlinetest91@gmail.com',
        'password' => 'slsplwzltpmpuvxd',
        'from' => 'no-reply@owlsonline.com',
        'options' => [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ]

    ],
    "solr" => [
        'host' => 'localhost',
        'port' => 8983,
        'username' => 'solr',
        'password' => 'SolrRocks',
        'admin_username' => 'solr',
        'admin_password' => 'SolrRocks',
        'collection' => 'forum_topics'
    ],
    'vendorDir' => SITE_ROOT . 'vendor/',
    'frontend' => [
        'controllersDir' => SITE_ROOT . 'apps/frontend/controllers/',
        'views' => [
            "desktop" => SITE_ROOT . 'apps/frontend/views/desktop/',
            "mobile" => SITE_ROOT . 'apps/frontend/views/mobile/'
        ],
        'cacheDir' => SITE_ROOT . 'apps/frontend/cache/',
        'formsDir' => SITE_ROOT . 'apps/frontend/forms/',
        'baseUri' => 'http://192.168.1.3:8080/twelftman/',
        'news' => [
            "article_limit" => 5,
            "text_limit" => 400,
            "max_icon_image_size" => 64000,
            "max_article_image_size" => 6400000,
            'group' => [
                "image_dir" => 'img/news/groups/'
            ],
            'article' => [
                'image_dir' => 'img/news/articles/',
                'settings' => [
                    'image_dir' => 'img/news/settings/'
                ]
            ]

        ],
        'my_account' => [
            'avatar_img_dir' => 'img/user-avatars/',
            'avatar_img_max_size' => 300000,                            //This is 300KB
            'avatar_img_default_size' => 100,
            'avatar_allowed_types' => ['image/jpeg', 'image/jpg', 'image/png']
        ],
        'admin' => [
            'contact' => 'admin@owlsonline.com'
        ],
        'modelsCache' => [
            'forum_type' => 60,
            'forum' => 60,
            'forum_topic' => 60,
            'forum_topic_post' => 60,
            'news' => 60,
            'results' => 60

        ]
    ],
    'backend' => [
        'controllersDir' => SITE_ROOT . 'apps/backend/controllers/',
        'views' => [
            "desktop" => SITE_ROOT . 'apps/backend/views/desktop/',
            "mobile" => SITE_ROOT . 'apps/backend/views/mobile/'
        ],
        'cacheDir' => SITE_ROOT . 'apps/backend/cache/',
        'formsDir' => SITE_ROOT . 'apps/backend/forms/',
        'baseUri' => 'http://192.168.1.3:8080/twelftman/',
        'members' => [
            'achievement' => [
                'image_dir' => 'img/user-achievements/',
                'max_file_size' => 64000
            ]
        ]
    ],
    'common' => [
        'modelsDir' => SITE_ROOT . 'apps/common/models/',
        'libraryDir' => SITE_ROOT . 'apps/common/library/',
        'pluginsDir' => SITE_ROOT . 'apps/common/plugins/',
        'team' => [
            'image' => [
                'allowed_types' => ['image/png'],
                'max_file_size' => 1000,
                'dir' => 'img/teams/'
            ]
        ],
        'leagues' => [
            'image_dir' => 'img/leagues/',
        ],
        'recaptcha' => [
            'data_site_key' => '6LcL7xgUAAAAAHF65eKH4sxO5u4bXT8GyEQVbBc-',
            'secret' => '6LcL7xgUAAAAAKHuf_vFYpYZ9hToJNzctcD10iy3'
        ]
    ],
    'javascript' => [
        'options' => [
            'baseUri' => 'http://192.168.1.3:8080/twelftman/',
            'logout' => [
                'elementId' => 'session/logout',
                'endSessionUri' => "session/end",
                'redirectUri' => 'news'
            ]
        ]
    ],
    'social' => [
        'facebook' => [
            'app_id' => 'asds'
        ]
    ]


]);