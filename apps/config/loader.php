<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 15/09/2016
 */

$loader = new \Phalcon\Loader(); //This loader is intended for cli-bootstrap.php

$loader->registerNamespaces(
    array(
        'Common\Library\Mail' => SITE_ROOT . 'apps/common/library/Mail',
        'Common\Library\Solr' => SITE_ROOT . 'apps/common/library/Solr',
        'Common\\Models' => SITE_ROOT . "apps/common/models",
        'Common\\Models\\Traits' => SITE_ROOT . "apps/common/models/traits",
    )
);

$loader->register();
