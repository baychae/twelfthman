<?php

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new Phalcon\DI\FactoryDefault();

$config = include SITE_ROOT . "apps/config/config.php";
$di->setShared('config', $config);

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->set('url', function () use ($config) {
    $url = new Phalcon\Mvc\Url();
    $url->setBaseUri($config->frontend->baseUri);

    return $url;
}, true);

/**
 * add routing capabilities
 */
$di->set('router', function() {
    $router = include __DIR__ . '/routes.php';
    
    $router->setDefaultModule('frontend');

//    echo $router->getRewriteUri();
//    exit(1);
    
    return $router;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function () use ($config) {

    $eventsManager = new \Phalcon\Events\Manager();
    $logger = new \Phalcon\Logger\Adapter\File(SITE_ROOT . "apps/logs/debug.log");

    // Listen all the database events
    $eventsManager->attach('db', function ($event, $connection) use ($logger) {
        if ($event->getType() == 'beforeQuery') {
            $logger->log($connection->getSQLStatement(), \Phalcon\Logger::INFO);
        }
    });

    $connection = new Phalcon\Db\Adapter\Pdo\Mysql(array(
        'host' => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname' => $config->database->dbname,
        'charset' => 'utf8',
    ));

    $connection->setEventsManager($eventsManager);

    return $connection;

});

// Simple database connection to localhost
$di->setShared('mongo', function () use ($config) {

    $mongo = new \Phalcon\Db\Adapter\MongoDB\Client('mongodb://' . $config->mongodb->host . ':27017?wTimeoutMS=40000&serverSelectionTryOnce=false');
    return $mongo->selectDatabase($config->mongodb->dbname);
});

$di->setShared('collectionManager', function () {

    return new Phalcon\Mvc\Collection\Manager();
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->set('modelsMetadata', function () {

    return new Phalcon\Mvc\Model\Metadata\Memory();
});

/**
 * Start the session the first time some component request the session service
 */
$di->set('session', function() {
    $session = new Phalcon\Session\Adapter\Files();
    $session->start();
    return $session;
});

/**
 * Registering user components
 */
$di->set('elements', function() {
    return new Common\Library\Elements();
});

$di->set('cookies', function() {
    $cookies = new Phalcon\Http\Response\Cookies();
    $cookies->useEncryption(false);
    return $cookies;
}, true);

$di->set('filter', function() {
    $filter = new \Phalcon\Filter();

    $filter->add('link', function($value) {
        $regex_alpha_num_whitespace = '/[^a-zA-Z0-9\s]/';
        $result_with_alphanum_whitespace = preg_replace($regex_alpha_num_whitespace, '', $value);
        $result_no_whitespace = preg_replace('!\s+!', '-', $result_with_alphanum_whitespace);
        $result_no_uc = strtolower($result_no_whitespace);
        return $result_no_uc;
    });

    $filter->add('safeName', function($value) {
        $passthrough = '/[^a-zA-Z0-9.@_!?\(\)\s]/';
        $filtered_result = preg_replace($passthrough, '', $value);
        return $filtered_result;
    });

    $filter->add('stripBbTagsHtmlContent', function($text) {
        //strip embed content
        $embed_stripped = preg_replace('/\[embed\].*\[\/embed\]/', '', $text);
        $youtube_stripped = preg_replace('/\[youtube\].*\[\/youtube\]/', '', $embed_stripped);
        $video_stripped = preg_replace('/\[video\].*\[\/video\]/', '', $youtube_stripped);
        $no_bb = preg_replace('/\[.*?]/', '', $video_stripped); //strip bb codes
        $result = preg_replace('/\<.*?\>(.*)\<\/.*\>/', '', $no_bb); // strip html content
        return $result;
    });
    
    $filter->add('seoUrl', function($string){
            //Lower case everything
            $string = strtolower($string);
            //Make alphanumeric (removes all other characters)
            $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
            //Clean up multiple dashes or whitespaces
            $string = preg_replace("/[\s-]+/", " ", $string);
            //Convert whitespaces and underscore to dash
            $string = preg_replace("/[\s_]/", "-", $string);
            return $string;
    });

    $filter->add('safeChars', function ($string) {
        $string = preg_replace("/[^\$\-\_\.\+\!\*\'\(\)\s,0-9a-zA-Z]/", "", $string);
        return $string;
    });

    $filter->add('datedecode', function ($string) {
        return \Common\Library\Helpers::datedecode($string);
    });

    return $filter;
});

$di->set('escaper', function () {
    return new Phalcon\Escaper();
});

$di->setShared("assets", function () use ($config) {

    //Bootstrapping universal js assets

    $assetManager = new \Phalcon\Assets\Manager();

    $assetManager->addJs('//code.jquery.com/jquery-2.1.1.min.js');
    $assetManager->addJs('js/logout.js');
    $assetManager->addInlineJs("Options = " . json_encode($config->javascript->options) . ";");

    return $assetManager;

});

$di->set("flashSession", function () {
    $flashSession = new Phalcon\Flash\Session();
    $flashSession->setEscaperService(new Phalcon\Escaper);

    return $flashSession;
});

$di->set(
    "flash",
    function () {
        return new Phalcon\Flash\Direct();
    }
);

$di->set('queue', function () use ($config) {
    $queue = new \Phalcon\Queue\Beanstalk(
        [
            "host" => $config->queue->host,
            "port" => $config->queue->port,
            "persistent" => $config->queue->persistent
        ]
    );

    return $queue;
});

$di->set('sms', function () use ($config) {
    $sms = new \Common\Library\Sms\Clockwork($config->sms->api_key);

    return $sms;
});

$di->set("logger", function () use ($config, $di) {

    $logger = new \Phalcon\Logger\Adapter\File($config->application->logPath . "/application.log");
    $logger->log("Service Logger");
    return $logger;
});

$di->setShared("redis", function () use ($config) {

    $redis = new \Redis();
    try {
        $redis->pconnect($config->redis->ip, $config->redis->port);
    } catch (Exception $e) {
        die($e->getMessage());
    }

    $redis->select(1);
    $redis->setOption(\Redis::OPT_SERIALIZER, 2);
//    $redis->setOption(\Redis::OPT_PREFIX, $_SERVER['HTTP_HOST'].":");

    if (isset($redis)) {
        return $redis;
    } else {
        return null;
    }

    return $redis;
});

//$di->set(
//    'modelsCache',
//    function () use($config) {
//        // Cache data for one day (default setting)
//        $frontCache = new Phalcon\Cache\Frontend\Data(
//            [
//                'lifetime' => 86400,
//            ]
//        );
//
//        // Memcached connection settings
//        $cache = new Phalcon\Cache\Backend\Redis(
//            $frontCache,
//            [
//                'host' => $config->redis->ip,
//                'port' => $config->redis->port,
//                'prefix' => 'modelsCache',
//                'persistent' => false
//            ]
//        );
//
//        return $cache;
//    }
//);

return $di;