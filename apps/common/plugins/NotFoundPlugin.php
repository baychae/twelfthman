<?php
/**
 * Source: https://github.com/phalcon/invo/blob/master/app/plugins/NotFoundPlugin.php
 *
 * NotFoundPlugin
 *
 * Handles not-found controller/actions
 */

namespace Common\Plugins;

class NotFoundPlugin extends \Phalcon\Mvc\User\Plugin
{
    /**
     * This action is executed before execute any action in the application
     *
     * @param Event $event
     * @param MvcDispatcher $dispatcher
     * @param Exception $exception
     * @return boolean
     */
    public function beforeException(\Phalcon\Events\Event $event, \Phalcon\Mvc\Dispatcher $dispatcher, \Exception $exception)
    {
        error_log($exception->getMessage() . PHP_EOL . $exception->getTraceAsString());

        $this->flashSession->error($exception->getMessage() . PHP_EOL . $exception->getTraceAsString());

        if ($exception instanceof \Phalcon\Mvc\Dispatcher\Exception) {
            switch ($exception->getCode()) {
                case \Phalcon\Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                case \Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                    $dispatcher->forward([
                        'controller' => 'errors',
                        'action' => 'show404'
                    ]);
                    return false;
            }
        }
        $dispatcher->forward([
            'controller' => 'errors',
            'action' => 'show500'
        ]);
        return false;
    }
}