<?php

namespace Common\Plugins;

/**
 * Security
 *
 * This is the security plugin which controls that users only have access to the modules they're assigned to
 */
class Security extends \Phalcon\Mvc\User\Plugin
{

    public function __construct($dependencyInjector)
    {
        $this->_dependencyInjector = $dependencyInjector;
    }

    public function getAcl()
    {
        //$this->session->destroy();

        if (!isset($this->acl)) {
//            $acl = new \Phalcon\Acl\Adapter\Memory();
            $acl = new \Phalcon\Acl\Adapter\Redis($this->redis);

            $acl->setDefaultAction(\Phalcon\Acl::DENY);

            //Ad Roles
            $roles = [
                'sysadmin' => 'Sysadmins manage site settings like ad space and manage teams and matches.',
                'admin' => 'Admins managed members, news and forum.',
                'poweruser' => 'Power Users can publish their own news articles!',
                'user' => 'Users or members contribute to forums and get member only access. ',
                'guest' => 'Guests can view but not contribute to the site.'
            ];

            foreach ($roles as $role => $description) {
                $acl->addRole(new \Phalcon\Acl\Role($role, $description));
            }

            //Add Inherits
            $inherits = [
                'user' => 'guest',
                'poweruser' => 'user',
                'admin' => 'poweruser',
                'sysadmin' => 'admin'
            ];

            foreach ($inherits as $inherit => $base) {
                $acl->addInherit($inherit, $base);
            }

            //Add Resources
            //Namespace\Controller => Action Resources

            //Admin area resources
            $sysAdmin_resources = [
                'Multiple\Backend\Controllers:admin' => [
                    'index', 'disclaimer', 'files', 'forum', 'members', 'messages', 'my_account',
                    'news', 'results', 'rivals', 'upnext'],
                'Multiple\Backend\Controllers:adSpace' => [
                    'index', 'save'
                ],
                'Multiple\Backend\Controllers:disclaimer' => [
                    'index', 'save'
                ],
                'Multiple\Backend\Controllers:layout' => [
                    'footer', 'save'
                ],
                'Multiple\Backend\Controllers:rivals' => [
                    'leagues', 'modifyLeague', 'updateLeague', 'addLeague', 'deleteLeague', 'teams',
                    'team', 'saveTeam', 'addTeam', 'deleteTeam', 'deleteTeamFlag'],
                'Multiple\Backend\Controllers:up_next' => [
                    'matches', 'match', 'updateMatch', 'deleteMatch', 'addmatch', 'settings', 'saveSettings'
                ]
            ];

            foreach ($sysAdmin_resources as $controller => $action) {
                $acl->addResource(new \Phalcon\Acl\Resource($controller), $action);
            }

            //Administrator Resources
            $admin_resources = [
                'Multiple\Backend\Controllers:members' => [
                    'index', 'teamGroups', 'teamGroupEdit', 'teamGroupUpdate', 'addTeamGroup', 'adminAccounts',
                    'adminAccount', 'adminSaveAccount', 'adminAchievements', 'adminAchievement', 'saveAchievement',
                    'editAchievement'
                ],
                'Multiple\Backend\Controllers:news' => [
                    'modifyaddnewsarticle', 'addnewsgroup', 'modifynewsgroup', 'newsgroups', 'settings'
                ],
                'Multiple\Backend\Controllers:forum' => [
                    'adminForums', 'adminListForum', 'adminAddForum', 'adminModifyForum', 'adminForumTopic',
                    'adminSaveForumTopic', 'adminAddForumType', 'adminModifyForumType', 'adminFlaggedPosts',
                    'adminFlaggedPost', 'adminSettings'
                ]
            ];

            foreach ($admin_resources as $controller => $action) {
                $acl->addResource(new \Phalcon\Acl\Resource($controller), $action);
            }

            //Power User Resources
            $powerUser_CA_resources = [
                'Multiple\Backend\Controllers:news' => ['index', 'newsarticles', 'modifyaddnewsarticle']
            ];

            foreach ($powerUser_CA_resources as $controller => $action) {
                $acl->addResource(new \Phalcon\Acl\Resource($controller), $action);
            }

            $member_resources = [
                'Multiple\Frontend\Controllers:forum' => [
                    'forumMarkRead',
                    'forumTopicMarkRead',
                    'addTopicReply',
                    'topicReply',
                    'search',
                    'allTopics'
                ],
                'Multiple\Frontend\Controllers:members' => ['index', 'profile', 'unfriend'],
                'Multiple\Frontend\Controllers:messages' => [
                    'inbox',
                    'outbox',
                    'new',
                    'send',
                    'view',
                    'reply',
                    'delete'
                    ,
                    'members',
                    'friends',
                    'friendRequest',
                    'friendRequestAccept',
                    'friendRequestDeny'
                ],
                'Multiple\Frontend\Controllers:my_account' => ['index', 'update']
            ];

            foreach ($member_resources as $controller => $action) {
                $acl->addResource(new \Phalcon\Acl\Resource($controller), $action);
            }


            //Public area resources
            $public_resources = [
                'Multiple\Frontend\Controllers:index' => ['index'],
                'Multiple\Frontend\Controllers:forum' => ['index', 'forum', 'topic'],
                'Multiple\Frontend\Controllers:disclaimer' => ['index'],
                'Multiple\Frontend\Controllers:news' => ['index', 'article'],
                'Multiple\Frontend\Controllers:results' => ['index'],
                'Multiple\Frontend\Controllers:rivals' => ['leagues', 'league'],
                'Multiple\Frontend\Controllers:session' => [
                    'index',
                    'register',
                    'start',
                    'logout',
                    'end',
                    'passwordReset',
                    'confirmedPasswordReset',
                    'passwordResetFailed',
                    'lostPassword'
                ],
                'Multiple\Frontend\Controllers:hooks' => ['upNextSmsUpdateScore'],
                'Multiple\Frontend\Controllers:errors' => ['show404']
            ];

            foreach ($public_resources as $controller => $action) {
                $acl->addResource(new \Phalcon\Acl\Resource($controller), $action);
            }

            //Add Controller->Action Resources to roles

            //Grant access to sys admin area to role System Administrator
            foreach ($sysAdmin_resources as $controller => $action) {
                foreach ($action as $action) {
                    $acl->allow('sysadmin', $controller, $action);
                }
            }

            //Grant access to admin area to role Administrator
            foreach ($admin_resources as $controller => $action) {
                foreach ($action as $action) {
                    $acl->allow('admin', $controller, $action);
                }
            }

            //Grant access to admin area to role Admin
            foreach ($powerUser_CA_resources as $controller => $action) {
                foreach ($action as $action) {
                    $acl->allow('poweruser', $controller, $action);
                }
            }

            //Grant access to private area to role User
            foreach ($member_resources as $controller => $action) {
                foreach ($action as $action) {
                    $acl->allow('user', $controller, $action);
                }
            }

            //Grant access to public areas to all roles
            foreach ($public_resources as $controller => $action) {
                foreach ($action as $action) {
                    $acl->allow('guest', $controller, $action);
                }
            }

            //The acl is stored in session, APC would be useful here too
//            $this->persistent->acl = $acl; //don't use this for redis
        }

//        return $this->persistent->acl; //dont use for redis
        return $acl;
    }

    /**
     * This action is executed before execute any action in the application
     */
    public function beforeExecuteRoute(\Phalcon\Events\Event $event, \Phalcon\Mvc\Dispatcher $dispatcher)
    {
        //TODO - When a logged in user fails to gain access to a restricted page log attempt and inform them that an
        // admin has been informed
        // TODO - When a logged in user fails to gain access to a restricted page over 3 times then logout and lock
        // their account email admin.

        $auth = $this->session->get('auth');

        $role = !$auth ? 'guest' : $auth['privelege'];

        $namespace = $dispatcher->getNamespaceName();
        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();

        $resource = $namespace . ":" . $controller;

        $acl = $this->getAcl();

        if (!$acl->isResource($resource)) {
            return false;
        }

        $resourceActionAllowed = $acl->isAllowed($role, $resource, $action);

        if ($resourceActionAllowed != \Phalcon\Acl::ALLOW) {
            $dispatcher->setModuleName('frontend');
            $dispatcher->setControllerName('errors');
            $dispatcher->setActionName('show403');
//            $this->session->destroy();
            return false;
        }
    }
}
