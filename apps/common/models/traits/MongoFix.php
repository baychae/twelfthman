<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 24/05/2017
 * Time: 11:41
 * TEMP FIX for https://github.com/phalcon/incubator/issues/760#issuecomment-303662803
 */

namespace Common\Models\Traits;

trait MongoFix
{
    /**
     * <------ WORKAROUND TO FIX PHALCON INCUBATOR MONGO BUG
     * https://github.com/phalcon/incubator/issues/760
     */
    public function save()
    {
        if (version_compare(PHP_VERSION, '7.1.0') >= 0) {
            $this->_dependencyInjector = \Phalcon\Di::getDefault();
            $this->_modelsManager = $this->_dependencyInjector->getShared("collectionManager");
            $this->_modelsManager->initialize($this);
        }
        return parent::save();
    }

    public function delete()
    {
        if (version_compare(PHP_VERSION, '7.1.0') >= 0) {
            $this->_dependencyInjector = \Phalcon\Di::getDefault();
            $this->_modelsManager = $this->_dependencyInjector->getShared("collectionManager");
            $this->_modelsManager->initialize($this);
        }
        return parent::delete();
    }
    /**
     * WORKAROUND TO FIX PHALCON INCUBATOR MONGO BUG ------>
     */
}