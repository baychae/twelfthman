<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Common\Models\Traits;

trait MyTimestampable 
{

    public function beforeValidationOnCreate()
    {
        list($eTime, $uTime) = explode(".", strval(microtime(true)));
        $this->created = date("Y-m-d H:i:s", $eTime) . "." . $uTime;
        $this->modified = date("Y-m-d H:i:s", $eTime) . "." . $uTime;
    }

    public function beforeUpdate()
    {
        list($eTime, $uTime) = explode(".", strval(microtime(true)));
        $this->modified = date("Y-m-d H:i:s", $eTime) . "." . $uTime;
    }

}
