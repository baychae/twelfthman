<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Common\Models\Traits;

use Phalcon\Validation\Validator\InclusionIn;

trait MyVisable {

    public function validation() {

        $this->view_type = isset($this->view_type) == false ? "P" : $this->view_type ;

        $validator = new \Phalcon\Validation();

        $validator->add(
            "view_type",
            new Inclusionin([
                "domain" => ["P", "M", "A"],
                "message" => "Visibility has an illegal type"
            ])
        );

        return $this->validate($validator);
    }

}
