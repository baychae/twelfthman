<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Common\Models\Traits;

use Phalcon\Validation\Validator\InclusionIn;

trait MySoftDeletable {

    public function beforeValidation() {

        $validator = new \Phalcon\Validation();

        $validator->add(
            "status",
            new Inclusionin([
                "domain" => ["A", "D", "L"]
            ])
        );

        return $this->validate($validator);
    }
    
    public function beforeDelete() {

        $validator = new \Phalcon\Validation();

        $validator->add(
            "status",
            new Inclusionin([
                "domain" => ["A", "D", "L"]
            ])
        );

        return $this->validate($validator);
    }
    
    

}
