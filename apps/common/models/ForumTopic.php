<?php

namespace Common\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Relation as Relation;

class ForumTopic extends \Phalcon\Mvc\Model
{

    use \Common\Models\Traits\MyTimestampable;
    use \Common\Models\Traits\MySoftDeletable;
    use \Common\Models\Traits\MyVisable;


    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var int
     */
    public $forum_id;

    /**
     *
     * @var int
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */
    public $description;

    /** MyDeleteStatus
     *
     * @var string
     */
    public $status;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var string
     */
    public $modified;

    /**
     *
     * @var string
     */
    public $last_updated;

    /**
     *
     * @var string
     */
    public $view_type;

    /**
     *
     * @var integer
     */
    public $pinned;

    public function onConstruct()
    {
        $this->status = "A"; //Every new instance becomes active -- Trait MySoftDeletable
    }

    public function initialize()
    {
        $this->addBehavior(new \Phalcon\Mvc\Model\Behavior\SoftDelete(
            [
                'field' => 'status',
                'value' => 'D'
            ]
        ));

        $this->belongsTo("forum_id", "Common\Models\Forum", "id", [
            "foreignKey" => ["message" => "The forum you are trying to save to no longer exists!"],
            'alias' => 'Forum'
        ]);

        $this->belongsTo("user_id", "Common\Models\SiteUser", "id", [
            "foreignKey" => ["message" => "You may not post as it appears you do not exist!"],
            'alias' => 'SiteUser'
        ]);

        $this->hasMany(
            "id",
            "Common\Models\ForumTopicPost",
            "topic_id",
            [
                'alias' => 'ForumTopicPost',
                "message" => "Topic cannot be deleted has it still has posts."
            ]
        );
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'id' => 'id',
            'forum_id' => 'forum_id',
            'user_id' => 'user_id',
            'modified' => 'modified',
            'created' => 'created',
            'title' => 'title',
            'description' => 'description',
            'status' => 'status',
            'last_updated' => 'last_updated',
            'view_type' => 'view_type',
            'pinned' => 'pinned'
        ];
    }

    public function beforeSave()
    {
        list($eTime, $uTime) = explode(".", strval(microtime(true)));
        $this->last_updated = date("Y:m:d H:i:s", $eTime) . "." . $uTime;
    }

    public function getFirstUnreadPost($user_id)
    {
        $forum_id = $this->forum_id;
        $topic_id = $this->id;

        $user_forum_read_tracker = \Common\Models\ForumUserReadTrackStats::findFirstByUserId($user_id);

        $aUser_forum_read_tracker = $user_forum_read_tracker->getForumsRead();
        $aUser_topics_read_tracker = $user_forum_read_tracker->getTopicsRead();

        $forum_last_read_date = isset($aUser_forum_read_tracker[$forum_id]) == true ?
            $aUser_forum_read_tracker[$forum_id] : false;

        $topic_last_read_date = isset($aUser_topics_read_tracker[$topic_id]) == true ?
            $aUser_topics_read_tracker[$topic_id] : false;

        if ($topic_last_read_date) { //They have topic tracker
            $post = $this->getForumTopicPost('created > "' . $topic_last_read_date . '" ')->getFirst();

            if (isset($post->id)) {
                return $post;
            } else { //There are no unread posts so direct user to last post
                return false;
            }
        } elseif ($forum_last_read_date) { //The have a Forum Tracker for threads forum
            $post = $this->getForumTopicPost('created > "' . $forum_last_read_date . '" ')->getFirst();

            if (isset($post->id)) {
                return $post;
            } else { //There are no unread posts so direct user to last post
                return false;
            }
        } else { //No tracker so as the user has not read anything in the forum give them the first post to read.
            return $this->getForumTopicPost()->getFirst();
        }
    }

    public function hasUserPosted($user_id)
    {
        $post = $this->getForumTopicPost('user_id = ' . $user_id)->getFirst();
        return isset($post->id) == true ? true : false;
    }

    public static function getForumView($forum_id)
    {
        $db = \Phalcon\Di::getDefault()->getDb();

        $sql = "select ftype.title as forum_type_title, "
            . "f.title as forum_title, "
            . "f.id as forum_id, "
            . "ft.title as topic_title, "
            . "ft.id as topic_id, "
            . "ftp1.user_id as last_post_user_id, "
            . "ftp1.created as last_post_created, "
            . "ftp1.topic_ordinal as last_post_topic_ordinal, "
            . "su.avatar_name as last_post_avatar_name "
            . "from forum_topic ft "
            . "join (select ft2.id , max(ftp1.id) as last_post_id "
            . "from forum_topic_post ftp1 "
            . "join forum_topic ft2 on ft2.id = ftp1.topic_id "
            . "group by topic_id "
            . "order by 2 desc) max_p on max_p.id = ft.id "
            . "join forum_topic_post ftp1 on ftp1.id = max_p.last_post_id "
            . "join forum f on f.id = ft.forum_id "
            . "join forum_type ftype on ftype.id = f.forum_type_id "
            . "join site_user su on su.id = ftp1.user_id "
            . "where f.id = ? "
            . "order by max_p.last_post_id desc";

        $query = $db->query(
            $sql,
            [
                $forum_id,
            ]
        );

        return $query->fetchAll();
    }

    public static function getPinnedForumView($forum_id)
    {
        $db = \Phalcon\Di::getDefault()->getDb();

        $sql = "select ftype.title as forum_type_title, "
            . "f.title as forum_title, "
            . "f.id as forum_id, "
            . "ft.title as topic_title, "
            . "ft.id as topic_id, "
            . "ftp1.user_id as last_post_user_id, "
            . "ftp1.created as last_post_created, "
            . "ftp1.topic_ordinal as last_post_topic_ordinal, "
            . "su.avatar_name as last_post_avatar_name "
            . "from forum_topic ft "
            . "join (select ft2.id , max(ftp1.id) as last_post_id "
            . "from forum_topic_post ftp1 "
            . "join forum_topic ft2 on ft2.id = ftp1.topic_id "
            . "group by topic_id "
            . "order by 2 desc) max_p on max_p.id = ft.id "
            . "join forum_topic_post ftp1 on ftp1.id = max_p.last_post_id "
            . "join forum f on f.id = ft.forum_id "
            . "join forum_type ftype on ftype.id = f.forum_type_id "
            . "join site_user su on su.id = ftp1.user_id "
            . "where f.id = ? AND ft.pinned = 1 "
            . "order by max_p.last_post_id desc";

        $query = $db->query(
            $sql,
            [
                $forum_id,
            ]
        );

        return $query->fetchAll();
    }

    public function getPostsForView()
    {
        $db = \Phalcon\Di::getDefault()->getDb();

        $sql = <<<SQL
            SELECT 
            COALESCE(su1.avatar_name, su1.login_id) AS display_name,
            ftp.id AS id,
            ftp.created AS created, 
            ftp.modified AS modified, 
            ft.forum_id AS forum_id, 
            ftp.topic_id AS topic_id, 
            ftp.topic_ordinal AS topic_ordinal,
            ftp.status AS status,
            su1.id AS user_id,
            su1.avatar_file_name AS avatar_file_name, 
            su1.created AS user_created_date,
            su1.signature_html AS user_signature_html,
            t.name AS user_team_name, 
            t.image_name AS user_team_image_name, 
            COALESCE(su2.avatar_name, su2.login_id) AS editor_display_name, 
            ftp.content_bb AS content_bb 
            FROM forum_topic_post ftp 
            JOIN forum_topic ft ON ft.id = ftp.topic_id 
            JOIN site_user su1 ON ftp.user_id = su1.id 
            LEFT JOIN site_user su2 ON ftp.editor_user_id = su2.id 
            LEFT JOIN team t ON t.id = su1.team_id 
            WHERE ftp.topic_id = ? 
            ORDER BY ftp.id ASC
SQL;

        $query = $db->query(
            $sql,
            [
                $this->id
            ]
        );

        return $query->fetchAll();
    }

    public static function getAllTopicsView()
    {
        $db = \Phalcon\Di::getDefault()->getDb();

        $sql = "select ftype.title as forum_type_title, "
            . "f.title as forum_title, "
            . "f.id as forum_id, "
            . "ft.title as topic_title, "
            . "ft.id as topic_id, "
            . "ftp1.user_id as last_post_user_id, "
            . "ftp1.created as last_post_created, "
            . "ftp1.topic_ordinal as last_post_topic_ordinal, "
            . "su.avatar_name as last_post_avatar_name "
            . "from forum_topic ft "
            . "join (select ft2.id , max(ftp1.id) as last_post_id "
            . "from forum_topic_post ftp1 "
            . "join forum_topic ft2 on ft2.id = ftp1.topic_id "
            . "group by topic_id "
            . "order by 2 desc) max_p on max_p.id = ft.id "
            . "join forum_topic_post ftp1 on ftp1.id = max_p.last_post_id "
            . "join forum f on f.id = ft.forum_id "
            . "join forum_type ftype on ftype.id = f.forum_type_id "
            . "join site_user su on su.id = ftp1.user_id "
            . "order by max_p.last_post_id desc";

        $query = $db->query($sql);

        return $query->fetchAll();
    }

    public static function getAllPinnedTopicsView()
    {
        $db = \Phalcon\Di::getDefault()->getDb();

        $sql = "select ftype.title as forum_type_title, "
            . "f.title as forum_title, "
            . "f.id as forum_id, "
            . "ft.title as topic_title, "
            . "ft.id as topic_id, "
            . "ftp1.user_id as last_post_user_id, "
            . "ftp1.created as last_post_created, "
            . "ftp1.topic_ordinal as last_post_topic_ordinal, "
            . "su.avatar_name as last_post_avatar_name "
            . "from forum_topic ft "
            . "join (select ft2.id , max(ftp1.id) as last_post_id "
            . "from forum_topic_post ftp1 "
            . "join forum_topic ft2 on ft2.id = ftp1.topic_id "
            . "group by topic_id "
            . "order by 2 desc) max_p on max_p.id = ft.id "
            . "join forum_topic_post ftp1 on ftp1.id = max_p.last_post_id "
            . "join forum f on f.id = ft.forum_id "
            . "join forum_type ftype on ftype.id = f.forum_type_id "
            . "join site_user su on su.id = ftp1.user_id "
            . "where ft.pinned = 1 "
            . "order by max_p.last_post_id desc";

        $query = $db->query($sql);

        return $query->fetchAll();
    }
}
