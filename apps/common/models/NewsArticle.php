<?php

namespace Common\Models;


class NewsArticle extends \Phalcon\Mvc\Model
{
    use \Common\Models\Traits\MyTimestampable;
    
    /**
     *
     * @var integer
     */
    public $id;
     
    /**
     *
     * @var string
     */
    public $title;
    
    /**
     *
     * @var string
     */
    public $image_upload;
    
    /**
     *
     * @var string
     */
    public $image_embed;
    
    /**
     *
     * @var string
     */
    public $image_caption;
     
    /**
     *
     * @var string
     */
    public $content_bb;
    
    /**
     *
     * @var string
     */
    public $content_html;
    
    /**
     *
     * @var string
     */
    public $content_plaintext;
    
    /**
     *
     * @var int
     */
    public $publish;
    
    /**
     *
     * @var string
     */
    public $published_first;
    
    /**
     *
     * @var string
     */
    public $published_last;
    
    /**
     *
     * @var int
     */
    public $user_id; 
    
    /**
     *
     * @var string
     */
    public $created;
     
    /**
     *
     * @var string
     */
    public $modified;
    
    /**
     *
     * @var integer
     */
    public $news_group_id;
     
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $image_embed = new \Phalcon\Db\RawValue('default');

        $this->belongsTo("news_group_id", "Common\\Models\\NewsGroup", "id", [
            "foreignKey" => [
                "message" => "The news group id does not exist!"
            ],
            'alias' => 'NewsGroup'
        ]);

        $this->belongsTo("user_id", "Common\\Models\\SiteUser", "id", [
            "foreignKey" => [
                "message" => "This user does not exist!"
            ],
            'alias' => 'SiteUser'
        ]);
    }
    
    public function beforeUpdate()
    {
        $this->image_embed == null ?: new \Phalcon\Db\RawValue('default') ;
    }
    
    public function beforeCreate()
    {
        $this->image_embed == null ?: new \Phalcon\Db\RawValue('default') ;
    }
    
    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'id' => 'id', 
            'title' => 'title',
            'image_upload' => 'image_upload',
            'image_embed' => 'image_embed',
            'image_caption' => 'image_caption',
            'content_bb' => 'bbcode_field', //enables auto bind to form
            'content_html' => 'content_html',
            'content_plaintext' => 'content_plaintext',
            'publish' => 'publish',
            'published_first' => 'published_first',
            'published_last' => 'published_last',
            'user_id' => 'user_id',
            'created' => 'created', 
            'modified' => 'modified',
            'news_group_id' => 'news_group_id'
        ];
    }

}
