<?php

namespace Common\Models;

class NewsGroup extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $link;

    /**
     *
     * @var string
     */
    public $label;

    /**
     *
     * @var integer
     */
    public $priority;

    /**
     *
     * @var string
     */
    public $image_name;

    /**
     *
     * @var string
     */
    public $image_type;

    public function initialize()
    {
        $this->hasMany("id", "Common\\Models\\NewsArticle", "news_group_id",
            [
                "message" => "You cannot delete this News Group. It is being used in a News Article!",
            'alias' => 'NewsArticle'
            ]
        );

    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'id' => 'id',
            'link' => 'link',
            'label' => 'label',
            'priority' => 'priority',
            'image_name' => 'image_name',
            'image_type' => 'image_type'
        ];
    }

    public function validation()
    {

        $validator = new \Phalcon\Validation();

        $validator->add("label", new \Phalcon\Validation\Validator\Uniqueness(
            ["message" => "The News Group name must be unique."]
        ));

        $validator->add("link", new \Phalcon\Validation\Validator\Uniqueness(
            ["message" => "The name is too similar to another News Group."]
        ));

        return $this->validate($validator);
    }


}
