<?php

namespace Common\Models;

class ForumTopicPostStats extends \Phalcon\Mvc\MongoCollection
{
    
    use \Common\Models\Traits\MyTimestampable;
    use \Common\Models\Traits\MongoFix;
    
    /**
     *
     * @var int
     */
    public $forum_id;
    
    /**
     *
     * @var int
     */
    public $topic_id;
    
    /**
     *
     * @var int
     */
    public $post_id;
    
    /**
     *
     * @var int
     */
    public $post_count;
    
     /**
     *
     * @var string
     */
    public $created;
    
    /**
     *
     * @var string
     */
    public $modified;

    public function beforeSave()
    {
        $this->forum_id = intval($this->forum_id);
        $this->topic_id = intval($this->topic_id);
        $this->post_id = intval($this->post_id);
        $this->post_count = intval($this->post_count);
    }
    
    
}