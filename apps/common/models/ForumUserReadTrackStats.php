<?php

/**
 * Created by PhpStorm.
 * User: Richard Sinclair
 * Date: 13/10/2015
 * Time: 14:50
 */

namespace Common\Models;


class ForumUserReadTrackStats extends \Phalcon\Mvc\MongoCollection
{

    use \Common\Models\Traits\MyTimestampable;
    use \Common\Models\Traits\MongoFix;

    /**
     *
     * @var int
     */
    public $user_id;

    /**
     *
     * @var array
     */
    public $forums;

    /**
     *
     * @var array
     */
    public $topics;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var string
     */
    public $modified;

    public static function findFirstByUserId($user_id)
    {
        $found_tracker = self::findFirst(array(
            array("user_id" => $user_id)
        ));

        if ($found_tracker == false) {
            $new_tracker = new self();
            $new_tracker->user_id = $user_id;
            if ($new_tracker->save()) {
                return $new_tracker;
            }
        } else {
            return $found_tracker;
        }

        return null;
    }

    public function setForumRead($add_forum_id)
    {
        $now = \Common\Library\DateTime::getFormattedSplitMilli();

        $forums = (Array)$this->forums;
        $forums[$add_forum_id] = $now;
        $this->forums = $forums;

        if ($this->save()) {
            return true;
        } else {
            return false;
        }
    }

    public function setTopicAllRead($topic_id)
    {
        $last_post_topic_ordinal = \Common\Models\ForumTopicPost::find(
            [
                "topic_id = " . $topic_id,
                "order" => "created asc "
            ]
        )->getLast()->topic_ordinal;

        $topics = (Array)$this->topics;
        $topics[$topic_id] = $last_post_topic_ordinal;
        $this->topics = $topics;

        if ($this->save()) {
            return true;
        } else {
            return false;
        }
    }

    public function updateTopicRead($topic_id, $topic_ordinal)
    {
        //If the date is less than the one recorded ignore and don't update

        $topics = (Array)$this->topics;

        if (!isset($topics[$topic_id])) {// If its not set create a new record and enter date
            $topics[$topic_id] = $topic_ordinal;
        } elseif ($topic_ordinal > $topics[$topic_id]) {
            $topics[$topic_id] = $topic_ordinal;
        }

        $this->topics = $topics;

        if ($this->save()) {
            return true;
        } else {
            return false;
        }
    }

    public function getTopicsRead()
    {
        return (Array)$this->topics;
    }

    public function setTopicsRead($topics)
    {
        if (is_array($topics)) {
            $this->topics = $topics;
        } else {
            throw new \TypeError("topics must be an array");
        }
    }

    public function getForumsRead()
    {
        return (Array)$this->forums;
    }

    public function setForumsRead($forums)
    {
        if (is_array($forums)) {
            $this->forums = $forums;
        } else {
            throw new \TypeError("forums must be an array");
        }
    }

    public function getLastReadPostUrls($paginated_page_items, $topic_ids)
    {
        $di = \Phalcon\Di::getDefault();

        $cookie = $di->getCookies();
        $cookie_page_size = $cookie->get('pageSize')->getValue();
        $page_size = $cookie_page_size != null ? $cookie_page_size : 15;

        //Get all topics in current page user may have posted in
        $topics_posted_in_result = \Common\Models\ForumTopicPost::find(
            [
                'columns' => 'topic_id',
                'conditions' => 'topic_id in ({topic_ids:array}) AND user_id = :user_id:',
                'bind' => [
                    'topic_ids' => $topic_ids,
                    'user_id' => intval($this->user_id)
                ],
                'group' => 'topic_id'
            ]
        )->toArray();

        $user_posted_in_topic = array_map(function ($item) {
            return $item['topic_id'];
        }, $topics_posted_in_result);

        $read_topics = [];

        foreach ($paginated_page_items as $page_item) {
            $forum_id = $page_item['forum_id'];
            $topic_id = $page_item['topic_id'];

            $forum_last_read_date = isset($this->forums[$forum_id]) == true ?
                $this->forums[$forum_id] : false;
            $topic_last_read_ordinal = isset($this->topics[$topic_id]) == true ?
                intval($this->topics[$topic_id]) : false;

            if ($topic_last_read_ordinal) {                                                 //Topic Tracker Found
                if ($page_item['last_post_topic_ordinal'] > $topic_last_read_ordinal) {     //New Posts (green)
                    $topic_last_read_ordinal += 1;
                    $page = floor($topic_last_read_ordinal / $page_size) + 1;

                    $url = 'forum/' . $page_item['forum_id'] . '/' .
                        $page_item['topic_id'] . '/' . $page . '#' . $topic_last_read_ordinal;

                    $html = '<a href="' . $url . '">';
                    if (isset($user_posted_in_topic[$topic_id]) == true) {
                        $html .= '<img 
                                    title="Unread Replies. You have written in this Topic." 
                                    alt="Unread Replies" 
                                    class="button success" 
                                    src="img/buttons/text_written.png"
                                    >';
                    } else {
                        $html .= '<img 
                                    title="Unread Replies" 
                                    alt="Unread Replies" 
                                    class="button success" 
                                    src="img/buttons/text.png">';
                    }
                    $html .= '</a>';


                    $read_topics[$page_item['topic_id']]['html'] = $html;
                    $read_topics[$page_item['topic_id']]['unread'] = true;

                } else {                                                                    //Everything has been read
                    $page = floor($topic_last_read_ordinal / $page_size) + 1;
                    $url = 'forum/' . $page_item['forum_id'] . '/' .
                        $page_item['topic_id'] . '/' . $page . '#' . $topic_last_read_ordinal;

                    $html = '<a href="' . $url . '">';
                    if (isset($user_posted_in_topic[$topic_id]) == true) {
                        $html .= '<img 
                                    title="Marked As Read"
                                    alt="Marked As Read"
                                    class="button info" 
                                    src="img/buttons/text_written.png"
                                    >';
                    } else {
                        $html .= '<img 
                                    title="Marked As Read"
                                    alt="Marked As Read"
                                    class="button info" 
                                    src="img/buttons/text.png"
                                    >';
                    }
                    $html .= '</a>';

                    $read_topics[$page_item['topic_id']]['html'] = $html;
                }

            } elseif ($forum_last_read_date) { //The have a Forum Tracker for threads forum
                $post = \Common\Models\ForumTopicPost::findFirst(
                    [
                        'created > "' . $forum_last_read_date . '" ',
                        'order' => 'id asc'
                    ]
                );

                if (isset($post->id)) {
                    $page = floor($post->topic_ordinal / $page_size) + 1;

                    $url = 'forum/' . $page_item['forum_id'] . '/' .
                        $page_item['topic_id'] . '/' . $page . '#' . $post->topic_ordinal;

                    $html = '<a href="' . $url . '">';
                    if (isset($user_posted_in_topic[$topic_id]) == true) {
                        $html .= '<img 
                                    title="Marked As Read"
                                    alt="Marked As Read"
                                    class="button info" 
                                    src="img/buttons/text_written.png"
                                    >';
                    } else {
                        $html .= '<img 
                                    title="Marked As Read"
                                    alt="Marked As Read"
                                    class="button info" 
                                    src="img/buttons/text.png"
                                    >';
                    }
                    $html .= '</a>';

                    $read_topics[$page_item['topic_id']]['html'] = $html;

                } else { //There are no unread posts so direct user to last post
                    $page = floor($page_item['last_post_topic_ordinal'] / $page_size) + 1;

                    $url = 'forum/' . $page_item['forum_id'] . '/' .
                        $page_item['topic_id'] . '/' . $page . '#' . $page_item['last_post_topic_ordinal'];

                    $html = '<a href="' . $url . '">';
                    if (isset($user_posted_in_topic[$topic_id]) == true) {
                        $html .= '<img 
                                    title="Marked As Read"
                                    alt="Marked As Read"
                                    class="button info" 
                                    src="img/buttons/text_written.png"
                                    >';
                    } else {
                        $html .= '<img 
                                    title="Marked As Read"
                                    alt="Marked As Read"
                                    class="button info" 
                                    src="img/buttons/text.png"
                                    >';
                    }
                    $html .= '</a>';

                    $read_topics[$page_item['topic_id']]['html'] = $html;
                }
            } else { //No tracker so as the user has not read anything in the forum give them the first post to read.
                $read_topics[$page_item['topic_id']]['unread'] = true;

                $url = 'forum/' . $page_item['forum_id'] . '/' .
                    $page_item['topic_id'] . '/1';

                $html = '<a href="' . $url . '">';
                $html .= '<img 
                            title="Unread Replies" 
                            alt="Unread Replies" 
                            class="button success" 
                            src="img/buttons/text.png"
                            >';
                $html .= '</a>';

                $read_topics[$page_item['topic_id']]['html'] = $html;
            }
        }
        return $read_topics;
    }


}
