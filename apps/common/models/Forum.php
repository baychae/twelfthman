<?php

namespace Common\Models;

class Forum extends \Phalcon\Mvc\Model {

    use \Common\Models\Traits\MyTimestampable;
    use \Common\Models\Traits\MySoftDeletable;
    use \Common\Models\Traits\MyVisable;
    
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $forum_type_id;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var string
     */
    public $view_type;
    
    /** MyDeleteStatus
     *
     * @var string
     */
    public $status;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var string
     */
    public $modified;

    public function onConstruct()
    {
        $this->status = "A"; //Every new instance becomes active -- Trait MySoftDeletable
    }

    public function initialize()
    {
        
        $this->addBehavior(new \Phalcon\Mvc\Model\Behavior\SoftDelete(
            ['field' => 'status', 'value' => 'D']
        ));
        
        $this->belongsTo("forum_type_id", "Common\Models\ForumType", "id", [
            "foreignKey" => [ "message" => "The forum type does not exist!" ],
            'alias' => 'ForumType'
        ]);

        $this->hasMany("id", "Common\Models\ForumTopic", "forum_id", [
            'foreignKey'  => [
                "action" => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE,
                "message" => "Cannot delete forum as it still contains topics"
            ],
            'alias' => 'ForumTopic'
        ]);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'id' => 'id',
            'forum_type_id' => 'forum_type_id',
            'title' => 'title',
            'description' => 'description',
            'view_type' => 'view_type',
            'status' => 'status',
            'created' => 'created',
            'modified' => 'modified'
        ];
    }
    
    public function hasNewReplies($user_id)
    {
        //Get the last read post from the forum context
        $read_tracker = \Common\Models\ForumUserReadTrackStats::findFirstByUserId($user_id);
        $read_tracker_forums = $read_tracker->getForumsRead();

        $post_last_read = isset($read_tracker_forums[$this->id]) == true ? $read_tracker_forums[$this->id] : false;

        // Get the most recent post made from Forum Topic Post Tracker
        $forum_last_post_result = \Common\Models\ForumTopicPostStats::aggregate([
            [
                '$match' => ['forum_id' => intval($this->id)]
            ],
            [
                '$group' => [
                    '_id' => ['forum_id' => '$forum_id'],
                    'modified' => ['$max' => '$modified']
                ]
            ]
        ]);

        $post_last_posted = $forum_last_post_result->toArray()[0]["modified"];

        if (!$post_last_read) { //No record of read post so they are all new replies
            return true;
        } elseif ($post_last_read >= $post_last_posted) {
            return false;
        } elseif ($post_last_read <= $post_last_posted) { //If the post last post is greater than the post last read
            return true;
        }
    }
}
