<?php

namespace Common\Models;

class ForumType extends \Phalcon\Mvc\Model
{

    use \Common\Models\Traits\MyTimestampable,
    	\Common\Models\Traits\MyVisable;
    
    /**
     *
     * @var integer
     */
    public $id;
    
    /**
     *
     * @var string
     */
    public $title;
    
    /**
     *
     * @var integer
     */
    public $display_order;
   
    /**
     *
     * @var string
     */
    public $created;
    
    /**
     *
     * @var string
     */
    public $modified;
    
    /**
     *
     * @var string
     */
    public $view_type;
    
    
    
    public function initialize()
    {
        $this->hasMany('id', 'Common\Models\Forum', "forum_type_id", [
            "foreignKey" => [
                "message" => "This Forum Type cannot be deleted as there are forums using it."
            ],
            'alias' => 'Forum'
        ]);
    }
    
    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'id' => 'id',
            'title' => 'title',
            'display_order' => 'display_order',
            'created' => 'created',
            'modified' => 'modified',
        	'view_type' => 'view_type'
        ];
    }
    

}
