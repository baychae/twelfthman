<?php
/**
 * Created by PhpStorm.
 * User: baychae@gmail.com
 * Date: 15/09/2017
 * Time: 17:11
 */

namespace Common\Models;

class ArticleUserReadTrackStats extends \Phalcon\Mvc\MongoCollection
{

    use \Common\Models\Traits\MyTimestampable;
    use \Common\Models\Traits\MongoFix;

    /**
     *
     * @var int
     */
    public $user_id;

    /**
     *
     * @var array of <article_id>:<read_date>
     */
    public $articles;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var string
     */
    public $modified;


    public static function findFirstByUserId($user_id)
    {
        $found_tracker = self::findFirst(array(
            array("user_id" => $user_id)
        ));

        if ($found_tracker == false) {
            $new_tracker = new self();
            $new_tracker->user_id = $user_id;
            if ($new_tracker->save()) {
                return $new_tracker;
            }
        } else {
            return $found_tracker;
        }

        return null;
    }

    public function updateArticleRead($article_id)
    {

        if (!isset($articles[$article_id])) {// If its not set create a new record and enter date
            $articles = (Array)$this->articles;

            $now = \Common\Library\DateTime::getFormattedSplitMilli();
            $articles[$article_id] = $now;
            $this->articles = $articles;

            if ($this->save()) {
                return true;
            } else {
                return false;
            }
        }
    }

}
