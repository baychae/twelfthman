<?php

namespace Common\Models;

use Phalcon\Mvc\Model;


class AdminSiteSection extends Model
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $sub_section;

    public function initialize()
    {
        $this->hasMany("id", "Common\Models\AdminSiteSectionData", "section_id",
            ['alias' => 'AdminSiteSectionData']
        );
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'id' => 'id',
            'name' => 'name',
            'sub_section' => 'sub_section'
        ];
    }

    public static function getNewsSettings($params = NULL)
    {
        return self::findFirst(
            ["conditions" => "name ='news' and " . "sub_section = 'Settings'"]
        );
    }

}
