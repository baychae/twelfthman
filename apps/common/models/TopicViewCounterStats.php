<?php

namespace Common\Models;

class TopicViewCounterStats extends \Phalcon\Mvc\MongoCollection
{
    
    use \Common\Models\Traits\MyTimestampable;
    use \Common\Models\Traits\MongoFix;
    /**
     *
     * @var string
     */
    public $forum_id;
    
    /**
     *
     * @var string
     */
    public $topic_id;
      
     /**
     *
     * @var string
     */
    public $created;
    
    /**
     *
     * @var string
     */
    public $modified;   
    
    
   
}