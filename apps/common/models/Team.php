<?php
/**
 * Author: Richard Sinclair
 * Date: 27/05/2016
 * Time: 11:45
 */

namespace Common\Models;

class Team extends \Phalcon\Mvc\Model
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $league_id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $nickname;

    /**
     *
     * @var string
     */
    public $website;

    /**
     *
     * @var string
     */
    public $image_name;


    public function initialize()
    {
        $this->belongsTo("league_id", "\\Common\\Models\\League", "id", [
            "foreignKey" => [
                "message" => "The league does not exist!"
            ],
            'alias' => 'League'
        ]);

        $this->hasMany("id", "\\Common\\Models\\SiteUser", "team_id",
            [
                "foreignKey" => ["message" => "Members are using this group."],
                "alias" => "SiteUser"
            ]);

        $this->hasMany("id", "\\Common\\Models\\Match", "home_team_id",
            [
                "foreignKey" => ["message" => "There are home matches for this team.."],
                "alias" => "HomeMatches"
            ]);

        $this->hasMany("id", "\\Common\\Models\\Match", "away_team_id",
            [
                "foreignKey" => ["message" => "There are away matches for this team."],
                "alias" => "AwayMatches"
            ]);

    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'id' => 'id',
            'name' => 'name',
            'image_name' => 'image_name',
            'nickname' => 'nickname',
            'website' => 'website',
            'image_name' => 'image_name',
            'league_id' => 'league_id'
        ];
    }

    public function validation()
    {
        $validator = new \Phalcon\Validation();

        $validator->add('name', new \Phalcon\Validation\Validator\Uniqueness(
            [
                'model' => $this,
                'message' => 'This display name is already in use. Please try another one.'
            ]
        ));

        $validator->add('name', new \Phalcon\Validation\Validator\PresenceOf(
            [
                "model" => $this,
                "message" => "You must enter the group name to save it."
            ]
        ));

        return $this->validate($validator);
    }

    public static function getSiteMemberUserId($login_id, $email)
    {
        $user = self::findFirst("login_id = '" . $login_id . "' AND email = '" . $email . "'");
        if (!$user) {
            return false;
        } else {
            return $user->id;
        }
    }

    public function getSiteMembersIndex($params = null)
    {
        $return_array = [];

        $sql = "SELECT DISTINCT LEFT( if(avatar_name is null, login_id, avatar_name), 1)
                FROM site_user
                order by 1 asc";

        $result = new \Phalcon\Mvc\Model\Resultset\Simple(null, $this, $this->getReadConnection()->query($sql, $params));

        $result_array = $result->toArray();

        foreach ($result_array as $key => $value) {
            $val = $value["LEFT( if(avatar_name is null, login_id, avatar_name), 1)"];
            $return_array[] = strtoupper($val);
        }

        return $return_array;

    }
}
