<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 19/06/2017
 * Time: 12:37
 */


namespace Common\Models;

class SiteUserAchievement extends \Phalcon\Mvc\Model
{
    use \Common\Models\Traits\MyTimestampable;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $site_user_id;

    /**
     *
     * @var integer
     */
    public $site_user_achievement_type_id;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var string
     */
    public $modified;

    public function initialize()
    {
        $this->belongsTo('site_user_id', 'Common\\Models\\SiteUser', 'id',
            [
                'alias' => 'SiteUser'
            ]
        );

        $this->belongsTo('site_user_achievement_type',
            'Common\\Models\\SiteUserAchievementType', 'id',
            [
                'alias' => 'SiteUserAchievementType'
            ]
        );

    }

    public function columnMap()
    {
        return [
            'id' => 'id',
            'site_user_id' => 'site_user_id',
            'site_user_achievement_type_id' => 'site_user_achievement_type_id',
            'created' => 'created',
            'modified' => 'modified'
        ];
    }

    public function validation()
    {

    }

}