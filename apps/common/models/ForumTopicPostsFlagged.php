<?php
/**
 * User: Richard Sinclair
 * Date: 29/09/2015
 * Time: 14:28
 */

namespace Common\Models;

use \Phalcon\Mvc\Model\Validator\InclusionIn;

class ForumTopicPostsFlagged extends \Phalcon\Mvc\Model {

    use \Common\Models\Traits\MyTimestampable;
    use \Common\Models\Traits\MySoftDeletable;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $post_id;

    /**
     *
     * @var int
     */
    public $topic_id;

    /**
     *
     * @var int
     */
    public $user_id;

    /** -- MyDeleteStatus
     *
     * @var string for M
     */
    public $status;

    /**
     *
     * @var string
     */
    public $modified;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var string
     */
    public $content_bb;

    /**
     *
     * @var string
     */
    public $content_html;

    public function onConstruct() {
        $this->status = "A"; //Every new instance becomes active -- Trait MySoftDeletable
    }

    public function initialize() {

        $this->addBehavior(new \Phalcon\Mvc\Model\Behavior\SoftDelete(
            [
                'field' => 'status',
                'value' => 'D'
            ]
        ));

        $this->belongsTo("topic_id", "Common\Models\ForumTopic", "id", [
            "foreignKey" => [
                "message" => "The forum thread you are trying to post to no longer exists!"
            ],
            'alias' => 'ForumTopic'
        ]);

        $this->belongsTo("user_id", "Common\Models\SiteUser", "id", [
            "foreignKey" => [
                "message" => "The site user cannot be used to post!"
            ],
            'alias' => 'SiteUser'
        ]);

        $this->belongsTo("post_id", "Common\Models\ForumTopicPost", "id", [
            "foreignKey" => [
                "message" => "The forum post you are trying to report no longer exists!"
            ],
            'alias' => 'ForumTopicPost'
        ]);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap() {
        return [
            'id' => 'id',
            'post_id' => 'post_id',
            'topic_id' => 'topic_id',
            'user_id' => 'user_id',
            'modified' => 'modified',
            'created' => 'created',
            'content_bb' => 'content_bb',
            'content_html' => 'content_html',
            'status' => 'status'
        ];
    }


}