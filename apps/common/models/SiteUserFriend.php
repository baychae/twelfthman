<?php
/**
 * Created by PhpStorm.
 * User: baychae@gmail.com
 * Date: 16/10/2017
 * Time: 14:03
 */

namespace Common\Models;

class SiteUserFriend extends \Phalcon\Mvc\Model
{
    use \Common\Models\Traits\MyTimestampable;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $site_user_id;

    /**
     *
     * @var integer
     */
    public $friend_site_user_id;

    /**
     *
     * @var integer
     */
    public $notified;

    /**
     *
     * @var integer
     */
    public $confirmed;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var string
     */
    public $modified;

    public function initialize()
    {
        $this->setSource('site_user_friend');

        $this->belongsTo(
            'site_user_id',
            'Common\\Models\\SiteUser',
            'id',
            [
                'alias' => 'SiteUser'
            ]
        );
    }

}