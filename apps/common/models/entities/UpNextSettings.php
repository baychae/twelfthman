<?php

namespace Common\Models\Entities;

class UpNextSettings
{

    private $_di;
    private $_modelsManager;

    public $liveScoreSmsActive;
    public $liveScoreEndpointSms;
    public $liveScoreSmsExpectFrom;

    function __construct($controller)
    {

        $this->_di = $controller->getDI();
        $this->_modelsManager = $controller->modelsManager;

        $result = $this->_modelsManager->createBuilder()
            ->from('Common\Models\AdminSiteSectionData')
            ->join("Common\Models\AdminSiteSection")
            ->where("Common\Models\AdminSiteSection.name = 'up_next' "
                . "AND Common\Models\AdminSiteSection.sub_section = 'Settings' "
                . "AND Common\Models\AdminSiteSectionData.name = 'live_score_sms_active' LIMIT 1")
            ->getQuery()
            ->getSingleResult();
        $this->liveScoreSmsActive = $result->value;

        $result = $this->_modelsManager->createBuilder()
            ->from('Common\Models\AdminSiteSectionData')
            ->join("Common\Models\AdminSiteSection")
            ->where("Common\Models\AdminSiteSection.name = 'up_next' "
                . "AND Common\Models\AdminSiteSection.sub_section = 'Settings' "
                . "AND Common\Models\AdminSiteSectionData.name = 'live_score_endpoint_sms' LIMIT 1")
            ->getQuery()
            ->getSingleResult();
        $this->liveScoreEndpointSms = $result->value;

        $result = $this->_modelsManager->createBuilder()
            ->from('Common\Models\AdminSiteSectionData')
            ->join("Common\Models\AdminSiteSection")
            ->where("Common\Models\AdminSiteSection.name = 'up_next' "
                . "AND Common\Models\AdminSiteSection.sub_section = 'Settings' "
                . "AND Common\Models\AdminSiteSectionData.name = 'live_score_sms_expect_from' LIMIT 1")
            ->getQuery()
            ->getSingleResult();
        $this->liveScoreSmsExpectFrom = $result->value;

    }

    public function getLiveScoreSmsActive()
    {
        return $this->liveScoreSmsActive;
    }

    public function getLiveScoreEndpointSms()
    {
        return $this->liveScoreEndpointSms;
    }

    public function getLiveScoreSmsExpectFrom()
    {
        return $this->liveScoreSmsExpectFrom;
    }

    public function save()
    {

        //live_score_sms_active_id
        $phql_get_live_score_sms_active_id = "SELECT Common\Models\AdminSiteSectionData.id "
            . "FROM Common\Models\AdminSiteSectionData "
            . "JOIN Common\Models\AdminSiteSection "
            . "WHERE Common\Models\AdminSiteSection.name = 'up_next' "
            . "AND Common\Models\AdminSiteSection.sub_section = 'Settings' "
            . "AND Common\Models\AdminSiteSectionData.name = 'live_score_sms_active' "
            . "LIMIT 1 ";
        $query = new \Phalcon\Mvc\Model\Query($phql_get_live_score_sms_active_id, $this->_di);
        $result = $query->getSingleResult();//use getSingleResult
        $get_live_score_sms_active_id = $result->id;

        $set_liveScoreSmsActive = "UPDATE Common\Models\AdminSiteSectionData "
            . "SET Common\Models\AdminSiteSectionData.value = '" . $this->liveScoreSmsActive . "'"
            . "WHERE Common\Models\AdminSiteSectionData.id = " . $get_live_score_sms_active_id;

        $query = new \Phalcon\Mvc\Model\Query($set_liveScoreSmsActive, $this->_di);
        $liveScoreSmsActive_result = $query->execute();

        if ($liveScoreSmsActive_result->success() != true) {
            return $liveScoreSmsActive_result;
        }

        //live_score_endpoint_sms
        $phql_get_live_score_endpoint_sms_id = "SELECT Common\Models\AdminSiteSectionData.id "
            . "FROM Common\Models\AdminSiteSectionData "
            . "JOIN Common\Models\AdminSiteSection "
            . "WHERE Common\Models\AdminSiteSection.name = 'up_next' "
            . "AND Common\Models\AdminSiteSection.sub_section = 'Settings' "
            . "AND Common\Models\AdminSiteSectionData.name = 'live_score_endpoint_sms' "
            . "LIMIT 1 ";
        $query = new \Phalcon\Mvc\Model\Query($phql_get_live_score_endpoint_sms_id, $this->_di);
        $result = $query->getSingleResult();//use getSingleResult
        $live_score_endpoint_sms_id = $result->id;

        $set_liveScoreEndpointSms = "UPDATE Common\Models\AdminSiteSectionData "
            . "SET Common\Models\AdminSiteSectionData.value = '" . $this->liveScoreEndpointSms . "'"
            . "WHERE Common\Models\AdminSiteSectionData.id = " . $live_score_endpoint_sms_id;

        $query = new \Phalcon\Mvc\Model\Query($set_liveScoreEndpointSms, $this->_di);
        $liveScoreEndpointSms_result = $query->execute();

        if ($liveScoreEndpointSms_result->success() != true) {
            return $liveScoreEndpointSms_result;
        }

        //live_score_sms_expect_from   liveScoreSmsExpectFrom
        $phql_get_live_score_sms_expect_from_id = "SELECT Common\Models\AdminSiteSectionData.id "
            . "FROM Common\Models\AdminSiteSectionData "
            . "JOIN Common\Models\AdminSiteSection "
            . "WHERE Common\Models\AdminSiteSection.name = 'up_next' "
            . "AND Common\Models\AdminSiteSection.sub_section = 'Settings' "
            . "AND Common\Models\AdminSiteSectionData.name = 'live_score_sms_expect_from' "
            . "LIMIT 1 ";
        $query = new \Phalcon\Mvc\Model\Query($phql_get_live_score_sms_expect_from_id, $this->_di);
        $result = $query->getSingleResult();//use getSingleResult
        $live_score_sms_expect_from_id = $result->id;

        $set_liveScoreEndpointSms = "UPDATE Common\Models\AdminSiteSectionData "
            . "SET Common\Models\AdminSiteSectionData.value = '" . $this->liveScoreSmsExpectFrom . "'"
            . "WHERE Common\Models\AdminSiteSectionData.id = " . $live_score_sms_expect_from_id;

        $query = new \Phalcon\Mvc\Model\Query($set_liveScoreEndpointSms, $this->_di);
        $liveScoreEndpointSms_result = $query->execute();

        if ($liveScoreEndpointSms_result->success() != true) {
            return $liveScoreEndpointSms_result;
        }

        return true;
    }

}