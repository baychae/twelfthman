<?php


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Common\Models\Entities;

class NewsArticleSettings {

    private $_di;
    private $_modelsManager;

    public $maxBlurbs;
    public $other_news_limit;
    public $defaultImage;

    function __construct($controller) {

        $this->_di = $controller->getDI();
        $this->_modelsManager = $controller->modelsManager;

        $result = $this->_modelsManager->createBuilder()
                ->from('Common\Models\AdminSiteSectionData')
                ->join("Common\Models\AdminSiteSection")
                ->where("Common\Models\AdminSiteSection.name = 'news' "
                        . "AND Common\Models\AdminSiteSection.sub_section = 'Settings' "
                        . "AND Common\Models\AdminSiteSectionData.name = 'article_limit' LIMIT 1")
                ->getQuery()
                ->getSingleResult();
        $this->maxBlurbs = $result->value;

        $result = $this->_modelsManager->createBuilder()
            ->from('Common\Models\AdminSiteSectionData')
            ->join("Common\Models\AdminSiteSection")
            ->where("Common\Models\AdminSiteSection.name = 'news' "
                . "AND Common\Models\AdminSiteSection.sub_section = 'Settings' "
                . "AND Common\Models\AdminSiteSectionData.name = 'other_news_limit' LIMIT 1")
            ->getQuery()
            ->getSingleResult();
        $this->other_news_limit = $result->value;

        $result = $this->_modelsManager->createBuilder()
               ->from('Common\Models\AdminSiteSectionData')
               ->join("Common\Models\AdminSiteSection")
               ->where("Common\Models\AdminSiteSection.name = 'news' "
                        . "AND Common\Models\AdminSiteSection.sub_section = 'Settings' "
                        . "AND Common\Models\AdminSiteSectionData.name = 'default_image' LIMIT 1")
               ->getQuery()
               ->getSingleResult();
        $this->defaultImage = $result->value;

    }

    public function getMaxBlurbs() {
        return $this->maxBlurbs;
    }

    public function getDefaultImage() {
        return $this->defaultImage;
    }

    public function save() {

        $phql_get_maxBlurbs_id = "SELECT Common\Models\AdminSiteSectionData.id "
                        . "FROM Common\Models\AdminSiteSectionData "
                        . "JOIN Common\Models\AdminSiteSection "
                        . "WHERE Common\Models\AdminSiteSection.name = 'news' "
                        . "AND Common\Models\AdminSiteSection.sub_section = 'Settings' "
                        . "AND Common\Models\AdminSiteSectionData.name = 'article_limit' "
                        . "LIMIT 1 ";
       $query = new \Phalcon\Mvc\Model\Query($phql_get_maxBlurbs_id , $this->_di);
       $result = $query->getSingleResult();//use getSingleResult
        $maxBlurbs_id = $result->id;

       $set_maxBlurbs =   "UPDATE Common\Models\AdminSiteSectionData "
                        . "SET Common\Models\AdminSiteSectionData.value = ".$this->maxBlurbs." "
                        . "WHERE Common\Models\AdminSiteSectionData.id = ".$maxBlurbs_id;

        $query = new \Phalcon\Mvc\Model\Query($set_maxBlurbs, $this->_di);
       $maxBlurbs_result = $query->execute();
       if ($maxBlurbs_result->success() != true) {
           return $maxBlurbs_result;
       }

        $phql_get_default_image_id = "SELECT Common\Models\AdminSiteSectionData.id "
            . "FROM Common\Models\AdminSiteSectionData "
            . "JOIN Common\Models\AdminSiteSection "
            . "WHERE Common\Models\AdminSiteSection.name = 'news' "
            . "AND Common\Models\AdminSiteSection.sub_section = 'Settings' "
            . "AND Common\Models\AdminSiteSectionData.name = 'default_image' "
            . "LIMIT 1 ";
        $query = new \Phalcon\Mvc\Model\Query($phql_get_default_image_id, $this->_di);
        $result = $query->getSingleResult();//use getSingleResult
        $default_image_id = $result->id;

        $set_default_image = "UPDATE Common\Models\AdminSiteSectionData "
            . "SET Common\Models\AdminSiteSectionData.value = '" . $this->defaultImage . "' "
            . "WHERE Common\Models\AdminSiteSectionData.id = " . $default_image_id;

        $query = new \Phalcon\Mvc\Model\Query($set_default_image, $this->_di);

        $default_image_result = $query->execute();

        if ($default_image_result->success() != true) {
            return $default_image_result;
        }

        $phql_get_other_news_limit_id = "SELECT Common\Models\AdminSiteSectionData.id "
            . "FROM Common\Models\AdminSiteSectionData "
            . "JOIN Common\Models\AdminSiteSection "
            . "WHERE Common\Models\AdminSiteSection.name = 'news' "
            . "AND Common\Models\AdminSiteSection.sub_section = 'Settings' "
            . "AND Common\Models\AdminSiteSectionData.name = 'other_news_limit' "
            . "LIMIT 1 ";
        $query = new \Phalcon\Mvc\Model\Query($phql_get_other_news_limit_id, $this->_di);
        $result = $query->getSingleResult();//use getSingleResult
        $other_news_limit_id = $result->id;

        $set_other_news_limit = "UPDATE Common\Models\AdminSiteSectionData "
            . "SET Common\Models\AdminSiteSectionData.value = " . $this->other_news_limit . " "
            . " WHERE Common\Models\AdminSiteSectionData.id = " . $other_news_limit_id;

        $query = new \Phalcon\Mvc\Model\Query($set_other_news_limit, $this->_di);
        $maxBlurbs_result = $query->execute();
        if ($maxBlurbs_result->success() != true) {
            return $maxBlurbs_result;
        }

       return true;
    }

}