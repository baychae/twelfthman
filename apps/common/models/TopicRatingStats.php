<?php

namespace Common\Models;

class TopicRatingStats extends \Phalcon\Mvc\MongoCollection
{
    
    use \Common\Models\Traits\MyTimestampable;
    use \Common\Models\Traits\MongoFix;
        /**
     *
     * @var int
     */
    public $forum_id;
    
    /**
     *
     * @var int
     */
    public $topic_id;
    
    /**
     *
     * @var array
     */
    public $users;
      
     /**
     *
     * @var string
     */
    public $created;
    
    /**
     *
     * @var string
     */
    public $modified;
    
    
        
    public static function getForumTopicRatings($topics) {
        foreach ($topics as $topic) {
            $rating = self::getTopicRating($topic['topic_id']);
            $ratings[$topic['topic_id']] = $rating;
        }
        
        return $ratings;
    }

    public static function getTopicRating($topic_id) {
        $result = self::findFirst(array(
            array("topic_id" => intval($topic_id))
            ));
        
        $rating  = isset($result->users) ? count($result->users) : NULL;
        
        return $rating;  
    }
    
    
}