<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 15/09/2016
 * Time: 12:19
 */

namespace Common\Models;


class Notification extends \Phalcon\Mvc\MongoCollection
{
    use \Common\Models\Traits\MyTimestampable;
    use \Common\Models\Traits\MongoFix;
    /**
     *
     * @var int
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $sent;

    /**
     *
     * @var string
     */
    public $type;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var string
     */
    public $modified;

    public function beforeValidationOnCreate()
    {
        $this->sent = 'N';
    }

    public function initialize()
    {

    }


}