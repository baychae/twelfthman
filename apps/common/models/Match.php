<?php
/**
 * AUTHOR: Richard Sinclair <rich.sinclair@gmail.com>
 * Date: 25/10/2016
 * Time: 17:40
 */

namespace Common\Models;


class Match extends \Phalcon\Mvc\Model
{
    use \Common\Models\Traits\MyTimestampable;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $home_team_id;

    /**
     *
     * @var integer
     */
    public $away_team_id;

    /**
     *
     * @var integer
     */
    public $home_score;

    /**
     *
     * @var integer
     */
    public $away_score;

    /**
     *
     * @var integer
     */
    public $preview_article_id;

    /**
     *
     * @var integer
     */
    public $report_article_id;

    /**
     *
     * @var string
     */
    public $start;

    public $kickoff_date;

    public $kickoff_time;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var string
     */
    public $modified;


    public function initialize()
    {
        $this->belongsTo("home_team_id", "Common\\Models\\Team", "id", [
            "foreignKey" => [
                "message" => "the home team does not exist!"
            ],
            'alias' => 'HomeTeam'
        ]);

        $this->belongsTo("away_team_id", "Common\\Models\\Team", "id", [
            "foreignKey" => [
                "message" => "the away team does not exist!"
            ],
            'alias' => 'AwayTeam'
        ]);

        $this->hasOne("preview_article_id", "Common\\Models\\NewsArticle", "id", [
            "foreignKey" => [
                "allowNulls" => true,
                "message" => "the preview article does not exist!"
            ],
            'alias' => 'PreviewNewsArticle'

        ]);

        $this->hasOne("report_article_id", "Common\\Models\\NewsArticle", "id", [
            "foreignKey" => [
                "allowNulls" => true,
                "message" => "the report article does not exist!"
            ],
            'alias' => 'ReportNewsArticle'

        ]);


    }

//    public function getPreviewNewsArticle($parameters = null)
//    {
//        return $this->getRelated("PreviewNewsArticle", $parameters);
//    }

    public function beforeValidation()
    {
        $raw_date = \DateTime::createFromFormat('d/m/Y H:i', $this->kickoff_date . " " . $this->kickoff_time);
        $this->start = $raw_date->format("Y-m-d H:i:s");
    }

    public function beforeSave()
    {
        $our_team_name = $this->getDI()->getConfig()->site->team;

        $our_team = \Common\Models\Team::findFirst([
            "columns" => "id",
            "conditions" => "name = '" . $our_team_name . "' "

        ]);

        $our_team_id = $our_team->id;

        if ($this->home_team_id == $our_team_id) {// Playing at home
            $this->home_score > $this->away_score ? $this->upshot = 1 : $this->upshot = 0;
        } else {                                 // Playing away
            $this->away_score > $this->home_score ? $this->upshot = 1 : $this->upshot = 0;
        }
    }




    public function afterFetch()
    {
        $raw_date = \DateTime::createFromFormat('Y-m-d H:i:s', $this->start);
        $this->kickoff_date = $raw_date->format('d/m/Y');
        $this->kickoff_time = $raw_date->format('H:i');
    }

    public function afterSave()
    {
        $raw_date = \DateTime::createFromFormat('d/m/YH:i', $this->kickoff_date . $this->kickoff_time);
        $this->start = $raw_date->format("Y-m-d H:i:s");
    }

    public function validation()
    {
        $validator = new \Phalcon\Validation();

        $validator->add("kickoff_date", new \Phalcon\Validation\Validator\Date([
            'format' => [
                'kickoff_date' => 'd/m/Y'
            ],
            'message' => [
                'kickoff_date' => 'the Kickoff date is invalid.'
            ]
        ]));

        $validator->add("kickoff_time", new \Phalcon\Validation\Validator\Date([
            'format' => [
                'kickoff_time' => 'H:i'
            ],
            'message' => [
                'kickoff_time' => 'the Kickoff time is invalid.'
            ]
        ]));

        $validator->add("start", new \Phalcon\Validation\Validator\Date([
            'format' => [
                'start' => 'Y-m-d H:i:s'
            ],
            'message' => [
                'start' => 'the start date time is invalid.'
            ]
        ]));

        return $this->validate($validator);
    }

    public static function getLiveMatch()
    {
        $today = date('Y-m-d');
        $tommorrow = date("Y-m-d", strtotime("+1 day"));

        $live_match = self::findFirst('start >= "' . $today . '" AND start < "' . $tommorrow . '"');

        if (isset($live_match->id)) {
            return $live_match;
        }

        return false;
    }
}