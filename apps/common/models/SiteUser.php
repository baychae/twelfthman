<?php

namespace Common\Models;

class SiteUser extends \Phalcon\Mvc\Model
{
    use \Common\Models\Traits\MyTimestampable;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $login_id;

    /**
     *
     * @var string
     */
    public $level;

    /**
     *
     * @var string
     */
    public $first_name;

    /**
     *
     * @var string
     */
    public $last_name;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var string
     */
    public $signature;

    /**
     *
     * @var string
     */
    public $avatar_name;

    /**
     *
     * @var string
     */
    public $avatar_file_name;

    /**
     *
     * @var string
     */
    public $password;

    /**
     *
     * @var string
     */
    public $password_legacy;

    /**
     *
     * @var string
     */
    public $active;

    /**
     *
     * @var integer
     */
    public $team_id;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var string
     */
    public $modified;

    public function initialize()
    {
        $this->setSource('site_user');
        $this->hasMany(
            "id",
            "Common\Models\NewsArticle",
            "user_id",
            [
                "foreignKey" => ["message" => "The user cannot be deleted. They have news articles."],
                "alias" => "NewsArticle"
            ]
        );

        $this->hasMany(
            "id",
            "Common\Models\ForumTopic",
            "user_id",
            [
                "foreignKey" => ["message" => "The user cannot be deleted. They have created a forum topic."],
                "alias" => "ForumTopic"
            ]
        );

        $this->hasMany(
            "id",
            "Common\Models\ForumTopicPost",
            "user_id",
            [
                "foreignKey" => ["message" => "The user cannot be deleted. They made forum posts."],
                "alias" => "ForumTopicPost"
            ]
        );

        $this->hasMany(
            "id",
            "Common\Models\ForumTopicPost",
            "editor_user_id",
            [
                "foreignKey" => ["message" => "The user cannot be deleted. They have edited posts."],
                "alias" => "EditedForumTopicPost"
            ]
        );

        $this->hasOne(
            "team_id",
            "Common\Models\Team",
            "id",
            [
                "alias" => "Team"
            ]
        );

        $this->hasMany(
            "id",
            "Common\\Models\\SiteUserAchievement",
            "site_user_id",
            [
                "alias" => "SiteUserAchievement"
            ]
        );

        $this->hasManyToMany(
            'id',
            'Common\\Models\\SiteUserAchievement',
            'site_user_id',
            'site_user_achievement_type_id',
            'Common\\Models\\SiteUserAchievementType',
            'id',
            ['alias' => 'SiteUserAchievementType']
        );

        $this->hasOne(
            "id",
            "Common\\Models\\SiteUserFriend",
            "site_user_id",
            [
                "alias" => "SiteUserFriend"
            ]
        );
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'id' => 'id',
            'login_id' => 'login_id',
            'level' => 'level',
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'email' => 'email',
            'signature_bb' => 'bbcode_field',
            'signature_html' => 'signature_html',
            'avatar_name' => 'avatar_name',
            'avatar_file_name' => 'avatar_file_name',
            'password' => 'password',
            'password_legacy' => 'password_legacy',
            'active' => 'active',
            'team_id' => 'team_id',
            'fav_away_ground' => 'favourite_away_ground',
            'fav_all_player' => 'favourite_all_time_player',
            'fav_curr_player' => 'favourite_current_player',
            'created' => 'created',
            'modified' => 'modified'
        ];
    }


    public function validation()
    {
        $validator = new \Phalcon\Validation();

        $validator->add('email', new \Phalcon\Validation\Validator\Uniqueness(
            ["message" => "This email address is already in use!"]
        ));

        $validator->add('login_id', new \Phalcon\Validation\Validator\Uniqueness(
            ["message" => "This login id is already in use. Please try another one."]
        ));

        $validator->add('avatar_name', new \Phalcon\Validation\Validator\Uniqueness(
            ["message" => "This display name is already in use. Please try another one."]
        ));

        $validator->add('active', new \Phalcon\Validation\Validator\InclusionIn(
            [
                "field" => "active",
                "domain" => ["Y", "N", "L"],
                "message" => "Active value is incorrect."
            ]
        ));

        $validator->add('level', new \Phalcon\Validation\Validator\InclusionIn(
            [
                "field" => "level",
                "domain" => ['sysadmin', 'admin', 'poweruser', 'user'],
                "message" => "Given level is not an acceptable domain."
            ]
        ));

        return $this->validate($validator);
    }

    public static function getSiteMemberUserId($login_id, $email)
    {
        $user = self::findFirst("login_id = '" . $login_id . "' AND email = '" . $email . "'");
        if (!$user) {
            return false;
        } else {
            return $user->id;
        }
    }

    public function getSiteMembersIndex($params = null)
    {
        $return_array = [];

        $sql = "SELECT DISTINCT LEFT( if(avatar_name is null, login_id, avatar_name), 1)
                FROM site_user
                order by 1 asc";

        $result = new \Phalcon\Mvc\Model\Resultset\Simple(
            null,
            $this,
            $this->getReadConnection()->query($sql, $params)
        );

        $result_array = $result->toArray();

        foreach ($result_array as $key => $value) {
            $val = $value["LEFT( if(avatar_name is null, login_id, avatar_name), 1)"];
            $return_array[] = strtoupper($val);
        }

        return $return_array;
    }

    public function getDisplayName()
    {
        return isset($this->avatar_name) ? $this->avatar_name : $this->login_id;
    }

    public static function getAchievementImageNamesForUsers($ids = null)
    {
        $users = self::find(
            [
                "id IN ({ids:array})",
                'bind' => [
                    'ids' => $ids
                ],
            ]
        );

        $achievements = [];

        foreach ($users as $user) {
            foreach ($user->siteUserAchievementType as $type) {
                $achievements[$user->id] [] = [
                    "file_name" => $type->file_name,
                    "title" => $type->title,
                ];
            }

        }

        return $achievements;
    }


}
