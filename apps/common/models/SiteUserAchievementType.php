<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 19/06/2017
 * Time: 12:37
 */

namespace Common\Models;

class SiteUserAchievementType extends \Phalcon\Mvc\Model
{
    use \Common\Models\Traits\MyTimestampable;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */
    public $file_name;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var string
     */
    public $modified;

    public function initialize()
    {
        $this->hasManyToMany(
            'id',
            'Common\\Models\\SiteUserAchievement',
            'site_user_achievement_type_id',
            'site_user_id',
            'Common\\Models\\SiteUser',
            'id',
            ['alias' => 'SiteUser']
        );
    }

    public function columnMap()
    {
        return [
            'id' => 'id',
            'title' => 'title',
            'file_name' => 'file_name',
            'created' => 'created',
            'modified' => 'modified'
        ];
    }

    public function validation()
    {
        $validator = new \Phalcon\Validation();

        $validator->add('title', new \Phalcon\Validation\Validator\Uniqueness(
            ["message" => "This achievement title is already in use!"]
        ));

        $validator->add('file_name', new \Phalcon\Validation\Validator\Uniqueness(
            ["message" => "This file name is already in use!"]
        ));

        return $this->validate($validator);

    }

}