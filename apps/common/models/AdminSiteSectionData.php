<?php

namespace Common\Models;

class AdminSiteSectionData extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;
     
    /**
     *
     * @var integer
     */
    public $section_id;
     
    /**
     *
     * @var string
     */
    public $name;
     
    /**
     *
     * @var string
     */
    public $value;
    
    public function initialize()
    {
        $this->belongsTo("section_id", "\Common\Models\AdminSiteSection", "id", [
            "foreignKey" => ["message" => "The section does not exist!"],
            'alias' => 'AdminSiteSection'
        ]);
    }


    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'id' => 'id', 
            'section_id' => 'section_id', 
            'name' => 'name', 
            'value' => 'value'
        ];
    }
    
    

}
