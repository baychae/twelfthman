<?php

namespace Common\Models;

use \Phalcon\Mvc\Model\Validator\InclusionIn;

class ForumTopicPost extends \Phalcon\Mvc\Model {
    
    use \Common\Models\Traits\MyTimestampable;
    use \Common\Models\Traits\MySoftDeletable;
    use \Common\Models\Traits\MyVisable;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var int
     */
    public $topic_id;

    /**
     *
     * @var integer
     */
    public $topic_ordinal;

    /**
     *
     * @var int
     */
    public $user_id;

    /**
     *
     * @var int
     */
    public $editor_user_id;
    
    /** -- MySoftDeletable
    *
    * @var string for M
    */
    public $status;

    /** -- MyVisable
     *
     * @var string for M
     */
    public $view_type;

    /** 
     *
     * @var string
     */
    public $modified;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var string
     */
    public $content_bb;

    /**
     *
     * @var string
     */
//    public $content_html;

    public function onConstruct() {
        $this->status = "A"; //Every new instance becomes active -- Trait MySoftDeletable
    }
    
    public function initialize() {
        
        $this->addBehavior(new \Phalcon\Mvc\Model\Behavior\SoftDelete(
            [
            'field' => 'status',
            'value' => 'D'
            ]
        ));

        $this->belongsTo("topic_id", "Common\Models\ForumTopic", "id", [
            "foreignKey" => [
                "message" => "The forum topic you are trying to create no longer exists!"
            ],
            'alias' => "ForumTopic"
        ]);

        $this->belongsTo("user_id", "Common\Models\SiteUser", "id", [
            "foreignKey" => [
                "message" => "The site user cannot be used to post!"
            ],
            'alias' => 'SiteUser'
        ]);

        $this->belongsTo("editor_user_id", "Common\Models\SiteUser", "id", [
            "foreignKey" => [
                "allowNulls" => true,
                "message" => "The editor site user cannot be used to post!"
            ],
            'alias' => 'EditorSiteUser'
        ]);

        $this->hasMany("id", "Common\Models\ForumTopicPostsFlagged", "post_id", [
            "foreignKey" => [
                "message" => "This post has active flags against it and cannot be deleted!"
            ]
        ]);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'id' => 'id',
            'topic_id' => 'topic_id',
            'topic_ordinal' => 'topic_ordinal',
            'user_id' => 'user_id',
            'modified' => 'modified',
            'created' => 'created',
            'content_bb' => 'content_bb',
//            'content_html' => 'content_html',
            'status' => 'status',
            'view_type' => 'view_type',
            'editor_user_id' => 'editor_user_id'
        ];
    }

    public function beforeSave()
    {
        $last_recorded_ordinal = \Common\Models\ForumTopicPost::findFirst(
            [
                'topic_id = ' . $this->topic_id,
                'order' => 'id desc'
            ]
        );

        $this->topic_ordinal = $last_recorded_ordinal->topic_ordinal + 1;
    }

    public function afterSave()
    {

        $forum_topic = \Common\Models\ForumTopic::findFirstById($this->topic_id);

        //This goes in the if statement
        $forum_replies = \Common\Models\ForumTopicPostStats::findFirst([
            ["topic_id" => intval($forum_topic->id)]
        ]);

        if (is_object($forum_replies) == false) {
            $forum_replies = new \Common\Models\ForumTopicPostStats();
            $forum_replies->topic_id = intval($forum_topic->id);
            $forum_replies->forum_id = intval($forum_topic->forum_id);
            $forum_replies->post_id = intval($this->id);
            $forum_replies->post_count = 1;

            if ($forum_replies->save() == false) {
                foreach ($forum_replies->getMessages() as $message) {
                    echo "Message: " . $message;
                }
            }
        } else {
            $forum_replies->post_id = intval($this->id);
            $forum_replies->post_count = $forum_replies->post_count + 1;

            if ($forum_replies->save() == false) {
                foreach ($forum_replies->getMessages() as $message) {
                    echo $message, "\n";
                }
            }
        }
    }

    public function getUrl($page_size = null)
    {
        $topic = $this->getForumTopic();
        $forum_id = $topic->forum_id;
        $topic_id = $this->topic_id;
        $post_count = $topic->getForumTopicPost()->count();
        $page = floor($post_count / $page_size) + 1;

        $post_url = "forum/" . $forum_id . "/" . $topic_id . "/" . $page . "#" . $post_count;

        return $post_url;
    }

    public function getAuthorisedUser($auth)
    {
        $auth_token_user_email = $auth["email"];
        $auth_token_user_login = $auth["name"];

        $logged_in_user = \Common\Models\SiteUser::findFirst([
            'email = "'.$auth_token_user_email.'" AND login_id = "'.$auth_token_user_login.'"'
        ]);

        $logged_in_user_email = $logged_in_user->email;
        $logged_in_user_permissions = $logged_in_user->level;

        $post_owner = $this->getSiteUser();
        $post_owner_email = $post_owner->email;

        if ($post_owner_email == $logged_in_user_email) {
            return $post_owner;
        } elseif ($logged_in_user_permissions == 'sysadmin' || $logged_in_user_permissions == 'admin') {
            return $logged_in_user;
        } else {
            return false;
        }
    }

    public function getEditorName()
    {
        if ($this->editor_user_id) {
            $user = $this->getEditorSiteUser();
            $user_name = $user->avatar_name > '' ? $user->avatar_name : $user->login_id;
            return $user_name;
        } else {
            return false;
        }
    }

    public static function getForumTopicPostView($topic_id = null)
    {
        if (!$topic_id) {
            return false;
        }

        $db = \Phalcon\Di::getDefault()->getDb();

        $sql = 'select ' .
            'COALESCE(su1.avatar_name, su1.login_id) as op_display_name, ' .
            'ftp.created as op_post_created, ' .
            'ftp.modified as op_post_modified, ' .
            'ft.forum_id as op_forum_id, ' .
            'ftp.topic_id as op_topic_id, ' .
            'ftp.id as op_post_id, ' .
            'su1.avatar_file_name as op_avatar_file_name, ' .
            'su1.created as op_site_user_created_date, ' .
            't.image_name as op_team_image_name, ' .
            'COALESCE(su2.avatar_name, su2.login_id) as ed_display_name, ' .
            'ftp.content_bb as op_post_content_bb ' .
            'from forum_topic_post ftp ' .
            'join forum_topic ft on ft.id = ftp.topic_id ' .
            'join site_user su1 on ftp.user_id = su1.id ' .
            'left join site_user su2 on ftp.editor_user_id = su2.id ' .
            'left join team t on t.id = su1.team_id ' .
            'where ftp.topic_id = ? ' .
            'ORDER BY ftp.id ASC';

        $query = $db->query(
            $sql,
            [
                $topic_id,
            ]
        );

        return $query->fetchAll();
    }


}
