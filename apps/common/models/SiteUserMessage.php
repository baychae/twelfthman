<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 13/01/2016
 * Time: 15:09
 */

namespace Common\Models;

class SiteUserMessage extends \Phalcon\Mvc\MongoCollection
{
    use \Common\Models\Traits\MyTimestampable;
    use \Common\Models\Traits\MySoftDeletable;
    use \Common\Models\Traits\MongoFix;
    /**
     *
     * @var string
     */
    public $to;

    /**
     *
     * @var int
     */
    public $to_site_user_id;

    /**
     *
     * @var string
     */
    public $from;

    /**
     *
     * @var int
     */
    public $from_site_user_id;

    /**
     *
     * @var string
     */
    public $subject;

    /**
     *
     * @var string
     */
    public $uri;

    /**
     *
     * @var string
     */
    public $body_bb;

    /**
     *
     * @var string
     */
    public $body_plaintext;

    /**
     *
     * @var int
     */
    public $recipient_read;

    /**
     *
     * @var int
     */
    public $is_copy;

    /**
     *
     * @var string
     */
    public $status;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var string
     */
    public $modified;

    protected $session;

    public function initialize()
    {
        $this->addBehavior(new \Phalcon\Mvc\Collection\Behavior\SoftDelete(
            array(
                'field' => 'status',
                'value' => 'D'
            )
        ));

        $this->status = "A";
    }

    public function send()
    {
        $send_message = new \Common\Models\SiteUserMessage();
        $send_message->to = $this->to;
        $send_message->to_site_user_id = $this->to_site_user_id;
        $send_message->from = $this->from;
        $send_message->from_site_user_id = $this->from_site_user_id;
        $send_message->subject = $this->subject;
        $send_message->body_bb = $this->body_bb;
        $send_message->body_plaintext = $this->body_plaintext;
        $send_message->status = "A";
        $send_message->recipient_read = 0;
        $send_message->is_copy = 0; // Ensure this can only be seen by recipient for soft delete behaviour
        $send_message->uri = $this->uri . "-" . uniqid();

        $this->uri = $this->uri . "-" . uniqid();

        $this->status = "A";

        $this->is_copy = 1; //Ensure that this can only be read by the sender for soft delete behaviour

        $send_message->save();

        if ($send_message->save()) {
            return $this->save();
        } else {
            return $send_message;
        }
        return false;
    }

    public function validation()
    {
        $this->session = $this->getDI()->getSession();

        $this->validate(
            new \Phalcon\Mvc\Model\Validator\PresenceOf(
                [
                    "field" => "to",
                    "message" => "Message must include a recipient!",
                ]
            )
        );
        if ($this->validationHasFailed() == true) {
            $this->session->set("new-message-subject", $this->subject);
            $this->session->set("new-message-body", $this->body_bb);
            return false;
        }

        $this->validate(
            new \Phalcon\Mvc\Model\Validator\PresenceOf(
                [
                    "field" => "from",
                    "message" => "Message cannot be sent due to an internal error. Please contact admin."
                ]
            )
        );
        if ($this->validationHasFailed() == true) {
            return false;
        }

        $this->validate(
            new \Phalcon\Mvc\Model\Validator\PresenceOf(
                [
                    "field" => "subject",
                    "message" => "Message must include a subject!"
                ]
            )
        );
        if ($this->validationHasFailed() == true) {
            $this->session->set("new-message-to", $this->to);
            $this->session->set("new-message-body", $this->body_bb);
            return false;
        }

        $this->validate(
            new \Phalcon\Mvc\Model\Validator\PresenceOf(
                [
                    "field" => "uri",
                    "message" => "Please contact admin! Uri not present."
                ]
            )
        );
        if ($this->validationHasFailed() == true) {
            return false;
        }

        if ($this->recipientUserNotValidated()) {
            $message = new \Phalcon\Mvc\Model\Message(
                "Could not find the member to send message to!",
                "to",
                "InvalidValue"
            );
            $this->appendMessage($message);
            $this->session->set("new-message-subject", $this->subject);
            $this->session->set("new-message-body", $this->body_bb);

            return false;
        }

        if ($this->returnUserNotValidated()) {
            $message = new \Phalcon\Mvc\Model\Message(
                "Message not sent. Please report to the administrator!",
                "from",
                "InvalidValue"
            );
            $this->appendMessage($message);

            $this->session->set("new-message-subject", $this->subject);
            $this->session->set("new-message-body", $this->body_bb);
            return false;
        }

        if ($this->sendingToSelf()) {
            $message = new \Phalcon\Mvc\Model\Message(
                "Message not sent. You cannot send a message to yourself!",
                "from",
                "InvalidValue"
            );

            $this->appendMessage($message);

            $this->session->set("new-message-subject", $this->subject);
            $this->session->set("new-message-body", $this->body_bb);
            return false;
        }

        if ($this->uriNotUnique()) {
            $message = new \Phalcon\Mvc\Model\Message(
                "Message not sent. Contact an admin. System not generating unique uri.",
                "uri",
                "InvalidValue"
            );
            $this->appendMessage($message);
            return false;
        }
    }

    protected function recipientUserNotValidated()
    {
        $user = \Common\Models\SiteUser::findFirst(
            [
                "conditions" => "login_id = ?1 OR avatar_name = ?1",
                "bind" => [1 => $this->to]
            ]
        );

        if (isset($user->id)) { //If the recipient does exist then
            $this->to_site_user_id = $user->id;
            return false;
        }

        return true;
    }

    protected function returnUserNotValidated()
    {
        $recipient_user = \Common\Models\SiteUser::findFirst(
            [
                "conditions" => "login_id = ?1 OR avatar_name = ?1",
                "bind" => [1 => $this->from]
            ]
        );

        if (isset($recipient_user->id)) {
            $this->from_site_user_id = $recipient_user->id;
            return false;
        }

        return true;
    }

    protected function sendingToSelf()
    {
        $to_user = \Common\Models\SiteUser::findFirst(
            [
                "conditions" => "login_id = ?1 OR avatar_name = ?1",
                "bind" => [1 => $this->to]
            ]
        );

        $from_user = \Common\Models\SiteUser::findFirst("login_id = '" . $this->from . "'");

        if ($to_user->id === $from_user->id) {
            return true;
        }

        return false;
    }

    protected function uriNotUnique()
    {
        $message = self::findFirst([["uri" => $this->uri]]);

        if (isset($message->uri)) { // Failing clause
            if ($message->_id != $this->_id) {
                return true;
            }
        }

        return false;
    }


}