<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 20/09/2016
 * Time: 14:58
 */

namespace Common\Models;

class PasswordReminderReceipt extends \Phalcon\Mvc\MongoCollection
{

    use \Common\Models\Traits\MyTimestampable;
    use \Common\Models\Traits\MongoFix;
    /**
     *
     * @var string
     */
    public $uuid;


    /**
     *
     * @var int
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $notification_id;

    /**
     *
     * @var string
     */
    public $used;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var string
     */
    public $modified;

    public function beforeValidationOnCreate()
    {
        $this->used = 'N';
    }

    public function validation()
    {

        $this->validate(
            new \Phalcon\Mvc\Model\Validator\InclusionIn(
                [
                    "field" => "used",
                    "message" => "Used must be either Y or N.",
                    "domain" => [
                        "Y",
                        "N"
                    ]
                ]
            ));

        $this->validate(
            new \Phalcon\Mvc\Model\Validator\PresenceOf(
                [
                    "field" => "notification_id",
                    "message" => "Notification id must be present. "
                ]
            ));

        $this->validate(
            new \Phalcon\Mvc\Model\Validator\PresenceOf(
                [
                    "field" => "uuid",
                    "message" => "Uuid must be present. "
                ]
            ));

        if ($this->validationHasFailed() == true) {
            return false;
        }
    }

    public function initialize()
    {
    }

}