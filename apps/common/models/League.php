<?php
/**
 * Created by PhpStorm.
 * User: Richard Sinclair
 * Date: 18/10/2016
 * Time: 17:47
 */

namespace Common\Models;

class League extends \Phalcon\Mvc\Model
{

    public function initialize()
    {
        $this->hasMany("id", "\\Common\\Models\\Team", "league_id", [
            'alias' => 'Team',
            "message" => "League cannot be deleted has it still has teams in it."
        ]);

    }

    public function columnMap()
    {
        return [
            'id' => 'id',
            'name' => 'name',
            'position' => 'position',
            'link' => 'link'
        ];
    }

    public function validation()
    {
        $validator = new \Phalcon\Validation();

        $validator->add(
            "name",
            new \Phalcon\Validation\Validator\Uniqueness([
                "message" => "the name must be unique!"
            ])
        );

        $validator->add(
            "position",
            new \Phalcon\Validation\Validator\Uniqueness([
                "message" => "the position must be unique!"
            ])
        );

        $validator->add(
            "link",
            new \Phalcon\Validation\Validator\Uniqueness([
                "message" => "the position must be unique!"
            ])

        );

        $validator->add(
            "name",
            new \Phalcon\Validation\Validator\PresenceOf([
                "message" => "the name must be unique!"
            ])
        );

        $validator->add(
            "position",
            new \Phalcon\Validation\Validator\PresenceOf([
                "message" => "the name must be unique!"
            ])
        );

        $validator->add(
            "link",
            new \Phalcon\Validation\Validator\PresenceOf([
                "message" => "the name must be unique!"
            ])
        );

        return $this->validate($validator);
    }


}