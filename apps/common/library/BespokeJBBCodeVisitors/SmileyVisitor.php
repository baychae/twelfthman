<?php

namespace BespokeJBBCodeVisitors;

/**
 * This visitor is an example of how to implement smiley parsing on the JBBCode
 * parse graph. It converts :) into image tags pointing to /smiley.png.
 *
 * @author jbowens
 * @since April 2013
 */
class SmileyVisitor implements \JBBCode\NodeVisitor, \Phalcon\Di\InjectionAwareInterface
{

    protected $di;

    public function setDI(\Phalcon\DiInterface $di)
    {
        $this->di = $di;
    }

    public function getDI()
    {
        return $this->di;
    }

    public function visitDocumentElement(\JBBCode\DocumentElement $documentElement)
    {
        foreach ($documentElement->getChildren() as $child) {
            $child->accept($this);
        }
    }

    public function visitTextNode(\JBBCode\TextNode $textNode)
    {

        $url = \Phalcon\DI::getDefault()->getUrl();
        /* Convert :) into an image tag. */

        $textNode->setValue(
            str_replace(
                '[:badtongue:]',
                '<img title="badtongue" alt="badtongue" src="' . $url->get("img/emoticons/Smilies/badtongue.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:bigsmile:]',
                '<img title="bigsmile" alt="bigsmile" src="' . $url->get("img/emoticons/Smilies/bigsmile.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:blink:]',
                '<img title="blink" alt="blink" src="' . $url->get("img/emoticons/Smilies/blink.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:blush:]',
                '<img title="blush" alt="blush" src="' . $url->get("img/emoticons/Smilies/blush.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:bounce:]',
                '<img title="bounce" alt="bounce" src="' . $url->get("img/emoticons/Smilies/bounce.gif") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:content:]',
                '<img title="content" alt="content" src="' . $url->get("img/emoticons/Smilies/content.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:cool:]',
                '<img title="cool" alt="cool" src="' . $url->get("img/emoticons/Smilies/cool.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:cry:]',
                '<img title="cry" alt="cry" src="' . $url->get("img/emoticons/Smilies/cry.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:dead:]',
                '<img title="dead" alt="dead" src="' . $url->get("img/emoticons/Smilies/dead.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:elephant:]',
                '<img title="elephant" alt="elephant" src="' . $url->get("img/emoticons/Smilies/elephant.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:erm:]',
                '<img title="erm" alt="erm" src="' . $url->get("img/emoticons/Smilies/erm.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:evil:]',
                '<img title="evil" alt="evil" src="' . $url->get("img/emoticons/Smilies/evil.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:finger:]',
                '<img title="finger" alt="finger" src="' . $url->get("img/emoticons/Smilies/finger.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:flag:]',
                '<img title="flag" alt="flag" src="' . $url->get("img/emoticons/Smilies/flag.gif") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:glare:]',
                '<img title="glare" alt="glare" src="' . $url->get("img/emoticons/Smilies/glare.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:grr:]',
                '<img title="grr" alt="grr" src="' . $url->get("img/emoticons/Smilies/grr.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:happy:]',
                '<img title="happy" alt="happy" src="' . $url->get("img/emoticons/Smilies/happy.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:heh:]',
                '<img title="heh" alt="heh" src="' . $url->get("img/emoticons/Smilies/heh.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:hm:]',
                '<img title="hm" alt="hm" src="' . $url->get("img/emoticons/Smilies/hm.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:huh:]',
                '<img title="huh" alt="huh" src="' . $url->get("img/emoticons/Smilies/huh.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:laugh:]',
                '<img title="laugh" alt="laugh" src="' . $url->get("img/emoticons/Smilies/laugh.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:mal:]',
                '<img title="mal" alt="mal" src="' . $url->get("img/emoticons/Smilies/mal.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:megson:]',
                '<img title="megson" alt="megson" src="' . $url->get("img/emoticons/Smilies/megson.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:ninja:]',
                '<img title="ninja" alt="ninja" src="' . $url->get("img/emoticons/Smilies/ninja.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:owl:]',
                '<img title="owl" alt="owl" src="' . $url->get("img/emoticons/Smilies/owl.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:pig:]',
                '<img title="pig" alt="pig" src="' . $url->get("img/emoticons/Smilies/pig.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:pinch:]',
                '<img title="pinch" alt="pinch" src="' . $url->get("img/emoticons/Smilies/pinch.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:plain:]',
                '<img title="plain" alt="plain" src="' . $url->get("img/emoticons/Smilies/plain.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:rolleyes:]',
                '<img title="rolleyes" alt="rolleyes" src="' . $url->get("img/emoticons/Smilies/rolleyes.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:sad:]',
                '<img title="sad" alt="sad" src="' . $url->get("img/emoticons/Smilies/sad.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:salute:]',
                '<img title="salute" alt="salute" src="' . $url->get("img/emoticons/Smilies/salute.png") . '"/>',
                $textNode->getValue()
            )
        );


        $textNode->setValue(
            str_replace(
                '[:shock:]',
                '<img title="shock" alt="shock" src="' . $url->get("img/emoticons/Smilies/shock.png") . '"/>',
                $textNode->getValue()
            )
        );


        $textNode->setValue(
            str_replace(
                '[:sick:]',
                '<img title="sick" alt="sick" src="' . $url->get("img/emoticons/Smilies/sick.png") . '"/>',
                $textNode->getValue()
            )
        );


        $textNode->setValue(
            str_replace(
                '[:sigh:]',
                '<img title="sigh" alt="sigh" src="' . $url->get("img/emoticons/Smilies/sigh.png") . '"/>',
                $textNode->getValue()
            )
        );


        $textNode->setValue(
            str_replace(
                '[:sleep:]',
                '<img title="sleep" alt="sleep" src="' . $url->get("img/emoticons/Smilies/sleep.png") . '"/>',
                $textNode->getValue()
            )
        );


        $textNode->setValue(
            str_replace(
                '[:smile:]',
                '<img title="smile" alt="smile" src="' . $url->get("img/emoticons/Smilies/smile.png") . '"/>',
                $textNode->getValue()
            )
        );


        $textNode->setValue(
            str_replace(
                '[:smug:]',
                '<img title="smug" alt="smug" src="' . $url->get("img/emoticons/Smilies/smug.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:thumbsup:]',
                '<img title="thumbsup" alt="thumbsup" src="' . $url->get("img/emoticons/Smilies/thumbsup.png") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:tongue:]',
                '<img title="tongue" alt="tongue" src="' . $url->get("img/emoticons/Smilies/tongue.png") . '"/>',
                $textNode->getValue()
            )
        );


        $textNode->setValue(
            str_replace(
                '[:um:]',
                '<img title="um" alt="um" src="' . $url->get("img/emoticons/Smilies/um.png") . '"/>',
                $textNode->getValue()
            )
        );


        $textNode->setValue(
            str_replace(
                '[:unhappy:]',
                '<img title="unhappy" alt="unhappy" src="' . $url->get("img/emoticons/Smilies/unhappy.png") . '"/>',
                $textNode->getValue()
            )
        );


        $textNode->setValue(
            str_replace(
                '[:wanker:]',
                '<img title="wanker" alt="wanker" src="' . $url->get("img/emoticons/Smilies/wanker.gif") . '"/>',
                $textNode->getValue()
            )
        );

        $textNode->setValue(
            str_replace(
                '[:wink:]',
                '<img title="wink" alt="wink" src="' . $url->get("img/emoticons/Smilies/wink.png") . '"/>',
                $textNode->getValue()
            )
        );


        $textNode->setValue(
            str_replace(
                '[:wow:]',
                '<img title="wow" alt="wow" title="" alt="" src="' . $url->get("img/emoticons/Smilies/wow.png") . '"/>',
                $textNode->getValue()
            )
        );
    }

    public function visitElementNode(\JBBCode\ElementNode $elementNode)
    {
        /* We only want to visit text nodes within elements if the element's
         * code definition allows for its content to be parsed.
         */
        if ($elementNode->getCodeDefinition()->parseContent()) {
            foreach ($elementNode->getChildren() as $child) {
                $child->accept($this);
            }
        }
    }

}
