<?php

namespace BespokeJBBCodeDefinitions;

class QuoteWithRef extends \JBBCode\CodeDefinition {

    public function __construct()
    {
        parent::__construct();
        $this->setTagName("quote");

        $this->setUseOption(true);
    }

    public function asHtml(\JBBCode\ElementNode $el)
    {
        $content = "";
        foreach($el->getChildren() as $child)
            $content .= $child->getAsBBCode();

        $sEl = $el->getAsBBCode();

        //Parse user from bbcode
        $reUser = "/user_id=&#39;(.*?)&#39;/";
        $foundMatchUser = preg_match($reUser, $sEl, $matches);
        $user = isset($matches[1]) == true ? $matches[1] : "";

        //Parse formatted date for quote footer

        $re = "/post_date=&#39;(.*?)&#39;/";
        $foundMatchDate = preg_match($re, $sEl, $matches);
        $sDate = preg_replace("/\s(.*)/","",$matches[1]);
        $sTime = preg_replace("/(.*)\sat/","",$matches[1]);
        $sDateTime = $sDate." ".$sTime;
        $dDate = date_create($sDateTime);
        $pretty_format = "jS M Y \a\\t g.ia";
        $formatted_date = date( $pretty_format, strtotime($sDateTime));


        if(!$foundMatchDate)
            return $el->getAsBBCode();
        else
            //return "<div class=\"video\"><iframe title=\"YouTube video player\" width=\"480\" height=\"390\" src=\"//www.youtube.com/embed/".$matches[1]."\" frameborder=\"0\" allowfullscreen></iframe></div>";
            return '<blockquote contenteditable="false">'.
                                                        $el->getAsText()
            . '<BR>'
            .'<footer><b>'.$user.', '.$formatted_date.'</b></footer>'
            .'</blockquote>';
    }


}