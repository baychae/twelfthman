<?php

namespace Common\Library;

class ModelPaginator
{
    
    public static function paginate($controller, $data) {
        //Paginator
        $page_requested = $controller->dispatcher->getParam("page_requested", "int");

        if ($page_requested) {

        } else {
            $page_requested = $controller->request->get("page_requested", "int");
        }

        //Handle for if cookie not there or blank then sanitize
        $cookie_value = $controller->cookies->get('pageSize')->getValue() ? $controller->cookies->get('pageSize')->getValue() : 15;
        $sanitized_limit = $controller->filter->sanitize($cookie_value, "int");
        $page_size = $sanitized_limit > 60 ? 60 : $sanitized_limit; //Limit to avoid cookie exploit
        
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $data,
                "limit" => $page_size,
                "page" => $page_requested
            )
        );
        
        $paginated_page = $paginator->getPaginate();
        return $paginated_page;
    }
    
}