<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Common\Library;

define("SITE_DB_DATE_FORMAT_BASIC","Y:m:d H:i:s");

class DateTime
{
    public static function getFormattedSplitMilli()
    {
        list($eTime, $uTime) = explode(".", strval(microtime(true)));
        $formatted_date = date(SITE_DB_DATE_FORMAT_BASIC, $eTime).".".$uTime;
        
        return $formatted_date;
    }
    
    public static function datetofloat($formatted_Split_milli)
    {
        $date = explode(".", $formatted_Split_milli);
        $date_time_seconds = strtotime($date[0]);
        $date_time_seconds_split = $date_time_seconds . "." . $date[1];
        $date_time_seconds_split_float = floatval($date_time_seconds_split);
        return $date_time_seconds_split_float;       
    }
    
}