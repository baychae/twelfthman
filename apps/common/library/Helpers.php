<?php

namespace Common\Library;

class Helpers {

    public static function substr($original, $length)
    {
        $substitute = substr($original, 0, $length);
        return $substitute;
    }

    public static function substrtows($text, $max_char)
    {
        if (strlen($text) > $max_char) {
            $from_here = strpos($text, ' ', $max_char);
            $stripped_to_whitespace = substr($text, 0, $from_here);
            return $stripped_to_whitespace;
        }

        return $text;
    }

    //#TODO TAKE OUT THIS FILTER AND DO NOT USE IT IF AT ALL POSSIBLE
    public static function htmldecode($text)
    {
        return html_entity_decode($text);
    }

    public static function datedecode($recorded_db_datetime)
    {

        $recorded_db_datetime_raw = new \DateTime($recorded_db_datetime); //convert incoming date string to date object
        $current_datetime = new \DateTime();

        $recorded_db_ymd = $recorded_db_datetime_raw->format("Ymd");
        $current_ymd = $current_datetime->format("Ymd");
        $days_difference = $current_ymd - $recorded_db_ymd;
        
        if ($days_difference == 0) {
            $pretty_format = "\\T\o\d\a\y \a\\t g.ia";
        } elseif ($days_difference == 1) {
            $pretty_format = "\Y\\e\s\\t\\e\\r\d\a\y \a\\t g.ia";
        } else {
            $pretty_format = "jS M Y \a\\t g.ia";
        }

        $formatted_date = date_format($recorded_db_datetime_raw, $pretty_format);
        return $formatted_date;
    }

    public static function emaildate($recorded_db_datetime)
    {

        $recorded_db_datetime_raw = new \DateTime($recorded_db_datetime); //convert incoming date string to date object
        $current_datetime = new \DateTime();

        $recorded_db_ymd = $recorded_db_datetime_raw->format("Ymd");
        $current_ymd = $current_datetime->format("Ymd");
        $days_difference = $current_ymd - $recorded_db_ymd;

        if ($days_difference == 0) {
            $pretty_format = "g.ia";
        } elseif ($days_difference == 1) {
            $pretty_format = "\Y\\e\s\\t\\e\\r\d\a\y \a\\t g.ia";
        } else {
            $pretty_format = "jS M Y";
        }

        $formatted_date = date_format($recorded_db_datetime_raw, $pretty_format);
        return $formatted_date;
    }

    public static function shortdate($recorded_db_datetime)
    {
        $recorded_db_datetime_raw = new \DateTime($recorded_db_datetime); //convert incoming date string to date object
        $formatted_date = date_format($recorded_db_datetime_raw, "jS M Y");
        return $formatted_date;
    }

    public static function ukDate($recorded_db_datetime)
    {
        $recorded_db_datetime_raw = new \DateTime($recorded_db_datetime); //convert incoming date string to date object
        $formatted_date = date_format($recorded_db_datetime_raw, "d/m/Y");
        return $formatted_date;
    }

    public static function iso8601($raw_date)
    {
        $raw_date = new \DateTime($raw_date); //convert incoming date string to date object
        $formatted_date = date_format($raw_date, "c");
        return $formatted_date;
    }

    public static function parsebbtohtml($bbcode)
    {
        // Find youtube links in tag
        $regex_youtube = "/\[youtube\]http[s]*:\/\/youtu\.be\/([a-z0-9_-]+)\[\/youtube]/i";
        $regex_youtube_alt = "/\[youtube\]([a-z0-9_-]+)\[\/youtube]/i";

        if (preg_match($regex_youtube, $bbcode, $url_frag)) {
            $bbcode = preg_replace(
                $regex_youtube,
                '<div class="video"><iframe title="YouTube video player" width="480" height="390" '
                . 'src="//www.youtube.com/embed/' . $url_frag[1] . '" frameborder="0" '
                . 'allowfullscreen></iframe></div>',
                $bbcode
            );
        } elseif (preg_match($regex_youtube_alt, $bbcode, $url_frag)) {
            $bbcode = preg_replace(
                $regex_youtube_alt,
                '<div class="video"><iframe title="YouTube video player" width="480" height="390" '
                . 'src="//www.youtube.com/embed/' . $url_frag[1] . '" frameborder="0" allowfullscreen>'
                . '</iframe></div>',
                $bbcode
            );
        }

        $parser = new \JBBCode\Parser() ;
        $parser->addCodeDefinitionSet(new \JBBCode\DefaultCodeDefinitionSet());

        $parser->addBBCode(
            'video',
            '<div class="video"><iframe title="YouTube video player" width="480" height="390" '
            . 'src="//www.youtube.com/embed/{param}" frameborder="0" allowfullscreen></iframe></div>'
        );

        $quote = new \JBBCode\CodeDefinitionBuilder(
            'quote',
            '<blockquote contenteditable="false">{param}</blockquote>'
        );
        $parser->addCodeDefinition($quote->build());

        $quote = new \BespokeJBBCodeDefinitions\QuoteWithRef();
        $parser->addCodeDefinition($quote);

        $strike = new \JBBCode\CodeDefinitionBuilder('strike', '<strike>{param}</strike>');
        $parser->addCodeDefinition($strike->build());

        $getty_embed = new \JBBCode\CodeDefinitionBuilder('embed', '<div class="wysibb-embed-image">{param}</div>');
        $parser->addCodeDefinition($getty_embed->build());

        $left_align = new \JBBCode\CodeDefinitionBuilder('left', '<p style="text-align: left;">{param}</p>');
        $parser->addCodeDefinition($left_align->build());

        $right_align = new \JBBCode\CodeDefinitionBuilder('right', '<p style="text-align: right;">{param}</p>');
        $parser->addCodeDefinition($right_align->build());

        $center_align = new \JBBCode\CodeDefinitionBuilder('center', '<p style="text-align: center;" >{param}</p>');
        $parser->addCodeDefinition($center_align->build());

        $parser->parse($bbcode);

        $smileyVisitor = new \BespokeJBBCodeVisitors\SmileyVisitor();
        $parser->accept($smileyVisitor);

        $html = $parser->getAsHTML();

        // Transpose links to hyperlinked html
        $reg_exUrl = '/\w*(?<!href\=")\w*(?<!\>)\w*(?<!src\=")'
            . '(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/i';
        if (preg_match($reg_exUrl, $html, $url)) {
            $html = preg_replace($reg_exUrl, "<a target='_blank' href=" . $url[0] . ">{$url[0]}</a> ", $html);
        }

        return $html;
    }

    public static function parseBbToText($bbcode)
    {
        // Find youtube links in tag
        $parser = new \JBBCode\Parser();
        $parser->addCodeDefinitionSet(new \JBBCode\DefaultCodeDefinitionSet());

        $parser->addBBCode(
            'video',
            '<div class="video"><iframe title="YouTube video player" width="480" height="390" '
            . 'src="//www.youtube.com/embed/{param}" frameborder="0" allowfullscreen></iframe></div>'
        );

        $quote = new \JBBCode\CodeDefinitionBuilder(
            'quote',
            '<blockquote contenteditable="false">{param}</blockquote>'
        );
        $parser->addCodeDefinition($quote->build());

        $quote = new \BespokeJBBCodeDefinitions\QuoteWithRef();
        $parser->addCodeDefinition($quote);

        $strike = new \JBBCode\CodeDefinitionBuilder('strike', '<strike>{param}</strike>');
        $parser->addCodeDefinition($strike->build());

        $getty_embed = new \JBBCode\CodeDefinitionBuilder('embed', '<div class="wysibb-embed-image">{param}</div>');
        $parser->addCodeDefinition($getty_embed->build());

        $left_align = new \JBBCode\CodeDefinitionBuilder('left', '<p style="text-align: left;">{param}</p>');
        $parser->addCodeDefinition($left_align->build());

        $right_align = new \JBBCode\CodeDefinitionBuilder('right', '<p style="text-align: right;">{param}</p>');
        $parser->addCodeDefinition($right_align->build());

        $center_align = new \JBBCode\CodeDefinitionBuilder('center', '<p style="text-align: center;" >{param}</p>');
        $parser->addCodeDefinition($center_align->build());

        $parser->parse($bbcode);

        $smileyVisitor = new \BespokeJBBCodeVisitors\SmileyVisitor();
        $parser->accept($smileyVisitor);

        $html = $parser->getAsText();

        return $html;
    }

    public static function subStrStartWithLimit($text, $term, $limit)
    {
        $return_string = substr($text, strpos($text, $term) - 10, $limit);
        $return_string = preg_replace('/(' . $term . '){1}/i', "<strong>$1</strong>", $return_string);
        return $return_string;
    }

    public static function highlightWord($text, $word)
    {
        $return_string = preg_replace('/(' . $word . '){1}/i', "<strong>$1</strong>", $text);
        return $return_string;
    }

    public static function ordinal($number)
    {
        $format = \numfmt_create('en_GB', \NumberFormatter::ORDINAL);
        $formatted_number = \numfmt_format($format, $number);

        return $formatted_number;
    }

    public static function prettyUrl($string)
    {
        return preg_replace('/[\s-]+/', '-', strtolower($string));
    }

    public static function isIpInRange($request_ip_addr_val, $ranges) //Array of arranges containing bottom, top
    {
        foreach ($ranges as $location) {
            $low_ip_val = ip2long($location->bottom);
            $high_ip_val = ip2long($location->top);

            if ($low_ip_val <= $request_ip_addr_val && $request_ip_addr_val <= $high_ip_val) {
                $ip_within_range_result[] = true;
            } else {
                $ip_within_range_result[] = false;
            }
        }

        $ip_within_range = array_search(true, $ip_within_range_result) === false ? false : true;

        return $ip_within_range;
    }

    public static function parseHashTag($string)
    {
        $camelCase = ucwords($string);
        $removedBadChars = preg_replace("/[^a-z0-9.]+/i", "", $camelCase);
        $strippedCamelCase = str_replace(' ', '', $removedBadChars);
        return $strippedCamelCase;
    }

}
