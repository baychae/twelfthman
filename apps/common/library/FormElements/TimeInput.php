<?php
/**
 * AUTHOR: Richard Sinclair <rich.sinclair@gmail.com>
 * Date: 31/10/2016
 * Time: 12:59
 */

namespace Common\Library\FormElements;

class TimeInput extends \Phalcon\Forms\Element
{

    public function render($attributes = null)
    {
        $html = "<input id='kickoff_time' name='kickoff_time' type='time' value='" . $this->getValue() . "'>";


        return $html;
    }

}