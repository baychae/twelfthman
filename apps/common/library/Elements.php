<?php

namespace Common\Library;

class Elements extends \Phalcon\Mvc\User\Component
{
    private $headerMenu = [
        'left' => [
            'News' => [
                'link' => 'news'
            ],
            'Forum' => [
                'link' => 'forum'
            ],
            'Members' => [
                'link' => 'members'
            ],
            'Results' => [
                'link' => 'results'
            ],
            'Rivals' => [
                'link' => 'rivals'
            ],
            'Disclaimer' => [
                'link' => 'disclaimer'
            ],
            'Admin' => [
                'link' => 'admin'
            ]
        ],
        'right' => [
            'Messages' => [
                'link' => 'messages'
            ],
            'My Account' => [
                'link' => 'my_account'
            ],
            'Log in' => [
                'link' => 'session'
            ],
            'Register' => [
                'link' => 'session/register'
            ]
        ]
    ];

    private $adminMenu = [
        'News' => 'news',
        'Forum' => 'forum',
        'Members' => 'members',
        'Rivals' => 'rivals',
        'Up Next' => 'upnext',
        'Layout' => 'layout',
        'Disclaimer' => 'disclaimer',
        'Ad Space' => 'adspace',
        'Disclaimer' => 'disclaimer'
    ];

    private $mobileMenu = [
        'News' => [
            'link' => 'news'
        ],
        'Forum' => [
            'link' => 'forum',
            'sub_menu' => [
                'Forums' => [
                    'link' => 'forum'
                ],
                'Search' => [
                    'link' => 'forum/search'
                ]
            ]
        ],
        'Members' => [
            'link' => 'members',
            'sub_menu' => [
                'View Members' => [
                    'link' => 'members'
                ],
                'Friends' => [
                    'link' => 'messages/friends'
                ],
                'My Account' => [
                    'link' => 'my_account'
                ]

            ]
        ],
        'Results' => [
            'link' => 'results'
        ],
        'Rivals' => [
            'link' => 'rivals'
        ],
        'Disclaimer' => [
            'link' => 'disclaimer'
        ],
        'Admin' => [
            'link' => 'admin',
            'sub_menu' => [
                'News' => [
                    'link' => 'admin/news',
                    'sub_menu' => [
                        'Articles' => [
                            'link' => 'admin/news/newsarticles'
                        ],
                        'Groups' => [
                            'link' => 'admin/news/newsgroups'
                        ],
                        'Add Group' => [
                            'link' => 'admin/news/addnewsgroup'
                        ],
                        'Add Article' => [
                            'link' => 'admin/news/addnewsarticle'
                        ],
                        'Settings' => [
                            'link' => 'admin/news/settings'
                        ]
                    ]
                ],
                'Forum' => [
                    'link' => 'admin/forum',
                    'sub_menu' => [
                        'Forums' => [
                            'link' => 'admin/forum'
                        ],
                        'Add Forum' => [
                            'link' => 'admin/forum/addforum'
                        ],
                        'Add Type' => [
                            'link' => 'admin/forum/addforumtype'
                        ],
                        'Types' => [
                            'link' => 'admin/forum/listforumtypes'
                        ],
                        'Settings' => [
                            'link' => 'admin/forum/settings'
                        ],
                        'Flagged Posts' => [
                            'link' => 'admin/forum/flaggedposts'
                        ]

                    ]
                ],
                'Members' => [
                    'link' => 'admin/members'
                ],
                'Rivals' => [
                    'link' => 'admin/rivals'
                ],
                'Up Next' => [
                    'link' => 'admin/upnext',
                    'sub_menu' => [
                        'Matches' => [
                            'link' => 'admin/upnext/matches'
                        ],
                        'Add Match' => [
                            'link' => 'admin/upnext/addmatch'
                        ]
                    ]
                ],
                'Layout' => [
                    'link' => 'admin/layout'
                ],
                'Disclaimer' => [
                    'link' => 'admin/disclaimer'
                ]
            ]
        ],
        'Messages' => [
            'link' => 'messages'
        ],
        'My Account' => [
            'link' => 'my_account'
        ],
        'Logout' => [
            'link' => 'session/end'
        ],
        'Login' => [
            'link' => 'session'
        ],
        'Register' => [
            'link' => 'session/register'
        ]

    ];


    /**
     * Builds header menu with left and right items
     *
     * @return string
     */
    public function getNavBar()
    {

        $auth = $this->session->get('auth');

        if ($auth) {
            unset($this->headerMenu['right']['Log in']);
            unset($this->headerMenu['right']['Register']);
            if (!in_array($auth['privelege'], ['sysadmin', 'admin', 'poweruser'])) {
                unset($this->headerMenu['left']['Admin']);
            }

            $this->headerMenu['right']['Logout'] = [
                'link' => 'session/logout'
            ];

        } else {
            unset($this->headerMenu['left']['Members']);
            unset($this->headerMenu['left']['Files']);
            unset($this->headerMenu['left']['Admin']);
            unset($this->headerMenu['right']['Messages']);
            unset($this->headerMenu['right']['My Account']);

        }

        $moduleName = $this->dispatcher->getModuleName();
        $controllerName = $this->view->getControllerName();

        foreach ($this->headerMenu as $position => $menu) {
            echo '<ul class="nav navbar navbar-', $position,' ">';

            foreach ($menu as $menu_item => $uri) {
                if ($controllerName == $uri['link'] && $moduleName == 'frontend') {
                    echo '<li class="selected">';
                } else {
                    echo '<li>';
                }

                $controller = $uri['link'];

                echo '<a href="' . $this->url->get($controller) . '">' . $menu_item . '</a>';

                echo '</li>';
            }

            echo '</ul>';
        }
    }

    public function getAdminHeading()
    {
        $controllerName = $this->view->getControllerName();
        $section = array_search($controllerName, $this->adminMenu);
        echo '<h1 id="admin-heading">' . $section . '</h1>';
    }

    public function getAdminHeadingMobile()
    {
        $controllerName = $this->view->getControllerName();
        $section = array_search($controllerName, $this->adminMenu);
        echo '<h1 id="admin-heading">Admin >> ' . $section . '</h1>';
    }

    public function getAdminNavBar()
    {
        echo '<ul id="nav">';
        foreach ($this->adminMenu as $menu_item => $action) {
            echo '<li>';
            echo \Phalcon\Tag::linkTo('admin/' . $action, $menu_item);
            echo '</li>';
        }
        echo '</ul>';
    }

    public function getAdminSelectBox()
    {
        $current_admin_section = $this->view->getControllerName();
        $current_admin_action_name = $this->view->getActionName();
        $current_admin_sub_section = strtolower(preg_replace("/admin/", "", $current_admin_action_name));

        $request_url = trim($this->router->getRewriteUri(), "/");

        //Set current option value in options select list
        $current_url = $this->url->get($request_url);

        //Get the action name then look for sub sections #TODO CACHE THIS INFO
        $admin_sub_section_db = \Common\Models\AdminSiteSection::find([
           "conditions" => "name ='".$current_admin_section."' and sub_section != ''",
           "columns" => "sub_section",
           "order" => "id"
        ]);

        //If there are subsections build the options list and output the select box
        if (count($admin_sub_section_db) > 0) {
            echo '<select id="admin-select-box" 
                            onchange = "window.location.assign(this.options[this.selectedIndex].value,\'_blank\')">';

            foreach ($admin_sub_section_db as $sub_section) {
                $url = $this->url->getBaseUri()
                    . "admin/"
                    . str_replace("_", "", $current_admin_section)
                    . "/"
                    . $sub_section->sub_section;
                $url = str_replace(" ", "", $url);
                $url = strtolower($url);

                echo $current_url;

                if ($current_url == $url) {
                    echo "<option value='" . $url . "' selected>" . $sub_section->sub_section . "</option>";
                } else {
                    echo "<option value='" . $url . "'>" . $sub_section->sub_section . "</option>";
                }

            }
            echo "</select>";
        }
    }

    public function buildVerticalNav($aLabel_link, $sBase_url)
    {
        $curr_cntrllr_name = $this->view->getControllerName();
        $prev_cntrllr_name = $this->dispatcher->getpreviouscontrollername();
        $controller_name = $this->dispatcher->wasForwarded() == true ? $prev_cntrllr_name : $curr_cntrllr_name;

        echo "<div>";

        foreach ($aLabel_link as $label => $link) {
                $src_link = $sBase_url.'/'.$link;
            $link_label_image_row = '<span>' . $label . '</span>' . '<img src="' . $src_link . '.png" alt="' . $src_link . '">';
                echo $this->tag->linkTo($controller_name.'/'.$link, $link_label_image_row);
        }

        echo "</div>";
    }



    public function buildHorizontalNav($aLabel_link, $sBase_url)
    {
        $group = $this->dispatcher->getParam("group");
        $controller_name =  $this->dispatcher->getControllerName();

        $uri_path = explode('/', rtrim($this->router->getRewriteUri(), '/'));
        $final_route = array_pop($uri_path);

        $nav_html = "<ul>";

        foreach ($aLabel_link as $label => $link) {
            if ($link == $group || $link == $final_route) {
                $nav_html .= "<li class = 'selected'>";
                $image = '<img src="' . $sBase_url . '/' . $link . '.png" alt="' . $link . '">';
                $nav_html .= $this->tag->linkTo($controller_name . '/' . $link, $image);
            } else {
                $image = '<img src="' . $sBase_url . '/' . $link . '.png" alt="' . $link . '">';
                $nav_html .= '<li>';
                $nav_html .= $this->tag->linkTo($controller_name . '/' . $link, $image);
            }
        }


        $nav_html .= "</ul>";

        return $nav_html;
    }

    public function getPaginator($page, $items_label)
    {
        $paginator_html = "";

        if (!defined("MAX_PAGES_TO_LIST")) {
            define("MAX_PAGES_TO_LIST", 9);
        }

        if (!defined("PAGINATOR_VIEW_STATS")) {
            define("PAGINATOR_VIEW_STATS", 1);
        }

        if (!defined("PAGINATOR_VIEW_WORD_PAGE")) {
            define("PAGINATOR_VIEW_WORD_PAGE", 1);
        }

        $current_url = $this->url->getBaseUri() . ltrim($_GET["_url"], "/");
        $url_no_page = preg_replace("/\/[0-9]+$/", "", $current_url);

        if (constant('PAGINATOR_VIEW_STATS')) {
            $paginator_html .= '<span>' . $page->total_items . ' ' . $items_label . ' </span>';
        }

        $paginator_html .= '<a class="paginator-button" href="' . $url_no_page . '/' . $page->before . '">'
            . $this->escaper->escapeHtml("<")
            . '</a>';

        $pages_displayed = $page->total_pages < constant("MAX_PAGES_TO_LIST")
            ? $page->total_pages : constant("MAX_PAGES_TO_LIST");

        for ($i = 1; $i <= $pages_displayed; $i++) {
            $paginator_html .= '<a class="paginator-button" href="' . $url_no_page . '/' . $i . '">' . $i . '</a>';
        }

        if ($page->total_pages > constant("MAX_PAGES_TO_LIST")) { //Show more pages to select dropdown
            $paginator_html .= '<a class="paginator-button paginator-select">...';
                #TODO - WRITE SELECT DROP DOWN IN SPANS AND LINKS
            $paginator_html .= '</a>';
        }

        $paginator_html .= '<a class="paginator-button" href="' . $url_no_page . '/' . $page->next . '">'
            . $this->escaper->escapeHtml(">")
            . '</a>';
        $paginator_html .= '<a class="paginator-button" href="' . $url_no_page . '/' . $page->last . '">'
            . $this->escaper->escapeHtml(">>")
            . '</a>';

        $paginator_html .= "<span class='span-padding'></span>";
        $paginator_html .= '<a class="paginator-button page-size" href="' . $url_no_page . '/1">15</a>';
        $paginator_html .= '<a class="paginator-button page-size" href="' . $url_no_page . '/1">30</a>';
        $paginator_html .= '<a class="paginator-button page-size" href="' . $url_no_page . '/1">60</a>';
        if (constant('PAGINATOR_VIEW_WORD_PAGE')) {
            $paginator_html .= '<span class="paginator-end">Page '
                . $page->current
                . ' of '
                . $page->total_pages
                . '</span>';
        } else {
            $paginator_html .= '<span class="paginator-end">'
                . $page->current
                . ' of '
                . $page->total_pages
                . '</span>';
        }

        //Pagination with rel=“next” and rel=“prev” for seo
        $rel_next = '<link rel="next" href="'.$url_no_page.'/'.$page->next.'"/>';
        $rel_prev = '<link rel="prev" href="'.$url_no_page.'/'.$page->before.'"/>';

        if ($page->current == $page->before) {
            $this->view->setVar('rel_next', $rel_next);
        } elseif ($page->current == $page->last) {
            $this->view->setVar('rel_prev', $rel_prev);
        } else {
            $this->view->setVar('rel_prev', $rel_prev);//Used in <head>
            $this->view->setVar('rel_next', $rel_next);
        }

        return $paginator_html;
    }

    public function getIndexedPaginator($page, $items_label, $indexed_by = null)
    {

        if (!defined("MAX_PAGES_TO_LIST")) {
            define("MAX_PAGES_TO_LIST", 9);
        }

        $current_base_uri = $this->url->getBaseUri() . ltrim($_GET["_url"], "/");

        echo '<span>' . $page->total_items . ' ' . $items_label . ' </span>';
        echo '<a class="paginator-button" href="'
            . $current_base_uri . '?' . $indexed_by . 'page_requested=.members' . $page->before . '">&lt;</a>';

        $pages_displayed = $page->total_pages < constant("MAX_PAGES_TO_LIST")
            ? $page->total_pages : constant("MAX_PAGES_TO_LIST");

        for ($i = 1; $i <= $pages_displayed; $i++) {
            echo '<a class="paginator-button" href="'
                . $current_base_uri . '?' . $indexed_by . 'page_requested=' . $i . '">' . $i . '</a>';
        }

        if ($page->total_pages > constant("MAX_PAGES_TO_LIST")) { //Show more pages to select dropdown
            echo '<a class="paginator-button paginator-select">...';
            #TODO - WRITE SELECT DROP DOWN IN SPANS AND LINKS
            echo '</a>';
        }

        echo '<a class="paginator-button" href="'
            . $current_base_uri . '?' . $indexed_by . 'page_requested=' . $page->next . '">&gt;</a>';
        echo '<a class="paginator-button" href="'
            . $current_base_uri . '?' . $indexed_by . 'page_requested=' . $page->last . '">&gt;&gt;</a>';

        echo "<span></span>";
        echo '<a class="paginator-button page-size" href="'
            . $current_base_uri . '?' . $indexed_by . 'page_requested=1">15</a>';
        echo '<a class="paginator-button page-size" href="'
            . $current_base_uri . '?' . $indexed_by . 'page_requested=1">30</a>';
        echo '<a class="paginator-button page-size" href="'
            . $current_base_uri . '?' . $indexed_by . 'page_requested=1">60</a>';

        echo '<span>Page ' . $page->current . ' of ' . $page->total_pages . '</span>';

        //Pagination with rel=“next” and rel=“prev” for seo
        $rel_next = '<link rel="next" href="' . $current_base_uri . '/' . $page->next . '"/>';
        $rel_prev = '<link rel="prev" href="' . $current_base_uri . '/' . $page->before . '"/>';

        if ($page->current == $page->before) {
            $this->view->setVar('rel_next', $rel_next);
        } elseif ($page->current == $page->last) {
            $this->view->setVar('rel_prev', $rel_prev);
        } else {
            $this->view->setVar('rel_prev', $rel_prev);//Used in <head>
            $this->view->setVar('rel_next', $rel_next);
        }
    }

    public function getAlphaNumPaginator($index_array)
    {
        $current_base_uri = $this->url->getBaseUri() . ltrim($_GET["_url"], "/");

        echo "<a class='paginator-button' href='" . $current_base_uri . "'" . ">All</a>";

        foreach ($index_array as $index) {
            echo "<a class='paginator-button' href='" . $current_base_uri . "?index=" . $index . "'>$index</a>";
        }
    }

    private function recurseMobileMenu($menu)
    {

        $list = "";

        foreach ($menu as $section_title => $menu_item) {              //1st order Menu
            if (isset($menu_item["sub_menu"])) {                                    //If second order menu present
                //treat it as such
                $list .= '<li class="no-padding">'
                    . '<ul class="collapsible collapsible-accordion">'
                    . '<li>'
                    . '<a class="collapsible-header">' . $section_title . '</a>' //Main title and framing for
                    . '<div class="collapsible-body">'                       //2nd order menu
                    . '<ul>';

                $list .= $this->recurseMobileMenu($menu_item["sub_menu"]);

                $list .= '</ul>'
                    . '</div>';

                $list .= '</li>'
                    . '</ul>';

            } else {
                $list .= '<li class="divider"></li><li><a href="'
                    . $menu_item["link"] . '">' . $section_title . '</a></li>';
            }

        }

        return $list;
    }

    /**
     * @return array
     */
    public function getMobileMenu()
    {

        $auth = $this->session->get('auth');

        if ($auth) {                                                                //Manipulate Menu based on state
            unset($this->mobileMenu['Login']);
            unset($this->mobileMenu['Register']);
            if ($auth['privelege'] != 'sysadmin') {
                unset($this->mobileMenu['Admin']);
            }

            $this->mobileMenu['Logout'] = [
                'link' => 'session/end
                '
            ];

        } else {
            unset($this->mobileMenu['Members']);
            unset($this->mobileMenu['Admin']);
            unset($this->mobileMenu['Messages']);
            unset($this->mobileMenu['My Account']);
            unset($this->mobileMenu['Logout']);

        }

        $list = $this->recurseMobileMenu($this->mobileMenu);

        $menu = '<ul id="slide-out" class="side-nav">' . $list . '</ul>';          //Frame for the slide-out

        return $menu;                                                               //Send it home
    }

    public function getTopicRatingStars($users_recommend)
    {
        $stars = null;

        switch (true) {
            case in_array($users_recommend, range(1, 3)):
                $stars = 1;
                break;
            case in_array($users_recommend, range(4, 6)):
                $stars = 2;
                break;
            case in_array($users_recommend, range(7, 9)):
                $stars = 3;
                break;
            case in_array($users_recommend, range(10, 12)):
                $stars = 4;
                break;
            case $users_recommend > 12:
                $stars = 5;
                break;
            default:
                $stars = null;
                break;
        }

        for ($i = 1; $i <= $stars; $i++) {
            echo '<img class="button forum-topic-rating-small" src="'
                . $this->url->get('img/buttons/star.png') . '" alt="*">';
        }
    }

    public function getLeftFooter()
    {
        $site_section = \Common\Models\AdminSiteSection::findFirst("name = 'layout' AND sub_section = 'footer' ");
        $site_section_data = $site_section->getAdminSiteSectionData("name = 'footer'")->getFirst();
        $escaped_footer_bb = $site_section_data->value;
        $html = \Common\Library\Helpers::parsebbtohtml($escaped_footer_bb);

        echo $html;
    }

    public function getLastMatchPreviewPromo()
    {

        $group = \Common\Models\NewsGroup::findFirst('link = "preview"'); //Get Last Match Preview
        $news_article = $group->getNewsArticle()->getLast();

//        $news_article = \Common\Models\NewsArticle::findFirst('image_upload != ""'); #DELETE BEFORE RELEASE!!!!

        if (isset($news_article->image_upload) == true) {                           // Sort out the Match Preview Image
            $art_url = $this->url->get($this->config->frontend->news->article->image_dir . $news_article->image_upload);
            $preview_img = '<img src="' . $art_url . '" class="article-feature-art">';
        } elseif (isset($news_article->image_embed)) {
            $art_embed = $news_article->image_embed;
            $preview_img = $art_embed;
        } else {
            $art_url = $this->url->get($this->config->frontend->news->article->settings->image_dir
                . "default-article-image.png");
            $preview_img = '<img src="' . $art_url . '" class="article-feature-art">';
        }

        echo "<div id='last-match-preview-promo'>";
        echo '<div id="news-splash-preview-art">';
        echo '<a href="' . $this->url->get("news/article/" . $news_article->id) . '">' . $news_article->title . '</a>';
        echo "</div>";
        echo '<div class="article-feature-art">';
        echo $preview_img;
        echo "</div>";
        echo "</div>";
    }

    public function getMatchDetails()
    {
        $todays_date = date("Y-m-d");


        $match = \Common\Models\Match::findFirst([                      //Is there a match today? If so status is
            "conditions" => "start like '%" . $todays_date . "%'"
        ]);

        if (isset($match->id)) {
            $status = "Todays";

            $timezone = new \DateTimeZone("Europe/London");
            $now_datetime = new \DateTime("now", $timezone);
            $match_start_datetime = new \DateTime($match->start, $timezone);

            if ($match_start_datetime < $now_datetime) { // If the match has started
                $scoreboard = "<div>" . $match->home_score . "-" . $match->away_score . "</div>";
            } else {
                $scoreboard = "<div>V</div>";
            }
        }

        if (!isset($match->id)) {
            $status = "Next";

            $tommorrow = date('Y-m-d', strtotime(' +1 day'));

            $match = \Common\Models\Match::findFirst([      // Look for next match
                "conditions" => "start > '" . $tommorrow . " 00:00:00'"
            ]);

            $scoreboard = "<div>V</div>";
            $status = "Next";
        }

        if (!isset($match->id)) {                                    // If no match after today get Last Match Results
            $status = "Last";
            $match = \Common\Models\Match::findFirst([      // Look for next match
                "conditions" => "start < '" . $todays_date . " 00:00:00'",
                "order" => "start DESC"
            ]);

            $scoreboard = "<div>" . $match->home_score . "-" . $match->away_score . "</div>";
        }

        if (isset($match->id)) {
            $home_team = $match->getHomeTeam();
            $away_team = $match->getAwayTeam();

            $rival_team = $home_team->name === $this->config->site->team ? $away_team : $home_team;

            echo "<div id='upnext'>";
            echo "<h3><span>" . $status . " Match</span></h3>";
            echo '<div class="upnext-team">';
            echo '<img src="'
                . $this->url->get("img/teams/" . $home_team->image_name)
                . '" alt="' . $home_team->name . '">';
            echo '<span>' . $home_team->name . '</span>';
            echo '</div>';

            echo $scoreboard;

            echo '<div class="upnext-team">';
            echo '<img src="'
                . $this->url->get("img/teams/"
                    . $away_team->image_name)
                . '" alt="' . $away_team->name . '">';
            echo '<span>' . $away_team->name . '</span>';
            echo '</div>';
            echo "<div class='text-center'><span>Kick Off: "
                . \Common\Library\Helpers::datedecode($match->start) . "</span></div>";

            //MATCH PREVIEW
            if (isset($match->getPreviewNewsArticle()->id)) {//Show natch Preview if exists
                $preview_id = $match->getPreviewNewsArticle()->id;
                echo '<div class="upnext-button" onclick="parent.location=\''
                    . $this->url->get("news/article/" . $preview_id) . '\'"><span>
                            <a href="' . $this->url->get("news/article/" . $preview_id) . '">
                            Match Preview
                            </a></span>
                     </div>';
            }
            //MATCH REPORT
            if (isset($match->getReportNewsArticle()->id)) {
                $report_id = $match->getReportNewsArticle()->id;
                echo '<div class="upnext-button" onclick="parent.location=\''
                    . $this->url->get("news/article/" . $report_id) . '\'"><span>
                            <a href="' . $this->url->get("news/article/" . $report_id) . '">
                            Match Report
                            </a></span>
                     </div>';
            }

            //View Past Results
            if (isset($rival_team->name)) {
                $team_id = $rival_team->id;
                echo '<div class="upnext-button" onclick="parent.location=\''
                    . $this->url->get("results/" . $team_id) . '\'"><span>
                            <a href="' . $this->url->get("results/" . $team_id) . '">
                            View Past Results
                            </a></span>
                     </div>';
            }

            //RIVALS SITE
            if (isset($rival_team->id)) {
                $website = $rival_team->website;
                echo '<div class="upnext-button" onclick="window.open(\'//' . $website . '\')"><span>
                            <a href="//' . $website . '" target="_blank">
                            Rivals Site
                            </a></span>
                     </div>';
            }

            if ($status === 'Next' || $status === 'Todays') { // If current or next match then show previous
                //Get the match before this one
                $previous_match = \Common\Models\Match::findFirst([      // Look for next match
                    "conditions" => "start < '" . $match->start . "'",
                    "order" => "start DESC"
                ]);

                $prev_match_home_team = $previous_match->getHomeTeam();
                $prev_match_away_team = $previous_match->getAwayTeam();

                $previous_rival_team = $prev_match_home_team->name === $this->config->site->team
                    ? $prev_match_away_team : $prev_match_home_team;

                echo "<div id='upnext'>";
                echo "<h3><span>Previous Match</span></h3>";
                echo '<div class="upnext-team">';
                echo '<img src="'
                    . $this->url->get("img/teams/"
                        . $prev_match_home_team->image_name)
                    . '" alt=' . $prev_match_home_team->name . '>';
                echo '<span>' . $prev_match_home_team->name . '</span>';
                echo '</div>';

                echo "<div>" . $previous_match->home_score . "-" . $previous_match->away_score . "</div>";

                echo '<div class="upnext-team">';
                echo '<img src="'
                    . $this->url->get("img/teams/" . $prev_match_away_team->image_name)
                    . '" alt="' . $prev_match_away_team->name . '">';
                echo '<span>' . $prev_match_away_team->name . '</span>';
                echo '</div>';
                echo "<div class='text-center'><span>Kick Off: "
                    . \Common\Library\Helpers::datedecode($previous_match->start) . "</span></div>";

                //PREVIOUS MATCH PREVIEW
                if (isset($previous_match->getPreviewNewsArticle()->id)) {//Show natch Preview if exists
                    $preview_id = $previous_match->getPreviewNewsArticle()->id;
                    echo '<div class="upnext-button" onclick="parent.location=\''
                        . $this->url->get("news/article/" . $preview_id) . '\'"><span>
                            <a href="' . $this->url->get("news/article/" . $preview_id) . '">
                            Match Preview
                            </a></span>
                     </div>';
                }
                //PREVIOUS MATCH REPORT
                if (isset($previous_match->getReportNewsArticle()->id)) {
                    $report_id = $previous_match->getReportNewsArticle()->id;
                    echo '<div class="upnext-button" onclick="parent.location=\''
                        . $this->url->get("news/article/" . $report_id) . '\'"><span>
                            <a href="' . $this->url->get("news/article/" . $report_id) . '">
                            Match Report
                            </a></span>
                     </div>';
                }

                //PREVIOUS View Past Results
                if (isset($rival_team->name)) {
                    $team_id = $previous_rival_team->id;
                    echo '<div class="upnext-button" onclick="parent.location=\''
                        . $this->url->get("results/" . $team_id) . '\'"><span>
                            <a href="' . $this->url->get("results/" . $team_id) . '">
                            View Past Results
                            </a></span>
                     </div>';
                }

                //PREVIOUS RIVALS SITE
                if (isset($rival_team->id)) {
                    $website = $previous_rival_team->website;
                    echo '<div class="upnext-button" onclick="window.open(\'//'
                        . $website . '\')" target="_blank"><span>
                            <a href="//' . $website . '" target="_blank">
                            Rivals Site
                            </a></span>
                     </div>';
                }
            }


        } else {
            echo "Match database empty? Error! Please inform admin!";
        }
        echo "</div>";
    }

    public function getForumSearch()
    {
        echo '<div class="ui-widget">'
            . '<form method="post" action="' . $this->url->get("forum/search") . '">'
            . '<input type="submit" value="Search" class="button-link">'
            . '<input id="forum-topic-search" type="text" placeholder="search for topics..." name="search-term">'
            . '</form>'
            . '</div>';
    }

}