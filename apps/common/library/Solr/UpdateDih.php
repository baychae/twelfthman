<?php
/**
 * Created by PhpStorm.
 * User: mrsinclair
 * Date: 26/05/2017
 * Time: 13:17
 */

namespace Common\Library\Solr;

class UpdateDih extends \Phalcon\Di\Injectable
{
    public function update()
    {
        $user = $this->config->solr->username;
        $password = $this->config->solr->password;
        $host = $this->config->solr->host;
        $port = $this->config->solr->port;
        $collection = $this->config->solr->collection;

        $ch = curl_init("https://" . $host . ":" . $port . "/solr/" . $collection . "/dataimport?command=full-import&wt=json");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERNAME, $this->config->solr->admin_username);
        curl_setopt($ch, CURLOPT_PASSWORD, $this->config->solr->admin_password);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);
        $messages = (array)json_decode($response)->statusMessages;
        curl_close($ch);

        $log_date = date("Y:m:d h:i:s - ");

        echo $log_date . "============IMPORT AND INDEX FORUM_TOPICS FOR AUTOCOMPLETE" . PHP_EOL;
        echo $log_date . "Total Requests made to DataSource: " . $messages["Total Requests made to DataSource"] . PHP_EOL;
        echo $log_date . "Total Rows Fetched: " . $messages["Total Rows Fetched"] . PHP_EOL;
        echo $log_date . "Total Documents Processed: " . $messages["Total Documents Processed"] . PHP_EOL;
        echo $log_date . "Total Documents Skipped: " . $messages["Total Documents Skipped"] . PHP_EOL;
        echo $log_date . "Full Dump Started: " . $messages["Full Dump Started"] . PHP_EOL;
        echo $log_date . "Committed: " . $messages["Committed"] . PHP_EOL;
        echo $log_date . "Time taken: " . $messages["Time taken"] . PHP_EOL;
    }
}