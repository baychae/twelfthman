<?php

/*
  +-------------------------------------------------------------------------+
  | Phosphorum                                                              .
  +-------------------------------------------------------------------------+
  | Copyright (c) 2013-2016 Phalcon Team and contributors                   |
  +-------------------------------------------------------------------------+
  | Redistribution and use in source and binary forms, with or without      .
  | modification, are permitted provided that the following conditions are  |
  | met:                                                                    |
  | - Redistributions of source code must retain the above copyright notice |
  | , this list of conditions and the following disclaimer.                 |
  | - Redistributions in binary form must reproduce the above copyright     |
  |  notice, this list of conditions and the following disclaimer in the    |
  |  documentation and/or other materials provided with the distribution.   |
  | - Neither the name of the Phalcon nor the names of its contributors may |
  |  be used to endorse or promote products derived from this software      |
  |  without specific prior written permission.                             |
  | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS     |
  | "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT       |
  | LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR   |
  | A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PHALCON          |
  | FRAMEWORK TEAM BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, |
  | EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,     |
  |  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR     |
  | PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  |
  | LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    |
  | NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS      |
  | SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            |
  +-------------------------------------------------------------------------+
*/

namespace Common\Library\Mail;


class SendSpool extends \Phalcon\Di\Injectable
{

    protected $transport;
    protected $mailer;


    public function send(\Common\Models\Notification $notification)
    {

        if ($notification->sent == 'Y') {
            return "Message already sent.";
        }

        $user = \Common\Models\SiteUser::findFirstById($notification->user_id);

        if (!$user) {
            //Log user not found
            return "User not found message not sent. id: " . $notification->user_id;
        }

        switch ($notification->type) {
            case "pswd-reset":
                $message = new \Swift_Message("[" . $this->config->site->name . " Forum] Password Reset!");
                $message->setTo($user->email);
                $message->addFrom($this->config->smtp->from);

                $random = new \Phalcon\Security\Random();
                $use_once_deny_hash = $random->uuid();

                $reset_uri = $this->url->get("/session/password-reset/" . $use_once_deny_hash);

                $htmlContent = 'This is an automated message from ' . $this->config->site->name . '.<BR>' .
                    '&mdash;<br>Reset your password ' .
                    PHP_EOL . '<a href="' . $reset_uri . '">here</a>';

                $plainContent = "This is a test message";
                break;
            case "friend-request":
                $message = new \Swift_Message("[" . $this->config->site->name . " Messages] Friend Request!");
                $message->setTo($user->email);
                $message->addFrom($this->config->smtp->from);

                $htmlContent = 'This is an automated message from '
                    . $this->config->site->name
                    . '.<BR>'
                    . '&mdash;<BR> You have received a friend request! Please login to '
                    . $this->config->site->name
                    . ' and view your Messages.';

                $plainContent = 'You have received a friend request! Please login to '
                    . $this->config->site->name
                    . ' and view your Messages.';
                break;
        }

        $bodyMessage = new \Swift_MimePart($htmlContent, 'text/html');
        $bodyMessage->setCharset('UTF-8');
        $message->attach($bodyMessage);
        $bodyMessage = new \Swift_MimePart($plainContent, 'text/plain');
        $bodyMessage->setCharset('UTF-8');
        $message->attach($bodyMessage);

        $https['ssl']['verify_peer'] = $this->config->smtp->options->ssl->verify_peer;
        $https['ssl']['verify_peer_name'] = $this->config->smtp->options->ssl->verify_peer_name;
        $https['ssl']['allow_self_signed'] = $this->config->smtp->options->ssl->allow_self_signed;

        if (!$this->transport) {
            $this->transport = (new \Swift_SmtpTransport(
                $this->config->smtp->host,
                $this->config->smtp->port,
                $this->config->smtp->security
            ))->setUsername($this->config->smtp->username)
                ->setPassword($this->config->smtp->password)
                ->setStreamOptions($https);
        }

        if (!$this->mailer) {
            $this->mailer = new \Swift_Mailer($this->transport);
        }

        if (!$this->mailer->send($message, $failures)) {
            return $failures;
        } elseif ($notification->type == "pswd-reset") {
            echo "Message Sent!";

            $reminder = new \Common\Models\PasswordReminderReceipt();
            $reminder->notification_id = $notification->_id;
            $reminder->user_id = $notification->user_id;
            $reminder->uuid = $use_once_deny_hash;

            if (!$reminder->save()) {
                foreach ($reminder->getMessages() as $message) {
                    echo $message->getMessage(), PHP_EOL;
                }
            }

            $notification->sent = 'Y';
            echo date("Y:m:d h:i:s - ") . "Password Reminder sent: " . $reminder->_id . PHP_EOL;

            if ($notification->save() == false) {
                foreach ($notification->getMessages() as $message) {
                    echo $message->getMessage(), PHP_EOL;
                }
            }

        } else {
            $notification->sent = 'Y';
            echo date("Y:m:d h:i:s - ") . "Notification : " . $notification->type;

            if ($notification->save() == false) {
                foreach ($notification->getMessages() as $message) {
                    echo $message->getMessage(), PHP_EOL;
                }
            }
        }

        return true;
    }

    /**
     * Check notifications marked as not send on the databases and send them
     */
    public function sendRemaining()
    {
        foreach (\Common\Models\Notification::find([['sent' => 'N']]) as $notification) {
            $this->send($notification);
        }
    }

    /**
     * Check the queue from Beanstalk and send the notifications scheduled there
     */
    public function consumeQueue()
    {
        echo "Start ConsumeQueue" . PHP_EOL;

        $this->queue->choose('email');
        $this->queue->watch('email');


        while (true) {
            while ($this->queue->peekReady() !== false) {
                $job = $this->queue->reserve();
                $message = $job->getBody();

                foreach ($message as $userId => $id) {
                    $notification = \Common\Models\Notification::findById($id);

                    echo date("Y:m:d h:i:s - ") . "Fetched notification: " . $id . PHP_EOL;

                    if ($notification) {
                        $success = $this->send($notification);

                        if ($success) {
                            echo date("Y:m:d h:i:s - ") . "Message Sent: " . $id . PHP_EOL;
                        } else {
                            echo date("Y:m:d h:i:s - ") . "Error Message Not Sent: " . $id . PHP_EOL;
                        }
                    }
                }
                if (is_object($this->transport)) {
                    $this->transport->stop();
                    $this->transport = null;
                    $this->mailer = null;
                }
                $job->delete();
            }

            sleep(5);
        }
    }


}