<?php

namespace Test;

/**
 * Class UnitTest
 */
class UserModelTest extends \UnitTestCase {
       
    private function resetUser(\SiteUser $user){
        foreach($user as $key => &$value){
            $value = NULL;
        }
    }
    
    private function getUserMessages(\SiteUser $user){
        $messages = array();
        foreach ($user->getMessages() as $message){
                array_push($messages, (String) $message);
            }
        return $messages;
    }

    public function testCreateUser() {
        $user = new \SiteUser();
        return $user;
    }
    
    /**
     * Sanity checking that database rules have been applied
     * @depends testCreateUser     
     * @expectedException \Phalcon\Exception
     */
    public function testSaveWithOnlyLoginId(\SiteUser $user) {
        $this->resetUser($user);
        $user->user_login_id = "test_user";
        $message_fixture = array('user_level is required','user_first_name is required','user_last_name is required','user_email is required');
        
        if ($user->save() == false) {
            $message_results = $this->getUserMessages($user);
            $this->assertEquals($message_results,$message_fixture);
            throw new \Phalcon\Exception ;
        }
    }
    
    /**
     * Sanity checking that database rules have been applied
     * @depends testCreateUser
     * @expectedException \Phalcon\Exception
     */
    public function testSaveWithOnlyUserLevel(\SiteUser $user) {
        $this->resetUser($user);
        $user->user_level = 3;
        $message_fixture = array('user_login_id is required','user_first_name is required','user_last_name is required','user_email is required');
        
        if ($user->save() == false) { 
            $message_results = $this->getUserMessages($user);
            $this->assertEquals($message_results,$message_fixture);
            throw new \Phalcon\Exception ;
        }
    }
    
    /**
     * Sanity checking that database rules have been applied
     * @depends testCreateUser
     * @expectedException \Phalcon\Exception
     */
    public function testSaveWithOnlyUserFirstName(\SiteUser $user) {
        $this->resetUser($user);
        $user->user_first_name = "Firstname";
        $message_fixture = array('user_login_id is required','user_level is required','user_last_name is required','user_email is required');
        
        if ($user->save() == false) {
            $message_results = $this->getUserMessages($user);
            $this->assertEquals($message_results,$message_fixture);
            throw new \Phalcon\Exception ;
        }
    }
    
    /**
     * Sanity checking that database rules have been applied
     * @depends testCreateUser
     * @expectedException \Phalcon\Exception
     */
    public function testSaveWithOnlyUserLastName(\SiteUser $user) {
        $this->resetUser($user);
        $user->user_last_name = "Lastname";
        $message_fixture = array('user_login_id is required','user_level is required','user_first_name is required','user_email is required');
        
        if ($user->save() == false) { 
            $message_results = $this->getUserMessages($user);
            $this->assertEquals($message_results,$message_fixture);
            throw new \Phalcon\Exception ;
        }
    }
    
    /**
     * Sanity checking that database rules have been applied
     * @depends testCreateUser
     * @expectedException \Phalcon\Exception
     */
    public function testSaveWithOnlyUserEmail(\SiteUser $user) {
        $this->resetUser($user);
        $user->user_email = "user@name.com";
        $message_fixture = array('user_login_id is required','user_level is required','user_first_name is required','user_last_name is required');
        
        if ($user->save() == false) { 
            $message_results = $this->getUserMessages($user);
            $this->assertEquals($message_results,$message_fixture);
            throw new \Phalcon\Exception ;
        } 
    }
    
    /**
     * Sanity checking that database rules have been applied
     * @depends testCreateUser
     * @expectedException \Phalcon\Exception
     */
    public function testSaveWithOnlyUserPassword(\SiteUser $user) {
        $this->resetUser($user);
        $user->user_password = "";
        $message_fixture = array('user_login_id is required','user_level is required','user_first_name is required','user_last_name is required');
        
        if ($user->save() == false) { 
            $message_results = $this->getUserMessages($user);
            $this->assertEquals($message_results,$message_fixture);
            throw new \Phalcon\Exception ;
        } 
    }    
    
    /**
     * Sanity checking that database rules have been applied
     * @depends testCreateUser
     */
    public function testSaveUser(\SiteUser $user){
        $user_fixture = array('user_login_id' => 'test_user', 'user_level' => 3, 'user_first_name' => "Firstname", 'user_last_name' => "Lastname", 'user_email' => "user@name.com");
        $user->assign($user_fixture);
        $user->save();
        $result_user = \SiteUser::find();
    }

    public function testDeleteUsers(){
        $users = \SiteUser::find();
        assert(count($users) == 1);
        $users->delete();
    }
    
    
}