<?php
use Phalcon\DI,
    Phalcon\DI\FactoryDefault;

ini_set('display_errors',1);
error_reporting(E_ALL);

define('ROOT_PATH', __DIR__);
define('PATH_INCUBATOR', __DIR__ . '/../vendor/incubator/');
define('PATH_CONFIG', __DIR__ . '/../app/config/config.php');
define('PATH_MODELS', __DIR__ . '/../app/models/');
//define('PATH_LIBRARY', __DIR__ . '/../app/library/');
//define('PATH_SERVICES', __DIR__ . '/../app/services/');
//define('PATH_RESOURCES', __DIR__ . '/../app/resources/');

set_include_path(
    ROOT_PATH . PATH_SEPARATOR . get_include_path()
);

// use the application autoloader to autoload the classes
// autoload the dependencies found in composer
$loader = new \Phalcon\Loader();

$loader->registerDirs(array(
    ROOT_PATH,
    PATH_CONFIG,
    PATH_MODELS
));

$loader->registerNamespaces(array(
    'Phalcon' => PATH_INCUBATOR.'Library/Phalcon/'
));

$loader->register();

$config = include PATH_CONFIG;

$di = new FactoryDefault();

DI::reset();

// add any needed services to the DI here
/**
	 * Database connection is created based in the parameters defined in the configuration file
	 */
	$di->set('db', function() use ($config) {
		return new \Phalcon\Db\Adapter\Pdo\Mysql(array(
			"host" => $config->database->host,
			"username" => $config->database->username,
			"password" => $config->database->password,
			"dbname" => $config->database->dbname
		));
	},true);

DI::setDefault($di);




