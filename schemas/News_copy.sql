DROP TABLE IF EXISTS news_copy;

CREATE TABLE `news_copy` (
  `news_copy_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_copy_title` tinytext NOT NULL,
  `news_copy_text` text NOT NULL,
  `news_copy_created` datetime NOT NULL DEFAULT '2000-00-00 00:00:00',
  `news_copy_modified` datetime NOT NULL DEFAULT '2000-00-00 00:00:00',
  PRIMARY KEY (`news_copy_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

USE `dev_twelfthman`;
DELIMITER //
	DROP TRIGGER IF EXISTS news_copy_BINS// 
	CREATE TRIGGER news_copy_BINS
		BEFORE INSERT ON news_copy FOR EACH ROW 
			BEGIN 
				IF NEW.news_copy_text = '' THEN set NEW.news_copy_text = NULL ;END IF;
				IF NEW.news_copy_title = '' THEN set NEW.news_copy_title = NULL ;END IF;
				IF NEW.news_copy_created = '2000-00-00 00:00:00' THEN SET NEW.news_copy_created = NOW();END IF;
				IF NEW.news_copy_modified = '2000-00-00 00:00:00' THEN SET NEW.news_copy_modified = NOW();END IF;
				IF NEW.news_copy_id IS NULL
						THEN
						SIGNAL SQLSTATE '45000'
						SET MESSAGE_TEXT = 'Must contain value';
					END IF;

				END; // DELIMITER ;

USE `dev_twelfthman`;
DELIMITER //
	DROP TRIGGER IF EXISTS news_copy_BUPD// 
	CREATE TRIGGER news_copy_BUPD
		BEFORE UPDATE ON news_copy FOR EACH ROW 

BEGIN 
		SET NEW.news_copy_modified = NOW();

END; // DELIMITER ;