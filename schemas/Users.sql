DROP TABLE users;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_login_id` varchar(45) NOT NULL,
  `user_level` varchar(10) NOT NULL,
  `user_first_name` varchar(45) NOT NULL,
  `user_last_name` varchar(45) NOT NULL,
  `user_email` varchar(45) NOT NULL,
  `user_password` varchar(128) NOT NULL,
  `user_active` varchar(1) NOT NULL,
  `user_created` datetime NOT NULL DEFAULT '2000-00-00 00:00:00',
  `user_modified` datetime NOT NULL DEFAULT '2000-00-00 00:00:00',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_login_id_UNIQUE` (`user_login_id`),
  UNIQUE KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=1345 DEFAULT CHARSET=latin1;

USE `dev_twelfthman`;
DELIMITER //
	DROP TRIGGER IF EXISTS users_BINS// 
	CREATE TRIGGER users_BINS
		BEFORE INSERT ON users FOR EACH ROW 
			BEGIN 
				IF NEW.user_login_id = '' THEN set NEW.user_login_id = NULL ;END IF;
				IF NEW.user_level = '' THEN set NEW.user_level = 'user' ;END IF;
				IF NEW.user_first_name = '' THEN set NEW.user_first_name = NULL ;END IF;
				IF NEW.user_last_name = '' THEN set NEW.user_last_name = NULL ;END IF;
				IF NEW.user_email = '' THEN set NEW.user_email = NULL ;END IF;
				IF NEW.user_password = '' THEN set NEW.user_password = NULL ;END IF;
				IF NEW.user_active = '' THEN set NEW.user_active = NULL ;END IF;
				IF NEW.user_created = '2000-00-00 00:00:00' THEN SET NEW.user_created = NOW();END IF;
				IF NEW.user_modified = '2000-00-00 00:00:00' THEN SET NEW.user_modified = NOW();END IF;
				IF NEW.user_id IS NULL
						THEN
						SIGNAL SQLSTATE '45000'
						SET MESSAGE_TEXT = 'Must contain value';
					END IF;

				END; // DELIMITER ;

USE `dev_twelfthman`;
DELIMITER //
	DROP TRIGGER IF EXISTS users_BUPD// 
	CREATE TRIGGER users_BUPD
		BEFORE UPDATE ON users FOR EACH ROW 

BEGIN 
		SET NEW.user_modified = NOW();

END; // DELIMITER ;