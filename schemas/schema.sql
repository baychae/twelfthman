CREATE DATABASE  IF NOT EXISTS `dev_twelfthman` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `dev_twelfthman`;
-- MySQL dump 10.13  Distrib 5.6.13, for osx10.6 (i386)
--
-- Host: 127.0.0.1    Database: dev_twelfthman
-- ------------------------------------------------------
-- Server version	5.6.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_site_section`
--

DROP TABLE IF EXISTS `admin_site_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_site_section` (
  `admin_site_section_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_site_section_name` varchar(45) DEFAULT NULL,
  `admin_site_section_sub_section` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`admin_site_section_id`),
  UNIQUE KEY `admin_site_section_id_UNIQUE` (`admin_site_section_id`),
  UNIQUE KEY `unique_index` (`admin_site_section_name`,`admin_site_section_sub_section`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admin_site_section_data`
--

DROP TABLE IF EXISTS `admin_site_section_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_site_section_data` (
  `admin_site_section_data_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_site_section_data_section_id` int(11) NOT NULL,
  `admin_site_section_data_name` varchar(45) NOT NULL,
  `admin_site_section_data_value` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`admin_site_section_data_id`),
  UNIQUE KEY `unique_index` (`admin_site_section_data_section_id`,`admin_site_section_data_name`),
  KEY `admin_site_section_idx` (`admin_site_section_data_section_id`),
  CONSTRAINT `admin_site_section` FOREIGN KEY (`admin_site_section_data_section_id`) REFERENCES `admin_site_section` (`admin_site_section_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `news_copy`
--

DROP TABLE IF EXISTS `news_copy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_copy` (
  `news_copy_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_copy_title` tinytext NOT NULL,
  `news_copy_text` text NOT NULL,
  `news_copy_published` datetime DEFAULT NULL,
  `news_copy_created` datetime NOT NULL DEFAULT '2000-00-00 00:00:00',
  `news_copy_modified` datetime NOT NULL DEFAULT '2000-00-00 00:00:00',
  PRIMARY KEY (`news_copy_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER news_copy_BINS
		BEFORE INSERT ON news_copy FOR EACH ROW 
			BEGIN 
				IF NEW.news_copy_text = '' THEN set NEW.news_copy_text = NULL ;END IF;
				IF NEW.news_copy_title = '' THEN set NEW.news_copy_title = NULL ;END IF;
				IF NEW.news_copy_created = '2000-00-00 00:00:00' THEN SET NEW.news_copy_created = NOW();END IF;
				IF NEW.news_copy_modified = '2000-00-00 00:00:00' THEN SET NEW.news_copy_modified = NOW();END IF;
				IF NEW.news_copy_id IS NULL
						THEN
						SIGNAL SQLSTATE '45000'
						SET MESSAGE_TEXT = 'Must contain value';
					END IF;

				END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER news_copy_BUPD
		BEFORE UPDATE ON news_copy FOR EACH ROW 

BEGIN 
		SET NEW.news_copy_modified = NOW();

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_login_id` varchar(45) NOT NULL,
  `user_level` varchar(10) NOT NULL,
  `user_first_name` varchar(45) NOT NULL,
  `user_last_name` varchar(45) NOT NULL,
  `user_email` varchar(45) NOT NULL,
  `user_password` varchar(128) NOT NULL,
  `user_active` varchar(1) NOT NULL,
  `user_created` datetime NOT NULL DEFAULT '2000-00-00 00:00:00',
  `user_modified` datetime NOT NULL DEFAULT '2000-00-00 00:00:00',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_login_id_UNIQUE` (`user_login_id`),
  UNIQUE KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=1348 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER users_BINS
		BEFORE INSERT ON users FOR EACH ROW 
			BEGIN 
				IF NEW.user_login_id = '' THEN set NEW.user_login_id = NULL ;END IF;
				IF NEW.user_level = '' THEN set NEW.user_level = 'user' ;END IF;
				IF NEW.user_first_name = '' THEN set NEW.user_first_name = NULL ;END IF;
				IF NEW.user_last_name = '' THEN set NEW.user_last_name = NULL ;END IF;
				IF NEW.user_email = '' THEN set NEW.user_email = NULL ;END IF;
				IF NEW.user_password = '' THEN set NEW.user_password = NULL ;END IF;
				IF NEW.user_active = '' THEN set NEW.user_active = NULL ;END IF;
				IF NEW.user_created = '2000-00-00 00:00:00' THEN SET NEW.user_created = NOW();END IF;
				IF NEW.user_modified = '2000-00-00 00:00:00' THEN SET NEW.user_modified = NOW();END IF;
				IF NEW.user_id IS NULL
						THEN
						SIGNAL SQLSTATE '45000'
						SET MESSAGE_TEXT = 'Must contain value';
					END IF;

				END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER users_BUPD
		BEFORE UPDATE ON users FOR EACH ROW 

BEGIN 
		SET NEW.user_modified = NOW();

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping routines for database 'dev_twelfthman'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-30 16:04:49
